"""Data reading abstractions"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from math import floor
from os import symlink
from os.path import split, dirname, basename, sep, splitext, exists

import re

import numpy as np

import mne

from anamnesis import AbstractAnam, register_class

from .options import YneOptions
from .channels import (ChannelSet, Channel, mne_chantype_to_yne)
from .epochdefiners import SliceEpochDefiner, ChannelEpochDefiner
from .rejection import SliceRejection


__all__ = []


#######################################################################
# MegReader ABC
#######################################################################
class AbstractMegReader(AbstractAnam):
    """Abstract base class for MEG Readers"""

    # Default channels used by load_data if not otherwise specified
    default_trig_chan = None
    default_resp_chan = None

    hdf5_outputs = ['filename',
                    'epochdefiner',
                    'output_path_base',
                    'rejected_chanset']

    def __init__(self):  # pragma: no cover
        """
        Initialize AbstractMegReader class.
        """
        AbstractAnam.__init__(self)

    def _init_variables(self):
        """Initialize common variables for class and subclasses"""

        # Public variables
        self.filename = None
        """Filename of loaded data"""

        self.epochdefiner = None
        """EpochDefiner object (if one is attached).
        Used by the get_epoch functions"""

        # Variables used for property storage
        # Public API (property) should be used to access these

        self.output_path_base = None
        """Output path base for writing out data"""

        self.datatype = None
        """Internal data type code"""

        self.rejected_chanset = ChannelSet()
        """ChannelSet containing any rejected channels in the dataset"""

        # Variables which we set up when needed - these are not saved in HDF5
        self.chanset = ChannelSet()
        """ChannelSet containing all channel in the dataset"""

        self.meg_chanset = ChannelSet()
        """ChannelSet containing all MEG channels in the dataset"""

        self.meg_ref_chanset = ChannelSet()
        """ChannelSet containing all MEG reference channels in the dataset"""

        self.eeg_chanset = ChannelSet()
        """ChannelSet containing all EEG channels in the dataset"""

        self.eeg_ref_chanset = ChannelSet()
        """ChannelSet containing all EEG reference channels in the dataset"""

        self.aux_chanset = ChannelSet()
        """ChannelSet containing all Auxillary channels in the dataset"""

        self.loader = None
        """Reference to MNE Raw object"""

        self.info = None
        """Reference to MNE Info object (often just self.loader.raw)"""

    def __eq__(self, other):
        if self.filename != other.filename:
            return False

        if self.epochdefiner != other.epochdefiner:
            return False

        if self.output_path_base != other.output_path_base:
            return False

        if self.datatype != other.datatype:
            return False

        if self.rejected_chanset != other.rejected_chanset:
            return False

        if self.chanset != other.chanset:
            return False

        if self.meg_chanset != other.meg_chanset:
            return False

        if self.meg_ref_chanset != other.meg_ref_chanset:
            return False

        if self.eeg_chanset != other.eeg_chanset:
            return False

        if self.eeg_ref_chanset != other.eeg_ref_chanset:
            return False

        if self.aux_chanset != other.aux_chanset:
            return False

        return True

    def init_from_hdf5(self):
        for name in ['meg', 'meg_ref', 'eeg', 'eeg_ref', 'aux', 'rejected']:
            # If None, make sure we have an empty channel set instead
            if (getattr(self, '%s_chanset', None) is None):
                setattr(self, '%s_chanset', ChannelSet())

        self.setup_readers()

    def setup_readers(self):  # pragma: nocover
        """
        This routine must set up self.loader and self.info with the relevant
        MNE objects.  It should also usually call self._populate_chanset()
        """
        raise NotImplementedError("This is an ABC")

    ###################################################################
    # Useful properties
    ###################################################################
    @property
    def sample_rate(self):
        return self.info['sfreq']

    @property
    def num_channels(self):
        """Returns number of channels in loaded data"""
        return len(self.loader.ch_names)

    ###################################################################
    # Number of slice functions
    ###################################################################
    @property
    def num_slices(self):
        """Returns number of slices in loaded data"""
        return len(self.loader)

    ###################################################################
    # Channel geometry routine
    ###################################################################
    def get_channel_geometry(self, chans=None, ras=False):  # pragma: nocover
        """
        Returns the position, orientations and radii of channels within
        the data.  Also returns a ChannelSet describing the returned
        channels.

        Each of the positions, orientations and radii is returned as a list.
        The list contains one entry for each Channel.  The entry for the position
        and orientation values is a :class:`naf.meg.coord.Coords` object of size
        (ncoils, 3) whilst for radii it is (ncoils, 1).

        In the case of Magnetometers therefore, only a single position will be
        returned for each channel, whilst for Gradiometers, 2 or more will be
        returned.

        :param chans: str, list, ChannelSet or ChannelType.  If set to None,
                      the data for meg_chanset will be returned

        :param ras: boolean.  If False, co-ordinates will returned in native
                    machine orientation.  If True, co-ordinates will be converted
                    to RAS before being returned.  Co-ordinates are based on
                    the positions and orientations in COORD_MEG.  If you need
                    an alternative co-ordinate space, use get_channel_coords.

        :rtype: tuple
        :return: Tuple of (positions, orientations, radii, channelset)

        :raise RuntimeError: If one of the channels exists, but we cannot
                             determine the geometry information (for instance,
                             it is missing from the data file).
        :raise ValueError: If one of the channels requested does not exist or
                           is the wrong type.
        """
        raise NotImplementedError("This is an ABC")

    ###################################################################
    # Slice range functions
    ###################################################################
    def get_slice_range(self, slices=None, picks=None, return_times=False):
        """
        Gets a range of slices for (optionally) a given set of channels

        :param slices: None or tuple of start and stop slices of data to return.
        :param picks: ChannelSet or list of channels to return.  If None,
                      return all channels.
                      If the list is a list of strings, will be interpreted
                      as names, if integers as indices.
        :param return_times: If True, return times as the second element of
                             a tuple.

        :raise RunTimeError: If slices are out of range of available slices
        """

        # If slices is None, return all data
        if slices is not None:
            start = slices[0]
            stop = slices[1]

            if (start < 0) or (stop > self.num_slices) or (start >= stop):
                raise ValueError(f'Invalid start and stop slices for get_slice_range(): {start}, {stop}')
        else:
            start = 0
            stop = None

        # Convert ChannelSet into list
        if isinstance(picks, ChannelSet):
            picks = picks.keys()

        return self.loader.get_data(picks=picks, start=start, stop=stop,
                                    return_times=return_times)

    ###################################################################
    # Get Epoch functions
    ###################################################################
    def get_epoch(self, epoch_num, picks=None, epochdefiner=None, return_slices=False, return_times=False):
        """
        Gets a range of slices for a given epoch number (based on an
        epochdefiner)

        :param epoch_num: The epoch number to return
        :param picks: ChannelSet of data to return.  If None, return all
                      channels.
        :param epochdefiner: EpochDefiner object to use.  If not set, will use
                             the epochdefiner attached to the data reader.
                             If neither are set, a ValueError will be raised.
        :param return_slices: If set, add (, epochslices) to the return tuple
        :param return_ttimes: If set add (, times) to the return tuple

        :raise ValueError: If slices are out of range of available slices or
                           no epochdefiner is provided or set in the data object.
        """

        if epochdefiner is None:
            if self.epochdefiner is None:
                raise ValueError('Need an epoch definer to use get_epoch')

        ret = []

        slices = self.epochdefiner.get_epoch_range(epoch_num)

        dat, t = self.get_slice_range(slices, picks=picks, return_times=True)

        ret.append(dat)

        if return_slices:
            ret.append(slices)

        if return_times:
            ret.append(t)

        if len(ret) == 1:
            ret = ret[0]
        else:
            ret = tuple(ret)

        return ret

    ###################################################################
    # Channel handling routines
    ###################################################################

    # Helper routines for subclasses
    def _populate_chansets(self):
        """
        Populate YNE ChannelSet representation based on MNE data

        Assumes that self.loader is set up properly
        """

        self.chanset = ChannelSet()
        self.meg_chanset = ChannelSet()
        self.meg_ref_chanset = ChannelSet()
        self.eeg_chanset = ChannelSet()
        self.aux_chanset = ChannelSet()

        ch_names = self.loader.ch_names
        ch_types = self.loader.get_channel_types()

        for idx, (name, ch_type) in enumerate(zip(ch_names, ch_types)):
            yne_type = mne_chantype_to_yne(ch_type, self.loader.info['chs'][idx])

            channel = Channel(name, yne_type)

            self.chanset.add_channel(channel)

            if channel.is_meg_data:
                self.meg_chanset.add_channel(channel)
            elif channel.is_meg_ref:
                self.meg_ref_chanset.add_channel(channel)
            elif channel.is_eeg:
                self.eeg_chanset.add_channel(channel)
            else:
                self.aux_chanset.add_channel(channel)

    def set_rejected_channels(self, chans):
        """
        Set the rejected chanset using either a channel name, chan, list of
        chans or chanset.

        :type chans: str, list or ChannelSet
        :param chans: Channels to reject
        """
        self.rejected_chanset = self.chanset.intersection(chans)

    def ensure_run_headshape(self, output_base):
        """
        Ensure that a usable headshape file exists as output_base.
        Routine adds either '.fif' or '.fif.gz' as needed.

        This can be achieved by either symlinking to the same file as
        filename or by creating a small FIF-compatible headshape file.
        """
        raise NotImplementedError("This is an ABC")


__all__.append('AbstractMegReader')


class BTIMegReader(AbstractMegReader):
    default_trig_chan = 'STI 014'
    default_resp_chan = 'STI 013'

    def __init__(self, filename=None):
        """Initialise a BTIMegReader object."""
        AbstractMegReader.__init__(self)

        self._init_variables()

        if filename is not None:
            self.filename = filename
            self.setup_readers()

    def setup_readers(self):
        try:
            self.loader = mne.io.read_raw_bti(self.filename)
            self.info = self.loader.info
        except Exception as e:
            raise RuntimeError(f'Unable to open raw data file {self.filename} {e}')

        # Set up our channel sets
        self._populate_chansets()

    def get_channel_geometry(self, chans=None, ras=False):  # noqa
        raise NotImplementedError("Not re-implemented for MNE reader yet")

    def ensure_run_headshape(self, output_base):
        # May as well save compressed
        output_path = output_base + '.fif.gz'

        if exists(output_path):
            return output_path

        # For 4D, we need to create a copy of the data in FIF format
        # Make it as small as possible - we can't use 0s as it
        # crashes on load in the coreg GUI
        data_copy = self.loader.copy()

        data_copy.crop(tmin=0, tmax=1)

        data_copy.save(output_path)

        return output_path


__all__.append('BTIMegReader')
register_class(BTIMegReader)


class CTFMegReader(AbstractMegReader):
    default_trig_chan = 'UPPT002'
    default_resp_chan = None

    def __init__(self, filename=None):
        """Initialise a CTFMegReader object."""
        AbstractMegReader.__init__(self)

        self._init_variables()

        if filename is not None:
            self.filename = filename
            self.setup_readers()

    def setup_readers(self):
        try:
            self.loader = mne.io.read_raw_ctf(self.filename)
            self.info = self.loader.info
        except Exception as e:
            raise RuntimeError(f'Unable to open raw data file {self.filename} {e}')

        # Set up our channel sets
        self._populate_chansets()

    def get_channel_geometry(self, chans=None, ras=False):
        raise NotImplementedError("Not re-implemented for MNE reader yet")

    def ensure_run_headshape(self, output_base):
        # May as well save compressed
        output_path = output_base + '.fif.gz'

        if exists(output_path):
            return output_path

        # For CTF, we need to create a copy of the data in FIF format
        # Make it as small as possible - we can't use 0s as it
        # crashes on load in the coreg GUI
        data_copy = self.loader.copy()

        data_copy.crop(tmin=0, tmax=1)

        data_copy.save(output_path)

        return output_path


__all__.append('CTFMegReader')
register_class(CTFMegReader)


class FIFFMegReader(AbstractMegReader):
    default_trig_chan = 'STI 014'
    default_resp_chan = 'STI 013'

    def __init__(self, filename=None):
        """Initialise a FIFFMegReader object."""
        AbstractMegReader.__init__(self)

        self._init_variables()

        if filename is not None:
            self.filename = filename
            self.setup_readers()

    def setup_readers(self):
        try:
            self.loader = mne.io.read_raw(self.filename)
            self.info = self.loader.info
        except Exception as e:
            raise RuntimeError(f'Unable to open raw data file {self.filename} {e}')

        # Set up our channel sets
        self._populate_chansets()

    def get_channel_geometry(self, chans=None, ras=False):
        raise NotImplementedError("Not re-implemented for MNE reader yet")

    def ensure_run_headshape(self, output_base):
        # For FIF we can just symlink to our real data
        if self.filename.endswith('.fif.gz'):
            output_path = output_base + '.fif.gz'
        else:
            output_path = output_base + '.fif'

        if exists(output_path):
            return output_path

        symlink(self.filename, output_path)


__all__.append('FIFFMegReader')
register_class(FIFFMegReader)


def guess_datatype(filename):
    """
    :param filename: Takes a filename and guesses which data type
                     should be used.

    :return: one of the data type description strings or None
    """
    # Split the filename out if we also have the directory
    dname, fname = split(filename)

    fname = fname.lower()

    if fname.endswith('.hdf5') or fname.endswith('.meghdf5'):
        return 'MEGHDF'
    elif fname.endswith('.ds') or fname.endswith('.res4'):
        return 'CTF'
    elif fname.endswith('.fif') or fname.endswith('.fif.gz'):
        return 'FIFF'
    else:
        # This is a horrendous regex for the 4D data files
        r = re.compile(r'^[ce],?(rf(DC|hp0.1Hz|hp1.0Hz))?(,COH1?)?')
        if r.search(fname):
            return 'BTI'

    return None


__all__.append('guess_datatype')


def get_meg_reader_class(datatype):
    """
    Returns a reference to the class which should be used for a given datatype.
    """

    datatypes = {'BTI': BTIMegReader,
                 'CTF': CTFMegReader,
                 # Support both spellings
                 'FIF': FIFFMegReader,
                 'FIFF': FIFFMegReader}

    reader = datatypes.get(datatype, None)

    if reader is None:
        raise RuntimeError(f"Cannot find reader for datatype {datatype}")

    return reader


__all__.append('get_meg_reader_class')


def munge_filename(filename):
    """
    Produces a version of a filename with everything non-safe for use as a
    directory name or shortened name removed.  Usually used to automatically
    generate short data names for runs

    :type filename: str
    :param filename: Filename to munge

    :return: A version of filename with everything non-safe for use as a
             directory name or shortened name removed.  Usually used
             to automatically generate short data names for runs
    """
    return re.sub('_{2,}', '_', re.sub('[^A-Za-z0-9]', '_', filename)).lstrip('_').rstrip('_')


def datafilename_to_outputname(filename):
    """
    Takes a file path to a data file and, based upon the file name, attempts
    to construct a unique ID if the user hasn't supplied one.

    :param filename: Filename to construct output name for

    :return: A cleaned up version of the filename useful as an identifier.
    """

    outname = None

    # Try the 4D method first: these files often have a structured path
    r = re.compile(r'.*/[ce],?(rf(DC|hp0.1Hz|hp1.0Hz))?(,COH1?)?$')
    if r.search(filename):
        fp = dirname(filename).split(sep)

        if len(fp) >= 4:
            runnum = fp[-1]
            datetime = fp[-2]
            projid = fp[-3]
            partid = fp[-4]

            outname = munge_filename('_'.join([partid, projid, datetime, runnum]))

    # Try the CTF approach
    if outname is None:
        if filename.endswith('.ds'):
            fn = basename(filename)[:-3]

            outname = munge_filename(fn)
        elif filename.endswith('.res4'):
            fn = basename(filename)[:-5]

            outname = munge_filename(fn)

    # If not, then try removing the MEG path prefix and then munging the
    # resulting filename
    if outname is None:
        # Can't use the newer .removeprefix as that's Python 3.9 and later
        msd = YneOptions().meg_storage_dir

        if filename.startswith(msd):
            filename = filename[len(msd):]

        # Handle compressed files with two extensions
        if filename.endswith('.gz'):
            filename = filename[:-3]

        outname = munge_filename(splitext(filename)[0])

    return outname


__all__.append('datafilename_to_outputname')


def load_data(filename,
              datatype=None,
              slice_file=None,
              slice_rejection_file=None,
              bad_channel_list=None,
              pretrig_dur=None,
              epoch_dur=None,
              trig_chan_name=None,
              resp_chan_name=None,
              output_path_base=None):
    """
    Given an filename and optional other parameters, initialise and set
    up a BTIMegReader object with an appropriate EpochDefiner attached

    :param filename: Path to an MEG data file
    :param datatype: String describing data type; if None, will be guessed
    :param slice_file: Path to a file containing slice information for epochs
    :param slice_rejection_file: Path to a file containing list of slice ranges to reject
    :type pretrig_dur: int
    :param pretrig_dur: Pretrigger duration to use (ms)
    :type epoch_dur: int
    :param epoch_dur: Epoch duration to use (dur)
    :param trig_chan_name: Channel used when looking for triggers.  If None, the default
                           for the data type will be used.
    :param resp_chan_name: Channel used when looking for repsonses.  If None, the default
                           for the data type will be used.
    :param output_path_base: Output path base to use.  If None, will be
                             calculated from filename using datafilename_to_outputname.

    :rtype: Subclass of AbstractMegReader
    :return: Fully configured MEG reader object with an appropriate EpochDefiner attached
    """

    if slice_file and (pretrig_dur or epoch_dur):
        raise ValueError('Error: cannot specify slice_file and pretrig_dur/epoch_dur')

    # Work out our datatype if not provided
    if datatype is None:
        datatype = guess_datatype(filename)

    meg_reader_cls = get_meg_reader_class(datatype)

    # Set up our basic MEG reader object
    data = meg_reader_cls(filename)

    # Attach a relevant EpochDefiner
    if slice_file:
        # Using a manual Slice Epoch Definer
        ed = SliceEpochDefiner.from_file(slice_file)
    else:
        if trig_chan_name is None:
            trig_chan_name = meg_reader_cls.default_trig_chan

        if trig_chan_name is None:
            raise ValueError("Cannot determine response channel")

        chans = [trig_chan_name]

        if resp_chan_name is None:
            resp_chan_name = meg_reader_cls.default_resp_chan

        if resp_chan_name is not None:
            chans.append(resp_chan_name)

        dat = data.get_slice_range(None, chans)

        if len(chans) > 1:
            resp_data = dat[0, :]
        else:
            resp_data = None

        # Turn durations from ms to samples as needed by the ED
        pretrig_samples = int(floor(pretrig_dur * (data.sample_rate / 1000)))
        epoch_samples = int(floor(epoch_dur * (data.sample_rate / 1000)))
        posttrig_samples = epoch_samples - pretrig_samples

        ed = ChannelEpochDefiner.from_arrays((pretrig_samples, posttrig_samples),
                                             trig_chan=dat[0, :],
                                             resp_chan=resp_data)

    # Slice rejection file if present
    if slice_rejection_file:
        ed.rejected_slices = SliceRejection.from_file(slice_rejection_file)

    # Attach to data reader
    data.epochdefiner = ed

    if output_path_base is None:
        data.output_path_base = datafilename_to_outputname(filename)
    else:
        data.output_path_base = output_path_base

    if bad_channel_list is not None:
        data.set_rejected_channels(bad_channel_list)

    return data


__all__.append('load_data')


# TODO: Move this somewhere more sensible?
def get_channel_2d_projection(chan_pos):
    """
    Returns a 2D projection of the MEG channels.

    Uses the get_channel_geometry routine to get the channel information
    in RAS format.

    :param chan_pos: numpy array.  Must be channel positions as returned from
                     the MegReader.get_channel_geometry routine with ras
                     set to True

    :rtype: numpy array
    :return: array of shape (nchannels, 2) with x, y co-ordinates for use as
             a 2D projection.  The flattened co-ordinate system is RA.  In
             other words, increasing X corresponds to moving to the right,
             increasing Y corresponds to moving anteriorly.
    """

    # We only want the first two dimensions
    if chan_pos.ndim > 2:
        chan_pos = chan_pos[:, :, 0]

    norm_coilpos = np.zeros(chan_pos.shape)

    # Set the first column to be left->right with 0 in the middle
    norm_coilpos[:, 0] = chan_pos[:, 0] - chan_pos[:, 0].mean()
    # Set the second column to be back->front with 0 in the middle
    norm_coilpos[:, 1] = chan_pos[:, 1] - chan_pos[:, 1].mean()
    # Set the third column to be down->up with 0 at the bottom
    norm_coilpos[:, 2] = chan_pos[:, 2] - chan_pos[:, 2].min()

    # Calculate the in-plane distance of the coils from 0,0,z
    pdist = (norm_coilpos[:, 0] ** 2 + norm_coilpos[:, 1] ** 2) ** 0.5
    # Get the z co-rdinate of each coil
    zdist = norm_coilpos[:, 2]

    # We know we'll get some /0 warnings, but that's ok 'cos we're going to
    # arctan them so for the time being ignore /0 warnings.
    old_settings = np.seterr(divide='ignore')
    tan_ang = pdist / zdist
    np.seterr(divide=old_settings['divide'])

    # Calculate the angle from the coil to vertical (a sensible scale for
    # how far down the coils is).
    ang = np.arctan(tan_ang)
    # Calculate the angle round the dewar, i.e. theta in the xy plane
    theta = np.arctan2(norm_coilpos[:, 0], norm_coilpos[:, 1])

    # Use these two angles to give a simple 2-D projection of the coil
    # locations
    xs = ang*(np.sin(theta))
    ys = ang*(np.cos(theta))

    ret = np.array([xs, ys]).T

    return ret
