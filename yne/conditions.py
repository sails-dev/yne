"""General condition definers"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from anamnesis import AbstractAnam, register_class

from .utils import parse_input_as_int_list, parse_codes

__all__ = []

#######################################################################
# Condition class
#######################################################################


class Condition(AbstractAnam):
    """
    Class which defines a trigger, group and response code list
    to define a Condition.

    The trigger, group and response lists can be normal python lists of
    integers, or can be in the form of a string containing a range, e.g. '3-7'
    which will assume all values in the range inclusive of end values.
    These will be converted into normal lists on load.

    If a constraint is None or an empty list, it will be ignored and
    all epochs will be used.
    """

    hdf5_outputs = ['_trigger_list', '_group_list', '_response_list', 'name']

    hdf5_mapnames = {'_trigger_list':  'trigger_list',
                     '_group_list':    'group_list',
                     '_response_list': 'response_list'}

    def __init__(self, name='', trigger_list=None, group_list=None, response_list=None):

        AbstractAnam.__init__(self)

        self.name = name
        self._trigger_list = []
        self._group_list = []
        self._response_list = []

        if trigger_list is not None:
            self.set_trigger_list(trigger_list)

        if group_list is not None:
            self.set_group_list(group_list)

        if response_list is not None:
            self.set_response_list(response_list)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if other is None:
            return False

        if self.name != other.name:
            return False

        if self._trigger_list != other._trigger_list:
            return False

        if self._group_list != other._group_list:
            return False

        if self._response_list != other._response_list:
            return False

        return True

    @property
    def is_simple(self):
        """
        Returns True if the condition is just a single trigger value.  This
        allows us to use some simpler logic in some of the MNE interface routines
        """
        if (len(self._trigger_list) == 1) and \
           (len(self._group_list) == 0) and \
           (len(self._response_list) == 0):
            return True

        return False

    # Trigger code list routines
    @property
    def all_triggers(self):
        """
        :type: bool

        True if epochs containing all triggers should be used, False otherwise.
        """
        if self._trigger_list is None or len(self._trigger_list) == 0:
            return True

        return False

    def get_trigger_list(self):
        return self._trigger_list

    def set_trigger_list(self, value):
        self._trigger_list = parse_input_as_int_list(value)

    trigger_list = property(get_trigger_list,
                            set_trigger_list,
                            doc='Trigger list')

    # Group code list routines
    @property
    def all_groups(self):
        """
        :type: bool

        True if epochs containing all group codes should be used, False
        otherwise.
        """
        if self._group_list is None or len(self._group_list) == 0:
            return True

        return False

    def get_group_list(self):
        return self._group_list

    def set_group_list(self, value):
        self._group_list = parse_input_as_int_list(value)

    group_list = property(get_group_list,
                          set_group_list,
                          doc='Group list')

    # Response code list routines
    @property
    def all_responses(self):
        """
        :type: bool

        True if epochs containing all response codes should be used, False
        otherwise.
        """
        if self.response_list is None or len(self.response_list) == 0:
            return True
        return False

    def get_response_list(self):
        return self._response_list

    def set_response_list(self, value):
        self._response_list = parse_input_as_int_list(value)

    response_list = property(get_response_list,
                             set_response_list,
                             doc='Response list')

    @classmethod
    def from_yaml_dict(cls, data):

        name = data['name']
        triggers = parse_codes(data['triggerCodes'])
        groups = parse_codes(data['groupCodes'])
        responses = parse_codes(data['responseCodes'])

        return cls(name=name,
                   trigger_list=triggers,
                   group_list=groups,
                   response_list=responses)

    def to_yaml_dict(self):
        ret = {}
        ret['name'] = self.name
        ret['groupCodes'] = self._group_list[:]
        ret['triggerCodes'] = self._trigger_list[:]
        ret['responseCodes'] = self._response_list[:]
        return ret


register_class(Condition)
__all__.append('Condition')


#######################################################################
# Simple dictionary based Condition Definer
#######################################################################


class ConditionDefiner(AbstractAnam):
    """
    Dictionary based implementation of a ConditionDefiner

    Note that ConditionDefiners do not handle artefact rejection, this is
    performed by the EpochDefiner class
    """

    hdf5_outputs = ['conditions']

    def __init__(self):
        """
        Initialize ConditionDefiner class.
        """
        AbstractAnam.__init__(self)

        self.conditions = {}

    def __eq__(self, other):
        if other is None:
            return False

        return self.conditions == other.conditions

    def keys(self):
        return sorted(self.conditions.keys())

    def has_condition(self, conditionname):
        return conditionname in self.conditions

    def add_condition(self, condition):
        self.conditions[condition.name] = condition

    def get_condition(self, conditionname):
        return self.conditions[conditionname]

    def del_condition(self, conditionname):
        del self.conditions[conditionname]

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        for entry in data:
            dc = Condition.from_yaml_dict(entry)
            ret.add_condition(dc)

        return ret

    def to_yaml_dict(self):
        ret = []

        for c in self.keys():
            ret.append(self.get_condition(c).to_yaml_dict())

        return ret


register_class(ConditionDefiner)
__all__.append('ConditionDefiner')
