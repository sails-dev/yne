#!/usr/bin/env python3

# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import sys
from os.path import abspath

from PyQt5.QtWidgets import QApplication

from yne.ui.study_def_app import StudyDefWin

from yne.parser import YneOptionParser


def main():
    if len(sys.argv) > 1 and sys.argv[1] == '--help':
        print("Usage: yne-study-definer")
        sys.exit(0)

    app = QApplication(sys.argv)

    usage_str = """%prog YAML_FILE

Create or edit a study defining YAML file.
    YAML_FILE         is the name of the project yaml file"""

    parser = YneOptionParser(usage=usage_str)
    op, args = parser.parse_args()

    # required parameters
    if len(args) > 1:
        parser.print_help()
        sys.exit(1)

    if len(args) == 1:
        fname = abspath(args[0])
    else:
        fname = None

    # Sanity check our arguments
    parser.sanity_check(op)

    win = StudyDefWin(fname)
    win.show()

    sys.exit(app.exec_())
