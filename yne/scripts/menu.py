#!/usr/bin/env python3

# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import sys

from PyQt5.QtWidgets import QApplication

from yne.ui.main_menu import YNEMenu

from yne.parser import YneOptionParser


def main():
    if len(sys.argv) > 1 and sys.argv[1] == '--help':
        print("Usage: yne-menu")
        sys.exit(0)

    app = QApplication(sys.argv)

    usage_str = """%prog

    Run the YNE main menu"""

    parser = YneOptionParser(usage=usage_str)
    op, args = parser.parse_args()

    # required parameters
    if len(args) > 0:
        parser.print_help()
        sys.exit(1)

    # Sanity check our arguments
    parser.sanity_check(op)

    win = YNEMenu()
    win.show()

    sys.exit(app.exec_())
