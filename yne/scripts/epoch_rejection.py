#!/usr/bin/env python3

# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import sys

from getopt import getopt
from os.path import basename
from operator import or_, and_

from PyQt5.QtWidgets import QApplication

from yne.ui.epoch_rejection import YNEEpochRejectMain


def usage(exit_code=0):
    print("""Usage: %s -f dataFile {-p preTrig -d duration} or {-s slicefile}
Perform artefact rejection on dataFile.

Mandatory arguments:
    -f, --filename         MEG data file to run on
  And either both:
    -p, --pretrig          Pretrigger duration (ms)
    -d, --duration         Epoch duration (ms)
  Or:
    -s, --slice-file       Slice file
Optional arguments:
    -i, --input-slicerej   Rejected slice file input filename
    -b, --bad-channels     Comma separated list of channels to ignore
    -n, --notch-filter     Apply a 50Hz notch filter to display
    -t, --trigger-reject   Comma seperated list of trigger codes to pre-reject
    -g, --group-reject     Comma seperated list of group codes to pre-reject
    -r, --response-reject  Comma seperated list of response codes to pre-reject

    Note if more than one of the --***-reject options are used the following must also be specified

    -j, --join-op          Logical operation on code types to pre-reject, one of 'or' or 'and'

    -E, --EEG              Display EOG/ECG channels
    -m  --meg-threshold    Magnitude threshold to apply before the first
                           pass.  Units are picotesla, i.e. -m 1.5 will
                           reject epochs with values above 1.5e-12.
    -l --low-freq          Apply a high pass filter at the specified frequency (in Hz)
    -u --high-freq         Apply a low pass filter at the specified frequency (in Hz)
""" % basename(sys.argv[0]))

    sys.exit(exit_code)


def parse():
    # TODO: Convert to at least OptionParser if not ArgumentParser!
    opts, args = getopt(sys.argv[1:],
                        "f:p:d:s:o:i:e:b:nt:g:r:j:l:u:m:Eh",
                        ['filename=', 'pretrig=', 'duration=', 'slice-file=', 'input-rejfile=',
                         'bad-channels=', 'notch-filter', 'trigger-reject=', 'group-reject=', 'response-reject=',
                         'join-op=', 'low-freq=', 'high-freq=', 'meg-threshold=', 'EEG', 'help'])

    r = {}
    r['filename'] = None
    r['pretrig'] = None
    r['duration'] = None
    r['sliceFile'] = None
    r['inputSlice'] = None
    r['notchFilter'] = False
    r['lowFreq'] = None
    r['highFreq'] = None
    r['EEG'] = False
    r['meg-threshold'] = False
    r['badchannels'] = []
    r['trigs'] = None
    r['groups'] = None
    r['resps'] = None
    r['join'] = None

    rejtypes = 0

    for o, a in opts:
        if o in ("-f", "--filename"):
            r['filename'] = str(a)
        elif o in ("-p", "--pretrig"):
            r['pretrig'] = int(a)
        elif o in ("-d", "--duration"):
            r['duration'] = int(a)
        elif o in ("-s", "--slice-file"):
            r['sliceFile'] = str(a)
        elif o in ("-i", "--input-slicerej"):
            r['inputSlice'] = str(a)
        elif o in ("-n", "--notch-filter"):
            r['notchFilter'] = True
        elif o in ("-b", "--bad-channels"):
            r['badchannels'] = [int(x) for x in a.split(',')]
        elif o in ("-t", "--trigger-reject"):
            r['trigs'] = [int(x) for x in a.split(',')]
            rejtypes += 1
        elif o in ("-g", "--group-reject"):
            r['groups'] = [int(x) for x in a.split(',')]
            rejtypes += 1
        elif o in ("-r", "--response-reject"):
            r['resps'] = [int(x) for x in a.split(',')]
            rejtypes += 1
        elif o in ("-j", "--join-op"):
            if str(a) == 'and':
                r['join'] = and_
            elif str(a) == 'or':
                r['join'] = or_
            else:
                print("E: Unknown joining operation '%s'" % str(a))
                usage()
        elif o in ("-l", "--low-freq"):
            r['lowFreq'] = int(a)
        elif o in ("-u", "--high-freq"):
            r['highFreq'] = int(a)
        elif o in ("-E", "--EEG"):
            r['EEG'] = True
        elif o in ("-m", "--meg-threshold"):
            r['meg-threshold'] = float(a)*1e-12
        elif o in ("-h", "--help"):
            usage()

    # Check that if we've been given two or more code types to pre-reject we
    # know whether we want the union or intersection
    if (rejtypes > 1) and (r['join'] is None):
        print("E: Must specify a joining operation when using "
              "more than one of --***-reject")
        usage()

    return r


def main():
    r = parse()

    app = QApplication(sys.argv)

    main = YNEEpochRejectMain()
    main.showMaximized()
    QApplication.processEvents()
    main.load_and_run(r)

    sys.exit(app.exec_())
