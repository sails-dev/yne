#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


__all__ = []


def parse_input_as_int_list(inp):
    """
    Parse an integer, string or list into a list.

    :type inp: str, list or None
    :param inp: Can be normal python lists of integers, or can be in the form
                  of a string containing a range, e.g. '3-7' which will assume
                  all values in the range inclusive of end values.

    :return: A list (which may be empty)
    """
    if inp is None:
        return []

    if isinstance(inp, list):
        return inp

    elif isinstance(inp, str):
        if inp == '':
            return []

        # Attempt to convert a string range to a list
        temp = inp.split('-')

        if (len(temp) == 2) and (int(temp[0]) < int(temp[1])):
            return list(range(int(temp[0]), int(temp[1]) + 1))
        elif len(temp) == 1:
            # Maybe we got a single number as a string
            try:
                return [int(inp)]
            except ValueError:
                pass

        raise ValueError(f'Invalid string passed: {inp}')

    else:
        # Hopefully we've got just a single integer
        return [int(inp)]


__all__.append('parse_input_as_int_list')


def parse_codes(inp):
    """Parse a list or string into a list of integer codes"""

    if inp is None:
        return []
    elif isinstance(inp, list):
        return [int(x) for x in inp]
    else:
        # Parse as string
        if (inp.strip() == 'ALL') or (inp.strip() == ''):
            return []
        elif inp.count(',') > 0:
            return [int(x) for x in inp.split(',')]
        else:
            return [int(x) for x in inp.split()]


__all__.append('parse_codes')
