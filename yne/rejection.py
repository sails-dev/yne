"""Slice rejection handlers"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from anamnesis import AbstractAnam, register_class

import numpy as np

__all__ = []


#######################################################################
# Slice Rejection
#######################################################################


class SliceRejection(AbstractAnam):
    """
    Class to represent slice rejections.

    Ranges are specified in python style, i.e. they are zero-indexed
    and the end slice is exclusive.

    For example:
    (0, 1)  will exclude slice 0
    (0, 10) will exclude slices 0 to 9

    It is invalid for the end slice to be less than or equal to the start
    slice.
    """

    hdf5_outputs = ['rejects']

    def __init__(self, inp=None):
        AbstractAnam.__init__(self)

        if inp:
            self.rejects = SliceRejection.__from_input(inp)
        else:
            self.rejects = np.zeros((0, 2), np.int)

    def __eq__(self, other):
        # Cope with None comparisons, especially on EpochDefiners
        if not hasattr(other, 'rejects'):
            return False

        if self.rejects.shape != other.rejects.shape:
            return False

        return np.all(self.rejects == other.rejects)

    def __getitem__(self, *args, **kwds):
        return self.rejects.__getitem__(*args, **kwds)

    def __iter__(self, *args, **kwds):
        return self.rejects.__iter__(*args, **kwds)

    def __len__(self):
        return self.rejects.shape[0]

    def add_reject_range(self, newrange):
        if len(newrange) != 2 or newrange[1] <= newrange[0]:
            raise ValueError("Invalid range")

        self.rejects = np.append(self.rejects, [[int(newrange[0]), int(newrange[1])]], axis=0)

    def remove_reject_range(self, remrange):
        if len(remrange) != 2 or remrange[1] <= remrange[0]:
            raise ValueError("Invalid range")

        startrej = remrange[0]
        endrej = remrange[1]

        ranges = []

        # For each range check if they overlap partially or fully
        for pos in range(self.rejects.shape[0]):
            thisstart = self.rejects[pos, 0]
            thisend = self.rejects[pos, 1]

            if (startrej <= thisstart and endrej <= thisstart) or \
               (startrej >= thisend and endrej >= thisend):
                # No overlap at all
                ranges.append((thisstart, thisend))
            elif startrej <= thisstart and endrej >= thisend:
                # Simple direct overlap case; get rid of it
                pass
            else:
                # Partial overlap
                if startrej > thisstart and endrej < thisend:
                    # Need to take a chunk out of the middle
                    # and therefore need more than one range adding
                    ranges.append((thisstart, startrej))
                    ranges.append((endrej, thisend))
                elif startrej <= thisstart:
                    # Case where end of removal range overlaps
                    # first part of range
                    ranges.append((endrej, thisend))
                else:
                    # Case where start of removal range overlaps
                    # latter part of range
                    ranges.append((thisstart, startrej))

        if len(ranges) == 0:
            self.rejects = np.zeros((0, 2), np.int)
        else:
            self.rejects = np.array(ranges, dtype=np.int)

    @property
    def num_rejections(self):
        return self.rejects.shape[0]

    def sanity_check(self):
        """
        Return a list of indices which contain bad ranges (i.e. endslice <=
        startslice)
        """
        count = 0

        badranges = []
        for start, end in self.rejects:
            if end <= start:
                badranges.append(count)
            count += 1

        return badranges

    def sanitise(self):
        """
        Clean up the list of rejects by merging any overlapping ranges and
        ensuring that the ranges are temporally ordered
        """
        badranges = self.sanity_check()
        if len(badranges) > 0:
            raise ValueError("SliceRejection object contains bad slice ranges")

        new = np.sort(self.rejects, axis=0)

        ranges = []
        curstart = None
        curend = None

        # For each range check if they overlap
        for pos in range(new.shape[0]):
            if curstart is None:
                curstart = new[pos, 0]
                curend = new[pos, 1]
            else:
                thisstart = new[pos, 0]
                thisend = new[pos, 1]
                # Ranges are normal python ranges, so
                # (0, 1), (1, 2) should give us (0, 2); i.e. 0 and 1 are selected
                if thisstart <= curend:
                    # We know thisend > thisstart as we already sanitised the array
                    if thisend > curend:
                        curend = thisend
                else:
                    # New range, so append old one and set up new one
                    ranges.append((curstart, curend))

                    curstart = thisstart
                    curend = thisend

        # Add the end case if necessary
        if curstart is not None:
            ranges.append((curstart, curend))

        if len(ranges) == 0:
            self.rejects = np.zeros((0, 2), np.int)
        else:
            self.rejects = np.array(ranges, dtype=np.int)

    @staticmethod
    def __from_input(inp):
        nums = []
        if isinstance(inp, str):
            inp = inp.split('\n')

        if isinstance(inp, list) or isinstance(inp, tuple):
            for item in inp:
                if isinstance(item, str):
                    # Strip any \n, \t or spaces and ignore any lines which are empty or
                    # start with a # after stripping
                    if item.startswith('#'):
                        continue

                    item = item.strip()
                    if len(item) == 0:
                        continue

                    item = [x.strip() for x in item.split(',')]

                # Try and parse as two integers
                if isinstance(item, list) or isinstance(item, tuple):
                    if len(item) != 2:
                        raise ValueError("Cannot parse item %s when initialising SliceRejection" % str(item))
                    try:
                        start, end = int(item[0]), int(item[1])
                    except ValueError:
                        err = f"Cannot convert numbers in {item} to integers when initialising SliceRejection"
                        raise ValueError(err)
                else:
                    raise ValueError("Cannot parse item %s when initialising SliceRejection" % str(item))

                nums.append((start, end))
        else:
            raise ValueError("Cannot parse type %s when initialising SliceRejection" % type(inp))

        return np.array(nums, dtype=np.int)

    def __repr__(self):
        s = 'SliceRejection(['
        for start, end in self.rejects:
            s += '(%d,%d),' % (start, end)
        s += '])'
        return s

    def to_string(self):
        s = ''
        for start, end in self.rejects:
            s += '%d,%d\n' % (start, end)
        return s

    def check_ranges(self, ranges):
        """
        This routine is used to check a list of (start, stop) ranges against
        the current set of slice rejections
        """
        rangerej = []
        for rangeidx in range(len(ranges)):
            for rejnum in range(self.rejects.shape[0]):
                if not ((self.rejects[rejnum, 0] >= ranges[rangeidx][1]) or
                        (self.rejects[rejnum, 1] <= ranges[rangeidx][0])):
                    # We reject the epoch
                    rangerej.append(rangeidx)
                    break

        return rangerej

    @classmethod
    def from_file(cls, filename):
        f = open(filename, 'r')
        data = f.readlines()
        f.close()
        return cls(inp=data)

    def save(self, filename):
        """
        Save the current rejection list to a file
        """
        f = open(filename, 'w')
        f.write(self.to_string())
        f.close()


__all__.append('SliceRejection')
register_class(SliceRejection)
