#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from os import environ

from .log import get_logger

__all__ = []


class YneOptions(object):
    """
    Singleton class for holding system-wide options
    """

    __shared_state = {}

    def __init__(self, *args, **kwargs):
        # Quick way of implementing a singleton
        self.__dict__ = self.__shared_state

        if not getattr(self, 'initialised', False):
            self.initialised = True
            self.setup(*args, **kwargs)

    def setup(self):
        self.verbose = 0
        self.progress = 0

        # Directories to default to in GUIs
        # TODO: Make overrideable by a config file / environment variable
        self.mri_storage_dir = '/mnt/mridata'
        self.meg_storage_dir = '/mnt/megdata'

        # Number of jobs to use in filtering etc
        self.n_jobs = 1

        # Whether to use advanced 3d rendering where available
        # (e.g. in MNE coreg)
        self.advanced_rendering = True

        # If we see we're on a remote session, disable advanced rendering
        # so that we don't die of old age before anything happens
        if 'YNE_FORCE_ADVANCED_RENDERING' not in environ:
            if 'X2GO_SESSION' in environ:
                self.advanced_rendering = False

    def write(self, s):
        if self.verbose > 0:
            get_logger().info(s)

    def write_progress(self, s):
        if self.progress > 0:
            get_logger().info(s)


__all__.append('YneOptions')
