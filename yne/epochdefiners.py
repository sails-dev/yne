"""Some general epoch definer classes"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the NeuroImaging Analysis Framework
# For copyright and redistribution details, please see the COPYING file

import numpy as np
import scipy

from anamnesis import (AbstractAnam, register_class, write_to_subgroup,
                       obj_from_hdf5group, parse_hdf5_value)


__all__ = []

# TODO: Move these routines elsewhere


def nested_list_to_string(lst):
    slist = []
    for j in lst:
        if isinstance(j, list):
            slist.append(':'.join([str(x) for x in j]))
        else:
            slist.append(str(j))

    return ','.join(slist)


def string_to_nested_list(s):
    lst = []
    slist = s.split(',')
    for j in slist:
        if j == '':
            lst.append([])
        else:
            lst.append([int(x) for x in j.split(':')])

    return lst


def combine_epoch_lists(targetlist, epochlist):
    ret_list = []
    c = 1
    # Two paths as list2 can be list of ints or list of lists
    for y in epochlist:
        if isinstance(y, list):
            for t in y:
                if t in targetlist:
                    ret_list.append(c)
                    break

        # Assume if y is not a list it is an integer
        elif y in targetlist:

            ret_list.append(c)

        c += 1

    return ret_list


class AbstractEpochDefiner(AbstractAnam):
    """Abstract base class for Epoch Definers"""

    hdf5_outputs = ['epochs', 'trigger_codes', 'group_codes',
                    'response_codes', 'rejected_slices']

    def __init__(self, filename=None, sanity_check=False):  # pragma: no cover
        """
        Initialize EpochDefiner class.
        """
        AbstractAnam.__init__(self)

    def _init_variables(self):
        """Initialize common variables for class and subclasses"""

        self.epochs = []
        """A list of 2-tuples (start slice, end slice) describing each epoch"""

        self.trigger_codes = []
        """A list of trigger codes found in each epoch"""

        self.group_codes = []
        """A list of group codes found in each epoch"""

        self.response_codes = []
        """A list of response codes found in each epoch"""

        self.rejected_slices = None
        """A SliceRejection object or None"""

    def __eq__(self, other):
        if other is None:
            return False

        if self.epochs != other.epochs:
            return False

        if self.trigger_codes != other.trigger_codes:
            return False

        if self.group_codes != other.group_codes:
            return False

        if self.response_codes != other.response_codes:
            return False

        if self.rejected_slices != other.rejected_slices:
            return False

        return True

    def add_epoch(self, rng, trig_code, group_codes=None, response_codes=None):
        try:
            start = int(rng[0])
            end = int(rng[1])
        except ValueError as e:
            raise ValueError("Could not parse range %s (%s)" % (rng, str(e)))

        if end <= start:
            raise ValueError("Invalid epoch range %d to %d" % (start, end))

        try:
            trig = int(trig_code)
        except ValueError as e:
            raise ValueError("Could not parse trigger %s (%s)" % (trig_code, str(e)))

        try:
            gc = []
            if group_codes is None:
                pass
            elif isinstance(group_codes, list) or isinstance(group_codes, tuple):
                # Try and use each element in turn
                for g in group_codes:
                    gc.append(int(g))
                # Use a set to unique the list then sort
                gc = sorted(list(set(gc)))
            else:
                # Try and use as a single int
                gc.append(int(group_codes))
        except ValueError as e:
            raise ValueError("Could not parse group_codes %s (%s)" % (str(group_codes), str(e)))

        try:
            rc = []
            if response_codes is None:
                pass
            elif isinstance(response_codes, list) or isinstance(response_codes, tuple):
                # Try and use each element in turn
                for r in response_codes:
                    rc.append(int(r))
                # Use a set to unique the list then sort
                rc = sorted(list(set(rc)))
            else:
                # Try and use as a single int
                rc.append(int(response_codes))
        except ValueError as e:
            raise ValueError("Could not parse response_codes %s (%s)" % (str(response_codes), str(e)))

        self.epochs.append((start, end, ))
        self.trigger_codes.append(trig)
        self.group_codes.append(gc)
        self.response_codes.append(rc)

    def to_mne_event_array(self, pretrig_slices):
        # Note that for historical reasons these are 1-indexed
        rejected_epochs = [x-1 for x in self.rejected_epochs]

        # NB: We currently do not handle group or responses here
        events = np.zeros((len(self.epochs) - len(rejected_epochs), 3), np.int)

        curidx = 0

        for epochidx, epoch in enumerate(self.epochs):
            if epochidx in rejected_epochs:
                continue

            # Because of an impedance mismatch in how we store epochs / windows
            # in YNE compared to MNE, we need to add the pre-trig to the start point
            # (YNE stores [start, end] tuples whereas MNE stores trig_p)
            trig_pt = epoch[0] + pretrig_slices

            events[curidx, 0] = trig_pt
            events[curidx, 2] = self.trigger_codes[epochidx]

            curidx += 1

        return events

    def to_hdf5(self, subgroup):
        """Custom hdf5 writer to write details of epoch definer"""
        # Write class name
        subgroup.attrs['class'] = self.get_hdf5name()

        # Write number of epochs
        subgroup.attrs['numepochs'] = len(self.epochs)

        # Write epochs as a numpy array
        subgroup.create_dataset('timepts', data=np.array(self.epochs, dtype=np.int))

        # Write rejected_slices as a subgroup
        if self.rejected_slices is not None:
            write_to_subgroup(subgroup, 'rejected_slices', self.rejected_slices)

        namemaps = {}

        # Hackishly write out the lists as strings
        for a in ['trigger_codes', 'group_codes', 'response_codes']:
            subgroup.attrs[a] = nested_list_to_string(getattr(self, a))

        # If our subclass has any extra attributes, treat them as normal
        for a in self.hdf5_outputs:
            if a in AbstractEpochDefiner.hdf5_outputs:
                continue

            # This code is only in case people subclass from here
            # and use mapnames or additional attributes

            if a in self.hdf5_mapnames.keys():  # pragma: nocover
                hname = self.hdf5_mapnames[a]
                namemaps[hname] = a
            else:  # pragma: nocover
                hname = a

            val = getattr(self, a)  # pragma: nocover
            if val is not None:  # pragma: nocover
                subgroup = write_to_subgroup(subgroup, hname, val)

        if len(namemaps.keys()) > 0:  # pragma: nocover
            subgroup.attrs['namemaps'] = list(namemaps.items())

        return subgroup

    @classmethod
    def from_hdf5(cls, subgroup):
        """Custom hdf5 reader to load details of epoch selector"""

        ret = cls()

        # Check the class name against the group
        if not ret.check_classname(subgroup.attrs['class']):  # pragma: nocover
            raise ValueError('Subgroup specifies %s instead of %s' % (subgroup.attrs['class'],
                                                                      ret.get_hdf5name()))

        # Read a namemap if we have one: again, only for subclasses
        namemaps = {}
        if 'namemaps' in subgroup.attrs.keys():  # pragma: nocover
            namemaps = dict(subgroup.attrs['namemaps'])
        # Load the number of epochs so we can sanity check it
        numepochs = subgroup.attrs['numepochs']

        timepts = subgroup.get('timepts', None)[...]

        # Check that our dimensions are sane
        if (timepts.ndim != 2) or (timepts.shape[0] != numepochs):  # pragma: nocover
            raise ValueError("timepts matrix is unexpected size")

        ret.epochs = []
        for n in range(numepochs):
            ret.epochs.append(tuple(timepts[n, :]))

        # Quickly build up a dictionary of values we're expecting and mark that
        # we haven't seen them
        honames = {}
        for hname in ret.hdf5_outputs:
            honames[hname] = False

        if 'rejected_slices' in subgroup.keys():
            ret.rejected_slices = obj_from_hdf5group(subgroup.get('rejected_slices', None))
            honames['rejected_slices'] = True

        # We know we've seen self.epochs
        honames['epochs'] = True

        # Read in the lists
        for a in ['trigger_codes', 'group_codes', 'response_codes']:
            lst = string_to_nested_list(subgroup.attrs[a])

            # Trigger codes should be single value per epoch as-per spec
            if a == 'trigger_codes':
                lst = [x[0] for x in lst]

            setattr(ret, a, lst)
            honames[a] = True

        # Read any other data in; starting with attributes
        for hname, value in subgroup.attrs.items():
            # Common things we use
            if hname == 'class' or hname.startswith('LIST'):  # pragma: nocover
                continue

            if hname in AbstractEpochDefiner.hdf5_outputs:
                continue

            # Apply the name map if necessary
            tgtname = namemaps.get(hname, hname)

            if tgtname in honames.keys():  # pragma: nocover
                setattr(ret, tgtname, value)
                honames[tgtname] = True
            else:  # pragma: nocover
                ret.extra_data[tgtname] = value

        # Read any data from the subgroup itself
        for hname, value in subgroup.items():
            # Apply the name map if necessary
            tgtname = namemaps.get(hname, hname)

            if tgtname in honames.keys():
                setattr(ret, tgtname, parse_hdf5_value(subgroup, hname, tgtname, value))
                honames[tgtname] = True
            else:
                ret.extra_data[tgtname] = parse_hdf5_value(subgroup, hname, tgtname, value)

        # Ensure that any compulsory attributes are set to None if we haven't
        # seen them
        for hname, val in honames.items():
            if not val:
                setattr(ret, hname, None)

        # Tell the object to sort itself out
        ret.init_from_hdf5()

        return ret

    def load(self, filename):  # pragma: no cover
        """Loads information from the given filename"""
        raise NotImplementedError('This is an ABC')

    ###################################################################
    # Epoch rejection functions
    ###################################################################
    @property
    def rejected_epochs(self):
        """
        Function to use the SliceRejection list and the list of epochs
        to work out which epochs should be rejected
        """
        if self.rejected_slices is None:
            return []
        else:
            return [x+1 for x in self.rejected_slices.check_ranges(self.epochs)]

    ###################################################################
    # Epoch range functions
    ###################################################################
    @property
    def num_epochs(self):
        """Returns the number of epochs known"""
        return len(self.epochs)

    def __len__(self):
        return self.num_epochs

    def get_all_epoch_ranges(self):
        return self.epochs

    def get_epoch_range(self, epoch_num):
        """
        Returns a tuple of (start slice, end slice) for a given epoch.  The
        returned range is python-style and usable as for get_slice_range.

        Note that epochs are 1-indexed, not 0-indexed
        """
        if epoch_num < 1:
            raise ValueError('Epoch number must be at least 1 (%d)' % (epoch_num))

        # >, not >= as we're using 1-indexing for epoch numbers
        if epoch_num > len(self.epochs):
            raise ValueError('Epoch %d requested but only %d exist' % (epoch_num, len(self.epochs)))

        return self.epochs[epoch_num-1]

    def get_epoch_trigger_code(self, epoch_num):
        """Returns a trigger code for a given epoch.

        Note that epochs are 1-indexed, not 0-indexed
        """
        if epoch_num < 1:
            raise ValueError('Epoch number must be at least 1 (%d)' % (epoch_num))

        # >, not >= as we're using 1-indexing for epoch numbers
        if epoch_num > len(self.epochs):
            raise ValueError('Epoch %d requested but only %d exist' % (epoch_num, len(self.epochs)))

        return self.trigger_codes[epoch_num-1]

    def get_epoch_group_codes(self, epoch_num):
        """Returns a list of group codes for a given epoch.

        Note that epochs are 1-indexed, not 0-indexed
        """
        if epoch_num < 1:
            raise ValueError('Epoch number must be at least 1 (%d)' % (epoch_num))

        # >, not >= as we're using 1-indexing for epoch numbers
        if epoch_num > len(self.epochs):
            raise ValueError('Epoch %d requested but only %d exist' % (epoch_num, len(self.epochs)))

        return self.group_codes[epoch_num-1]

    def get_epoch_response_codes(self, epoch_num):
        """Returns a list of response codes for a given epoch.

        Note that epochs are 1-indexed, not 0-indexed
        """
        if epoch_num < 1:
            raise ValueError('Epoch number must be at least 1 (%d)' % (epoch_num))

        # >, not >= as we're using 1-indexing for epoch numbers
        if epoch_num > len(self.epochs):
            raise ValueError('Epoch %d requested but only %d exist' % (epoch_num, len(self.epochs)))

        return self.response_codes[epoch_num-1]

    ###################################################################
    # Epoch finding functions
    ###################################################################
    def get_epoch_list(self, condition=None):
        """
        Get a list of epochs to use based on a DataCondition

        :param condition: a DataCondition defining the trigger, group and
                            response code lists to use.  If None, return all epochs

        :rtype: list
        :return: A list of epochs which satisfy all of the constraints given.
        """

        # Prepare any rejected epochs
        r_set = set(self.rejected_epochs)

        # If condition is None, return a list of all epochs except rejected ones
        if condition is None:
            e_set = set(range(1, self.num_epochs + 1))

            return sorted(list(e_set.difference(r_set)))

        if condition.all_triggers:
            t_set = set(range(1, self.num_epochs+1))
        else:
            t_set = set(combine_epoch_lists(condition.trigger_list, self.trigger_codes))

        if condition.all_groups:
            g_set = set(range(1, self.num_epochs+1))
        else:
            g_set = set(combine_epoch_lists(condition.group_list, self.group_codes))

        if condition.all_responses:
            resp_set = set(range(1, self.num_epochs+1))
        else:
            resp_set = set(combine_epoch_lists(condition.response_list, self.response_codes))

        # only want epochs that are in all three, and not in the reject list
        ret_list = list(t_set.intersection(g_set).intersection(resp_set).difference(r_set))

        return sorted(ret_list)


__all__.append('AbstractEpochDefiner')


class SliceEpochDefiner(AbstractEpochDefiner):
    """
    Epoch definer using fixed slices.

    The format for the slice file is documented in the
    arrays_from_string function
    """
    def __init__(self, filename=None):
        """
        Initialize SliceEpochDefiner class.
        If a filename is passed, open and parse data file
        """
        AbstractEpochDefiner.__init__(self)

        self._init_variables()

        if filename:
            self.load(filename)

    @classmethod
    def from_repeating_timing(cls, start, epoch_len, num_slices, trig_code):
        """
        Sets up a SliceEpochDefiner which just starts at slice start,
        contains epoch_len slices for a dataset of num_slices.
        Each epoch will be labelled with trig_code
        """
        ret = cls()
        for spt in range(start, num_slices, epoch_len):
            ret.add_epoch((spt, spt+epoch_len), trig_code)

        return ret

    @staticmethod
    def arrays_from_string(inp):
        """
        Returns (epochs, trig_codes, group_codes, response_codes) suitable

        from a string (or list of strings) formatted as:

        TRIGCODE[:GROUPCODES:RESPONSECODES],STARTSLICE:ENDSLICE

        GROUPCODES and RESPONSECODES must be @ seperated if provided

        Entries on the same line must be comma seperated

        Newlines and whitespace will be ignored (except that newlines will
        negate the need for the entries to be comma seperated)

        Please note that end slices are exclusive and 0-indexed, as is normal
        in python
        """

        epochs = []
        trig_codes = []
        group_codes = []
        response_codes = []

        if isinstance(inp, str):
            inp = inp.split()

        d = []
        for line in inp:
            for entry in [dat.strip() for dat in str(line).strip().split(',')]:
                if len(entry) > 0:
                    d.append(entry)

        if len(d) % 2 != 0:
            raise ValueError('Cannot parse input string %s' % (str(inp)))

        for i in range(0, len(d), 2):
            meta, epochdata = d[i], d[i+1]

            epochs.append(tuple([int(x) for x in epochdata.split(':')]))

            meta = d[i].split(':')
            try:
                trig_codes.append(int(meta[0]))
            except ValueError as e:
                raise ValueError('Cannot parse trigger code %s (%s)' % (str(meta[0]), str(e)))

            # See if we have any group codes
            if len(meta) > 1:
                try:
                    gc = []
                    for g in meta[1].split('@'):
                        if len(g) > 0:
                            gc.append(int(g))
                    group_codes.append(gc)
                except ValueError as e:
                    raise ValueError('Cannot parse group codes %s (%s)' % (str(meta[1]), str(e)))
            else:
                group_codes.append([])

            # See if we have any response codes
            if len(meta) > 2:
                try:
                    rc = []
                    for r in meta[2].split('@'):
                        if len(r) > 0:
                            rc.append(int(r))
                    response_codes.append(rc)
                except ValueError as e:
                    raise ValueError('Cannot parse response codes %s (%s)' % (str(meta[2]), str(e)))
            else:
                response_codes.append([])

        return epochs, trig_codes, group_codes, response_codes

    def to_string(self):
        s = ''
        for enum in range(len(self.epochs)):
            dat = [str(self.trigger_codes[enum])]
            if not (len(self.group_codes[enum]) == 0 and len(self.response_codes[enum]) == 0):
                # Always need to write group codes even if empty here as we must
                # either have a group code or response code and will need the
                # seperator
                dat.append('@'.join(str(g) for g in self.group_codes[enum]))
                if len(self.response_codes[enum]) > 0:
                    dat.append('@'.join(str(r) for r in self.response_codes[enum]))

            s += '%s,%d:%d\n' % (':'.join(dat), self.epochs[enum][0], self.epochs[enum][1])

        return s

    @classmethod
    def from_file(cls, filename):
        return cls(filename=filename)

    @classmethod
    def from_string(cls, data):
        e, tc, gc, rc = cls.arrays_from_string(data)

        ret = cls()

        ret.epochs = e
        ret.trigger_codes = tc
        ret.group_codes = gc
        ret.response_codes = rc

        return ret

    def load(self, filename):
        """
        Loads information from the given filename using arrays_from_string
        """

        with open(filename, 'r') as d:
            data = d.readlines()

        e, tc, gc, rc = self.arrays_from_string(data)

        # Reset ourself first
        self._init_variables()

        self.epochs = e
        self.trigger_codes = tc
        self.group_codes = gc
        self.response_codes = rc


register_class(SliceEpochDefiner)
__all__.append('SliceEpochDefiner')


class ChannelEpochDefiner(AbstractEpochDefiner):
    """
    Epoch definer using information from channels.
    """
    def __init__(self):
        """
        Initialize ChannelEpochDefiner class.
        """
        AbstractEpochDefiner.__init__(self)

        self._init_variables()

    @classmethod
    def from_arrays(cls, epoch_size, trig_chan, resp_chan=None, trig_group_split=256):
        ret = cls()

        ret.setup_from_arrays(epoch_size, trig_chan, resp_chan, trig_group_split)

        return ret

    @staticmethod
    def chan_to_ptvals(dat):
        # Ensure we're using a 1-d array
        dat = dat.squeeze()

        # Get the points where diff is != 0 and add one to find the point we
        # care about
        pts = np.where(scipy.diff(dat) != 0)[0]
        # NB: This adding 1 is *not* to do with 0-indexing so don't be tempted
        # to remove it
        pts += 1

        # Get the vals
        vals = dat[pts]

        # Kill the points where we're going back to 0
        pts = pts[np.where(vals != 0)]
        vals = vals[np.where(vals != 0)]

        return pts, vals

    def setup_from_arrays(self, epoch_size, trig_chan, resp_chan=None, trig_group_split=256):
        """
        Loads information from the given filename using arrays_from_string.

        Warning: resets all existing information in the epoch definer

        epoch_size is (pretrigpoints, posttrigpoints).  Note that the trigger
        point itself is included in posttrigpoints (due to python's exclusive
        end indexing)
        """

        # Reset ourself first
        self._init_variables()

        # Work out the trigger codes first
        pts, vals = self.chan_to_ptvals(trig_chan)

        # Split the pts and vals into trigger and group ones
        grp_pts = pts[np.where(vals < trig_group_split)]
        grp_codes = vals[np.where(vals < trig_group_split)]

        trig_pts = pts[np.where(vals >= trig_group_split)]
        trig_codes = vals[np.where(vals >= trig_group_split)]

        # Now extract the response values
        if resp_chan is None:
            rpts = np.array([])
            rvals = np.array([])
        else:
            rpts, rvals = self.chan_to_ptvals(resp_chan)

        # We now know how many triggers we have so we can work out our epochs
        for enum in range(len(trig_pts)):
            start = trig_pts[enum] - epoch_size[0]
            end = trig_pts[enum] + epoch_size[1]

            gc = []
            rc = []

            # We can get away with being this simple because add_epoch de-dups
            # group and response codes for us
            for g in range(len(grp_pts)):
                if grp_pts[g] >= start and grp_pts[g] < end:
                    gc.append(grp_codes[g])

            for r in range(len(rpts)):
                if rpts[r] >= start and rpts[r] < end:
                    rc.append(rvals[r])

            self.add_epoch((start, end), trig_codes[enum], group_codes=gc, response_codes=rc)


register_class(ChannelEpochDefiner)
__all__.append('ChannelEpochDefiner')


# TODO: Write RegularEpochDefiner (one which defines epochs based on a regular
#       pattern of timings / codes / data length)

# TODO: Write RandomEpochDefiner (one which defines epochs based on a random
#       pattern of timings / codes / data length) [but keeps epoch length the same]
