"""General window definers"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import numpy as np

from anamnesis import AbstractAnam, register_class

__all__ = []


#######################################################################
# Time Window
#######################################################################


class TimeWindow(AbstractAnam):
    """
    Time window class used by WindowDefiner
    """

    hdf5_outputs = ['name', 'data']

    def __init__(self, name='', ms=None):
        AbstractAnam.__init__(self)

        self.name = name
        self.data = ms

    def __eq__(self, other):
        if other is None:
            return False

        if self.data != other.data:
            return False

        return True

    def __str__(self):
        return self.name

    def get_ms(self):
        if self.data is None:
            return None

        return self.data

    def get_slices(self, sample_rate, pretrig_slices=0):
        if self.data is None:
            return None

        # When we have to convert from time to slices or vice-versa, we use
        # length rather than directly converting dp1 and dp2 to avoid strange
        # rounding effects when doing different windows which should be the
        # same length but have different starting points

        start = self.data[0]
        end = self.data[1]

        dur_ms = end - start

        dur_slices = int(np.floor((dur_ms) * sample_rate / 1000.0))

        start_slice = int(np.floor(start * sample_rate / 1000.0)) + pretrig_slices
        end_slice = start_slice + dur_slices

        return (start_slice, end_slice, )

    @classmethod
    def from_yaml_dict(cls, data):
        name = data['name']

        if 'start' in data and 'end' in data:
            ret = cls(name, ms=(data['start'], data['end'],))
        else:
            ret = cls(name)

        return ret

    def to_yaml_dict(self):
        ret = {}

        ret['name'] = self.name

        if self.data is not None:
            ret['start'] = self.data[0]
            ret['end'] = self.data[1]

        return ret


register_class(TimeWindow)
__all__.append('TimeWindow')


#######################################################################
# WindowDefiner
#######################################################################


class WindowDefiner(AbstractAnam):
    """
    Dictionary based implementation of WindowDefiner.
    """

    hdf5_outputs = ['windows']

    def __init__(self):
        """
        Initialize WindowDefiner class.
        """
        AbstractAnam.__init__(self)

        self.windows = {}

    def __eq__(self, other):
        if other is None:
            return False

        return self.windows == other.windows

    def keys(self):
        return sorted(self.windows.keys())

    def has_window(self, windowname):
        return windowname in self.windows.keys()

    def add_window(self, window):
        """
        :param windowname: Name of condition to set
        :param window: TimeWindow object
        """
        self.windows[window.name] = window

    def get_window(self, windowname):
        return self.windows[windowname]

    def del_window(self, windowname):
        del self.windows[windowname]

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        for entry in data['windows']:
            ret.add_window(TimeWindow.from_yaml_dict(entry))

        return ret

    def to_yaml_dict(self):
        ret = {}

        ret['windows'] = []

        for c in self.windows:
            ret['windows'].append(self.get_window(c).to_yaml_dict())

        return ret


register_class(WindowDefiner)
__all__.append('WindowDefiner')
