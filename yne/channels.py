"""Channel handling code"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import numpy

from copy import deepcopy

from anamnesis import AbstractAnam, register_class, find_class, MPIHandler
from mne.io import constants as mc

__all__ = []

#######################################################################
# Channel type constants
#######################################################################

# TODO: REPLACE WITH REFERENCE MNE'S CHANNEL TYPES

CHANNEL_MEG = 1
CHANNEL_MEG_REF = 2
CHANNEL_EEG = 4
CHANNEL_AUX = 8

__all__ += ['CHANNEL_MEG', 'CHANNEL_MEG_REF', 'CHANNEL_EEG', 'CHANNEL_AUX']

CHANNEL_TYPES = {CHANNEL_MEG: 'MEG',
                 CHANNEL_MEG_REF: 'MEG_REF',
                 CHANNEL_EEG: 'EEG',
                 CHANNEL_AUX: 'AUX'}

CHANNEL_TYPE_STR = dict([(CHANNEL_TYPES[j], j) for j in CHANNEL_TYPES.keys()])

__all__ += ['CHANNEL_TYPES', 'CHANNEL_TYPE_STR']

CHANNEL_MEG_MAG = 64
CHANNEL_MEG_GRAD = 128

__all__ += ['CHANNEL_MEG_MAG', 'CHANNEL_MEG_GRAD']


#######################################################################
# Channel naming class
#######################################################################

def natural_key(s):
    # Split a string into numbers and strings
    from re import split
    return [int(s) if s.isdigit() else s for s in split(r'(\d+)', s) if len(s) > 0]


class ChannelName(str):
    """
    Stub class for subclassing for handling Channel names

    This class handles channel names of the form:
      * A1, A2, A200, ...
      * A001, A002,...
      * ABC001, ABC002

    The natural sort order for this is to divide up the alphabetical part
    then the number part. We use the natural_key routine for this.

    Note that this means that the class will consider A1 and A001 to be the
    same in a comparison.

    Also implements some HDF5 functionality for use by ChannelSet
    """

    def __hash__(self):
        return str.__hash__(self)

    def __lt__(self, other):
        return natural_key(self) < natural_key(other)

    def __le__(self, other):
        return natural_key(self) <= natural_key(other)

    def __ne__(self, other):
        if other is None:
            return True

        return natural_key(self) != natural_key(other)

    def __eq__(self, other):
        if other is None:
            return False

        return natural_key(self) == natural_key(other)

    def __ge__(self, other):
        return natural_key(self) >= natural_key(other)

    def __gt__(self, other):
        return natural_key(self) > natural_key(other)

    @classmethod
    def get_hdf5name(cls):
        return '.'.join([cls.__module__, cls.__name__])


__all__.append('ChannelName')
register_class(ChannelName)


class ChannelGroup(AbstractAnam):
    """
    A class representing one or more of CHANNEL_MEG, CHANNEL_MEG_REF, CHANNEL_EEG and/or
    CHANNEL_AUX
    """
    hdf5_outputs = ['name', 'channelgroup']

    def __init__(self, name='', channelgroup=0):
        AbstractAnam.__init__(self)

        self.name = name
        self.channelgroup = 0

        cg = channelgroup
        if isinstance(cg, int):
            self.channelgroup = cg
        elif isinstance(cg, str):
            # Attempt to parse string as an integer first
            try:
                cgint = int(cg)
                self.channelgroup = cgint
            except ValueError:
                # Try and split up the string using the repr syntax
                for t in cg.split(','):
                    val = t.upper().lstrip().rstrip()

                    if len(val) == 0:
                        continue

                    if val == 'NONE':
                        self.channelgroup = None
                    elif val in CHANNEL_TYPE_STR.keys():
                        self.channelgroup |= CHANNEL_TYPE_STR[val]
                    else:
                        raise ValueError("Invalid argument to ChannelGroup init (%s)" % val)

    def __eq__(self, other):
        if other is None:
            return False

        if self.name != other.name:
            return False

        if self.channelgroup is None and other.channelgroup is None:
            return True

        if self.channelgroup != other.channelgroup:
            return False

        return True

    def _generic_get(self, bit):
        if self.channelgroup is None:
            return False

        return bool(self.channelgroup & bit)

    def _generic_set(self, bit, value):
        if value:
            self.channelgroup |= bit
        else:
            self.channelgroup ^= bit

    def get_meg(self):
        return self._generic_get(CHANNEL_MEG)

    def set_meg(self, value):
        return self._generic_set(CHANNEL_MEG, value)

    meg = property(get_meg, set_meg,
                   """Get or set the MEG channeltype property""")

    def get_meg_ref(self):
        return self._generic_get(CHANNEL_MEG_REF)

    def set_meg_ref(self, value):
        return self._generic_set(CHANNEL_MEG_REF, value)

    meg_ref = property(get_meg_ref, set_meg_ref,
                       """Get or set the MEG REF channeltype property""")

    def get_eeg(self):
        return self._generic_get(CHANNEL_EEG)

    def set_eeg(self, value):
        return self._generic_set(CHANNEL_EEG, value)

    eeg = property(get_eeg, set_eeg,
                   """Get or set the EEG channeltype property""")

    def get_aux(self):
        return self._generic_get(CHANNEL_AUX)

    def set_aux(self, value):
        return self._generic_set(CHANNEL_AUX, value)

    aux = property(get_aux, set_aux,
                   """Get or set the AUX channeltype property""")

    def is_channel(self, chan):
        return bool(chan.ctype.chantype & self.channelgroup)

    def __str__(self):
        if self.channelgroup is None:
            return "NONE"

        chan_l = []
        for j in [CHANNEL_MEG, CHANNEL_MEG_REF, CHANNEL_EEG, CHANNEL_AUX]:
            if self.channelgroup & j:
                chan_l.append(CHANNEL_TYPES[j])

        return ','.join(chan_l)

    def __repr__(self):
        return self.__str__()

    def to_yaml_dict(self):
        ret = {}

        ret['type'] = 'ChannelGroup'
        ret['name'] = self.name
        ret['channelType'] = str(self)

        return ret

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls(name=data['name'],
                  channelgroup=data['channelType'])

        return ret


__all__ += ['ChannelGroup']
register_class(ChannelGroup)


class ChannelType(AbstractAnam):
    hdf5_outputs = ['chantype']

    def __init__(self, chantype=None):
        AbstractAnam.__init__(self)

        self.chantype = chantype

    def __eq__(self, other):
        if other is None:
            return False

        return self.chantype == other.chantype

    def __ne__(self, other):
        if other is None:
            return True

        return not (self.chantype == other.chantype)

    def major_compare(self, other):
        """
        The major_compare routine compares the chantype against another
        chantype but is slightly more relaxed when it comes to subtypes.  For
        example:

        self.chantype, other.chantype, result
        MEG (MAG)        MEG (MAG)     True
        MEG              MEG (MAG)     True
        MEG              MEG (GRAD)    True
        MEG (MAG)        MEG (GRAD)    False
        EEG              MEG           False
        MEG              MEG_REF       False
        """
        if self.chantype is None or other.chantype is None:
            return False

        # Am I MEG Sensor
        if bool(self.chantype & (CHANNEL_MEG)):
            if not bool(other.chantype & (CHANNEL_MEG)):
                return False

            # Check whether we differ in sensor type:
            if ((self.chantype & (CHANNEL_MEG_MAG)) and (other.chantype & (CHANNEL_MEG_GRAD))) or \
               ((self.chantype & (CHANNEL_MEG_GRAD)) and (other.chantype & (CHANNEL_MEG_MAG))):
                return False
            # Otherwise, we're ok
            return True
        # Am I MEG Ref
        elif bool(self.chantype & (CHANNEL_MEG_REF)):
            if not bool(other.chantype & (CHANNEL_MEG_REF)):
                return False
            # Check whether we differ in sensor type:
            if ((self.chantype & (CHANNEL_MEG_MAG)) and (other.chantype & (CHANNEL_MEG_GRAD))) or \
               ((self.chantype & (CHANNEL_MEG_GRAD)) and (other.chantype & (CHANNEL_MEG_MAG))):
                return False
            # Otherwise, we're ok
            return True

        # Finally, for the other types, just check the major type
        return bool(self.chantype == other.chantype)

    @property
    def is_meg(self):
        if self.chantype is None:
            return False

        return bool(self.chantype & (CHANNEL_MEG | CHANNEL_MEG_REF))

    @property
    def is_meg_data(self):
        if self.chantype is None:
            return False

        return bool(self.chantype & (CHANNEL_MEG))

    @property
    def is_meg_ref(self):
        if self.chantype is None:
            return False

        return bool(self.chantype & (CHANNEL_MEG_REF))

    @property
    def is_meg_mag(self):
        if self.chantype is None or not (bool(self.chantype & (CHANNEL_MEG | CHANNEL_MEG_REF))):
            return False

        return bool(self.chantype & (CHANNEL_MEG_MAG))

    @property
    def is_meg_grad(self):
        if self.chantype is None or not (bool(self.chantype & (CHANNEL_MEG | CHANNEL_MEG_GRAD))):
            return False

        return bool(self.chantype & (CHANNEL_MEG_GRAD))

    @property
    def is_eeg(self):
        if self.chantype is None:
            return False

        return bool(self.chantype & (CHANNEL_EEG))

    @property
    def is_aux(self):
        if self.chantype is None:
            return False

        return bool(self.chantype & (CHANNEL_AUX))

    def sanity_check(self):
        # Check the value is an int (use numpy.int as it's a superset of int)
        # The int64 check avoids issues on PPC...
        if not isinstance(self.chantype, numpy.int) and not isinstance(self.chantype, numpy.int64):
            raise ValueError('Channel type is not an integer (%s; type %s)' % (self.chantype, type(self.chantype)))

        # Check a channel isn't defined as multiple types
        if self.is_meg and self.is_eeg:
            raise ValueError('Inconsistent channel type (%d)' % self.chantype)

        # Don't take MEG properties if the channel is an EEG one
        if self.is_eeg and self.chantype != CHANNEL_EEG:
            raise ValueError('EEG Channels cannot take additional properties (%d)' % self.chantype)

        # Require MEG channel type if MEG channel
        if self.is_meg and \
           (self.chantype & (CHANNEL_MEG_MAG | CHANNEL_MEG_GRAD) not in [CHANNEL_MEG_MAG, CHANNEL_MEG_GRAD]):
            raise ValueError('MEG Channels must be either MAG or GRAD (%d)' % self.chantype)

        return True

    def __str__(self):
        s = ""
        if self.chantype is None:
            return "UNSET"

        if self.chantype & (CHANNEL_MEG | CHANNEL_MEG_REF):
            s += "MEG"
            if self.chantype & CHANNEL_MEG_REF:
                s += "REF"

            if self.chantype & CHANNEL_MEG_MAG:
                s += " (MAG)"
            elif self.chantype & CHANNEL_MEG_GRAD:
                s += " (GRAD)"

        elif self.chantype & CHANNEL_EEG:
            s += "EEG"
        elif self.chantype & CHANNEL_AUX:
            s += "AUX"
        else:
            s += "UNKNOWN"

        return s

    def __repr__(self):
        return self.__str__()


__all__.append('ChannelType')
register_class(ChannelType)

#######################################################################
# Channel class
#######################################################################


class Channel(AbstractAnam):
    """
    Class representing a Channel (MEG or EEG)
    """

    def __init__(self, name=None, ctype=None):
        AbstractAnam.__init__(self)

        # Always use the ChannelName class for channel names
        if name is not None and not isinstance(name, ChannelName):
            name = ChannelName(name)

        self.name = name
        """Channel name (as a ChannelName instance)"""

        # Always use the ChannelType class for channel names
        if ctype is not None and not isinstance(ctype, ChannelType):
            ctype = ChannelType(ctype)

        self.ctype = ctype
        """Channel type.  Bit mask using CHANNEL_* values"""

    def __eq__(self, other):
        if other is None:
            return False

        return (self.name == other.name) and (self.ctype == other.ctype)

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        """Sort on channel name (lt)"""
        return self.name < other.name

    def __le__(self, other):
        """Sort on channel name (le)"""
        return self.name <= other.name

    def __gt__(self, other):
        """Sort on channel name (gt)"""
        return self.name > other.name

    def __ge__(self, other):
        """Sort on channel name (ge)"""
        return self.name >= other.name

    def __str__(self):
        return "%s (%s)" % (self.name, self.ctype)

    def __repr__(self):
        return self.__str__()

    @property
    def is_meg(self):
        return self.ctype.is_meg

    @property
    def is_meg_data(self):
        return self.ctype.is_meg_data

    @property
    def is_meg_ref(self):
        return self.ctype.is_meg_ref

    @property
    def is_meg_mag(self):
        return self.ctype.is_meg_mag

    @property
    def is_meg_grad(self):
        return self.ctype.is_meg_grad

    @property
    def is_eeg(self):
        return self.ctype.is_eeg

    @property
    def is_aux(self):
        return self.ctype.is_aux


__all__.append('Channel')
register_class(Channel)


#######################################################################
# ChannelSet class
#######################################################################


class ChannelSet(dict, AbstractAnam):
    """
    Class representing a ordered set of channels.  Behaves like an ordered
    dictionary but also has serialisation methods for to and from hdf5
    """

    @classmethod
    def get_hdf5name(cls):
        return '.'.join([cls.__module__, cls.__name__])

    def __init__(self, *args, **kwargs):
        AbstractAnam.__init__(self)

        if len(args) == 1 and isinstance(args[0], str):
            self.name = args[0]
            args = args[1:]
        elif 'name' in kwargs:
            self.name = kwargs.pop('name', '')
        else:
            self.name = ''

        dict.__init__(self, *args, **kwargs)

    def __repr__(self):
        return 'ChannelSet(%s:' % self.name + ' {%s})' % ', '.join('%r: %r' % c for c in self.iteritems())

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if other is None:
            return False

        if self.name != other.name:
            return False

        return dict.__eq__(self, other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def keys(self):
        return list(sorted(dict.keys(self)))

    def at_index(self, index):
        """
        Returns the name of the channel at a given index in the ChannelSet.
        This is useful when tracking down which channel corresponds to a given
        row in actual data.

        :param index: Index to look up channel name for
        :return: Channel name at index
        """
        chans = self.keys()

        if index < 0 or index >= len(chans):
            raise ValueError("Index value beyond number of valid channels")

        return chans[index]

    def __iter__(self):
        return iter(sorted(self.keys()))

    iterkeys = __iter__

    def itervalues(self):
        return (self[c] for c in self.iterkeys())

    def iteritems(self):
        return ((c, self[c]) for c in self.iterkeys())

    def values(self):
        return list(self.itervalues())

    def items(self):
        return list(self.iteritems())

    def add_channel(self, *args):
        chan = None

        if len(args) == 1:
            if isinstance(args[0], Channel):
                chan = args[0]
            elif isinstance(args[0], str):
                # Channel name without type
                chan = Channel(args[0])
        elif len(args) == 2:
            chan = Channel(args[0], args[1])

        if chan is None:
            raise ValueError("add_channel needs either a Channel or name and type")

        self[chan.name] = chan

    def __add__(self, other):
        """
        Add together two ChannelSet objects.
        """
        if other is None:
            return ChannelSet(self)

        newc = ChannelSet(self)
        if isinstance(other, ChannelSet):
            for c in other.keys():
                if c in newc.keys():
                    if other[c].ctype != newc[c].ctype:
                        err = "Cannot combine two ChannelSets with channels of the same name but different types"
                        raise ValueError(err)
                else:
                    newc[c] = deepcopy(other[c])
        else:
            # Assume they're trying to add a single channel
            if other.name in newc:
                if newc[other.name].ctype != other.ctype:
                    raise ValueError("Cannot add a Channel of a different type with the same name")
            newc[other.name] = deepcopy(other)

        return newc

    def __sub__(self, other):
        """
        Subtract one ChannelSet from another.

        :param other: Either a ChannelSet or a string specifying a channel name

        :raise ValueError: if a Channel is specified in the second set which is
                not in the first.
        """
        newc = ChannelSet(self)
        if isinstance(other, str):
            # Just a single channel name presumably
            if other in newc:
                newc.pop(other)
            else:
                raise ValueError("%s does not exist in ChannelSet" % other)

        elif isinstance(other, list):
            # List of channel names or channels
            for c in other:
                if isinstance(c, str):
                    cname = c
                else:
                    # Assume channel
                    cname = c.name

                if cname in newc:
                    newc.pop(cname)
                else:
                    raise ValueError("%s does not exist in ChannelSet" % cname)
        else:
            # It had better be a ChannelSet
            for c in other:
                if c in newc:
                    newc.pop(c)
                else:
                    raise ValueError("%s does not exist in ChannelSet" % c)

        return newc

    def discard(self, other):
        """
        Discards one ChannelSet from another.

        This differs from __sub__ in that an error will not be raised if a
        channel is in `other` which is not in the ChannelSet being operated on.

        :param other: Either a ChannelSet or a string specifying a channel name
        """
        newc = ChannelSet(self)
        if isinstance(other, str):
            # Just a single channel name presumably
            if other in newc:
                newc.pop(other)

        elif isinstance(other, list):
            # List of channel names or channels
            for c in other:
                if isinstance(c, str):
                    cname = c
                else:
                    # Assume channel
                    cname = c.name

                if cname in newc:
                    newc.pop(cname)
        else:
            # It had better be a ChannelSet
            for c in other:
                if c in newc:
                    newc.pop(c)

        return newc

    def intersection(self, other, check_types=False):
        """
        Return the intersection of the current ChannelSet and either another
        ChannelSet or all the channels in the current set which are of the type
        referred to by the passed ChannelGroup.  Unlike __sub__, this will not
        raise an error if a channel is found in the second set which is not in the
        first.

        :type other: ChannelSet or ChannelGroup
        :param other: The group to intersect with.

        :type check_types: bool
        :param check_types: Check the channel types when performing an
                            intersection (if possible) and raise an error if
                            the types don't match.

        :rtype: ChannelSet
        :return: The intersection of either the two ChannelSets or all of the
                 channels in the ChannelSet which are of the type passed as ChannelGroup

        :raise ValueError: If check_types is True and two channels with the same
                           name have different types
        """
        newc = ChannelSet()

        if isinstance(other, ChannelSet):
            for name, chan in self.items():
                if name in other:
                    if check_types and chan != other[name]:
                        raise ValueError("Channel %s has different types in intersection" % name)
                    newc = newc + chan

        elif isinstance(other, ChannelGroup):
            # Assume ChannelGroup
            for name, chan in self.items():
                if other.is_channel(chan):
                    newc = newc + chan

        elif isinstance(other, list):
            for c in other:
                if isinstance(c, Channel):
                    if c.name in self:
                        if check_types and c != self[c.name]:
                            raise ValueError("Channel %s has different types in intersection" % c.name)
                        newc = newc + self[c.name]
                else:
                    # Assume a string name
                    if c in self:
                        newc = newc + self[c]
        else:
            # Assume a Channel or String
            if isinstance(other, Channel):
                if other.name in self:
                    if check_types and other != self[other.name]:
                        raise ValueError("Channel %s has different types in intersection" % other.name)
                    newc = newc + other
            else:
                # Assume a string name
                if other in self:
                    newc = newc + self[other]

        return newc

    @property
    def num_channels(self):
        return len(self)

    def to_yaml_dict(self):
        ret = {}

        ret['type'] = 'ChannelSet'
        ret['name'] = self.name
        ret['chans'] = []

        for c in self.keys():
            # We need to ensure that the name is str, not ChannelName
            # otherwise PyYAML barfs on serialisation
            # Adding a representer doesn't seem to help, so this works
            # around it perfectly well
            info = [str(self[c].name), self[c].ctype.chantype]
            ret['chans'].append(info)

        return ret

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        ret.name = data['name']

        for cinfo in data['chans']:
            name, ctype = cinfo
            ret.add_channel(Channel(name, ctype))

        return ret

    def to_hdf5(self, subgroup):
        # We do this in a custom way as we don't want to save out 248 small objects
        # for the channels which are just (effectively) each a string and int.

        # Write class name
        subgroup.attrs['class'] = self.get_hdf5name()

        # Write out our informative name
        subgroup.attrs['name'] = self.name

        # We add the constraint that all of our channels needs to be of the same
        # channel naming type.  We should probably do a sanity check on this before
        # assuming it though
        chans = self.keys()

        if len(chans) > 0:
            subgroup.attrs['channamingclass'] = self[chans[0]].name.get_hdf5name()

        channames = []
        chantypes = []

        for c in chans:
            channames.append(self[c].name)
            chantypes.append(self[c].ctype.chantype)

        if len(channames) > 0:
            subgroup.attrs['channames'] = [x.encode('utf-8') for x in channames]

        if len(chantypes) > 0:
            subgroup.attrs['chantypes'] = chantypes

        return subgroup

    @classmethod
    def from_hdf5(cls, subgroup):
        # Check the class name against the group
        if not cls().check_classname(subgroup.attrs['class']):  # pragma: nocover
            raise ValueError('Subgroup specifies %s instead of %s' % (subgroup.attrs['class'],
                                                                      cls().get_hdf5name()))

        # Handle an empty list
        if 'channames' not in subgroup.attrs.keys():
            # We have an empty ChannelSet
            return cls(name=subgroup.attrs['name'])

        raw_channames = subgroup.attrs['channames']
        channametype = find_class(subgroup.attrs['channamingclass'])

        channames = [channametype(x.decode('utf-8')) for x in raw_channames]

        chantypes = subgroup.attrs['chantypes']

        if len(channames) != len(chantypes):  # pragma: nocover
            raise ValueError("Can't parse HDF5 subgroup: channames and chantypes are not the same length")

        ret = cls(name=subgroup.attrs['name'])

        for i in range(len(channames)):
            ret[channames[i]] = Channel(channames[i], ChannelType(chantypes[i]))

        # Check the ordering is the same as in the HDF5 file and error if not
        if ret.keys() != channames:  # pragma: nocover
            msg = "Ordering of ChannelSet is incorrect after load"
            raise ValueError(msg)

        return ret

    def bcast(self, root=0):  # pragma: nocover: Issue #3
        m = MPIHandler()

        # Send class name
        m.bcast(self.get_hdf5name(), root=root)

        # Write out our informative name
        m.bcast(self.name, root=root)

        # We add the constraint that all of our channels needs to be of the same
        # channel naming type.  We should probably do a sanity check on this before
        # assuming it though

        chans = self.keys()

        m.bcast(len(chans), root=root)

        if len(chans) > 0:
            m.bcast(self[chans[0]].name.get_hdf5name(), root=root)

            for c in chans:
                m.bcast(self[c].name, root=root)
                m.bcast(self[c].ctype.chantype, root=root)

        return self

    @classmethod
    def bcast_recv(cls, root=0):  # pragma: nocover: Issue #3
        m = MPIHandler()

        clsname = m.bcast(None, root=root)

        # Check the class name against the group
        if clsname != cls.get_hdf5name():
            raise ValueError('MPI specified %s instead of %s' % (clsname,
                                                                 cls.get_hdf5name()))

        name = m.bcast(None, root=root)

        numchans = m.bcast(None, root=root)

        ret = cls(name=name)

        if numchans < 1:
            # We have an empty ChannelSet
            return ret

        chanclassname = m.bcast(None, root=root)

        channametype = find_class(chanclassname)

        for j in range(numchans):
            channame = m.bcast(None, root=root)
            chantype = m.bcast(None, root=root)

            ret[channame] = Channel(channametype(channame), ChannelType(chantype))

        return ret

    def send(self, dest=0, tag=0):  # pragma: nocover: Issue #3
        m = MPIHandler()

        # Send class name
        m.send(self.get_hdf5name(), dest=dest, tag=tag)

        # Write out our informative name
        m.send(self.name, dest=dest, tag=tag)

        # We add the constraint that all of our channels needs to be of the same
        # channel naming type.  We should probably do a sanity check on this before
        # assuming it though

        chans = self.keys()

        m.send(len(chans), dest=dest, tag=tag)

        if len(chans) > 0:
            m.send(self[chans[0]].name.get_hdf5name(), dest=dest, tag=tag)

            for c in chans:
                m.send(self[c].name, dest=dest, tag=tag)
                m.send(self[c].ctype.chantype, dest=dest, tag=tag)

        return self

    @classmethod
    def recv(cls, source=0, tag=0, status=None):  # pragma: nocover: Issue #3
        m = MPIHandler()

        clsname = m.recv(None, source=source, tag=tag, status=status)

        # Check the class name against the group
        if clsname != cls.get_hdf5name():
            raise ValueError('MPI specified %s instead of %s' % (clsname,
                                                                 cls.get_hdf5name()))

        name = m.recv(None, source=source, tag=tag, status=status)

        numchans = m.recv(None, source=source, tag=tag, status=status)

        ret = cls(name=name)

        if numchans < 1:
            # We have an empty ChannelSet
            return ret

        chanclassname = m.recv(None, source=source, tag=tag, status=status)

        channametype = find_class(chanclassname)

        for j in range(numchans):
            channame = m.recv(None, source=source, tag=tag, status=status)
            chantype = m.recv(None, source=source, tag=tag, status=status)

            ret[channame] = Channel(channametype(channame), ChannelType(chantype))

        return ret


register_class(ChannelSet)
__all__.append('ChannelSet')


#######################################################################
# Dictionary based Channel Definer
#######################################################################


class ChannelDefiner(AbstractAnam):
    """
    Class for handling channelset definer information
    """

    hdf5_outputs = ['chansets']

    def __init__(self):
        """
        Initialize ChannelDefiner class.
        """
        AbstractAnam.__init__(self)
        self.chansets = {}

    def __eq__(self, other):
        if other is None:
            return False

        return self.chansets == other.chansets

    def keys(self):
        return sorted(self.chansets.keys())

    def has_chanset(self, chansetname):
        return chansetname in self.chansets.keys()

    def add_chanset(self, chanset):
        self.chansets[chanset.name] = chanset

    def get_chanset(self, chansetname):
        return self.chansets[chansetname]

    def del_chanset(self, chansetname):
        del self.chansets[chansetname]

    def to_yaml_dict(self):
        ret = []

        for c in sorted(self.keys()):
            cs = self.get_chanset(c)
            ret.append(cs.to_yaml_dict())

        return ret

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        for entry in data:
            if entry['type'] == 'ChannelGroup':
                cg = ChannelGroup.from_yaml_dict(entry)
                ret.add_chanset(cg)
            else:
                cs = ChannelSet.from_yaml_dict(entry)
                ret.add_chanset(cs)

        return ret


register_class(ChannelDefiner)
__all__.append('ChannelDefiner')


#######################################################################
# Helper routines
#######################################################################


def mne_chantype_to_yne(ch_type, raw_ch_info):
    """
    Converts an MNE channel type into a YNE one.

    :param ch_type: Channel type as string (e.g. 'meg')
    :param raw_ch_info: raw.info['chs'][x] dictionary for the channel
                        (used for analysing subtypes)
    """

    # Put everything in AUX if we can't find it
    yne_type = CHANNEL_AUX

    # Figure out the channel type
    if ch_type == 'mag':
        yne_type = CHANNEL_MEG | CHANNEL_MEG_MAG
    elif ch_type == 'grad':
        yne_type = CHANNEL_MEG | CHANNEL_MEG_GRAD
    elif ch_type == 'ref_meg':
        # Need to determine type by looking at internal code
        subtype = raw_ch_info['coil_type']

        if subtype in (mc.FIFF.FIFFV_COIL_MAGNES_REF_GRAD,
                       mc.FIFF.FIFFV_COIL_MAGNES_OFFDIAG_REF_GRAD,
                       mc.FIFF.FIFFV_COIL_MAGNES_R_GRAD,
                       mc.FIFF.FIFFV_COIL_MAGNES_R_GRAD_OFF,
                       mc.FIFF.FIFFV_COIL_CTF_REF_GRAD,
                       mc.FIFF.FIFFV_COIL_CTF_OFFDIAG_REF_GRAD):
            yne_type = CHANNEL_MEG_REF | CHANNEL_MEG_GRAD
        elif subtype in (mc.FIFF.FIFFV_COIL_MAGNES_REF_MAG,
                         mc.FIFF.FIFFV_COIL_MAGNES_R_MAG,
                         mc.FIFF.FIFFV_COIL_CTF_REF_MAG):
            yne_type = CHANNEL_MEG_REF | CHANNEL_MEG_MAG
        else:
            # Unknown reference type
            yne_type = CHANNEL_MEG_REF
    elif ch_type == 'eeg':
        yne_type = CHANNEL_EEG

    return yne_type


__all__.append('mne_chantype_to_yne')
