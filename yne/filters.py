"""Data filtering code"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from anamnesis import AbstractAnam, register_class, find_class

from yne.readers import AbstractMegReader
from yne.options import YneOptions


__all__ = []


#######################################################################
# Filters
#######################################################################


class AbstractFilter(AbstractAnam):
    """Abstract base class for DataFilters"""

    def __init__(self):  # pragma: no cover
        """
        Initialize AbstractFilter class.
        """
        AbstractAnam.__init__(self)

    def filter_data(self, data):  # pragma: no cover
        """
        Filter data and return changed data-set.

        :param data: Data to filter as an MNE Raw object
        """
        raise NotImplementedError('This is an ABC')

    def get_rec_freq_range(self):  # pragma: no cover
        """
        Returns a tuple of (low_freq, high_freq) containing the frequency range
        over which data should be considered after this filter.

        This is often used by time-frequency plots to determine ranges to plot.

        Can also return None if the filter has no effect on frequency content.
        """
        raise NotImplementedError("This is an ABC")


__all__.append('AbstractFilter')


#######################################################################
# Filters
#######################################################################


class BandFilter(AbstractFilter):
    """Class which implements BandPass/Stop and Low/High-Pass methods"""

    hdf5_outputs = ['low_freq', 'high_freq', 'method',
                    'fir_window', 'fir_design'
                    'low_trans_bandwidth', 'high_trans_bandwidth',
                    'phase', 'pad']

    def __init__(self, **kwds):
        """
        Initialize BandFilter class

        :param low_freq: Low frequency
        :param high_freq: High frequency

        Details of other parameters can be found in the _get_defaults() routine.

        If low_freq < high_freq, this is a Band-Pass filter

        If low_freq > high_freq, this is a Band Stop filter

        If low_freq is not None and high_freq is None, this is a High-Pass filter

        If low_freq is None and high_freq is not None, this is a Low-Pass filter

        If both low_freq and high_freq are None, the filter does nothing.
        """

        AbstractFilter.__init__(self)

        defaults = self._get_defaults()

        # Configure all of the defaults
        for key, default in defaults.items():
            setattr(self, key, kwds.get(key, default))

    @classmethod
    def _get_defaults(cls):
        defaults = {}

        defaults['low_freq'] = None
        defaults['high_freq'] = None
        defaults['method'] = 'fir'
        defaults['filter_length'] = 'auto'
        defaults['fir_window'] = 'hamming'
        defaults['fir_design'] = 'firwin'
        defaults['low_trans_bandwidth'] = 'auto'
        defaults['high_trans_bandwidth'] = 'auto'
        defaults['phase'] = 'zero'
        defaults['pad'] = 'reflect_limited'

        return defaults

    def __eq__(self, other):
        if other is None:
            return False

        defaults = self._get_defaults()

        for key in defaults.keys():
            us = getattr(self, key)
            them = getattr(other, key)

            if us is None and them is None:
                continue

            if us != them:
                return False

        return True

    def filter_data(self, data):
        """
        Filter the data
        """

        opt = YneOptions()

        # Allow passing a MegReader object to make life easier
        if isinstance(data, AbstractMegReader):
            data = data.loader

        # Ensure that the data is loaded for us to use
        if not data.preload:
            # If the data is not loaded, we make a copy of the object
            # so that garbage collection will not retain the unfiltered
            # data in memory later on the original copy
            data = data.copy()

            data.load_data(verbose=opt.verbose)

        return data.filter(l_freq=self.low_freq,
                           h_freq=self.high_freq,
                           filter_length=self.filter_length,
                           l_trans_bandwidth=self.low_trans_bandwidth,
                           h_trans_bandwidth=self.high_trans_bandwidth,
                           n_jobs=opt.n_jobs,
                           method=self.method,
                           phase=self.phase,
                           fir_window=self.fir_window,
                           fir_design=self.fir_design,
                           pad=self.pad,
                           verbose=opt.verbose)

    @property
    def description(self):
        if self.low_freq is not None and self.high_freq is not None:
            if self.low_freq < self.high_freq:
                return f'BandPass ({self.low_freq}-{self.high_freq}Hz)'
            else:
                return f'BandStop ({self.high_freq}-{self.low_freq}Hz)'

        elif self.low_freq is not None and self.high_freq is None:
            return f'HighPass ({self.low_freq}Hz-)'

        elif self.low_freq is None and self.high_freq is not None:
            return f'LowPass (0-{self.high_freq}Hz)'

        return 'NoFilter (Unconfigured)'

    def __str__(self):
        return f'<BandFilter [{self.description}]>'

    @classmethod
    def from_yaml_dict(cls, data):
        defaults = cls._get_defaults()

        args = {}

        # Configure all of the defaults
        for key, default in defaults.items():
            args[key] = data.get(key, default)

        # Perform some conversion
        if args['low_freq'] is not None:
            args['low_freq'] = float(args['low_freq'])

        # Perform some conversion
        if args['high_freq'] is not None:
            args['high_freq'] = float(args['high_freq'])

        return cls(**args)

    def to_yaml_dict(self):
        ret = {'Filter': self.get_hdf5name()}

        defaults = self._get_defaults()

        for key in defaults.keys():
            ret[key] = getattr(self, key)

        return ret

    def get_rec_freq_range(self):
        if self.low_freq is not None and self.high_freq is not None:
            if self.low_freq < self.high_freq:
                return (self.low_freq, self.high_freq)
            else:
                # Can't give advice based on a stop filter
                return None

        elif self.low_freq is not None and self.high_freq is None:
            return (self.low_freq, None)

        elif self.low_freq is None and self.high_freq is not None:
            return (None, self.high_freq)

        return None


register_class(BandFilter)
__all__.append('BandFilter')

#######################################################################
# Filter Chain
#######################################################################


class FilterChain(AbstractAnam):
    """
    Filter Chain class.
    """
    hdf5_defaultgroup = 'filterchain'

    hdf5_outputs = ['name', 'filters']

    def __init__(self, name=''):
        """Initialize FilterChain class with an empty chain"""
        AbstractAnam.__init__(self)

        self.name = name
        self.filters = []

    def __eq__(self, other):
        if other is None:
            return False

        if self.name != other.name:
            return False

        if len(self.filters) != len(other.filters):
            return False

        for f, g in zip(self.filters, other.filters):
            if f != g:
                return False

        return True

    def __str__(self):
        return self.name

    def add_filter(self, filter):
        """Adds an object derived from AbstractFilter to the filter chain"""
        self.filters.append(filter)

    def get_rec_freq_range(self):
        """
        Find the most sensible frequency range for this dataset

        May return None if filter chain does not affect frequency content,
        otherwise will return (low_freq, high_freq)

        Note that high_freq may be less than low_freq, it is up to the
        caller to detect and deal with this
        """
        frange = [None, None]

        for fc in self.filters:
            tmp = fc.get_rec_freq_range()
            if tmp is None:
                continue

            if tmp[0] is not None:
                # Need to update the low frequency
                if frange[0] is None:
                    frange[0] = tmp[0]
                else:
                    # Only update the low frequency if we're more restrictive
                    if tmp[0] > frange[0]:
                        frange[0] = tmp[0]

            if tmp[1] is not None:
                # Need to update the high frequency
                if frange[1] is None:
                    frange[1] = tmp[1]
                else:
                    # Only update the high frequency if we're more restrictive
                    if tmp[1] < frange[1]:
                        frange[1] = tmp[1]

        if frange[0] is None and frange[1] is None:
            return None

        return tuple(frange)

    def filter_data(self, data):
        for fc in self.filters:
            data = fc.filter_data(data)

        return data

    @property
    def num_filters(self):
        return len(self.filters)

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls(name=data['name'])

        for fd in data['filters']:
            # Find the appropriate class
            f = find_class(fd['Filter'])
            ret.add_filter(f.from_yaml_dict(fd))

        return ret

    def to_yaml_dict(self):
        ret = {}

        ret['name'] = self.name

        ret['filters'] = []

        for fc in self.filters:
            ret['filters'].append(fc.to_yaml_dict())

        return ret


register_class(FilterChain)
__all__.append('FilterChain')


#######################################################################
# Filter Chain Definer
#######################################################################


class FilterChainDefiner(AbstractAnam):
    """
    Dictionary based implementation of a FilterChainDefiner
    """

    hdf5_outputs = ['chains']

    def __init__(self):
        """
        Initialize FilterChainDefiner class.
        """
        AbstractAnam.__init__(self)

        self.chains = {}

    def __eq__(self, other):
        if other is None:
            return False

        return self.chains == other.chains

    def keys(self):
        return sorted(self.chains.keys())

    def has_filterchain(self, chainname):
        return chainname in self.chains.keys()

    def get_filterchain(self, chainname):
        return self.chains[chainname]

    def add_filterchain(self, chain):
        """
        :param chain: FilterChain object to set
        """
        self.chains[chain.name] = chain

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        for entry in data['chains']:
            ret.add_filterchain(FilterChain.from_yaml_dict(entry))

        return ret

    def to_yaml_dict(self):
        ret = {}

        ret['chains'] = []

        for fc in sorted(self.keys()):
            ret['chains'].append(self.get_filterchain(fc).to_yaml_dict())

        return ret


register_class(FilterChainDefiner)
__all__.append('FilterChainDefiner')
