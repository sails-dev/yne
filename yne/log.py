"""YNE logging configuration"""

import logging

YNE_LOGGER_NAME = 'yne'


def get_logger():
    """
    Gets the primary logger for YNE
    """

    return logging.getLogger('yne')


__all__ = ['get_logger']
