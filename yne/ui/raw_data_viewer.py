#!/usr/bin/python3

from os.path import join, dirname

import matplotlib.pyplot as plt

from PyQt5.QtCore import QRunnable, pyqtSlot, QThreadPool
from PyQt5.QtWidgets import (QFileDialog, QMessageBox, QMainWindow)
from PyQt5.uic import loadUi

from yne import (WIDGET_PATH, YneOptions, guess_datatype, get_meg_reader_class,
                 ChannelSet)

from .bandpass import BandPassUI
from .worker_support import WorkerSignals, get_log_time


class FilterWorker(QRunnable):
    def __init__(self, data, filterchain):
        QRunnable.__init__(self)

        self.start_data = data

        self.filterchain = filterchain

        self.output_data = None

        self.exception = None
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        try:
            self.output_data = self.filterchain.filter_data(self.start_data)

            success = True
        except Exception as e:
            self.exception = e
            success = False

        self.signals.finished.emit(success)


class SaveWorker(QRunnable):
    def __init__(self, data, path):
        QRunnable.__init__(self)

        self.data = data

        self.path = path

        self.exception = None
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        try:
            self.data.loader.save(self.path)

            success = True
        except Exception as e:
            self.exception = e
            success = False

        self.signals.finished.emit(success)


class YNERawViewer(QMainWindow):
    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self, *args, **kwargs)

        loadUi(join(WIDGET_PATH, 'rawUi.ui'), self)

        self.reader = None
        self.view_window = None

        self.threadpool = QThreadPool()

        self.proc_object = None

        self.setup_menu()

    def add_log(self, txt):
        logtime = f'<tt>{get_log_time()} : </tt>'

        self.procSteps.append(logtime + txt)

    def setup_menu(self):
        # File menu
        self.action_Open.triggered.connect(self.load_data)
        self.action_SaveAs.triggered.connect(self.start_save_data)
        self.action_Exit.triggered.connect(self.close)

        # Processing menu
        self.action_Filter.triggered.connect(self.start_filter_data)

        self.action_ICA.setEnabled(False)
        self.action_ICA.triggered.connect(self.ica_data)

        # Sync button
        self.sync_bad_button.clicked.connect(self.sync_bad_chans_from_window)

    def load_data(self):
        types = "BTI (c c,rf* e e,rf*);;All files (*.*)"

        path, typ = QFileDialog.getOpenFileName(self,
                                                'Data to load',
                                                YneOptions().meg_storage_dir,
                                                types)

        if path == '':
            return

        if typ.startswith('BTI'):
            # Assume BTI
            datatype = 'BTI'
        else:
            try:
                # Guess type and error if not
                datatype = guess_datatype(typ)
            except RuntimeError:
                datatype = None

        if datatype is None:
            QMessageBox.warning(self,
                                "Error",
                                f"Could not detect file type for {path}")
            return

        try:
            self.reader = get_meg_reader_class(datatype)(path)
        except Exception:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to read data from file {path}")
            return

        if self.view_window is not None:
            plt.close(self.view_window)

            self.view_window = None

        # Call the MNE plotting routine and keep a copy of the window
        self.view_window = self.reader.loader.plot()

        # Update the status window
        self.filename_label.setText(path)

        self.sync_info()

        # Update processing steps
        self.procSteps.clear()
        self.add_log(f'<b>Loaded file: </b>{path}')

        self.menuProcessing.setEnabled(True)

    def sync_bad_chans_from_window(self):
        if self.view_window is None:
            return

        bads = self.view_window.mne.info['bads']

        cset = ChannelSet()
        for chan in bads:
            cset.add_channel(chan)

        # Log it
        bad_c_list = ','.join(bads)
        self.add_log(f'<b>Set bad chans to: </b>{bad_c_list}<br/>')

        # Keep things in sync
        self.reader.set_rejected_channels(cset)
        self.reader.loader.info['bads'] = bads

        self.sync_info()

    def sync_info(self):
        if self.reader is None:

            self.duration_label.setText('N/A')
            self.num_slices_label.setText('N/A')
            self.sample_rate_label.setText('N/A')

            self.meg_chan_label.setText('N/A')
            self.megref_chan_label.setText('N/A')
            self.eeg_chan_label.setText('N/A')
            self.aux_chan_label.setText('N/A')

            self.bad_chan_label.setText('')

            return

        dur = self.reader.num_slices / self.reader.sample_rate
        self.duration_label.setText(f'{dur:.1f}')
        self.num_slices_label.setText(f'{self.reader.num_slices}')
        self.sample_rate_label.setText(f'{self.reader.sample_rate:.2f}')

        self.meg_chan_label.setText(str(len(self.reader.meg_chanset)))
        self.megref_chan_label.setText(str(len(self.reader.meg_ref_chanset)))
        self.eeg_chan_label.setText(str(len(self.reader.eeg_chanset)))
        self.aux_chan_label.setText(str(len(self.reader.aux_chanset)))

        self.bad_chan_label.setText(','.join(self.reader.rejected_chanset))

    def start_save_data(self):
        if self.reader is None:
            return

        if self.proc_object is not None:
            return

        types = "FIFF file (*.fif *.fif.gz);;"

        path, typ = QFileDialog.getSaveFileName(self,
                                                'Save data as FIF',
                                                directory=dirname(self.reader.filename),
                                                filter=types)

        if path == '':
            return

        self.menuProcessing.setEnabled(False)

        self.add_log(f'<b>Started saving file: </b>{path} </b><br/>')

        self.proc_object = SaveWorker(self.reader, path)
        self.proc_object.signals.finished.connect(self.save_data_finished)

        # Execute
        self.threadpool.start(self.proc_object)

    def save_data_finished(self, success):
        if success:
            self.add_log(f'<b>Saved file: </b> {self.proc_object.path}<br/>')
        else:
            self.add_log(f'<b>Failed to save file: </b> {self.proc_object.path} ({self.proc_object.exception})<br/>')

        # Clean up
        self.proc_object = None

        self.menuProcessing.setEnabled(True)

    def start_filter_data(self):
        if self.reader is None:
            return

        if self.proc_object is not None:
            return

        bpui = BandPassUI()

        # Don't continue on Cancel
        if not bpui.exec_():
            return

        # Close any existing plots
        if self.view_window is not None:
            plt.close(self.view_window)

            self.view_window = None

        self.menuProcessing.setEnabled(False)

        filterchain = bpui.filterchain()

        self.add_log(f'<b>Started filtering: </b>{bpui.description} </b><br/>')

        self.proc_object = FilterWorker(self.reader, filterchain)
        self.proc_object.signals.finished.connect(self.filter_data_finished)

        # Execute
        self.threadpool.start(self.proc_object)

    def filter_data_finished(self, success):
        if success:
            self.add_log('<b>Finished filtering</b><br/>')

            # Update data
            self.reader.loader = self.proc_object.output_data
        else:
            self.add_log(f'<b>Filtering failed: {self.proc_object.exception}</b><br/>')

        # Show the data we have available (probably filtered)
        self.view_window = self.reader.loader.plot()

        # Clean up
        self.proc_object = None
        self.menuProcessing.setEnabled(True)

    def ica_data(self):
        # TODO: Implement ICA
        print("ICA")
