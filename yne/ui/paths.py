#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


from os.path import join, dirname

__all__ = []


# Exported variable
WIDGET_PATH = join(dirname(__file__), 'widgets')


__all__.append('WIDGET_PATH')
