#!/usr/bin/python3

from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtWidgets import QLabel

from .selector_mixins import (ChanCheckMixin, CheckBoxMixin, FloatTxtMixin,
                              ListCtrlMixin, MinMaxMixin, FloatListMixin)
from .run_cond_select import RunCondSelect


#############################################################################
# Specialised subclasses used directly in place of RunCondSelect
#############################################################################


class TopologyImageOpts(RunCondSelect, MinMaxMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # Add custom options
        self.add_minmax_ctrls('v', 'v', 0)

        # TODO: Add

        # sigma
        # colorbar
        # cmap

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_minmax_args('v', 'vmin', 'vmax'))

        return dat


class AverageMethodPlotOpts(RunCondSelect, ListCtrlMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # Add custom options
        self.add_listctrl_ctrls('weights', 'Weighting', 0, ['equal', 'nave'])

        self.run_choice.currentIndexChanged.connect(self.sync_status)
        self.sync_status()

    def sync_status(self):
        self.listctrls['weights']['combo'].setEnabled(self.all_runs_combined)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_listctrl_args('weights'))

        return dat


class AverageRawPlotOpts(RunCondSelect, CheckBoxMixin, MinMaxMixin, ListCtrlMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # Add custom options
        self.add_checkbox_ctrls('spatial_colors', 'Spatial Colours', 0)
        self.add_checkbox_ctrls('gfp', 'Show GFP', 1)
        self.add_minmax_ctrls('y', 'Y-axis', 2)
        self.add_listctrl_ctrls('weights', 'Group level weighting', 3, ['equal', 'nave'])

        self.run_choice.currentIndexChanged.connect(self.sync_status)
        self.sync_status()

    def sync_status(self):
        self.listctrls['weights']['combo'].setEnabled(self.all_runs_combined)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_checkbox_args('spatial_colors'))
        dat.update(self.get_checkbox_args('gfp'))
        dat.update(self.get_listctrl_args('weights'))
        meg = self.get_minmax_args('y', 'ylim')

        # TODO: Handle mag, grad, eeg differently later
        if len(meg) > 0:
            dat['ylim'] = {}
            dat['ylim']['mag'] = meg['ylim']
            dat['ylim']['grad'] = meg['ylim']

        return dat


class TopologyOpts(RunCondSelect, FloatListMixin, MinMaxMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # Add custom options
        self.add_floatlist_ctrls('times', 'Times', kwargs['tstartvals'],
                                 kwargs['tmin'], kwargs['tmax'], 0)

        self.add_minmax_ctrls('v', 'v', 1)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_floatlist_args('times'))
        dat.update(self.get_minmax_args('v', 'vmin', 'vmax'))

        return dat


class AverageCompOptions(RunCondSelect, FloatTxtMixin, CheckBoxMixin, ListCtrlMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # We need to map some internal names to the real ones
        self.floats_condmap = {}

        # General plotting options
        self.add_checkbox_ctrls('spatial_colors', 'Spatial Colours', 0)
        self.add_checkbox_ctrls('gfp', 'Show GFP', 1)

        self.add_listctrl_ctrls('weights', 'Weighting', 2, ['equal', 'nave'])

        label = QLabel('Comparison Weights')
        label.setStyleSheet("font-weight: bold;")
        self.customLayout.addWidget(label, 3, 0)

        rownum = 4

        for condition in self.conditions:
            cond_map_name = f'condition_{rownum}'

            self.floats_condmap[cond_map_name] = condition

            self.add_float_ctrls(cond_map_name, condition, rownum)

            self.floats[cond_map_name]['lineedit'].setText("0")

            rownum += 1

        self.run_choice.currentIndexChanged.connect(self.sync_status)
        self.sync_status()

    def sync_status(self):
        self.listctrls['weights']['combo'].setEnabled(self.all_runs_combined)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_checkbox_args('spatial_colors'))
        dat.update(self.get_checkbox_args('gfp'))
        dat.update(self.get_listctrl_args('weights'))

        weights = {}

        for map_name in self.floats_condmap.keys():
            condition = self.floats_condmap[map_name]

            weights[condition] = float(self.get_float_args(map_name)[map_name])

        dat.update({'comp_weights': weights})

        return dat


class SingleChanOpts(RunCondSelect, ChanCheckMixin, CheckBoxMixin, ListCtrlMixin, MinMaxMixin, FloatTxtMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        chan_names = kwargs.get('chan_names', list())

        # Pre-selected channels
        chan_sel = kwargs.get('chan_sel', list())

        # Add a channel box
        self.add_chancheck_ctrls('picks', 'Channel', 0, chan_names, chan_sel)

        # Time-series Plotting options
        self.add_checkbox_ctrls('spatial_colors', 'Spatial Colours', 1)
        self.add_minmax_ctrls('y', 'Y-axis', 2)

        # Time-frequency choice
        label = QLabel('Time-frequency analysis')
        label.setStyleSheet("font-weight: bold;")
        self.customLayout.addWidget(label, 3, 0)

        self.add_listctrl_ctrls('tf', 'Time-Frequency', 4, ['None', 'Multitaper', 'Morlet', 'Stockwell'])

        # TF calculation options
        self.add_minmax_ctrls('tf_freq', 'Frequencies', 5, check=False, delta=True)
        self.add_float_ctrls('tf_ncycles', 'N_cycles divisor', 6)
        self.add_float_ctrls('tf_time_bandwidth', 'Time Bandwidth', 7)

        self.add_minmax_ctrls('tf_freq_stock', 'Frequencies', 8, check=False)
        self.add_float_ctrls('tf_width_stock', 'Width', 9)

        # TF Plotting options
        label = QLabel('Time-frequency plotting')
        label.setStyleSheet("font-weight: bold;")
        self.customLayout.addWidget(label, 10, 0)

        self.add_minmax_ctrls('tf_baseline', 'Baseline (s)', 11)
        self.add_checkbox_ctrls('tf_dB', 'dB', 12)
        self.add_minmax_ctrls('tf_v', 'vmin/max', 13,
                              validator_mode=QDoubleValidator.ScientificNotation)

        self.listctrls['tf']['combo'].currentIndexChanged.connect(self.sync_status)
        self.sync_status()

    def sync_status(self):
        val = self.listctrls['tf']['combo'].currentText()

        # Assume plotting unless told otherwise
        enable_plotting = True

        if val == 'None':
            # Disable everything
            self.set_minmax_enabled('tf_freq', False)
            self.set_float_enabled('tf_ncycles', False)
            self.set_float_enabled('tf_time_bandwidth', False)

            self.set_minmax_enabled('tf_freq_stock', False)
            self.set_float_enabled('tf_width_stock', False)

            enable_plotting = False
        elif val == 'Multitaper':
            # Enable freqs, ncycles and time-bandwidth
            self.set_minmax_enabled('tf_freq', True)
            self.set_float_enabled('tf_ncycles', True)
            self.set_float_enabled('tf_time_bandwidth', True)

            self.set_minmax_enabled('tf_freq_stock', False)
            self.set_float_enabled('tf_width_stock', False)
        elif val == 'Morlet':
            # Enable freqs and ncycles
            self.set_minmax_enabled('tf_freq', True)
            self.set_float_enabled('tf_ncycles', True)
            self.set_float_enabled('tf_time_bandwidth', False)

            self.set_minmax_enabled('tf_freq_stock', False)
            self.set_float_enabled('tf_width_stock', False)
        else:
            # Enable stockwell options
            self.set_minmax_enabled('tf_freq', False)
            self.set_float_enabled('tf_ncycles', False)
            self.set_float_enabled('tf_time_bandwidth', False)

            self.set_minmax_enabled('tf_freq_stock', True)
            self.set_float_enabled('tf_width_stock', True)

        # And plotting
        self.set_minmax_enabled('tf_baseline', enable_plotting)
        self.set_checkbox_enabled('tf_dB', enable_plotting)
        self.set_minmax_enabled('tf_v', enable_plotting)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_chancheck_args('picks'))
        dat.update(self.get_checkbox_args('spatial_colors'))
        meg = self.get_minmax_args('y', 'ylim')

        # TODO: Handle mag, grad, eeg differently later
        if len(meg) > 0:
            dat['ylim'] = {}
            dat['ylim']['mag'] = meg['ylim']
            dat['ylim']['grad'] = meg['ylim']

        tf = {}

        # Update the TF options
        mode = self.listctrls['tf']['combo'].currentText()

        tf['mode'] = mode

        if mode == 'Multitaper':
            tf['analysis'] = {}
            tf['analysis']['freqs'] = self.get_minmax_args('tf_freq', 'min', 'max', 'delta')
            tf['analysis']['n_cycles'] = self.get_float_args('tf_ncycles')['tf_ncycles']
            tf['analysis']['time_bandwidth'] = self.get_float_args('tf_time_bandwidth')['tf_time_bandwidth']

        elif mode == 'Morlet':
            tf['analysis'] = {}
            tf['analysis']['freqs'] = self.get_minmax_args('tf_freq', 'min', 'max', 'delta')
            tf['analysis']['n_cycles'] = self.get_float_args('tf_ncycles')['tf_ncycles']
        elif mode == 'Stockwell':
            tf['analysis'] = {}
            tf['analysis']['freqs'] = self.get_minmax_args('tf_freq_stock', 'min', 'max')
            tf['analysis']['width'] = self.get_float_args('tf_width_stock')['tf_width_stock']

        if mode != "None":
            # Get plotting args
            # And plotting
            tf['plot'] = {}
            tf['plot'].update(self.get_minmax_args('tf_baseline', 'baseline'))
            tf['plot']['dB'] = self.get_checkbox_args('tf_dB')['tf_dB']
            tf['plot'].update(self.get_minmax_args('tf_v', 'vmin', 'vmax'))

        dat['tf'] = tf

        return dat


class MVPAPlotOpts(RunCondSelect, CheckBoxMixin, FloatTxtMixin):
    def __init__(self, *args, **kwargs):
        RunCondSelect.__init__(self, *args, **kwargs)

        # Add custom options
        self.add_checkbox_ctrls('show_chance', 'Show Chance Line', 0, default=True)
        self.add_float_ctrls('ci', 'CI (0.0 - 1.0)', 1, min=0, max=1.0, default=0.95)
        self.add_float_ctrls('alpha', 'CI Transparency ', 3, min=0.0, max=1.0, default=0.4)

    def get_extra_args(self):
        dat = {}

        dat.update(self.get_checkbox_args('show_chance'))
        dat.update(self.get_float_args('ci'))
        dat.update(self.get_float_args('alpha'))

        return dat
