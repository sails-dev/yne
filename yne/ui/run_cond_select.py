#!/usr/bin/python3

from os.path import join

from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from yne import WIDGET_PATH


class RunCondSelect(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'runCondSelect.ui'), self)

        self.runs = kwargs.get('runs', list())
        self.conditions = kwargs.get('conditions', list())

        self.all_run = kwargs.get('all_run', True)
        self.all_run_combined = kwargs.get('all_run_combined', False)
        self.all_run_individual = kwargs.get('all_run_individual', False)

        self.show_cond = kwargs.get('show_cond', True)
        self.all_cond = kwargs.get('all_cond', True)
        self.all_cond_combined = kwargs.get('all_cond_combined', False)
        self.all_cond_individual = kwargs.get('all_cond_individual', False)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.sync_info()

    def sync_info(self):
        # Populate runs
        runs_to_show = []

        if len(self.runs) < 2:
            runs_to_show = self.runs
        else:
            if self.all_run:
                runs_to_show.append('Group')

            if self.all_run_combined:
                runs_to_show.append('Group: Combined')

            if self.all_run_individual:
                runs_to_show.append('Group: Per-Run')

            runs_to_show.extend(self.runs)

        self.run_choice.clear()
        self.run_choice.addItems(runs_to_show)

        # Populate conditions
        if not self.show_cond:
            self.condition_choice.setVisible(False)
            self.conditionLabel.setVisible(False)

        conds_to_show = []

        if len(self.conditions) < 2:
            conds_to_show = self.runs
        else:
            if self.all_cond:
                conds_to_show.append('All Conditions')

            if self.all_cond_combined:
                conds_to_show.append('All Conditions: Combined')

            if self.all_cond_individual:
                conds_to_show.append('All Conditions: Per-Condition')

            conds_to_show.extend(self.conditions)

        self.condition_choice.clear()
        self.condition_choice.addItems(conds_to_show)

    @property
    def run_name(self):
        return self.run_choice.currentText()

    @property
    def all_runs(self):
        return self.run_choice.currentText() == 'Group'

    @property
    def all_runs_combined(self):
        return self.run_choice.currentText() == 'Group: Combined'

    @property
    def all_runs_individual(self):
        return self.run_choice.currentText() == 'Group: Per-Run'

    @property
    def runs_list(self):
        if self.all_runs_combined:
            runs = None
        elif self.all_runs_individual:
            runs = self.runs
        else:
            runs = [self.run_name]

        return runs

    @property
    def condition_name(self):
        return self.condition_choice.currentText()

    @property
    def all_conditions(self):
        return self.condition_choice.currentText() == 'All Conditions'

    @property
    def all_conditions_combined(self):
        return self.condition_choice.currentText() == 'All Conditions: Combined'

    @property
    def all_conditions_individual(self):
        return self.condition_choice.currentText() == 'All Conditions: Per-Condition'

    @property
    def condition_list(self):
        if self.all_conditions_combined:
            conds = None
        elif self.all_conditions_individual:
            conds = self.conditions
        else:
            conds = [self.condition_name]

        return conds

    def get_extra_args(self):
        # Should be implemented by subclasses to deal with extra fields
        return {}
