#!/usr/bin/python3

from os.path import join

from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from yne import (WIDGET_PATH, FilterChain, BandFilter)


class BandPassUI(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self, *args, **kwargs)

        loadUi(join(WIDGET_PATH, 'bandPass.ui'), self)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    @property
    def low_freq(self):
        return self.low_freq_spin.value()

    @property
    def high_freq(self):
        return self.high_freq_spin.value()

    @property
    def description(self):
        lf = self.low_freq
        hf = self.high_freq

        if (lf == 0 and hf == 0) or (lf == hf):
            return 'No valid filter'
        elif lf == 0:
            return f'Low-Pass 0-{hf}Hz'
        elif hf == 0:
            return f'High-Pass {lf}-Hz'
        elif lf < hf:
            return f'Band-Pass {lf}-{hf}Hz'

        return f'Band-Stop {hf}-{lf}Hz'

    def filterchain(self):
        filterchain = FilterChain()

        low_freq = self.low_freq if self.low_freq != 0 else None
        high_freq = self.high_freq if self.high_freq != 0 else None

        filterchain.add_filter(BandFilter(low_freq=low_freq, high_freq=high_freq))

        return filterchain
