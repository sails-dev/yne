#!/usr/bin/env python3

# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from os.path import join, dirname

from PyQt5 import QtWidgets
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMessageBox

from yne import Study, WIDGET_PATH


__all__ = []


class StudyDefWin(QtWidgets.QMainWindow):
    def __init__(self, fname=None, parent=None):
        super(StudyDefWin, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'studyDefWin_ui.ui'), self)

        self.LoadStudy(fname)

        self.action_Open.triggered.connect(self.openYAMLFile)
        self.action_Save.triggered.connect(self.save)
        self.actionSave_As.triggered.connect(self.saveAs)
        self.action_Quit.triggered.connect(self.close)
        self.action_New.triggered.connect(self.new)

        self.cleanDescendants = []
        self.cleanEditDescendants = []

        self.InitDescendants()
        self.SetupDescendants()

        self.clean = True

        self.SetWindowTitle()

    def SetWindowTitle(self):
        if self.clean:
            if self.fname is None:
                windowtitle = "YNE Study Definer: New Study"
            else:
                windowtitle = "YNE Study Definer: " + self.fname
        else:
            if self.fname is None:
                windowtitle = "YNE Study Definer: New Study *"
            else:
                windowtitle = "YNE Study Definer: " + self.fname + " *"
        self.setWindowTitle(windowtitle)

    def InitDescendants(self):
        self.studyParams.initialise()
        self.conditionParams.initialise()
        self.windowParams.initialise()
        self.dataSetParams.initialise()
        self.chansetParams.initialise()
        self.filterchainParams.initialise()

    def SetupDescendants(self):
        self.studyParams.setup(self.study)
        self.conditionParams.setup(self.study)
        self.windowParams.setup(self.study)
        self.dataSetParams.setup(self.study)
        self.chansetParams.setup(self.study)
        self.filterchainParams.setup(self.study)

    def RegisterCleanliness(self, descendant):
        self.cleanDescendants.append(descendant)

    def RegisterCleanEdits(self, descendant):
        self.cleanEditDescendants.append(descendant)

    def CheckDescendantEdits(self):
        for j in self.cleanEditDescendants:
            while not j.updateclean:
                buttons = QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel

                resp = QMessageBox.question(self, "Unsubmitted changes!",
                                            "You have made changes to this study that have not been updated."
                                            "\nDo you wish to update these changes before continuing?",
                                            buttons)

                if resp == QMessageBox.Cancel:
                    return False

                if resp == QMessageBox.No:
                    j.clearData(force=True)

                elif resp == QMessageBox.Yes:
                    j.updateData()
        return True

    def UpdateCleanliness(self):
        for x in self.cleanDescendants:
            self.clean = self.clean and x.clean
        self.SetWindowTitle()

    def openYAMLFile(self):
        self.openfile()

    def LoadStudy(self, fname):
        if fname is None:
            study = Study()
        else:
            try:
                study = Study.from_yaml_file(fname)
            except Exception as e:
                QMessageBox.warning(None, "Invalid YAML File",
                                    "Cannot load yaml file %s:\n%s" % (fname, e))
                study = Study()
                fname = None

        self.study = study
        self.fname = fname

    def new(self):
        # This is really ugly, but I can't seem to find another way of catching
        # a mid-edit change in the tableWidgets. Otherwise the close event
        # takes presendence and you loose the last change (even though the code
        # knows it's been changed, by the time it notices it's too late)
        self.studyParams.PreTrigEdit.setFocus()
        self.studyParams.StudyNameEdit.setFocus()

        if not self.CheckDescendantEdits():
            return

        if not self.clean:
            ans = QMessageBox.question(self, "Unsaved changes!",
                                       "You have made unsaved changes.\n"
                                       "Do you wish to save this study YAML before opening another?",
                                       QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)

            if ans == QMessageBox.Yes:
                self.save()
                if not self.clean:
                    return
            elif ans == QMessageBox.Cancel:
                return

        self.study = Study()
        self.fname = None
        self.SetupDescendants()
        self.clean = True
        self.SetWindowTitle()

    def openfile(self, extension=None):
        # This is really ugly, but I can't seem to find another way of catching
        # a mid-edit change in the tableWidgets. Otherwise the close event
        # takes presendence and you loose the last change (even though the code
        # knows it's been changed, by the time it notices it's too late)
        self.studyParams.PreTrigEdit.setFocus()
        self.studyParams.StudyNameEdit.setFocus()

        if not self.CheckDescendantEdits():
            return

        if not self.clean:
            ans = QMessageBox.question(self, "Unsaved changes!",
                                       "You have made unsaved changes.\n"
                                       "Do you wish to save this study YAML before opening another?",
                                       QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)

            if ans == QMessageBox.Yes:
                self.save()
                if not self.clean:
                    return
            elif ans == QMessageBox.Cancel:
                return

        if self.fname is not None:
            startfile = self.fname
        else:
            startfile = '.'
        if extension is None:
            extstr = '*.yaml'
        else:
            extstr = '*.' + extension
        fname, ffilter = QtWidgets.QFileDialog.getOpenFileName(self, 'Open Study File.', startfile, extstr)

        if fname == '':
            return

        try:
            self.LoadStudy(fname)
            self.SetupDescendants()
            self.clean = True

        except Exception as e:
            QMessageBox.warning(self, "Study Read Error",
                                "This study file is invalid and could not be loaded:\n%s" % e)

        self.SetWindowTitle()

    def validateStudy(self):
        if self.study.name is None or self.study.name == '' or \
           self.study.storage_dir is None or self.study.storage_dir == '':
            QMessageBox.warning(self, "Incomplete Study information",
                                "Study must have a name and storage directory\n"
                                "Please add this information before saving.")
            return False

        # TODO: Re-add sanity checks in Study object
        return True

    def save(self):
        if not self.CheckDescendantEdits():
            return

        if not self.validateStudy():
            return
        if self.fname is None:
            self.saveAs()
            return

        self.study.save_yaml_file(self.fname)
        for x in self.cleanDescendants:
            x.clean = True
        self.clean = True
        self.SetWindowTitle()

    def saveAs(self):
        if not self.CheckDescendantEdits():
            return

        if not self.validateStudy():
            return
        if self.fname is not None:
            startdir = dirname(self.fname)
        else:
            startdir = '.'
        fname, ffilter = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Study YAML File.', startdir, '*.yaml')
        if fname == '':
            return
        if not fname.endswith('.yaml'):
            fname = fname + '.yaml'
        self.fname = fname
        self.study.save_yaml_file(self.fname)
        for x in self.cleanDescendants:
            x.clean = True
        self.clean = True
        self.SetWindowTitle()

    def closeEvent(self, event):
        # This is really ugly, but I can't seem to find another way of catching
        # a mid-edit change in the tableWidgets. Otherwise the close event
        # takes presendence and you loose the last change (even though the code
        # knows it's been changed, by the time it notices it's too late)
        self.studyParams.PreTrigEdit.setFocus()
        self.studyParams.StudyNameEdit.setFocus()
        if not self.CheckDescendantEdits():
            event.ignore()
            return

        if self.clean:
            event.accept()
        else:
            ans = QMessageBox.question(self, "Unsaved changes!",
                                       "You have made unsaved changes.\n"
                                       "Do you wish to save this study YAML before quitting?",
                                       QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)

            if ans == QMessageBox.Yes:
                self.save()
                if self.clean:
                    event.accept()
                    return

            elif ans == QMessageBox.No:
                event.accept()
                return

            event.ignore()


__all__.append('StudyDefWin')
