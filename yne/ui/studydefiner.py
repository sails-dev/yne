"""Study Definer GUI code"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from copy import deepcopy
from os.path import join

import numpy as np

from PyQt5 import QtWidgets

from PyQt5.QtCore import Qt, QVariant, QAbstractTableModel, QModelIndex, QTimer
from PyQt5.QtWidgets import (QMessageBox, QTableWidgetItem, QFileDialog,
                             QTextEdit, QLineEdit)
from PyQt5.uic import loadUi

from yne import (ChannelGroup, ChannelSet, CHANNEL_MEG, Condition,
                 DataRun, TimeWindow, guess_datatype, get_meg_reader_class,
                 datafilename_to_outputname, FilterChain, BandFilter,
                 Study, WIDGET_PATH)

from yne.options import YneOptions


def strlist2str(lst):
    """
    Parse a list of strings into a comma seperatedstring dealing with
    integers

    :param lst: List of strings (e.g. channel names)

    :rtype: str
    :return: Comma seperated string or NULL string if list is empty
    """

    if isinstance(lst, list):
        if len(lst) == 0:
            return ''
        else:
            return ', '.join(['%s' % x for x in lst])
    else:
        return '%s' % lst


def str2strlist(strng):
    """
    Parse a comma seperated string into a list of strings.

    :param strng: Comma separated string

    :rtype: list
    :return: List of strings (e.g. channel names)
    """
    if (strng is None) or (strng.strip() == ''):
        return []
    else:
        return [str(x).strip() for x in strng.split(',')]


def codes2str(lst):
    """Parse a list of codes into a string dealing with integers"""
    if isinstance(lst, list):
        if len(lst) == 0:
            return 'ALL'
        else:
            return ', '.join(['%d' % x for x in lst])
    else:
        return '%d' % lst


def str2codes(strng):
    """Parse a string into a list of codes"""
    if (strng is None) or (strng.strip() == 'ALL') or (strng.strip() == ''):
        return []
    else:
        if strng.count(',') > 0:
            return [int(x) for x in strng.split(',')]
        else:
            return [int(x) for x in strng.split()]


def freq2str(freq):
    """Convert a frequency into a usable string"""
    if freq is None:
        return 'N/A'
    else:
        return '%d' % freq


def str2freq(strng):
    """Convert a frequency string into an int / None"""
    if (strng.strip() == 'N/A') or (strng.strip() == 'None') or (strng.strip() == ''):
        return None
    else:
        return int(strng)


def triglst2str(triglst):
    """Convert a (long) list of trigger codes into a human readable string"""
    if len(triglst) == 0:
        return ''

    if len(triglst) == 1:
        return str(triglst[0])

    triglst = sorted(triglst)
    trigary = np.array(triglst)
    trigdiff = np.diff(trigary)
    curdiff = trigdiff[0]
    count = 0
    strlst = ''

    for j in range(trigdiff.shape[0]):
        if trigdiff[j] == curdiff:
            count += 1
        else:
            if count > 5:
                strlst += str(triglst[j-count])
                strlst += ':'
                if curdiff > 1:
                    strlst += str(curdiff)
                    strlst += ':'
                strlst += str(triglst[j])
                strlst += ', '
                count = 0
                if (j+1) < trigdiff.shape[0]:
                    curdiff = trigdiff[j+1]
            else:
                for k in range(count):
                    strlst += str(triglst[j-count+k])
                    strlst += ', '
                count = 1
                curdiff = trigdiff[j]

    j += 1
    if count > 5:
        strlst += str(triglst[j-count])
        strlst += ':'
        if curdiff > 1:
            strlst += str(curdiff)
            strlst += ':'
        strlst += str(triglst[j])
    else:
        for k in range(count+1):
            strlst += str(triglst[j-count+k])
            if k < count:
                strlst += ', '

    return strlst


def str2triglst(trigstr):
    """Convert a string of trigger codes into a list"""
    if trigstr == '':
        return []
    splitstr = trigstr.split(',')
    triglist = []
    for j in splitstr:
        strrange = [int(x) for x in j.split(':')]
        if len(strrange) == 1:
            triglist.append(strrange[0])
        elif len(strrange) == 2:
            for k in range(strrange[0], strrange[1]+1):
                triglist.append(k)
        else:
            for k in range(strrange[0], strrange[2]+strrange[1], strrange[1]):
                triglist.append(k)

    return triglist


class StudyParams(QtWidgets.QGroupBox):
    def __init__(self, parent=None):
        super(StudyParams, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'studyParams_ui.ui'), self)

        self.show()

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("studyParams", "Study Parameters"))

        self.StudyNameEdit.editingFinished.connect(self.nameChanged)
        self.PreTrigEdit.editingFinished.connect(self.preTrigChanged)
        self.EpDurEdit.editingFinished.connect(self.epDurChanged)
        self.OutPathEdit.editingFinished.connect(self.pathChanged)
        self.BrowseButton.clicked.connect(self.browsePath)

        self.window().RegisterCleanliness(self)

    def setup(self, study):
        self.study = study

        if self.study.name is not None:
            self.StudyNameEdit.setText(self.study.name)
        else:
            self.StudyNameEdit.setText('')

        if self.study.pretrig_dur is not None:
            self.PreTrigEdit.setText(str(self.study.pretrig_dur))
        else:
            self.PreTrigEdit.setText('')

        if self.study.epoch_dur is not None:
            self.EpDurEdit.setText(str(self.study.epoch_dur))
        else:
            self.EpDurEdit.setText('')

        if self.study.storage_dir is not None:
            self.OutPathEdit.setText(self.study.storage_dir)
        else:
            self.OutPathEdit.setText('')

        self.clean = True

    def nameChanged(self):
        if (self.study.name == str(self.StudyNameEdit.text())) or \
           (self.study.name is None and self.StudyNameEdit.text() == ''):
            return

        if self.StudyNameEdit.text() == '':
            QMessageBox.warning(self, "Invalid Study Name", "Study must have a name.")

        self.study.name = str(self.StudyNameEdit.text())
        self.clean = False
        self.window().UpdateCleanliness()

    def preTrigChanged(self):
        pretrig = str(self.PreTrigEdit.text())

        if pretrig == '':
            pretrig = None
        else:
            try:
                pretrig = int(self.PreTrigEdit.text())
            except ValueError as e:
                QMessageBox.warning(self, "Invalid Pre Trigger",
                                    "Pre trigger must be an integer number of ms:\n%s" % e)

                if self.study.pretrig_dur is not None:
                    self.PreTrigEdit.setText(str(self.study.pretrig_dur))
                else:
                    self.PreTrigEdit.setText('')

                return

        if self.study.pretrig_dur == pretrig:
            return

        self.study.pretrig_dur = pretrig
        self.clean = False
        self.window().UpdateCleanliness()

    def epDurChanged(self):
        epdur = str(self.EpDurEdit.text())

        if epdur == '':
            epdur = None
        else:
            try:
                epdur = int(self.EpDurEdit.text())
            except ValueError as e:
                QMessageBox.warning(self, "Invalid Epoch Duration",
                                    "Epoch duration must be an integer number of ms:\n%s" % e)

                if self.study.epoch_dur is not None:
                    self.EpDurEdit.setText(str(self.study.epoch_dur))
                else:
                    self.EpDurEdit.setText('')

                return

        if self.study.epoch_dur == epdur:
            return

        self.study.epoch_dur = epdur
        self.clean = False
        self.window().UpdateCleanliness()

    def pathChanged(self):
        if (self.study.storage_dir == str(self.OutPathEdit.text())) or \
           (self.study.storage_dir is None and self.OutPathEdit.text() == ''):
            return

        if self.OutPathEdit.text() == '':
            QMessageBox.warning(self, "Invalid Output Directory",
                                "Study must have an output directory.")

        self.study.storage_dir = str(self.OutPathEdit.text())
        self.clean = False
        self.window().UpdateCleanliness()

    def browsePath(self):
        if self.study.storage_dir is None:
            startdir = '.'
        else:
            startdir = self.study.storage_dir

        path = QFileDialog.getExistingDirectory(self, 'Output Directory', startdir)

        if path == '' or path == self.study.storage_dir:
            return

        self.study.storage_dir = path
        self.OutPathEdit.setText(self.study.storage_dir)
        self.clean = False
        self.window().UpdateCleanliness()


class DataSets(QtWidgets.QGroupBox):
    RUN, FNAME, TAGS, COREG, REJ_FILE, SLICE_FILE, BAD_CHANNELS = range(7)

    def __init__(self, parent=None):
        super(DataSets, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'dataSets_ui.ui'), self)

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("dataSets", "Data Sets"))

        self.currun = None
        self.adding = False
        self.returningToRow = False

        self.DataTableWidget.currentItemChanged.connect(self.tableItemClicked)

        self.DataTableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.RunFileEdit.editingFinished.connect(self.runFileChanged)
        self.RunFileEdit.textEdited.connect(self.runFileEdited)

        self.BrowseRunFileButton.clicked.connect(self.browseRunFile)

        self.TagsEdit.editingFinished.connect(self.tagsChanged)
        self.TagsEdit.textEdited.connect(self.tagsEdited)

        self.CoregEdit.editingFinished.connect(self.coregChanged)
        self.CoregEdit.textEdited.connect(self.coregEdited)

        self.BrowseCoregButton.clicked.connect(self.browseCoreg)

        self.SliceRejEdit.editingFinished.connect(self.sliceRejChanged)
        self.SliceRejEdit.textEdited.connect(self.sliceRejEdited)

        self.BrowseSliceRejButton.clicked.connect(self.browseSliceRej)

        self.SliceEdit.editingFinished.connect(self.sliceChanged)
        self.SliceEdit.textEdited.connect(self.sliceEdited)

        self.BrowseSliceButton.clicked.connect(self.browseSlice)

        # TODO: Should populate this from known types in readers.py
        self.DataTypeComboBox.currentIndexChanged.connect(self.dataTypeChanged)
        self.DataTypeComboBox.addItem('BTI')
        self.DataTypeComboBox.addItem('CTF')
        self.DataTypeComboBox.addItem('FIF')
        self.DataTypeComboBox.setEditable(False)

        self.BadChannelsEdit.editingFinished.connect(self.badChannelsChanged)
        self.BadChannelsEdit.textEdited.connect(self.badChannelsEdited)

        self.AddDataButton.clicked.connect(self.addData)
        self.RemDataButton.clicked.connect(self.removeData)
        self.UpdateDataButton.clicked.connect(self.updateData)
        self.ClearDataButton.clicked.connect(self.clearData)

        self.addData_timer = QTimer()
        self.addData_timer.setSingleShot(True)

        self.table_timer = QTimer()
        self.table_timer.setSingleShot(True)

        self.addData_timer.timeout.connect(self.addDataTrig)
        self.table_timer.timeout.connect(self.tableItemClickedTrig)

        self.window().RegisterCleanliness(self)
        self.window().RegisterCleanEdits(self)

    def setup(self, study):
        self.study = study

        self.UpdateDataButton.setEnabled(False)

        self.populateDataSets()

        self.clean = True
        self.updateclean = True
        self.currun = None

    def populateDataSets(self, runname=None):
        selected = None
        self.DataTableWidget.clear()
        self.DataTableWidget.setSortingEnabled(False)
        self.DataTableWidget.setRowCount(len(self.study.runs))

        headers = ['  Run   ', '  File   ', '  Tags   ', '  MRI   ',
                   '  Slice Rejection File   ', '  Slice File   ',
                   '  Bad Channels   ']

        self.DataTableWidget.setColumnCount(len(headers))
        self.DataTableWidget.setHorizontalHeaderLabels(headers)

        for row, j in enumerate(self.study.runs):
            run = self.study.get_run(j)
            tags = strlist2str(run.tags)

            item = QTableWidgetItem(j)
            item.setData(Qt.UserRole, QVariant(j))

            if runname is not None and runname == j:
                selected = item

            self.DataTableWidget.setItem(row, self.RUN, item)

            self.DataTableWidget.setItem(row, self.FNAME,
                                         QTableWidgetItem(run.filename))

            self.DataTableWidget.setItem(row, self.TAGS,
                                         QTableWidgetItem(tags))

            self.DataTableWidget.setItem(row, self.COREG,
                                         QTableWidgetItem(run.extra_data.get('mri_structural_dir', '')))

            self.DataTableWidget.setItem(row, self.REJ_FILE,
                                         QTableWidgetItem(run.get_value('slice_rejection_file', '')))

            self.DataTableWidget.setItem(row, self.SLICE_FILE,
                                         QTableWidgetItem(run.get_value('slice_file', '')))

            self.DataTableWidget.setItem(row, self.BAD_CHANNELS,
                                         QTableWidgetItem(strlist2str(run.get_value('bad_channels', ''))))

        self.DataTableWidget.setSortingEnabled(True)
        self.DataTableWidget.resizeColumnsToContents()

        if selected is not None:
            selected.setSelected(True)
            self.DataTableWidget.setCurrentItem(selected)
            self.populateRun(runname)
            self.highlightRun(runname)
        else:
            self.populateRun()

    def highlightRun(self, runname):
        itemslist = self.DataTableWidget.findItems(runname, Qt.MatchExactly)

        if len(itemslist) != 1:
            QMessageBox.warning(self, "Data Run Name Error",
                                "An internal error has caused a problem with automated run naming.\n"
                                "This shouldn't happen, please report this fault, along with your YAML file.")
            return

        curitem = itemslist[0]

        self.DataTableWidget.setCurrentItem(curitem)

    def tableItemClicked(self, item, olditem=None):
        self.table_timer_item = item
        self.table_timer_olditem = olditem
        self.table_timer.start(50)

    def tableItemClickedTrig(self):
        item = self.table_timer_item

        if self.adding or self.returningToRow:
            return

        if item is None:
            return

        row = self.DataTableWidget.row(item)
        runname = str(self.DataTableWidget.item(row, self.RUN).text())

        if self.currun is not None and self.currun.name == runname:
            return

        while not self.updateclean:
            if self.currun is None:
                currunname = 'new run'
            else:
                currunname = self.currun.name

            resp = QMessageBox.question(self, "Run not updated!",
                                        "Currently selected run (%s) has not been updated. "
                                        "Do you wish to update current run?" % currunname,
                                        QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)

            if resp == QMessageBox.Cancel:
                if self.currun is None:
                    self.DataTableWidget.setCurrentItem(None)
                else:
                    self.returningToRow = True
                    self.highlightRun(currunname)
                    self.returningToRow = False

                return

            if resp == QMessageBox.No:
                self.updateclean = True
            else:
                self.updateData()

        self.populateRun(runname)
        self.highlightRun(runname)

    def populateRun(self, runname=None):
        self.updateclean = True

        if runname is None:
            self.RunFileEdit.setText('')
            self.TagsEdit.setText('')
            self.CoregEdit.setText('')
            self.SliceRejEdit.setText('')
            self.SliceEdit.setText('')

            self.BadChannelsEdit.setText('')
            self.UpdateDataButton.setEnabled(False)
            self.ClearDataButton.setEnabled(False)
            self.RemDataButton.setEnabled(False)
            self.currun = None

            return

        run = self.study.get_run(runname)
        self.currun = run
        tags = strlist2str(run.tags)

        self.RunFileEdit.setText(run.filename)
        self.TagsEdit.setText(tags)
        self.CoregEdit.setText(run.extra_data.get('mri_structural_dir', ''))
        self.SliceRejEdit.setText(run.extra_data.get('slice_rejection_file', ''))
        self.SliceEdit.setText(run.extra_data.get('slice_file', ''))

        idx = self.DataTypeComboBox.findText(run.datatype)

        if idx == -1:
            self.DataTypeComboBox.insertItem(self.DataTypeComboBox.count(), str(run.datatype))
            idx = self.DataTypeComboBox.findText(run.datatype)

        self.DataTypeComboBox.setCurrentIndex(idx)
        self.BadChannelsEdit.setText(strlist2str(run.extra_data.get('bad_channels', '')))
        self.UpdateDataButton.setEnabled(not self.updateclean)
        self.ClearDataButton.setEnabled(True)
        self.RemDataButton.setEnabled(True)

    def browseRunFile(self):
        startfile = str(self.RunFileEdit.text())

        if startfile == '':
            startfile = YneOptions().meg_storage_dir

        fname, ffilter = QFileDialog.getOpenFileName(self,
                                                     'Select Data File.',
                                                     startfile)

        if fname == '':
            return

        self.RunFileEdit.setText(fname)
        self.runFileChanged()

    def browseCoreg(self):
        startdir = str(self.CoregEdit.text())

        if startdir == '':
            startdir = YneOptions().mri_storage_dir

        dname = QFileDialog.getExistingDirectory(self, 'Select MRI Structural Directory.', startdir)

        if dname == '':
            return

        self.CoregEdit.setText(dname)
        self.coregChanged()

    def browseSliceRej(self):
        startfile = str(self.SliceRejEdit.text())

        fname, ffilter = QFileDialog.getOpenFileName(self, 'Select Slice Rejection File.', startfile)

        if fname == '':
            return

        self.SliceRejEdit.setText(fname)
        self.sliceRejChanged()

    def browseSlice(self):
        startfile = str(self.SliceEdit.text())

        fname, ffilter = QFileDialog.getOpenFileName(self, 'Select Slice File.', startfile)

        if fname == '':
            return

        self.SliceEdit.setText(fname)
        self.sliceChanged()

    def runFileChanged(self):
        if self.currun is not None and str(self.RunFileEdit.text()) == self.currun.filename:
            return

        # Try and guess the new data type
        dt = guess_datatype(str(self.RunFileEdit.text()))

        if dt is not None:
            idx = self.DataTypeComboBox.findText(dt)
            if idx != -1:
                self.DataTypeComboBox.setCurrentIndex(idx)

        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

        if str(self.RunFileEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)

    def runFileEdited(self, txt):
        self.runFileChanged()

    def tagsChanged(self):
        if self.currun is not None and str(self.TagsEdit.text()) == strlist2str(self.currun.tags):
            return

        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

        if str(self.TagsEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)

    def tagsEdited(self, txt):
        self.tagsChanged()

    def coregChanged(self):
        if self.currun is None:
            return

        mri_struct = self.currun.extra_data.get('mri_structural_dir', '')

        if self.currun is not None and str(self.CoregEdit.text()) == mri_struct:
            return

        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

        if str(self.CoregEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)

    def coregEdited(self, txt):
        self.coregChanged()

    def sliceRejChanged(self):
        if self.currun is None:
            return

        srf = self.currun.extra_data.get('slice_rejection_file', '')

        if self.currun is not None and str(self.SliceRejEdit.text()) == srf:
            return

        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

        if str(self.SliceRejEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)
            self.SliceEdit.setText('')

    def sliceRejEdited(self, txt):
        self.sliceRejChanged()

    def sliceChanged(self):
        if self.currun is not None and str(self.SliceEdit.text()) == self.currun.extra_data.get('slice_file', ''):
            return
        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)
        if str(self.SliceEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)
            self.SliceRejEdit.setText('')

    def sliceEdited(self, txt):
        self.sliceChanged()

    def dataTypeChanged(self, idx=0):
        if self.currun is not None and str(self.DataTypeComboBox.currentText()) == self.currun.datatype:
            return
        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

    def badChannelsChanged(self):
        if self.currun is None:
            return

        bad_chans = strlist2str(self.currun.extra_data.get('bad_channels', ''))

        if self.currun is not None and str(self.BadChannelsEdit.text()) == bad_chans:
            return

        self.updateclean = False
        self.UpdateDataButton.setEnabled(self.currun is not None)

        if str(self.BadChannelsEdit.text()) != '':
            self.ClearDataButton.setEnabled(True)

    def badChannelsEdited(self, txt):
        self.badChannelsChanged()

    def badChannelsCheck(self):
        # Try and work out our channels based on our datatype
        RejChans = str2strlist(str(self.BadChannelsEdit.text()))

        datatype = str(self.DataTypeComboBox.currentText())

        try:
            get_meg_reader_class(datatype)
        except RuntimeError:
            QMessageBox.warning(self, "Data Type Error", "Data Type is not valid")
            return False

        # Construct a temporary Study and DataRun
        study = Study()

        study.pretrig_dur = 0
        study.epoch_dur = 10
        study.storage_dir = ''

        datarun = DataRun(name='tmp',
                          filename=str(self.RunFileEdit.text()),
                          datatype=datatype)

        try:
            reader = get_meg_reader_class(datatype)(datarun.filename)
        except Exception as e:
            if QMessageBox.question(self, "File Error",
                                    "Cannot read file: \n%s \nDo still wish to add/update dataset?" % e,
                                    QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes:
                return True
            else:
                return False

        ChanNames = [str(x) for x in reader.meg_chanset]

        MissingChans = []
        SuggestedChans = []

        for chan in RejChans:
            if ChanNames.count(chan) == 0:
                MissingChans.append(chan)

                # Turn 55 into MEG055 etc
                try:
                    sugname = f'MEG {int(chan):03d}'

                    if ChanNames.count(sugname) != 0:
                        SuggestedChans.append(sugname)
                except ValueError:
                    pass

        if len(MissingChans) > 0:
            MissingChans = sorted(MissingChans)
            SuggestedChans = sorted(SuggestedChans)

            messg = 'Channel(s) ' + strlist2str(MissingChans) + ' could not be found'

            if len(SuggestedChans) > 0:
                messg += ', closest matching channel(s) present: ' + strlist2str(SuggestedChans)

                if len(MissingChans) > len(SuggestedChans):
                    messg += '\n(NB, not all channels had a match.)'

                messg += '\n\nDo you wish to substitute and only reject found channels?'
            else:
                messg += '\n\nDo you remove missing channel(s) from bad channel list?'

            ans = QMessageBox.question(self, "Update Bad Channel List?", messg,
                                       QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
            if ans == QMessageBox.Yes:
                newchans = sorted(list(set(RejChans).difference(set(MissingChans)).union(set(SuggestedChans))))

                self.BadChannelsEdit.setText(strlist2str(newchans))

                return True

            if ans == QMessageBox.No:
                return True

            return False

        return True

    def addData(self):
        self.addData_timer.start(50)

    def addDataTrig(self):
        self.adding = True
        newrunFile = str(self.RunFileEdit.text())
        if newrunFile == '':
            QMessageBox.warning(self, "No Data File",
                                "All runs must have a data file. Please choose a data file before adding a data run.")
            return
        try:
            newrunNamebase = datafilename_to_outputname(newrunFile)
        except Exception:
            newrunNamebase = 'NewRun'

        newrunName = newrunNamebase
        count = 1
        while newrunName in self.study.runs:
            newrunName = newrunNamebase + '_' + str(count)
            count += 1

        newrunType = str(self.DataTypeComboBox.currentText())

        newRun = DataRun(newrunName, newrunFile, newrunType)
        for tag in str2strlist(str(self.TagsEdit.text())):
            newRun.add_tag(tag)

        newRun.extra_data['mri_structural_dir'] = str(self.CoregEdit.text())
        newrunSliceRej = str(self.SliceRejEdit.text())
        newrunSliceFile = str(self.SliceEdit.text())
        if newrunSliceRej != '':
            newRun.set_value('slice_rejection_file', newrunSliceRej)
        elif newrunSliceFile != '':
            newRun.set_value('slice_file', newrunSliceFile)

        if not self.badChannelsCheck():
            return
        newrunBadChans = str2strlist(str(self.BadChannelsEdit.text()))
        if len(newrunBadChans) > 0:
            newRun.set_value('bad_channels', newrunBadChans)

        self.study.run_definer.add_run(newRun)
        self.populateDataSets(newrunName)
        self.clean = False
        self.window().UpdateCleanliness()
        self.adding = False

    def removeData(self):
        if self.currun is None:
            QMessageBox.warning(self, "No run selected",
                                "Cannot remove run as none is selected.")
            return

        if QMessageBox.question(self, "Remove Data Run?",
                                "Are you sure you wish to delete data run %s?" % self.currun.name,
                                QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
            return
        self.study.run_definer.del_run(self.currun.name)
        self.populateDataSets()
        self.clean = False
        self.window().UpdateCleanliness()

    def updateData(self):
        newDataFile = str(self.RunFileEdit.text())
        newTags = str2strlist(str(self.TagsEdit.text()))
        newCoreg = str(self.CoregEdit.text())
        newSliceRej = str(self.SliceRejEdit.text())
        newSliceFile = str(self.SliceEdit.text())
        newType = str(self.DataTypeComboBox.currentText())
        newBadChans = str2strlist(str(self.BadChannelsEdit.text()))

        if not self.badChannelsCheck():
            return

        if newDataFile != self.currun.filename:
            if newDataFile == '':
                QMessageBox.warning(self, "No Data File",
                                    "All runs must have a data file. "
                                    "Please choose a data file before updating this data run.")
                return
            try:
                newrunNamebase = datafilename_to_outputname(newDataFile)
            except Exception:
                newrunNamebase = 'NewRun'

            self.study.run_definer.del_run(self.currun.name)
            newrunName = newrunNamebase
            count = 1
            while newrunName in self.study.runs:
                newrunName = newrunNamebase + '_' + str(count)
                count += 1

            self.currun.name = newrunName
            self.currun.filename = newDataFile
            self.study.run_definer.add_run(self.currun)

        newrunName = self.currun.name

        if newTags != self.currun.tags:
            self.currun.tags = []
            for tag in newTags:
                self.currun.add_tag(tag)

        if newCoreg != self.currun.extra_data.get('mri_structural_dir', ''):
            self.currun.extra_data['mri_structural_dir'] = newCoreg

        if newSliceRej != self.currun.get_value('slice_rejection_file', ''):
            if newSliceRej == '' and 'slice_rejection_file' in self.currun.extra_data:
                self.currun.del_value('slice_rejection_file')
            else:
                self.currun.set_value('slice_rejection_file', newSliceRej)

        if newSliceFile != self.currun.get_value('slice_file', ''):
            if newSliceFile == '' and 'slice_file' in self.currun.extra_data:
                self.currun.del_value('slice_file')
            else:
                self.currun.set_value('slice_file', newSliceFile)

        if newType != self.currun.datatype:
            self.currun.datatype = newType

        newBadChans = str2strlist(str(self.BadChannelsEdit.text()))

        if newBadChans != self.currun.get_value('bad_channels', ''):
            if len(newBadChans) > 0:
                self.currun.set_value('bad_channels', newBadChans)
            elif 'bad_channels' in self.currun.extra_data:
                self.currun.del_value('bad_channels')

        self.updateclean = True
        self.populateDataSets(newrunName)
        self.clean = False
        self.window().UpdateCleanliness()

    def clearData(self, force=False):
        if (not self.updateclean) and (not force):
            if self.currun is None:
                currunname = 'new run'
            else:
                currunname = self.currun.name
            if QMessageBox.question(self, "Run not updated!",
                                    f"Currently selected run ({currunname}) has not been updated. "
                                    "Are you sure you wish to clear changes?",
                                    QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
                return
        self.DataTableWidget.setCurrentItem(None)
        self.populateRun()


class ConditionParams(QtWidgets.QGroupBox):
    NAME, TRIG_CO, GRP_CO, RESP_CO = range(4)

    def __init__(self, parent=None):
        super(ConditionParams, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'conditionParams_ui.ui'), self)

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("conditionParams", "Conditions"))

        self.ConditionsTableWidget.itemChanged.connect(self.tableItemChanged)
        self.AddCondButton.clicked.connect(self.addCondition)
        self.RemCondButton.clicked.connect(self.removeCondition)

        self.window().RegisterCleanliness(self)

    def setup(self, study):
        self.study = study

        self.populateConditions()

        self.clean = True

    def addCondition(self):
        newCond = Condition()
        count = 2
        namebase = 'NewCondition'
        newCondName = namebase
        while newCondName in self.study.conditions:
            newCondName = namebase + str(count)
            count += 1
        newCond.name = newCondName
        self.study.condition_definer.add_condition(newCond)
        self.populateConditions(newCondName)
        self.ConditionsTableWidget.setFocus()
        self.ConditionsTableWidget.editItem(self.ConditionsTableWidget.currentItem())
        self.clean = False
        self.window().UpdateCleanliness()

    def removeCondition(self):
        cond = self.currentTableCondition()

        if cond is None:
            return

        cond = str(cond)

        if QMessageBox.question(self, "Remove Condition?",
                                "Are you sure you wish to delete condition %s?" % cond,
                                QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
            return

        self.study.condition_definer.del_condition(cond)
        self.populateConditions()
        self.clean = False
        self.window().UpdateCleanliness()

    def tableItemChanged(self, item):
        oldCondName = self.currentTableCondition()

        if oldCondName is None:
            return

        oldCondName = str(oldCondName)

        column = self.ConditionsTableWidget.currentColumn()

        if column == self.NAME:
            newName = item.text().strip()

            if newName == oldCondName:
                return

            if newName in self.study.conditions:
                if QMessageBox.question(self, "Condition - Overwrite?",
                                        "Condition name already exists, do you want to overwrite old condition?",
                                        QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes:

                    self.study.condition_definer.del_condition(newName)
                else:
                    return

            cond = self.study.condition_definer.get_condition(oldCondName)
            cond.name = newName

            self.study.condition_definer.add_condition(cond)
            self.study.condition_definer.del_condition(oldCondName)

            self.populateConditions()

        elif column == self.TRIG_CO:
            newtrig = item.text().strip()

            if newtrig == '':
                triglist = []
            else:
                try:
                    triglist = str2triglst(newtrig)
                except ValueError as e:
                    QMessageBox.warning(self, "Invalid Trigger Code",
                                        "Trigger Code must be a comma seperated list of integers or ranges "
                                        "of the form startpoint:(step=1):endpoint\n%s" % e)

                    self.populateConditions(selectedCondition=oldCondName)

                    return

            if sorted(triglist) == sorted(self.study.get_condition(oldCondName).get_trigger_list()):
                return

            self.study.condition_definer.get_condition(oldCondName).set_trigger_list(triglist)

        elif column == self.GRP_CO:
            newgrp = item.text().strip()

            if newgrp == '':
                grplist = []
            else:
                try:
                    grplist = str2triglst(newgrp)
                except ValueError as e:
                    QMessageBox.warning(self, "Invalid Group Code",
                                        "Group Code must be a comma seperated list of integers or ranges "
                                        "of the form startpoint:(step=1):endpoint\n%s" % e)

                    self.populateConditions(selectedCondition=oldCondName)

                    return

            if sorted(grplist) == sorted(self.study.get_condition(oldCondName).get_group_list()):
                return

            self.study.get_condition(oldCondName).set_group_list(grplist)

        elif column == self.RESP_CO:
            newresp = item.text().strip()

            if newresp == '':
                resplist = []
            else:
                try:
                    resplist = [int(x) for x in newresp.strip(',').split(',')]
                except ValueError as e:
                    QMessageBox.warning(self, "Invalid Response Code",
                                        "Response Code must be a comma seperated list of integers:\n%s" % e)

                    self.populateConditions(selectedCondition=oldCondName)

                    return

            if sorted(resplist) == sorted(self.study.get_condition(oldCondName).get_response_list()):
                return

            self.study.get_condition(oldCondName).set_response_list(resplist)

        self.clean = False
        self.window().UpdateCleanliness()

    def currentTableCondition(self):
        item = self.ConditionsTableWidget.item(self.ConditionsTableWidget.currentRow(), 0)

        if item is None:
            return None

        return item.data(Qt.UserRole)

    def populateConditions(self, selectedCondition=None):
        selected = None

        self.ConditionsTableWidget.clear()
        self.ConditionsTableWidget.setSortingEnabled(False)
        self.ConditionsTableWidget.setRowCount(len(self.study.conditions))

        headers = ['  Name   ', '  Trig Codes   ', '  Group Codes   ', '  Resp Codes   ']

        self.ConditionsTableWidget.setColumnCount(len(headers))
        self.ConditionsTableWidget.setHorizontalHeaderLabels(headers)

        for row, j in enumerate(self.study.conditions):
            cond = self.study.get_condition(j)
            item = QTableWidgetItem(cond.name)
            item.setData(Qt.UserRole, QVariant(cond.name))

            if selectedCondition is not None and selectedCondition == cond.name:
                selected = item

            self.ConditionsTableWidget.setItem(row, self.NAME, item)

            triggers = triglst2str(cond.get_trigger_list())
            self.ConditionsTableWidget.setItem(row, self.TRIG_CO, QTableWidgetItem(triggers))

            groups = triglst2str(cond.get_group_list())
            self.ConditionsTableWidget.setItem(row, self.GRP_CO, QTableWidgetItem(groups))

            responses = ', '.join([str(num) for num in cond.get_response_list()])
            self.ConditionsTableWidget.setItem(row, self.RESP_CO, QTableWidgetItem(responses))

        self.ConditionsTableWidget.setSortingEnabled(True)
        self.ConditionsTableWidget.resizeColumnsToContents()

        if selected is not None:
            selected.setSelected(True)
            self.ConditionsTableWidget.setCurrentItem(selected)


class WindowParams(QtWidgets.QGroupBox):
    NAME, START_MS, END_MS = range(3)

    def __init__(self, parent=None):
        super(WindowParams, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'windowParams_ui.ui'), self)

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("windowParams", "Windows (ms)"))

        self.WindowsTableWidget.itemChanged.connect(self.tableItemChanged)
        self.AddWinButton.clicked.connect(self.addWindow)
        self.RemWinButton.clicked.connect(self.removeWindow)

        self.window().RegisterCleanliness(self)

    def setup(self, study):
        self.study = study

        self.populateWindows()

        self.clean = True

    def addWindow(self):
        count = 2

        namebase = 'NewWindow'
        newWinName = namebase

        while newWinName in self.study.windows:
            newWinName = namebase + str(count)
            count += 1

        self.study.window_definer.add_window(TimeWindow(name=newWinName, ms=(0, 1)))
        self.populateWindows(newWinName)
        self.WindowsTableWidget.setFocus()
        self.WindowsTableWidget.editItem(self.WindowsTableWidget.currentItem())

        self.clean = False
        self.window().UpdateCleanliness()

    def removeWindow(self):
        window = self.currentTableWindow()

        if window is None:
            return

        window = str(window)

        if QMessageBox.question(self, "Remove Window",
                                "Are you sure you wish to delete window %s?" % window,
                                QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
            return

        self.study.window_definer.del_window(window)
        self.populateWindows()

        self.clean = False
        self.window().UpdateCleanliness()

    def tableItemChanged(self, item):
        oldWinName = self.currentTableWindow()

        if oldWinName is None:
            return

        oldWinName = str(oldWinName)
        oldWinms = self.study.get_window(oldWinName).get_ms()
        column = self.WindowsTableWidget.currentColumn()

        if column == self.NAME:
            newName = item.text().strip()
            if newName == oldWinName:
                return
            if newName in self.study.windows:
                if QMessageBox.question(self, "Window - Overwrite?",
                                        "Window name already exists, do you want to overwrite old window?",
                                        QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes:
                    self.study.window_definer.del_window(newName)
                    self.study.window_definer.add_window(TimeWindow(name=newName, ms=oldWinms))
                    self.study.window_definer.del_window(oldWinName)
                self.populateWindows()
            else:
                self.study.window_definer.add_window(TimeWindow(name=newName, ms=oldWinms))
                self.study.window_definer.del_window(oldWinName)
                item.setData(Qt.UserRole, QVariant(newName))
        elif column == self.START_MS:
            newstart = item.text().strip()
            try:
                newstartms = int(newstart)
            except ValueError as e:
                QMessageBox.warning(self, "Invalid Start Point",
                                    "Start point must be an integer number of milliseconds:\n%s" % e)

                self.populateWindows(selectedWindow=oldWinName)
                return
            self.study.window_definer.del_window(oldWinName)
            self.study.window_definer.add_window(TimeWindow(name=oldWinName, ms=(newstartms, oldWinms[1])))
        elif column == self.END_MS:
            newend = item.text().strip()
            try:
                newendms = int(newend)
            except ValueError as e:
                QMessageBox.warning(self, "Invalid End Point",
                                    "End point must be an integer number of milliseconds:\n%s" % e)

                self.populateWindows(selectedWindow=oldWinName)
                return

            self.study.window_definer.del_window(oldWinName)
            self.study.window_definer.add_window(TimeWindow(name=oldWinName, ms=(oldWinms[0], newendms)))

        self.clean = False
        self.window().UpdateCleanliness()

    def currentTableWindow(self):
        item = self.WindowsTableWidget.item(self.WindowsTableWidget.currentRow(), 0)

        if item is None:
            return None

        return item.data(Qt.UserRole)

    def populateWindows(self, selectedWindow=None):
        selected = None

        self.WindowsTableWidget.clear()
        self.WindowsTableWidget.setSortingEnabled(False)
        self.WindowsTableWidget.setRowCount(len(self.study.windows))

        headers = ['  Name   ', '  Start   ', '  End   ']

        self.WindowsTableWidget.setColumnCount(len(headers))
        self.WindowsTableWidget.setHorizontalHeaderLabels(headers)

        for row, j in enumerate(self.study.windows):
            item = QTableWidgetItem(j)
            item.setData(Qt.UserRole, QVariant(j))

            if selectedWindow is not None and selectedWindow == j:
                selected = item

            self.WindowsTableWidget.setItem(row, self.NAME, item)
            window_times = self.study.get_window(j).get_ms()

            self.WindowsTableWidget.setItem(row, self.START_MS, QTableWidgetItem(str(window_times[0])))
            self.WindowsTableWidget.setItem(row, self.END_MS, QTableWidgetItem(str(window_times[1])))

        self.WindowsTableWidget.setSortingEnabled(True)
        self.WindowsTableWidget.resizeColumnsToContents()

        if selected is not None:
            selected.setSelected(True)
            self.WindowsTableWidget.setCurrentItem(selected)


class FCParams(QtWidgets.QGroupBox):
    NAME, LOW_HZ, HIGH_HZ = range(3)

    def __init__(self, parent=None):
        super(FCParams, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'filterChainParams_ui.ui'), self)

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("filterChainParams", "Filter Chains (Hz)"))

        self.FilterChainsTableWidget.itemChanged.connect(self.tableItemChanged)
        self.AddFCButton.clicked.connect(self.addFilterChain)
        self.RemFCButton.clicked.connect(self.removeFilterChain)

        self.window().RegisterCleanliness(self)

    def setup(self, study):
        self.study = study

        self.populateFilterChains()

        self.clean = True

    def addFilterChain(self):
        count = 2

        namebase = 'NewFC_DC'
        newFCName = namebase

        while newFCName in self.study.filters:
            newFCName = namebase + '_' + str(count)
            count += 1

        newfilterchain = FilterChain(name=newFCName)

        self.study.filter_definer.add_filterchain(newfilterchain)
        self.populateFilterChains(newFCName)
        self.FilterChainsTableWidget.setFocus()
        self.FilterChainsTableWidget.editItem(self.FilterChainsTableWidget.currentItem())

        self.clean = False
        self.window().UpdateCleanliness()

    def removeFilterChain(self):
        filterchain = self.currentTableFilterChain()

        if filterchain is None:
            return

        filterchain = str(filterchain)

        if QMessageBox.question(self, "Remove Filter Chain",
                                "Are you sure you wish to delete filter chain %s?" % filterchain,
                                QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
            return

        del self.study.filter_definer.chains[filterchain]
        self.populateFilterChains()

        self.clean = False
        self.window().UpdateCleanliness()

    def tableItemChanged(self, item):
        oldFCName = self.currentTableFilterChain()

        if oldFCName is None:
            return

        oldFCName = str(oldFCName)
        oldFC = self.study.filter_definer.chains[oldFCName]

        column = self.FilterChainsTableWidget.currentColumn()

        if column == self.NAME:
            newName = item.text().strip()

            if newName == oldFCName:
                return

            if newName in self.study.filters:
                if QMessageBox.question(self, "Filter Chain - Overwrite?",
                                        "Filter chain name already exists, do you want to overwrite old filter chain?",
                                        QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
                    return

            oldFC.name = newName
            self.study.filter_definer.chains[newName] = oldFC
            del self.study.filter_definer.chains[oldFCName]

            item.setData(Qt.UserRole, QVariant(newName))

        elif column == self.LOW_HZ:
            newlfstr = item.text().strip()

            try:
                newlf = str2freq(newlfstr)
            except ValueError as e:
                QMessageBox.warning(self, "Invalid Low Frequency",
                                    "Low frequency must be an integer Hz, or None (N/A) for low pass filter:\n%s" % e)

                self.populateFilterChains(selectedFC=oldFCName)
                return

            # For the time being we can assume that we've only got one or two
            # filters in the chain. The first will always be a remove DC and
            # the second, if present, a bandpass. Otherwise we won't have
            # loaded and displayed it so we should never get to this point.
            # This will need to be extended if the GUI is to be used for more
            # complex filterchains.

            if newlf is None:
                if oldFC.num_filters == 0:
                    # There wasn't a band pass and we've not set a low_freq, so
                    # nothing has actually changed
                    return
                if oldFC.filters[0].low_freq is None:
                    # There was a band pass, but the low_freq hasn't actually
                    # changed, so we can return
                    return
                if oldFC.filters[0].high_freq is None:
                    # We've set the low_freq to None, and the high_freq was
                    # None so we've effectively removed the band pass
                    oldFC.filters.pop()
                else:
                    # If we get to here we've changed the low_freq and there is
                    # a high_freq so we just need to update the low_freq
                    oldFC.filters[0].low_freq = newlf
            else:
                if oldFC.num_filters == 1:
                    if oldFC.filters[0].low_freq == newlf:
                        # There already was a band pass, and we've not changed
                        # anything
                        return
                    # Otherwise we've changed it so we need to update it
                    oldFC.filters[0].low_freq = newlf
                else:
                    # There isn't a band pass yet so we need to add it with the
                    # relevant low_freq
                    oldFC.add_filter(BandFilter(low_freq=newlf, high_freq=None))

        elif column == self.HIGH_HZ:
            newhfstr = item.text().strip()
            try:
                newhf = str2freq(newhfstr)
            except ValueError as e:
                QMessageBox.warning(self, "Invalid High Frequency",
                                    "High frequency must be an integer Hz, or None (N/A) for high pass filter:\n%s" % e)

                self.populateFilterChains(selectedFC=oldFCName)
                return

            # For the time being we can assume that we've only got one or two
            # filters in the chain. The first will always be a remove DC and
            # the second, if present, a bandpass. Otherwise we won't have
            # loaded and displayed it so we should never get to this point.
            # This will need to be extended if the GUI is to be used for more
            # complex filterchains.

            if newhf is None:
                if oldFC.num_filters == 0:
                    # There wasn't a band pass and we've not set a high_freq, so
                    # nothing has actually changed
                    return
                if oldFC.filters[0].high_freq is None:
                    # There was a band pass, but the high_freq hasn't actually
                    # changed, so we can return
                    return
                if oldFC.filters[0].low_freq is None:
                    # We've set the high_freq to None, and the low_freq was
                    # None so we've effectively removed the band pass
                    oldFC.filters.pop()
                else:
                    # If we get to here we've changed the high_freq and there is
                    # a low_freq so we just need to update the high_freq
                    oldFC.filters[0].high_freq = newhf
            else:
                if oldFC.num_filters == 1:
                    if oldFC.filters[0].high_freq == newhf:
                        # There already was a band pass, and we've not changed
                        # anything
                        return
                    # Otherwise we've changed it so we need to update it
                    oldFC.filters[0].high_freq = newhf
                else:
                    # There isn't a band pass yet so we need to add it with the
                    # relevant high_freq
                    oldFC.add_filter(BandFilter(low_freq=None, high_freq=newhf))

        self.clean = False
        self.window().UpdateCleanliness()

    def currentTableFilterChain(self):
        item = self.FilterChainsTableWidget.item(self.FilterChainsTableWidget.currentRow(), 0)

        if item is None:
            return None

        return item.data(Qt.UserRole)

    def populateFilterChains(self, selectedFC=None):
        selected = None

        self.FilterChainsTableWidget.clear()
        self.FilterChainsTableWidget.setSortingEnabled(False)
        self.FilterChainsTableWidget.setRowCount(len(self.study.filters))

        headers = ['  Name   ', '  Low   ', '  High   ']

        self.FilterChainsTableWidget.setColumnCount(len(headers))
        self.FilterChainsTableWidget.setHorizontalHeaderLabels(headers)

        for row, j in enumerate(self.study.filters):
            curchain = self.study.get_filterchain(j)
            item = QTableWidgetItem(j)
            item.setData(Qt.UserRole, QVariant(j))

            if curchain.num_filters == 0:
                self.FilterChainsTableWidget.setItem(row, self.NAME, item)
                self.FilterChainsTableWidget.setItem(row, self.LOW_HZ, QTableWidgetItem('N/A'))
                self.FilterChainsTableWidget.setItem(row, self.HIGH_HZ, QTableWidgetItem('N/A'))
            elif curchain.num_filters == 1:
                if (not isinstance(curchain.filters[0], BandFilter)):
                    QMessageBox.warning(self, "Unknown Filter Chain",
                                        "This version of Study Definer can only handle simple filter chains.\n"
                                        "Filter chain %s is not a know type and cannot be displayed." % j)

                    return

                if selectedFC is not None and selectedFC == j:
                    selected = item

                low_f = freq2str(curchain.filters[0].low_freq)
                high_f = freq2str(curchain.filters[0].high_freq)

                self.FilterChainsTableWidget.setItem(row, self.NAME, item)
                self.FilterChainsTableWidget.setItem(row, self.LOW_HZ, QTableWidgetItem(low_f))
                self.FilterChainsTableWidget.setItem(row, self.HIGH_HZ, QTableWidgetItem(high_f))
            else:
                QMessageBox.warning(self, "Unknown Filter Chain",
                                    "This version of Study Definer can only handle simple filter chains.\n"
                                    "Filter chain %s is not a know type and cannot be displayed." % j)

                return

        self.FilterChainsTableWidget.setSortingEnabled(True)
        self.FilterChainsTableWidget.resizeColumnsToContents()

        if selected is not None:
            selected.setSelected(True)
            self.FilterChainsTableWidget.setCurrentItem(selected)


class ChannelSetParams(QtWidgets.QGroupBox):
    NAME, CHANNELS = range(2)

    def __init__(self, parent=None):
        super(ChannelSetParams, self).__init__(parent)

        loadUi(join(WIDGET_PATH, 'chanSetParams_ui.ui'), self)

    def initialise(self):
        self.setTitle(QtWidgets.QApplication.translate("chanSetParams", "Channel Sets"))

        self.AddChannelsButton.clicked.connect(self.addChannelset)
        self.RemChannelsButton.clicked.connect(self.removeChannelset)

        self.window().RegisterCleanliness(self)

    def modelDataChanged(self, QModelIndex1, QModelIndex2):
        self.clean = False
        self.window().UpdateCleanliness()

    def setup(self, study):
        self.study = study

        self.model = ChannelsTableModel(self.study)
        self.model.dataChanged.connect(self.modelDataChanged)
        self.ChannelsTableView.setModel(self.model)

        self.ChannelsTableView.setItemDelegate(ChannelDelegate(self))

        self.clean = True

    def addChannelset(self):
        row = self.model.rowCount()

        self.model.insertRows(row)

        index = self.model.index(row, 0)

        self.ChannelsTableView.setFocus()
        self.ChannelsTableView.setCurrentIndex(index)
        self.ChannelsTableView.edit(index)

        self.clean = False
        self.window().UpdateCleanliness()

    def removeChannelset(self):
        index = self.ChannelsTableView.currentIndex()

        if not index.isValid():
            return

        row = index.row()

        name = self.model.data(self.model.index(row, self.NAME))

        if QMessageBox.question(self, "Remove Channelset",
                                "Are you sure you wish to delete channelset %s?" % name,
                                QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
            return

        self.model.removeRows(row)

        self.clean = False
        self.window().UpdateCleanliness()


class ChannelsTableModel(QAbstractTableModel):
    NAME, CHANNELS = range(2)

    def __init__(self, study):
        super(ChannelsTableModel, self).__init__()

        self.study = study
        self.chansets = []

        for j in self.study.channels:
            self.chansets.append(j)

        self.chansetdefaultGroups = {}
        self.chansetdefaultSets = {}
        self.chansetdefaultGroups['All Meg Channels'] = ChannelGroup(channelgroup=CHANNEL_MEG)

    def chanSet2NiceStr(self, chanset):
        # Is this chan set a chan group...
        if isinstance(chanset, ChannelGroup):
            # If so set the display string as the chan group
            chanstr = str(chanset)
            # but check if it's in our default list and therefore has a nicer display string
            for k in self.chansetdefaultGroups.keys():
                if chanset == self.chansetdefaultGroups[k]:
                    chanstr = k
                    break

        # ...or is it a chan set
        else:
            # in which case just turn the list into a string for display
            chanstr = strlist2str(chanset.keys())
            # again, unless it's in the default list and has a nicer display.
            for k in self.chansetdefaultSets.keys():
                if chanset == self.chansetdefaultSets[k]:
                    chanstr = k
                    break
        return chanstr

    def sortByName(self):
        self.chansets = sorted(self.chansets)
        self.reset()

    def flags(self, index):
        if not index.isValid():
            return Qt.ItemIsEnabled

        return Qt.ItemFlags(QAbstractTableModel.flags(self, index) | Qt.ItemIsEditable)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < len(self.chansets)):
            return QVariant()

        name = self.chansets[index.row()]
        cset = self.study.get_chanset(name)
        column = index.column()

        if role == Qt.DisplayRole:
            if column == self.NAME:
                return QVariant(name)
            elif column == self.CHANNELS:
                return QVariant(self.chanSet2NiceStr(cset))

        return QVariant()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.TextAlignmentRole:
            if orientation == Qt.Horizontal:
                return QVariant(int(Qt.AlignLeft | Qt.AlignVCenter))
            return QVariant(int(Qt.AlignRight | Qt.AlignVCenter))

        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            if section == self.NAME:
                return QVariant("  Name   ")
            elif section == self.CHANNELS:
                return QVariant("  Channels  ")

        return QVariant(int(section + 1))

    def rowCount(self, index=QModelIndex()):
        return len(self.chansets)

    def columnCount(self, index=QModelIndex()):
        return 2

    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid() and 0 <= index.row() < len(self.chansets):
            row = index.row()

            name = self.chansets[row]
            cset = self.study.get_chanset(name)
            column = index.column()

            if column == self.NAME:
                newName = value.value()

                if newName == name:
                    return False

                if newName in self.study.channels:
                    if QMessageBox.question(None, "Channelset - Overwrite?",
                                            "Channelset name already exists, do you want to overwrite old channelset?",
                                            QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
                        return False

                    self.study.chan_definer.chansets[newName] = cset
                    self.removeRows(self.chansets.index(name))
                else:
                    cset.name = newName
                    self.study.chan_definer.add_chanset(cset)
                    del self.study.chan_definer.chansets[name]
                    self.chansets[self.chansets.index(name)] = newName

            elif column == self.CHANNELS:
                cstr = str(value.value())

                if cstr in self.chansetdefaultGroups:
                    newcset = deepcopy(self.chansetdefaultGroups[cstr])
                elif cstr in self.chansetdefaultSets:
                    newcset = deepcopy(self.chansetdefaultSets[cstr])
                else:
                    newcset = ChannelSet(name=name)
                    for cname in str2strlist(cstr):
                        newcset.add_channel(cname, CHANNEL_MEG)

                newcset.name = name

                try:
                    if newcset == cset:
                        return
                except Exception:
                    pass

                self.study.chan_definer.chansets[name] = newcset

            self.dataChanged.emit(index, index)

            return True

        return False

    def insertRows(self, position, rows=1, index=QModelIndex()):
        self.beginInsertRows(QModelIndex(), position, position + rows - 1)

        count = 2
        namebase = 'NewChanSet'
        newCSName = namebase

        for row in range(rows):
            while newCSName in self.study.channels:
                newCSName = namebase+str(count)
                count += 1

            self.study.chan_definer.add_chanset(ChannelSet(name=newCSName))
            self.chansets.insert(position + row, newCSName)

        self.endInsertRows()

        return True

    def removeRows(self, position, rows=1, index=QModelIndex()):
        self.beginRemoveRows(QModelIndex(), position, position + rows - 1)

        for j in range(rows):
            del self.study.chan_definer.chansets[self.chansets[position + j]]

        self.chansets = self.chansets[:position] + self.chansets[position + rows:]
        self.endRemoveRows()

        return True


class ChannelDelegate(QtWidgets.QItemDelegate):
    NAME, CHANNELS = range(2)

    def __init__(self, parent=None):
        super(ChannelDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        if index.column == self.NAME:
            editor = QLineEdit(parent)
            self.editor.returnPressed.connect(self.commitAndCloseEditor)

            return editor
        elif index.column() == self.CHANNELS:
            cset = index.model().study.get_chanset(index.model().chansets[index.row()])

            combobox = QtWidgets.QComboBox(parent)
            combobox.addItems(sorted(index.model().chansetdefaultGroups.keys()))
            combobox.addItems(sorted(index.model().chansetdefaultSets.keys()))

            nondefaultCSet = True

            if isinstance(cset, ChannelGroup):
                for k in index.model().chansetdefaultGroups.keys():
                    if cset == index.model().chansetdefaultGroups[k]:
                        nondefaultCSet = False
                        break
            else:
                for k in index.model().chansetdefaultSets.keys():
                    if cset == index.model().chansetdefaultSets[k]:
                        nondefaultCSet = False
                        break

            if nondefaultCSet:
                if isinstance(cset, ChannelGroup):
                    combobox.insertItem(combobox.count(), str(cset))
                else:
                    combobox.insertItem(combobox.count(), strlist2str(cset.keys()))

            combobox.setEditable(True)
            return combobox
        else:
            return QtWidgets.QItemDelegate.createEditor(self, parent, option, index)

    def commitAndCloseEditor(self):
        editor = self.sender()

        if isinstance(editor, (QTextEdit, QLineEdit)):
            self.commitData.emit(editor)
            self.closeEditor.emit(editor)

    def setEditorData(self, editor, index):
        if index.column() == self.NAME:
            if isinstance(index.model().chansets[index.row()], QVariant):
                nstr = str(index.model().chansets[index.row()].value())
            else:
                nstr = index.model().chansets[index.row()]

            editor.setText(nstr)
        elif index.column() == self.CHANNELS:
            cset = index.model().study.get_chanset(index.model().chansets[index.row()])
            cstr = index.model().chanSet2NiceStr(cset)
            idx = editor.findText(cstr)

            if idx == -1:
                idx = 0

            editor.setCurrentIndex(idx)
        else:
            QtWidgets.QItemDelegate.setModelData(self, editor, index.model(), index)

    def setModelData(self, editor, model, index):
        if index.column() == self.NAME:
            model.setData(index, QVariant(editor.text()))
        elif index.column() == self.CHANNELS:
            model.setData(index, QVariant(editor.currentText()))
        else:
            QtWidgets.QItemDelegate.setModelData(self, editor, model, index)
