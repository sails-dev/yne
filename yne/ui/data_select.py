#!/usr/bin/python3

from os.path import join

from PyQt5.QtWidgets import QFrame
from PyQt5.uic import loadUi

from yne import WIDGET_PATH

from .multicheck import MultiCheckBox


class FlexibleDataSelect(QFrame):
    def __init__(self, *args, **kwargs):
        QFrame.__init__(self)

        loadUi(join(WIDGET_PATH, 'dataSelect.ui'), self)

        self.multi_file = kwargs.get('multi_file', False)
        self.multi_condition = kwargs.get('multi_condition', False)

        self.setup_study(kwargs.get('study', None),
                         kwargs.get('limit_run_list', None))

        # Might need to track multiple conditions and runs
        self.runs = []
        self.conditions = []

        self.setup_ui()

    def setup_ui(self):
        if self.multi_file:
            # Turn off the combo box
            self.file_choice.setVisible(False)
            # and on the label and button
            self.filenames_info.setVisible(True)
            self.filenames_button.setVisible(True)

            # Link up the button
            self.filenames_button.clicked.connect(self.select_files)
        else:
            # Turn on the combo box
            self.file_choice.setVisible(True)
            # and off the label and button
            self.filenames_info.setVisible(False)
            self.filenames_button.setVisible(False)

        if self.multi_condition:
            # Turn off the combo box
            self.condition_choice.setVisible(False)
            # and on the label and button
            self.condition_info.setVisible(True)
            self.condition_button.setVisible(True)

            # Link up the button
            self.condition_button.clicked.connect(self.select_conditions)
        else:
            # Turn on the combo box
            self.condition_choice.setVisible(True)
            # and on the label and button
            self.condition_info.setVisible(False)
            self.condition_button.setVisible(False)

    def setup_study(self, study, limit_run_list):
        self.study = study
        self.limit_run_list = limit_run_list

        self.prepare_info()
        self.sync_info()

    @property
    def available_run_list(self):
        # Combine study runs and limit list
        if self.limit_run_list is None:
            return sorted(self.study.runs)

        return sorted(list(set(self.study.runs).intersection(self.limit_run_list)))

    def prepare_info(self):
        if self.study is None:

            self.filename_label.setText('N/A')
            self.name_label.setText('N/A')
            self.output_label.setText('N/A')

            self.pretrig_dur_label.setText('N/A')
            self.epoch_dur_label.setText('N/A')

            if self.multi_file:
                self.filenames_info.setText('N/A')
            else:
                self.file_choice.clear()

            if self.multi_condition:
                self.condition_info.setText('N/A')
            else:
                self.condition_choice.clear()

            self.window_choice.clear()
            self.filter_choice.clear()
            self.chan_choice.clear()

            return

        self.filename_label.setText(self.study.filename)
        self.name_label.setText(self.study.name)
        self.output_label.setText(self.study.get_output_dir())

        self.pretrig_dur_label.setText(str(self.study.pretrig_dur))
        self.epoch_dur_label.setText(str(self.study.epoch_dur))

        if not self.multi_file:
            self.file_choice.addItems(sorted(self.available_run_list))

        if not self.multi_condition:
            self.condition_choice.addItems(sorted(self.study.conditions))

        self.window_choice.addItems(sorted(self.study.windows))
        self.filter_choice.addItems(sorted(self.study.filters))
        self.chan_choice.addItems(sorted(self.study.channels))

    def sync_info(self):
        if self.study is None:
            return

        if self.multi_file:
            if len(self.runs) == 0:
                self.filenames_info.setText('All files (GROUP)')
            elif len(self.runs) == 1:
                self.filenames_info.setText(self.runs[0])
            else:
                self.filenames_info.setText(f'{len(self.runs)} files')

        if self.multi_condition:
            if len(self.conditions) == 0:
                self.condition_info.setText('All conditions')
            else:
                self.condition_info.setText(', '.join(self.conditions))

    def select_files(self):
        if self.study is None or not self.multi_file:
            return

        options = sorted([x for x in self.available_run_list])

        chosen = MultiCheckBox('File Selector',
                               'Select files.  An empty selection means all runs',
                               options, self.runs)

        # Check for cancel
        if chosen is None:
            return

        # We get back the indices of the options so we need to convert them back
        self.runs = [options[idx] for idx in chosen]

        self.sync_info()

    def select_conditions(self):
        if self.study is None or not self.multi_condition:
            return

        options = [x for x in self.study.conditions
                   if self.study.get_condition(x).is_simple]

        chosen = MultiCheckBox('Condition Selector',
                               'Select conditions.  An empty selection means all conditions',
                               options)

        # Check for cancel
        if chosen is None:
            return

        # We get back the indices of the options so we need to convert them back
        self.conditions = [options[idx] for idx in chosen]

        self.sync_info()

    def get_data_dict(self):
        dat = {}

        if self.multi_file:
            if len(self.runs) == 0:
                dat['run_names'] = [x for x in self.available_run_list]
            else:
                dat['run_names'] = self.runs
        else:
            dat['run_name'] = self.file_choice.currentText()

        if self.multi_condition:
            # Only pass on simple conditions
            if len(self.conditions) == 0:
                dat['conditions'] = [x for x in self.study.conditions
                                     if self.study.get_condition(x).is_simple]
            else:
                dat['conditions'] = self.conditions
        else:
            dat['condition_name'] = self.condition_choice.currentText()

        dat['window_name'] = self.window_choice.currentText()
        dat['filter_name'] = self.filter_choice.currentText()
        dat['chanset_name'] = self.chan_choice.currentText()

        return dat


class SingleCond(FlexibleDataSelect):
    def __init__(self, *args, **kwargs):
        kwargs['multi_file'] = False
        kwargs['multi_condition'] = False

        FlexibleDataSelect.__init__(self, *args, **kwargs)


class MultiFileMultiCond(FlexibleDataSelect):
    def __init__(self, *args, **kwargs):
        kwargs['multi_file'] = True
        kwargs['multi_condition'] = True

        FlexibleDataSelect.__init__(self, *args, **kwargs)
