#!/usr/bin/python3

from os import makedirs
from os.path import join, isfile

from PyQt5.QtCore import QFileSystemWatcher
from PyQt5.QtWidgets import QFrame
from PyQt5.uic import loadUi

from yne import WIDGET_PATH


class StudyDisplay(QFrame):
    def __init__(self, *args, **kwargs):
        QFrame.__init__(self, *args, **kwargs)

        loadUi(join(WIDGET_PATH, 'studyDisplay.ui'), self)

        self.fs_watch = QFileSystemWatcher()

        self.setup_study(kwargs.get('study', None),
                         kwargs.get('limit_run_list', None))

        self.file_choice.currentIndexChanged.connect(self.sync_coreg)

    def setup_study(self, study, limit_run_list):
        self.study = study
        self.limit_run_list = limit_run_list

        self.sync_info()

        # Add a watcher for the coreg files
        if study is not None:
            # Clear any existing watches
            self.fs_watch.removePaths(self.fs_watch.directories())

            hstree_dir = self.study.get_headshape_tree_dir()
            makedirs(hstree_dir, exist_ok=True)
            self.fs_watch.addPath(hstree_dir)

        # Connect any changes to the update routine
        self.fs_watch.directoryChanged.connect(self.sync_coreg)

    @property
    def available_run_list(self):
        # Combine study runs and limit list
        if self.limit_run_list is None:
            return sorted(self.study.runs)

        return sorted(list(set(self.study.runs).intersection(self.limit_run_list)))

    def sync_info(self):
        if self.study is None:

            self.filename_label.setText('N/A')
            self.name_label.setText('N/A')
            self.output_label.setText('N/A')

            self.pretrig_dur_label.setText('N/A')
            self.epoch_dur_label.setText('N/A')

            self.file_choice.clear()
            self.condition_choice.clear()
            self.window_choice.clear()
            self.filter_choice.clear()
            self.chan_choice.clear()

            return

        self.filename_label.setText(self.study.filename)
        self.name_label.setText(self.study.name)
        self.output_label.setText(self.study.get_output_dir())

        self.pretrig_dur_label.setText(str(self.study.pretrig_dur))
        self.epoch_dur_label.setText(str(self.study.epoch_dur))

        self.file_choice.addItems(sorted(self.available_run_list))
        self.condition_choice.addItems(sorted(self.study.conditions))
        self.window_choice.addItems(sorted(self.study.windows))
        self.filter_choice.addItems(sorted(self.study.filters))
        self.chan_choice.addItems(sorted(self.study.channels))

        # Force a coreg sync
        self.sync_coreg()

    def sync_coreg(self):
        # TODO: Add a timer to keep re-checking this
        try:
            current_run = self.file_choice.currentText()

            if current_run in self.available_run_list:
                coreg_path = self.study.get_run_coreg_filename(current_run)
                if isfile(coreg_path):
                    self.coregStatusLabel.setText('Present')
                else:
                    self.coregStatusLabel.setText('Missing')
            else:
                self.coregStatusLabel.setText('Unknown')
        except Exception:
            self.coregStatusLabel.setText('Unknown')

    def get_data_dict(self):
        dat = {}

        dat['run_name'] = self.file_choice.currentText()
        dat['condition_name'] = self.condition_choice.currentText()
        dat['window_name'] = self.window_choice.currentText()
        dat['filter_name'] = self.filter_choice.currentText()
        dat['chanset_name'] = self.chan_choice.currentText()

        return dat
