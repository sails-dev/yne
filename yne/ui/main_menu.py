#!/usr/bin/python3

import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QPushButton, QDialog, QGridLayout, QLabel, QVBoxLayout)


def launch_child_process(script, args):
    newpid = os.fork()

    if newpid == 0:
        # Run the child process
        os.execvp(script, args)


# Store buttons to create in a hash
buttons = {}


class YNEMenu(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self, *args, **kwargs)

        self.title = 'YNE Main Menu'

        self.setMinimumWidth(250)

        self.prepareUI()

    def run_study_definer(self):
        launch_child_process('yne-study-definer', ['yne-study-definer'])

    buttons[(0, 0)] = ('Study Definer', 'run_study_definer')

    def run_raw_data_viewer(self):
        launch_child_process('yne-raw-data-viewer', ['yne-raw-data-viewer'])

    buttons[(1, 0)] = ('Show Raw Data', 'run_raw_data_viewer')

    def run_epoch_rejection(self):
        launch_child_process('yne-epoch-rejection', ['yne-epoch-rejection'])

    buttons[(2, 0)] = ('Epoch Rejection', 'run_epoch_rejection')

    def run_study_analyser(self):
        launch_child_process('yne-study-analyser', ['yne-study-analyser'])

    buttons[(3, 0)] = ('Study Analyser', 'run_study_analyser')

    def prepareUI(self):
        self.setWindowTitle(self.title)

        layout = QVBoxLayout()

        label = QLabel('YNE Utilities')
        label.setAlignment(Qt.AlignCenter)
        label.setStyleSheet("font-size: 12pt; font-weight: bold;")

        button_layout = QGridLayout()

        for (row, col), (name, target) in buttons.items():
            button = QPushButton(name)

            button.pressed.connect(getattr(self, target))

            button_layout.addWidget(button, row, col)

        layout.addWidget(label)
        layout.addLayout(button_layout)

        self.setLayout(layout)
