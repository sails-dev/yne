#!/usr/bin/python3

from os import makedirs
from os.path import join, dirname
from uuid import uuid4

import numpy as np
import matplotlib.pyplot as plt

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot, QThreadPool
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QDialog
from PyQt5.uic import loadUi

import mne
from mne.time_frequency import (tfr_multitaper, tfr_stockwell, tfr_morlet)

from yne import WIDGET_PATH, get_logger

from .worker_support import WorkerSignals
from .run_cond_select import RunCondSelect
from .plot_selectors import (AverageRawPlotOpts, TopologyOpts,
                             AverageMethodPlotOpts, AverageCompOptions,
                             SingleChanOpts)


logger = get_logger()

#############################################################################
# Result classes
#############################################################################


class SensorAverageResult(QObject):
    def __init__(self, filename, averages, **kwargs):
        QObject.__init__(self)

        self.filenames = filename
        self.averages = averages

        # These may be None if not known
        self.window_name = kwargs.get('window_name')
        self.filter_name = kwargs.get('filter_name')
        self.chanset_name = kwargs.get('chanset_name')

    @property
    def run_names(self):
        return sorted(self.averages.keys())

    @property
    def condition_names(self):
        conds = {}

        for a in self.averages.keys():
            for c in self.averages[a].keys():
                if c not in conds:
                    conds[c] = True

        return sorted(list(conds.keys()))

    def find_first_result(self):
        avg = None

        for run_name in self.run_names:
            for cond_name in self.condition_names:
                avg = self.averages[run_name][cond_name]
                if avg is not None:
                    break

            if avg is not None:
                break

        return avg

    def all_meg_chans(self):
        chans = set()

        for run in self.averages:
            for cond in self.averages[run]:
                avg = self.averages[run][cond]

                chans = chans.union(avg.ch_names)

        return sorted(list(chans))


#############################################################################
# Load / Save routines
#############################################################################


class LoadAveragesThread(QRunnable):
    """
    Loads average data and provides progress
    """
    def __init__(self, study, run_names, condition_names, window_name,
                 filter_name, chanset_name):

        QRunnable.__init__(self)

        self.study = study

        self.run_names = run_names
        self.condition_names = condition_names

        self.window_name = window_name
        self.filter_name = filter_name
        self.chanset_name = chanset_name

        self.expected_count = 0
        self.found_count = 0
        self.missing = []

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        # Results
        self.result = None

    @pyqtSlot()
    def run(self):
        filenames = {}
        averages = {}

        base_name = [self.window_name,
                     self.filter_name,
                     self.chanset_name]

        base_name = ':'.join(base_name)

        num_to_load = len(self.run_names) * len(self.condition_names)
        load_idx = 1

        self.expected_count = num_to_load

        for run_name in sorted(self.run_names):
            # Find the relevant directory
            run = self.study.get_run(run_name)
            run_path = join(self.study.get_run_output_dir(run), 'average')

            filenames[run_name] = run.filename
            averages[run_name] = {}

            for condition in self.condition_names:
                self.signals.progress.emit(f'Loading {condition} for {run_name} ({load_idx}/{num_to_load})')
                load_idx += 1

                fname = f'{condition}:{base_name}-ave.fif.gz'
                fpath = join(run_path, fname)

                # Try and load it
                try:
                    ev = mne.read_evokeds(fpath)[0]
                    averages[run_name][condition] = ev
                    self.found_count += 1
                except Exception:
                    self.missing.append(fpath)
                    averages[run_name][condition] = None

        self.result = SensorAverageResult(filenames, averages,
                                          window_name=self.window_name,
                                          filter_name=self.filter_name,
                                          chanset_name=self.chanset_name)

        success = True

        self.signals.progress.emit('Loading complete')

        self.signals.finished.emit(success)


class LoadAveragesFromStudyDlg(QDialog):
    def __init__(self, study):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'avgFileSelect.ui'), self)

        self.setWindowTitle('Load average data for multiple files/conditions')

        # Set up the study in the frame
        self.study = study
        self.info_frame.setup_study(self.study, None)

        # Buttons
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)


def configure_load_averages(parent, finished_callback, statusbar_callback,
                            study):

    dlg = LoadAveragesFromStudyDlg(study)

    if not dlg.exec_():
        return None

    # Find which files we need to load
    data = dlg.info_frame.get_data_dict()

    proc_object = LoadAveragesThread(study,
                                     data['run_names'],
                                     data['conditions'],
                                     data['window_name'],
                                     data['filter_name'],
                                     data['chanset_name'])

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object

#############################################################################
# Worker analysis classes
#############################################################################


class SensorAverageAnalysis(QRunnable):
    """
    Run an average analysis on one or more runs with possibly multiple conditions
    """
    def __init__(self, run_names, condition_names, filenames, epochs, method, **kwargs):
        QRunnable.__init__(self)

        self.run_names = run_names
        self.condition_names = condition_names

        # These may be None if not known
        self.window_name = kwargs.get('window_name', None)
        self.filter_name = kwargs.get('filter_name', None)
        self.chanset_name = kwargs.get('chanset_name', None)

        self.filenames = filenames
        self.epochs = epochs

        self.method = method

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        self.result = None

    @pyqtSlot()
    def run(self):

        # Map from run names to filenames
        filenames = {}

        # This will be [run_name][condition]
        results = {}

        try:
            if self.condition_names is not None and len(self.condition_names) < 1:
                raise Exception("No conditions found")

            for runcount, run_name in enumerate(self.run_names, 1):
                self.signals.progress.emit(f'Averaging {run_name} ({runcount}/{len(self.run_names)})')

                # Prepare somewhere to store the data
                filenames[run_name] = self.filenames[run_name]
                results[run_name] = {}

                run_epoch = self.epochs[run_name]

                if self.condition_names is None:
                    # Average the whole shooting match
                    avg = run_epoch.average(method=self.method)
                    avg.info['yne_avg_method'] = self.method
                    results[run_name]['all_conditions'] = avg
                else:
                    for condition in self.condition_names:
                        msg = f'Averaging {run_name} ({runcount}/{len(self.run_names)}) ({condition})'
                        self.signals.progress.emit(msg)
                        sub_epoch = run_epoch[condition]

                        avg = sub_epoch.average(method=self.method)
                        avg.info['yne_avg_method'] = self.method

                        results[run_name][condition] = avg

            self.result = SensorAverageResult(filenames, results,
                                              window_name=self.window_name,
                                              filter_name=self.filter_name,
                                              chanset_name=self.chanset_name)

            success = True

        except Exception as e:
            self.exception = e
            success = False

        self.signals.progress.emit('Averaging complete')

        self.signals.finished.emit(success)


#############################################################################
# GUI helper routines
#############################################################################


def configure_sensor_avg(parent, finished_callback, statusbar_callback,
                         run_names, condition_names, filenames, epochs, method,
                         **kwargs):

    # Things that are optional
    window_name = kwargs.get('window_name', None)
    filter_name = kwargs.get('filter_name', None)
    chanset_name = kwargs.get('chanset_name', None)

    proc_object = SensorAverageAnalysis(run_names, condition_names,
                                        filenames, epochs, method,
                                        window_name=window_name,
                                        filter_name=filter_name,
                                        chanset_name=chanset_name)

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object


#############################################################################
# Interface classes
#############################################################################


class SensorAverageResultUI(QMainWindow):
    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self)

        loadUi(join(WIDGET_PATH, 'sensorAverageResult.ui'), self)

        self.internal_window_id = str(uuid4())

        self.study = kwargs.get('study', None)
        self.data = kwargs.get('data', dict())

        # Share the threadpool if passed
        self.threadpool = kwargs.get('threadpool', QThreadPool())

        self.sync_values()

        self.setup_buttons()

    def sync_values(self):
        if len(self.data.averages) < 1:
            return

        # Use the first object to get some metadata
        avg = self.data.find_first_result()

        # Can't continue
        if avg is None:
            return

        dur = avg.tmax - avg.tmin
        sfreq = avg.info['sfreq']

        self.sample_rate_label.setText(f'{sfreq:.3f}')
        self.epoch_dur_label.setText(f'{dur:.3f}')

        self.tmin = avg.tmin
        self.tmax = avg.tmax

        self.tmin_label.setText(f'{avg.tmin:.3f}')
        self.tmax_label.setText(f'{avg.tmax:.3f}')

        method = avg.info.get('yne_average_method', 'Unknown')

        self.method_label.setText(f'{method}')

        # List runs and conditions
        run_names = self.data.run_names
        condition_names = self.data.condition_names

        table_txt = '<table>'
        table_txt += '<tr>'
        table_txt += '<th>Run Name</th>'
        table_txt += '<th>Filename</th>'

        for condition in condition_names:
            table_txt += f'<th>{condition}</th>'

        table_txt += '</tr>'

        for run_name in run_names:
            filename = self.data.filenames[run_name]

            table_txt += '<tr>'
            table_txt += f'<td>{run_name}</td>'
            table_txt += f'<td><tt>{filename}</tt></td>'

            for cond_name in condition_names:
                if condition not in self.data.averages[run_name]:
                    table_txt += '<td>0 epochs</td>'
                else:
                    avg = self.data.averages[run_name].get(cond_name, None)
                    if avg is None:
                        table_txt += '<td>0 epochs</td>'
                    else:
                        table_txt += f'<td>{avg.nave} epochs</td>'

            table_txt += '</tr>'

        table_txt += '</table>'

        self.avg_browser.setText(table_txt)

    def setup_buttons(self):
        self.actionPlot_Raw.triggered.connect(self.show_avg)
        self.actionPlot_GFP.triggered.connect(self.show_gfp)
        self.actionPlot_Topo.triggered.connect(self.show_topo)
        self.actionPlot_TopoMap.triggered.connect(self.show_topomap)
        self.actionPlot_Joint.triggered.connect(self.show_joint)
        self.actionPlot_ERFDiff.triggered.connect(self.show_erfcomp)
        self.actionPlot_SingleChan.triggered.connect(self.show_singlechan)

        self.action_Save_All_Averages.triggered.connect(self.save_all)

    def get_single_run(self, OptDialog, **kwargs):
        if 'all_cond' not in kwargs:
            kwargs['all_cond'] = False

        # Only handle one run at a time here
        dlg = OptDialog(runs=self.data.filenames,
                        conditions=self.data.condition_names,
                        all_run=False, **kwargs)

        if not dlg.exec_():
            return None, None

        if dlg.all_conditions:
            conds = self.data.condition_names
        else:
            conds = [dlg.condition_name]

        extra_args = dlg.get_extra_args()

        # Handle multiple conditions at once
        if len(conds) > 1:
            avg = []

            for cond in conds:
                avg.append(self.data.averages[dlg.run_name][cond])
        else:
            # Make the single case easy
            avg = self.data.averages[dlg.run_name][conds[0]]

        return avg, extra_args

    def get_multiple_runs(self, OptDialog, **kwargs):
        if 'all_cond' not in kwargs:
            kwargs['all_cond'] = False

        # Only handle one run at a time here
        dlg = OptDialog(runs=self.data.filenames,
                        conditions=self.data.condition_names,
                        all_run=False, all_run_combined=True,
                        **kwargs)

        if not dlg.exec_():
            return None, None

        if dlg.all_conditions:
            conds = sorted(self.data.condition_names)
        else:
            conds = [dlg.condition_name]

        extra_args = dlg.get_extra_args()

        extra_args['conditions'] = conds

        if dlg.runs_list is not None and len(dlg.runs_list) == 1:
            run_name = dlg.runs_list[0]

            # Handle multiple conditions at once
            if len(conds) > 1:
                avg = []

                for cond in conds:
                    avg.append(self.data.averages[run_name][cond])
            else:
                # Make the single case easy
                avg = self.data.averages[run_name][conds[0]]

            return avg, extra_args

        # Otherwise need to combine the evokeds

        # Careful; dlg.runs_list here will be None to indicate that we're
        # combining the individual runs together; access dlg.runs instead
        runs_list = dlg.runs

        avgs = []

        for cond in sorted(conds):
            cond_ev = []

            # Find all of the relevant evoked object
            for run_name in runs_list:
                avg = self.data.averages[run_name].get(cond, None)

                if avg is not None:
                    cond_ev.append(avg)

            weights = extra_args.get('weights', 'equal')

            avgs.append(mne.combine_evoked(cond_ev, weights))

        return avgs, extra_args

    def show_avg(self):
        try:
            data, extra_args = self.get_multiple_runs(AverageRawPlotOpts,
                                                      all_cond=True)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is None:
            return

        # Need to remove these before passing things on to plot
        conditions = extra_args.pop('conditions')
        extra_args.pop('weights')

        if isinstance(data, list):
            for item, condition in zip(data, conditions):
                item.plot(window_title=condition, **extra_args)
        else:
            data.plot(**extra_args)

    def show_gfp(self):
        try:
            data, extra_args = self.get_multiple_runs(AverageMethodPlotOpts,
                                                      all_cond=True)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is None:
            return

        # Need to remove these before passing things on to plot
        conditions = extra_args.pop('conditions')
        extra_args.pop('weights', None)

        if isinstance(data, list):
            # Vaguely match some of MNEs sizing etc
            fig = plt.figure(figsize=(8.8, 3))
            ax = fig.gca()

            labels = []

            for item, condition in zip(data, conditions):
                # Scale data into fT to make the y labelling easier
                data = item.data * 1e15

                # Calculate the GFP
                gfp = np.sqrt((data ** 2)).mean(axis=0)

                # Stash the label for later use
                # nave = r'N$_{\mathrm{ave}}$'
                # labels.append(f'{item.comment}: {nave}={item.nave}')
                labels.append(condition)

                # Plot the GFP
                ax.plot(item.times, gfp)

            ax.set_ylabel('fT')
            ax.legend(labels)

            fig.show()

    def show_topo(self):
        try:
            data, extra_args = self.get_single_run(RunCondSelect)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is not None:
            data.plot_topo()

    def show_topomap(self):
        topotimes = getattr(self, 'topotimes', None)

        try:
            data, extra_args = self.get_single_run(TopologyOpts, tstartvals=topotimes,
                                                   tmin=self.tmin, tmax=self.tmax)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is not None:
            # Save for later GUI use
            if 'times' in extra_args:
                self.topotimes = extra_args['times']

            data.plot_topomap(**extra_args)

    def show_joint(self):
        topotimes = getattr(self, 'topotimes', None)
        try:
            data, extra_args = self.get_single_run(TopologyOpts, tstartvals=topotimes,
                                                   tmin=self.tmin, tmax=self.tmax)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is not None:
            # Save for later GUI use
            if 'times' in extra_args:
                self.topotimes = extra_args['times']

            data.plot_joint(**extra_args)

    def get_comparisons_runs(self, OptDialog, **kwargs):
        # Only handle one run at a time here
        dlg = OptDialog(runs=self.data.filenames,
                        conditions=self.data.condition_names,
                        all_run=False, all_run_combined=True,
                        **kwargs)

        if not dlg.exec_():
            return None, None

        extra_args = dlg.get_extra_args()

        # Figure out which conditions are involved
        conditions = []
        cond_weights = []

        for cond in extra_args['comp_weights']:
            # Skip anything with a 0 weight
            if extra_args['comp_weights'][cond] == 0:
                continue

            conditions.append(cond)
            cond_weights.append(extra_args['comp_weights'][cond])

        if len(conditions) < 1:
            QMessageBox.warning(self, "Error",
                                "No non-zero weights")
            return None, None

        # Figure out which runs to use
        if dlg.runs_list is None:
            # Use everything
            runs_list = dlg.runs
        else:
            runs_list = dlg.runs_list

        comparisons = []

        # Do the comparison for each run
        for run_name in runs_list:
            evokeds = [self.data.averages[run_name][cond] for cond in conditions]

            comp = mne.combine_evoked(evokeds, cond_weights)

            comparisons.append(comp)

        # Now figure out whether we need to average this lot
        if len(comparisons) > 1:
            final_comp = mne.combine_evoked(comparisons, extra_args['weights'])
        else:
            # If just one run, take the comparison
            final_comp = comparisons[0]

        return final_comp, extra_args

    def show_erfcomp(self):
        try:
            data, extra_args = self.get_comparisons_runs(AverageCompOptions,
                                                         show_cond=False)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is None:
            return

        # Need to remove the non-plotting arguments
        del extra_args['comp_weights']
        del extra_args['weights']

        data.plot(**extra_args)

    def save_all(self):
        # TODO: Make work without study by making up names
        # based on filename / condition
        if self.study is None:
            QMessageBox.warning(self, "Error",
                                "Currently only support saving all averages in study mode")
            return

        # Work out what the base name will be
        win_name = 'unknownwin' if self.data.window_name is None else self.data.window_name
        filt_name = 'unknownfilt' if self.data.filter_name is None else self.data.filter_name
        chans_name = 'unknownchans' if self.data.chanset_name is None else self.data.chanset_name

        base_name = [win_name, filt_name, chans_name]

        base_name = ':'.join(base_name)

        # Set up a dictionary of filenames / object to save
        to_save = {}

        # Only works with a study at the moment
        for run_name in sorted(self.data.averages):
            run_path = join(self.study.get_run_output_dir(run_name), 'average')

            for condition in self.data.averages[run_name]:
                out_name = f'{condition}:{base_name}-ave.fif.gz'
                out_file = join(run_path, out_name)

                # Take a reference to the object to save
                to_save[out_file] = self.data.averages[run_name][condition]

        if len(to_save) == 0:
            QMessageBox.warning(self, "Error",
                                "Could not find any files to save")
            return

        resp = QMessageBox.question(self, f"Saving {len(to_save)} files",
                                    f"Will save {len(to_save)} files.  Continue?",
                                    QMessageBox.Yes | QMessageBox.No)

        if resp == QMessageBox.Yes:
            try:
                for fname, avg in to_save.items():
                    # Ensure that the directory exists
                    dname = dirname(fname)
                    makedirs(dname, exist_ok=True)

                    # Save the object
                    # Note that the average version doesn't have the overwrite
                    # argument for some reason, it just does it; yay, consistency
                    avg.save(fname)
            except Exception as e:
                QMessageBox.warning(self, "Failed to save",
                                    f"Failed to save one or more files: {e}")

    def show_singlechan(self):
        all_meg_chans = self.data.all_meg_chans()

        sel_chans = getattr(self, 'sel_chans', list())

        try:
            data, extra_args = self.get_multiple_runs(SingleChanOpts,
                                                      all_cond=True,
                                                      chan_names=all_meg_chans,
                                                      chan_sel=sel_chans)
        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if data is None:
            return

        # We always want a list here
        if not isinstance(data, list):
            data = [data]

        if len(extra_args['picks']) == 0:
            QMessageBox.warning(self, "No channels selected",
                                "Need to pick at least one channel")

            return

        # Cache for future use
        self.sel_chans = extra_args['picks']

        # Need to pop non-plotting args out of extra_args before use
        conditions = extra_args.pop('conditions')

        tf = extra_args.pop('tf')

        exceptions = []

        for dat, condition in zip(data, conditions):
            # Doing this makes the TFR routines a lot quicker below
            item = dat.copy()

            item = item.pick(extra_args['picks'])

            item.plot(window_title=condition, **extra_args)

            # Always wrap the tf in an exception handler as it's fragile
            try:
                tf_mode = tf.get('mode', 'None')
                tf_analysis = tf.get('analysis', dict())

                power = None

                if tf_mode == 'Multitaper':
                    freqs = np.arange(tf_analysis['freqs']['min'],
                                      tf_analysis['freqs']['max'] + 0.1,
                                      tf_analysis['freqs']['delta'])

                    n_cycles = freqs / tf_analysis['n_cycles']

                    time_bandwidth = tf_analysis['time_bandwidth']

                    power = tfr_multitaper(item,
                                           freqs=freqs,
                                           n_cycles=n_cycles,
                                           time_bandwidth=time_bandwidth,
                                           return_itc=False)

                elif tf_mode == 'Morlet':
                    freqs = np.arange(tf_analysis['freqs']['min'],
                                      tf_analysis['freqs']['max'] + 0.1,
                                      tf_analysis['freqs']['delta'])

                    n_cycles = tf_analysis['n_cycles']

                    power = tfr_morlet(item,
                                       freqs=freqs,
                                       n_cycles=n_cycles,
                                       return_itc=False)

                elif tf_mode == 'Stockwell':
                    power = tfr_stockwell(item,
                                          fmin=tf_analysis['freqs']['min'],
                                          fmax=tf_analysis['freqs']['max'],
                                          width=tf_analysis['width'])

                if power is not None:
                    power.plot(title=f'{condition} {tf_mode}', **tf['plot'])

            except Exception as e:
                exceptions.append(e)

        if len(exceptions) > 0:
            msg = "Some errors were encountered whilst plotting: \n\n"
            msg += '\n'.join([str(e) for e in exceptions])

            QMessageBox.warning(self, "Errors during plotting", msg)
