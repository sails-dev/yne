#!/usr/bin/python3

from copy import deepcopy
from os import makedirs
from os.path import join, dirname
from uuid import uuid4

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot, pyqtSignal, QThreadPool
from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtWidgets import (QDialog, QMainWindow, QMessageBox, QDialogButtonBox)
from PyQt5.uic import loadUi

import mne

from yne import WIDGET_PATH

from .run_cond_select import RunCondSelect
from .sensor_average import SensorAverageResultUI, configure_sensor_avg
from .sensor_mvpa import SensorMVPAUI, SensorMVPAResultUI, configure_mvpa
from .window_register import WindowRegister
from .worker_support import WorkerSignals
from .plot_selectors import TopologyImageOpts


#############################################################################
# Result classes
#############################################################################


class SensorEpochResult(QObject):
    def __init__(self, filename, epochs, **kwargs):
        QObject.__init__(self)

        self.filenames = filename
        self.epochs = epochs

        # Things that may be None
        self.window_name = kwargs.get('window_name', None)
        self.filter_name = kwargs.get('filter_name', None)
        self.chanset_name = kwargs.get('chanset_name', None)


#############################################################################
# Load / Save routines
#############################################################################

class LoadEpochsThread(QRunnable):
    """
    Load epochs data and provide progress
    """
    def __init__(self, study, run_names, window_name, filter_name, chanset_name):
        QRunnable.__init__(self)

        self.study = study

        self.run_names = run_names

        self.window_name = window_name
        self.filter_name = filter_name
        self.chanset_name = chanset_name

        self.expected_count = 0
        self.found_count = 0
        self.missing = []

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        # Results
        self.result = None

    @pyqtSlot()
    def run(self):

        filenames = {}
        epochs = {}

        base_name = [self.window_name,
                     self.filter_name,
                     self.chanset_name]

        base_name = ':'.join(base_name)

        num_runs = len(self.run_names)

        for run_idx, run_name in enumerate(sorted(self.run_names), 1):
            self.signals.progress.emit(f'Loading epochs for {run_name} ({run_idx}/{num_runs})')

            # Find the relevant directory
            run = self.study.get_run(run_name)
            run_path = join(self.study.get_run_output_dir(run), 'epochs')

            filenames[run_name] = run.filename

            self.expected_count += 1

            fname = f'{base_name}-epo.fif.gz'
            fpath = join(run_path, fname)

            # Try and load it
            try:
                ep = mne.read_epochs(fpath)
                epochs[run_name] = ep
                self.found_count += 1
            except Exception:
                del filenames[run_name]
                self.missing.append(fpath)

        self.result = SensorEpochResult(filenames, epochs,
                                        window_name=self.window_name,
                                        filter_name=self.filter_name,
                                        chanset_name=self.chanset_name)

        success = True

        self.signals.progress.emit('Loading complete')

        self.signals.finished.emit(success)


class LoadEpochsFromStudyDlg(QDialog):
    def __init__(self, study):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'avgFileSelect.ui'), self)

        self.setWindowTitle('Load epoch data for multiple runs')

        # Set up the study in the frame
        self.study = study
        self.info_frame.setup_study(self.study, None)

        # Bit of a botch but as we don't want to ask about condition,
        # just hide the UI elements
        self.info_frame.conditionLabel.setVisible(False)
        self.info_frame.condition_button.setVisible(False)
        self.info_frame.condition_choice.setVisible(False)
        self.info_frame.condition_info.setVisible(False)

        # Buttons
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)


def configure_load_epoch(parent, finished_callback, statusbar_callback,
                         study):

    dlg = LoadEpochsFromStudyDlg(study)

    if not dlg.exec_():
        return None

    data = dlg.info_frame.get_data_dict()

    proc_object = LoadEpochsThread(study,
                                   data['run_names'],
                                   data['window_name'],
                                   data['filter_name'],
                                   data['chanset_name'])

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object


#############################################################################
# Worker analysis classes
#############################################################################


class SensorEpochAnalysis(QRunnable):
    """
    Run an average analysis on one or more runs with possibly multiple conditions
    """
    def __init__(self, study, run_names, conditions, window_name, filter_name,
                 chanset_name, epoch_args):
        QRunnable.__init__(self)

        self.study = study

        self.run_names = run_names
        self.conditions = conditions
        self.window_name = window_name
        self.filter_name = filter_name
        self.chanset_name = chanset_name

        # Extra options to pass to mne.Epochs as keyword arguments
        self.epoch_args = epoch_args

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        self.result = None

    @pyqtSlot()
    def run(self):
        try:
            filenames = {}
            epochs = {}

            if len(self.conditions) == 0:
                raise Exception("No conditions found")

            for runcount, run_name in enumerate(self.run_names, 1):
                self.signals.progress.emit(f'Epoching {run_name} ({runcount}/{len(self.run_names)})')

                e = self.study.to_mne_epochs(run_name,
                                             self.conditions,
                                             self.window_name,
                                             self.filter_name,
                                             self.chanset_name,
                                             **self.epoch_args)

                filenames[run_name] = self.study.get_run_data(run_name).filename
                epochs[run_name] = e

            self.result = SensorEpochResult(filenames, epochs,
                                            window_name=self.window_name,
                                            filter_name=self.filter_name,
                                            chanset_name=self.chanset_name)

            success = True

        except Exception as e:
            self.exception = e
            success = False

        self.signals.progress.emit('Epoching complete')

        self.signals.finished.emit(success)


#############################################################################
# GUI helper routines
#############################################################################


def configure_sensor_epoch(parent, finished_callback, statusbar_callback,
                           study):

    # Keep an unchanging copy of the study
    study = deepcopy(study)

    dlg = SensorEpochUI(parent, study=study)

    if not dlg.exec_():
        return None

    # TODO: Catch errors when getting data and show a message dialog
    # TODO: Maybe add a loop to allow person to go back and fix errors
    dat = dlg.get_data_dict()

    dat['study'] = study

    proc_object = SensorEpochAnalysis(**dat)

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object


#############################################################################
# Interface classes
#############################################################################


class SensorEpochUI(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'sensorEpoch.ui'), self)

        self.setup_study(kwargs.get('study', None),
                         kwargs.get('limit_run_list', None))

        # Set up validators on input fields
        sig_validator = QDoubleValidator(-999999, 999999, 3)
        sig_validator.setNotation(QDoubleValidator.ScientificNotation)

        self.aer_mag_edit.setValidator(sig_validator)
        self.aer_grad_edit.setValidator(sig_validator)
        self.aer_eeg_edit.setValidator(sig_validator)
        self.aer_eog_edit.setValidator(sig_validator)

        self.baseline_check.stateChanged.connect(self.baseline_start_edit.setEnabled)
        self.baseline_check.stateChanged.connect(self.baseline_start_label.setEnabled)
        self.baseline_check.stateChanged.connect(self.baseline_end_edit.setEnabled)
        self.baseline_check.stateChanged.connect(self.baseline_end_label.setEnabled)

        self.aer_check.stateChanged.connect(self.aer_mag_edit.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_mag_label.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_grad_edit.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_grad_label.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_eeg_edit.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_eeg_label.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_eog_edit.setEnabled)
        self.aer_check.stateChanged.connect(self.aer_eog_label.setEnabled)

        self.update_baseline()

        self.study_frame.window_choice.currentIndexChanged.connect(self.update_baseline)

        self.baseline_check.stateChanged.connect(self.update_ok)
        self.baseline_start_edit.textEdited.connect(self.update_ok)
        self.baseline_end_edit.textEdited.connect(self.update_ok)

        self.buttonBox.accepted.connect(self.check_values)
        self.buttonBox.rejected.connect(self.reject)

    def update_baseline(self):
        try:
            win_name = self.study_frame.window_choice.currentText()
            win_start, win_end = self.study.get_window(win_name).get_ms()

            # Our window times are in ms, display in s
            win_start /= 1000
            win_end /= 1000
        except Exception:
            win_start = -999999999
            win_end = 999999999

        validator = QDoubleValidator(win_start, win_end, QDoubleValidator.StandardNotation)
        validator.setDecimals(5)

        self.baseline_start_edit.setValidator(validator)
        self.baseline_end_edit.setValidator(validator)

        self.baseline_start_edit.setText(str(win_start))
        self.baseline_end_edit.setText(str(0 if (win_end > 0 and win_start < 0) else win_end))

    def update_ok(self):
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(self.check_values())

    def check_values(self):
        # Catch bad errors here
        try:
            if self.baseline_check.isChecked():
                start = end = None

                try:
                    start = float(self.baseline_start_edit.text())
                    end = float(self.baseline_end_edit.text())
                except Exception:
                    pass

                if start is not None and end is not None:
                    # Check baseline sanity
                    if start >= end:
                        return False
                    else:
                        # Check that it fits with the window
                        win_name = self.study_frame.window_choice.currentText()
                        win_start, win_end = self.study.get_window(win_name).get_ms()

                        # Our window times are in ms, and we display in s
                        win_start /= 1000
                        win_end /= 1000

                        if start < win_start or start > win_end or end < win_start or end > win_end:
                            return False

        except Exception as e:
            QMessageBox.warning(self,
                                "Internal Error",
                                f"Internal error while validating input ({e})")
            return False

        # We're ok!
        return True

    def setup_study(self, study, limit_run_list):
        self.study = study
        self.limit_run_list = limit_run_list

        self.study_frame.setup_study(study, limit_run_list)

    def get_data_dict(self):
        dat = self.study_frame.get_data_dict()

        # Find additional values
        dtr_txt = self.detrend_combo.currentText()

        if dtr_txt == 'DC removal':
            detrend = 0
        elif dtr_txt == 'Linear detrending':
            detrend = 1
        elif dtr_txt == 'No detrending':
            detrend = None
        else:
            raise ValueError(f"Unknown value of detrend: {dtr_txt}")

        if self.baseline_check.isChecked():
            # We want a baseline correction
            # Assume that we've pre-checked this
            baseline = (float(self.baseline_start_edit.text()),
                        float(self.baseline_end_edit.text()))
        else:
            baseline = None

        if self.aer_check.isChecked():
            # We want automatic rejection
            reject = {}

            reject['mag'] = float(self.aer_mag_edit.text())
            reject['grad'] = float(self.aer_grad_edit.text())
            reject['eeg'] = float(self.aer_eeg_edit.text())
            reject['eog'] = float(self.aer_eog_edit.text())

        else:
            reject = None

        dat['epoch_args'] = {'detrend': detrend,
                             'baseline': baseline,
                             'reject': reject}

        return dat


class SensorEpochResultUI(QMainWindow):
    result_window_closing = pyqtSignal(str)

    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self)

        loadUi(join(WIDGET_PATH, 'sensorEpochResult.ui'), self)

        self.proc_object = None

        self.internal_window_id = str(uuid4())

        self.study = kwargs.get('study', None)
        self.data = kwargs.get('data', None)

        # Share the threadpool if passed
        self.threadpool = kwargs.get('threadpool', QThreadPool())

        self.sync_values()

        self.setup_buttons()

    @property
    def conditions(self):
        # Find conditions
        condition_names = {}

        for run_name in self.data.epochs.keys():
            epoch = self.data.epochs[run_name]

            for condname in epoch.event_id.keys():
                if condname not in condition_names:
                    condition_names[condname] = True

        return list(condition_names.keys())

    def sync_values(self):
        if self.data is None:
            return

        # Use the first data for this
        # TODO: Add routine to check consistency at some point
        first_key = list(sorted(self.data.epochs.keys()))[0]

        epochs = self.data.epochs[first_key]

        dur = epochs.tmax - epochs.tmin
        sfreq = epochs.info['sfreq']

        self.sample_rate_label.setText(f'{sfreq:.3f}')
        self.epoch_dur_label.setText(f'{dur:.3f}')

        self.tmin_label.setText(f'{epochs.tmin:.3f}')
        self.tmax_label.setText(f'{epochs.tmax:.3f}')

        if epochs.baseline is None:
            self.baseline_min_label.setText('N/A')
            self.baseline_max_label.setText('N/A')
        else:
            self.baseline_min_label.setText(f'{epochs.baseline[0]:.3f}')
            self.baseline_max_label.setText(f'{epochs.baseline[1]:.3f}')

        # List conditions
        run_names = sorted(self.data.epochs.keys())

        condition_names = self.conditions

        table_txt = '<table>'
        table_txt += '<tr>'
        table_txt += '<th>Run Name</th>'
        table_txt += '<th>Filename</th>'

        for condition in condition_names:
            table_txt += f'<th>{condition}</th>'

        table_txt += '</tr>'

        for run_name in run_names:
            epochs = self.data.epochs[run_name]

            table_txt += '<tr>'
            table_txt += f'<td>{run_name}</td>'
            table_txt += f'<td><tt>{self.data.filenames[run_name]}</tt></td>'

            for condition in condition_names:
                if condition not in epochs.event_id:
                    table_txt += '<td>Code: None<br/>0 epochs</td>'
                else:
                    ev_id = epochs.event_id[condition]
                    num_epochs = len(epochs[condition])
                    table_txt += f'<td>Code: {ev_id}<br/>{num_epochs} epochs</td>'

            table_txt += '</tr>'

        table_txt += '</table>'

        self.cond_browser.setText(table_txt)

    def setup_buttons(self):
        self.actionPlot_Raw_Epoch.triggered.connect(self.show_raw)
        self.actionPlot_PSD.triggered.connect(self.show_psd)
        self.actionPlot_Topology.triggered.connect(self.show_topoimage)
        self.actionPlot_Epoch_Image.triggered.connect(self.show_image)

        self.actionAna_Mean.triggered.connect(self.calc_mean)
        self.actionAna_Median.triggered.connect(self.calc_median)
        self.actionAna_MVPA.triggered.connect(self.calc_mvpa)

        self.action_SaveAllEpochs.triggered.connect(self.save_all_epochs)

    def update_statusbar(self, txt=None):
        if txt is None:
            self.statusBar().clearMessage()
        else:
            self.statusBar().showMessage(txt)

    def save_all_epochs(self):
        # TODO: Make work without study by making up names
        # based on filename / condition
        if self.study is None:
            QMessageBox.warning(self, "Error",
                                "Currently only support saving all epochs in study mode")
            return

        # Work out what the base name will be
        win_name = 'unknownwin' if self.data.window_name is None else self.data.window_name
        filt_name = 'unknownfilt' if self.data.filter_name is None else self.data.filter_name
        chans_name = 'unknownchans' if self.data.chanset_name is None else self.data.chanset_name

        base_name = [win_name, filt_name, chans_name]

        base_name = ':'.join(base_name)

        # Set up a dictionary of filenames / object to save
        to_save = {}

        # Only works with a study at the moment
        for run_name in sorted(self.data.epochs):
            run_path = join(self.study.get_run_output_dir(run_name), 'epochs')

            out_name = f'{base_name}-epo.fif.gz'
            out_file = join(run_path, out_name)

            # Take a reference to the object to save
            to_save[out_file] = self.data.epochs[run_name]

        if len(to_save) == 0:
            QMessageBox.warning(self, "Error",
                                "Could not find any files to save")
            return

        resp = QMessageBox.question(self, f"Saving {len(to_save)} files",
                                    f"Will save {len(to_save)} files.  Continue?",
                                    QMessageBox.Yes | QMessageBox.No)
        if resp == QMessageBox.Yes:
            try:
                for fname, ep in to_save.items():
                    # Ensure that the directory exists
                    dname = dirname(fname)
                    makedirs(dname, exist_ok=True)

                    # Save the object
                    ep.save(fname, overwrite=True)
            except Exception as e:
                QMessageBox.warning(self, "Failed to save",
                                    f"Failed to save one or more files: {e}")

    def get_multiple_runs_individual(self, OptDialog):
        dlg = OptDialog(runs=self.data.filenames,
                        conditions=self.conditions,
                        all_run=False,
                        all_run_combined=False,
                        all_run_individual=True,
                        all_cond=False,
                        all_cond_combined=True,
                        all_cond_individual=True)

        if not dlg.exec_():
            return None, None, None

        # None here means to combine them together
        # Currently we don't support that at this level for runs (see above)
        runs = dlg.runs_list
        conds = dlg.condition_list
        extra_args = dlg.get_extra_args()

        if len(runs) < 1:
            return None, None

        if conds is not None and len(conds) < 1:
            return None, None

        return runs, conds, extra_args

    def get_single_run(self, OptDialog):
        # Only handle one run at a time here
        dlg = OptDialog(runs=self.data.filenames,
                        conditions=self.conditions,
                        all_run=False)

        if not dlg.exec_():
            return None, None

        if dlg.all_conditions:
            conds = self.conditions
        else:
            conds = [dlg.condition_name]

        extra_args = dlg.get_extra_args()

        if len(conds) < 1:
            return None, None

        epoch = self.data.epochs[dlg.run_name]

        # Get the right data
        data = epoch[conds]

        return data, extra_args

    def show_raw(self):
        data, extra_args = self.get_single_run(RunCondSelect)

        if data is not None:
            data.plot(**extra_args)

    def show_psd(self):
        data, extra_args = self.get_single_run(RunCondSelect)

        # TODO: Lots of PSD options: Leave for now
        # fmin, fmax
        # bandwidth
        # adaptive
        # low_bias
        # normalization : 'full', 'length' (default)
        # spatial_colors: True/False
        # average: True/False
        # estimate: 'auto' 'power' 'amplitude'; 'auto' is default
        # dB: True/False

        if data is not None:
            data.plot_psd(**extra_args)

    def show_topoimage(self):
        data, extra_args = self.get_single_run(TopologyImageOpts)

        if data is not None:
            data.plot_topo_image(**extra_args)

    def show_image(self):
        data, extra_args = self.get_single_run(RunCondSelect)

        # TODO: plot_iamge

        # picks (chans)
        # vmin, vmax (fT) [None]

        if data is not None:
            data.plot_image(**extra_args)

    def _calc_average(self, method):
        if self.proc_object is not None:
            return

        runs, conds, extra_args = self.get_multiple_runs_individual(RunCondSelect)

        if runs is None:
            return

        proc_object = configure_sensor_avg(self,
                                           self.average_finished,
                                           self.update_statusbar,
                                           runs,
                                           conds,
                                           self.data.filenames,
                                           self.data.epochs,
                                           method,
                                           window_name=self.data.window_name,
                                           filter_name=self.data.filter_name,
                                           chanset_name=self.data.chanset_name)
        if proc_object is None:
            return

        self.proc_object = proc_object

        self.threadpool.start(self.proc_object)

    def average_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to average data: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('Analysis Failed')

            return

        result = self.proc_object.result

        res_dialog = SensorAverageResultUI(data=result, study=self.study,
                                           threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        self.update_statusbar(None)

        # Allow more processing
        # TODO: Enable/disable the analysis bar when this is happening
        self.proc_object = None

    def calc_mean(self):
        self._calc_average('mean')

    def calc_median(self):
        self._calc_average('median')

    def calc_mvpa(self):
        if self.proc_object is not None:
            return

        dlg = SensorMVPAUI(self, study=self.study,
                           limit_run_list=list(self.data.filenames.keys()))

        if not dlg.exec_():
            return None, None

        dat = dlg.get_data_dict()

        if len(dat['run_names']) < 1:
            return None, None

        if 'conditions' not in dat or len(dat['conditions']) < 1:
            return None, None

        # Shouldn't be able to happen but just in case
        if len(dat['conditions']) < 2:
            QMessageBox.warning(self,
                                "Error",
                                "Need at least two conditions for MVPA")

        proc_object = configure_mvpa(self.study.get_output_dir(),
                                     filenames=self.data.filenames,
                                     window_name=self.data.window_name,
                                     filter_name=self.data.filter_name,
                                     chanset_name=self.data.chanset_name,
                                     epochs=self.data.epochs,
                                     finished_callback=self.mvpa_finished,
                                     statusbar_callback=self.update_statusbar,
                                     **dat)

        if proc_object is None:
            return

        self.proc_object = proc_object

        self.threadpool.start(self.proc_object)

    def mvpa_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to run MVPA on data: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('MVPA Analysis Failed')

            return

        result = self.proc_object.result

        res_dialog = SensorMVPAResultUI(data=result, study=self.study,
                                        threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        self.update_statusbar(None)

        # Allow more processing
        # TODO: Enable/disable the analysis bar when this is happening
        self.proc_object = None
