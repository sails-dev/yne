#!/usr/bin/python3

from os import makedirs
from os.path import join, dirname, exists
from uuid import uuid4

import multiprocessing as mp
import h5py

import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import norm

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot, QThreadPool
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QDialog
from PyQt5.uic import loadUi

from sklearn.model_selection import cross_val_score
from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

from anamnesis import AbstractAnam, register_class, obj_from_hdf5file
from yne import WIDGET_PATH, get_logger

from .worker_support import WorkerSignals
from .plot_selectors import MVPAPlotOpts

logger = get_logger()


#############################################################################
# Result classes
#############################################################################


class SensorMVPAResult(AbstractAnam):

    hdf5_outputs = ['times', 'scores', 'condition_names', 'decimation', 'C',
                    'window_name', 'filter_name', 'chanset_name']

    def __init__(self, **kwargs):
        AbstractAnam.__init__(self)

        self.times = kwargs.get('times', None)
        self.scores = kwargs.get('scores', None)
        self.decimation = kwargs.get('decimation', None)
        self.C = kwargs.get('C', None)
        self.kernel = kwargs.get('kernel', None)

        self.condition_names = kwargs.get('condition_names', None)

        self.window_name = kwargs.get('window_name', None)
        self.filter_name = kwargs.get('filter_name', None)
        self.chanset_name = kwargs.get('chanset_name', None)

    @property
    def tpts(self):
        if self.times is None:
            return None

        return len(self.times)

    @property
    def cvs(self):
        if self.scores is None:
            return None

        return self.scores.shape[1]

    @property
    def tmin(self):
        if self.times is None:
            return None

        return self.times[0]

    @property
    def tmax(self):
        if self.times is None:
            return None

        return self.times[-1]


register_class(SensorMVPAResult)


class SensorMVPAGroupResult(QObject):
    def __init__(self, filename, mvpas, **kwargs):
        QObject.__init__(self)

        self.filenames = filename
        self.mvpas = mvpas

        # These may be None if not known
        self.window_name = kwargs.get('window_name', None)
        self.filter_name = kwargs.get('filter_name', None)
        self.chanset_name = kwargs.get('chanset_name', None)

    @property
    def run_names(self):
        return sorted(self.mvpas.keys())


#############################################################################
# GUI interface
#############################################################################


class SensorMVPAUI(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'sensorMVPA.ui'), self)

        # Disable window/filter/channel selection
        self.study_frame.windowLabel.setVisible(False)
        self.study_frame.window_choice.setVisible(False)
        self.study_frame.filterLabel.setVisible(False)
        self.study_frame.filter_choice.setVisible(False)
        self.study_frame.chanLabel.setVisible(False)
        self.study_frame.chan_choice.setVisible(False)

        # Default slots to number of CPUs
        self.slots_spin.setValue(mp.cpu_count())

        self.setup_study(kwargs.get('study', None),
                         kwargs.get('limit_run_list', None))

        self.buttonBox.accepted.connect(self.check_values)
        self.buttonBox.rejected.connect(self.reject)

    def setup_study(self, study, limit_run_list):
        self.study = study
        self.limit_run_list = limit_run_list

        self.study_frame.setup_study(study, limit_run_list)

    def check_values(self):
        dat = self.study_frame.get_data_dict()

        if 'conditions' not in dat or len(dat['conditions']) < 2:
            QMessageBox.warning(self, "More conditions needed",
                                "Need at least two conditions for MVPA")

            return False

        return True

    def get_data_dict(self):
        dat = self.study_frame.get_data_dict()

        # Remove irrelevant info
        dat.pop('window_name', None)
        dat.pop('filter_name', None)
        dat.pop('chanset_name', None)

        # And add our useful info
        dat['kernel'] = self.kernel_combo.currentText()
        dat['crossvalid'] = self.crossvalid_spin.value()
        dat['C'] = self.c_spin.value()
        dat['decimation'] = self.decimation_spin.value()
        dat['recalculate'] = self.recalc_check.isChecked()
        dat['slots'] = self.slots_spin.value()

        return dat


#############################################################################
# Load / Save routines
#############################################################################


class LoadMVPAThread(QRunnable):
    """
    Loads MVPA data and provides progress
    """
    def __init__(self, study, run_names, condition_names, window_name,
                 filter_name, chanset_name, decimation, crossvalid):

        QRunnable.__init__(self)

        self.study = study

        self.run_names = run_names
        self.condition_names = condition_names

        self.window_name = window_name
        self.filter_name = filter_name
        self.chanset_name = chanset_name

        self.decimation = decimation
        self.crossvalid = crossvalid

        self.expected_count = 0
        self.found_count = 0
        self.missing = []

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        # Results
        self.result = None

    @pyqtSlot()
    def run(self):
        filenames = {}
        mvpas = {}

        namevals = [self.window_name, self.filter_name, self.chanset_name]
        namevals += self.condition_names
        namevals += [str(self.decimation), str(self.crossvalid)]

        base_name = ':'.join(namevals) + '.hdf5'

        num_to_load = len(self.run_names)

        load_idx = 1

        self.expected_count = num_to_load

        for run_name in sorted(self.run_names):
            # Find the relevant directory
            run = self.study.get_run(run_name)
            run_path = join(self.study.get_run_output_dir(run), 'mvpa', base_name)

            self.signals.progress.emit(f'Loading MVPA from {run_path} ({load_idx}/{num_to_load})')
            load_idx += 1

            # Try and load it
            try:
                # Load from the HDF5 file
                mv = obj_from_hdf5file(run_path)

                if not isinstance(mv, SensorMVPAResult):
                    raise Exception("Not an MVPA file")

                filenames[run_name] = run.filename
                mvpas[run_name] = mv

                self.found_count += 1
            except Exception:
                self.missing.append(run_path)

        self.result = SensorMVPAGroupResult(filenames, mvpas,
                                            window_name=self.window_name,
                                            filter_name=self.filter_name,
                                            chanset_name=self.chanset_name)

        success = True

        self.signals.progress.emit('Loading complete')

        self.signals.finished.emit(success)


class LoadMVPAFromStudyDlg(QDialog):
    def __init__(self, study):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'mvpaFileSelect.ui'), self)

        self.setWindowTitle('Load MVPA data for multiple files')

        # Set up the study in the frame
        self.study = study
        self.info_frame.setup_study(self.study, None)

        # Buttons
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def get_data_dict(self):
        dat = self.info_frame.get_data_dict()

        dat['decimation'] = self.decimation_spin.value()
        dat['crossvalid'] = self.crossvalid_spin.value()

        return dat


def configure_load_mvpa(parent, finished_callback, statusbar_callback, study):

    dlg = LoadMVPAFromStudyDlg(study)

    if not dlg.exec_():
        return None

    # Find which files we need to load
    data = dlg.get_data_dict()

    proc_object = LoadMVPAThread(study,
                                 data['run_names'],
                                 data['conditions'],
                                 window_name=data['window_name'],
                                 filter_name=data['filter_name'],
                                 chanset_name=data['chanset_name'],
                                 decimation=data['decimation'],
                                 crossvalid=data['crossvalid'])

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object


#############################################################################
# Worker analysis classes
#############################################################################


def mvpa_analysis(idx, data_list, labels, kernel, C, cv):
    # Put together our data for this time point
    tp_data = np.concatenate([x[:, :, idx] for x in data_list])

    # Set up a scaling then SVM pipeline
    svm_pipe = make_pipeline(StandardScaler(),
                             svm.SVC(kernel=kernel, C=C))

    return cross_val_score(svm_pipe, tp_data, labels, cv=cv)


class SensorMVPAAnalysis(QRunnable):
    """
    Run an MVPA analysis on one or more runs
    """

    def __init__(self, output_base, run_names, condition_names, window_name,
                 filter_name, chanset_name, filenames,
                 epochs, kernel, crossvalid, C, decimation, slots,
                 recalculate):

        QRunnable.__init__(self)

        self.output_base = output_base

        self.run_names = run_names

        # We always want these sorted to make the filename predictable
        self.condition_names = sorted(condition_names)

        self.window_name = window_name
        self.filter_name = filter_name
        self.chanset_name = chanset_name

        self.filenames = filenames
        self.epochs = epochs

        self.kernel = kernel
        self.crossvalid = crossvalid
        self.C = C
        self.decimation = decimation
        self.slots = slots
        self.recalculate = recalculate

        # Wiring up properties
        # TODO: Make this part of a parent class we can just inherit from
        self.signals = WorkerSignals()

        self.exception = None

        self.result = None

    @property
    def base_outputname(self):
        # Include the decimation and cross validation values in the name
        namevals = [self.window_name, self.filter_name, self.chanset_name] + \
                   self.condition_names + \
                   [str(self.decimation), str(self.crossvalid)]

        base = ':'.join(namevals)

        return join('mvpa', f'{base}.hdf5')

    @pyqtSlot()
    def run(self):

        # Map from run names to filenames
        filenames = {}

        # This will be [run_name] to SensorMVPAResult object
        mvpas = {}

        # Use multi-processing
        # TODO: It would be a good idea to just make this use Qt's thread pool, but
        # that's way more work than I have time for right now - mp.starmap works
        # and works quickly (it does mean, however, that we can't show much in
        # the way of status updates)
        pool = mp.Pool(self.slots)

        try:
            if self.condition_names is None or len(self.condition_names) < 2:
                raise Exception("Need at least two conditions")

            for runcount, run_name in enumerate(self.run_names, 1):
                prog_base = f'MVPA on {run_name} ({runcount}/{len(self.run_names)})'
                self.signals.progress.emit(prog_base)

                # Work out the output filename
                output_name = join(self.output_base, run_name, self.base_outputname)

                # If we're not forcing a recalculation, skip if the file is already there
                if not self.recalculate:
                    if exists(output_name):
                        self.signals.progress.emit(f'MVPA on {run_name}: skipping, exists')

                        # Stash the data in our group object for later
                        try:
                            res = obj_from_hdf5file(output_name)
                            filenames[run_name] = self.filenames[run_name]
                            mvpas[run_name] = res
                            continue
                        except Exception:
                            # Just recompute
                            pass

                # Prepare somewhere to store the data
                makedirs(dirname(output_name), exist_ok=True)

                # Find our data
                run_epoch = self.epochs[run_name]

                inds = range(0, len(run_epoch.times), self.decimation)
                ntpts = len(inds)

                if ntpts < 1:
                    raise Exception("Need at least one time point")

                # Stash the real times for plotting later
                times = run_epoch.times[inds]

                # Sort out our data and labels
                data = []
                labels = []

                for condidx, cond in enumerate(self.condition_names):
                    # Grab the relevant data
                    cond_data = run_epoch[cond].get_data()

                    # Store it and generate labels
                    data.append(cond_data)

                    labels.extend([condidx] * cond_data.shape[0])

                # Set up the MVPA for each time point
                args = [(idx, data, labels, self.kernel, self.C, self.crossvalid) for idx in inds]

                # Run the analysis
                scores = pool.starmap(mvpa_analysis, args)

                scores = np.array(scores)

                # Set up individual result object
                res = SensorMVPAResult(times=times,
                                       scores=scores,
                                       condition_names=self.condition_names,
                                       decimation=self.decimation,
                                       C=self.C,
                                       kernel=self.kernel,
                                       window_name=self.window_name,
                                       filter_name=self.filter_name,
                                       chanset_name=self.chanset_name)

                # Save as HDF5
                try:
                    h = h5py.File(output_name, 'w')
                    res.to_hdf5(h.create_group('mvpa'))
                    h.close()
                except Exception:
                    # Skip saving if we can't
                    pass

                # Stash the data in our group object for later
                filenames[run_name] = self.filenames[run_name]
                mvpas[run_name] = res

            self.result = SensorMVPAGroupResult(filenames, mvpas)

            success = True

        except Exception as e:
            self.exception = e
            success = False

        self.signals.progress.emit('Averaging complete')

        self.signals.finished.emit(success)


#############################################################################
# GUI helper routines
#############################################################################


def configure_mvpa(output_base, run_names, conditions, window_name,
                   filter_name, chanset_name, filenames,
                   epochs, kernel, crossvalid, C, decimation, slots,
                   recalculate, finished_callback, statusbar_callback):

    proc_object = SensorMVPAAnalysis(output_base, run_names, conditions,
                                     window_name, filter_name, chanset_name,
                                     filenames, epochs, kernel,
                                     crossvalid, C, decimation, slots,
                                     recalculate)

    if finished_callback is not None:
        proc_object.signals.finished.connect(finished_callback)

    if statusbar_callback is not None:
        proc_object.signals.progress.connect(statusbar_callback)

    return proc_object


#############################################################################
# Interface classes
#############################################################################


class SensorMVPAResultUI(QMainWindow):
    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self)

        loadUi(join(WIDGET_PATH, 'sensorMVPAResult.ui'), self)

        self.internal_window_id = str(uuid4())

        self.study = kwargs.get('study', None)
        self.data = kwargs.get('data', dict())

        # Share the threadpool if passed
        self.threadpool = kwargs.get('threadpool', QThreadPool())

        self.sync_values()

        self.setup_buttons()

    def sync_values(self):
        if len(self.data.mvpas) < 1:
            return

        # Use the first object to get some metadata
        key = sorted(list(self.data.mvpas.keys()))[0]

        res = self.data.mvpas[key]

        tmin = 'N/A' if res.tmin is None else f'{res.tmin:.3f}'
        tmax = 'N/A' if res.tmax is None else f'{res.tmax:.3f}'

        self.timepts_label.setText(f'{res.tpts}')
        self.cv_label.setText(f'{res.cvs}')
        self.tmin_label.setText(f'{tmin}')
        self.tmax_label.setText(f'{tmax}')

        # List runs and conditions
        run_names = self.data.run_names

        table_txt = '<table>'
        table_txt += '<tr>'
        table_txt += '<th>Run Name</th>'
        table_txt += '<th>Filename</th>'
        table_txt += '<th>MVPA Conditions</th>'
        table_txt += '</tr>'

        for run_name in run_names:
            filename = self.data.filenames[run_name]

            table_txt += '<tr>'
            table_txt += f'<td>{run_name}</td>'
            table_txt += f'<td><tt>{filename}</tt></td>'

            res = self.data.mvpas[run_name]

            condtxt = ','.join(res.condition_names)

            table_txt += f'<td>{condtxt}</td>'

            table_txt += '</tr>'

        table_txt += '</table>'

        self.mvpa_browser.setText(table_txt)

    def setup_buttons(self):
        self.actionPlot_Accuracy.triggered.connect(self.show_accuracy)

    def show_accuracy(self):
        try:
            # Only handle one run at a time here
            dlg = MVPAPlotOpts(runs=list(self.data.filenames.keys()),
                               show_cond=False, all_run=False,
                               all_run_combined=True)

            if not dlg.exec_():
                return None

            runs = dlg.runs_list
            extra_args = dlg.get_extra_args()

            # Need at least one run
            if runs is None:
                # Use all runs
                runs = dlg.runs

        except Exception as e:
            QMessageBox.warning(self, "Error in plot settings",
                                f'Error in plot settings: {e}')
            return

        if len(runs) < 1:
            return

        try:
            times = self.data.mvpas[runs[0]].times

            # Are we doing a group analysis
            if len(runs) == 1:
                # Individual level - within estimate of SE
                datasets = self.data.mvpas[runs[0]].scores
            else:
                # Group analysis
                datasets = []

                # Pull together the datasets
                for run in runs:
                    # Take the participant mean
                    datasets.append(self.data.mvpas[run].scores.mean(axis=1))

                datasets = np.array(datasets).T

            # Mean and SE
            m = datasets.mean(axis=1)
            se = datasets.std(axis=1) / np.sqrt(datasets.shape[1])

            # Calculate CI width from normal-distribution
            # Two tailed so we calculate half the CI in the upper tail
            upper_tail = 1 - ((1 - extra_args['ci']) / 2)
            mult = norm().ppf(upper_tail)

            ci_lower = m - (mult * se)
            ci_upper = m + (mult * se)

            # Vaguely match some of MNEs sizing etc
            fig = plt.figure(figsize=(8.8, 3))
            ax = fig.gca()

            ax.plot(times, m, color='b')
            ax.fill_between(times, ci_lower, ci_upper, color='b', alpha=extra_args['alpha'])

            ax.set_ylabel('Classification accuracy')
            ax.set_xlabel('Time (s)')

            if extra_args['show_chance']:
                # Add on chance line
                num_conds = len(self.data.mvpas[runs[0]].condition_names)
                ax.hlines(1 / num_conds, times[0], times[-1], color='k', linestyle='--')

            fig.show()

        except Exception as e:
            import traceback
            traceback.print_exc()
            QMessageBox.warning(self, "Error in plotting data",
                                f'Failed to plot data ({e})')
            return
