#!/usr/bin/python3

from PyQt5.QtCore import QSignalMapper
from PyQt5.QtGui import QDoubleValidator, QIntValidator
from PyQt5.QtWidgets import (QCheckBox, QLabel, QLineEdit, QInputDialog,
                             QPushButton, QAction, QMessageBox, QComboBox)

from .multicheck import MultiCheckBox


#############################################################################
# Mix-in classes for handling different GUI option settings
#############################################################################


class ChanCheckMixin():
    def add_chancheck_ctrls(self, key, txt, rownum, ch_list, ch_sel):
        if not hasattr(self, 'chancheck'):
            self.chanchecks = {}

            self.chancheck_mapper_set = QSignalMapper(self)
            self.chancheck_mapper_set.mapped[str].connect(self.set_chancheck_values)

            self.chancheck_mapper_clear = QSignalMapper(self)
            self.chancheck_mapper_clear.mapped[str].connect(self.clear_chancheck_values)

        self.chanchecks[key] = {}

        tgt = self.chanchecks[key]

        tgt['chans_sel'] = None

        tgt['available_chans'] = ch_list
        tgt['selected_chans'] = ch_sel

        tgt['txt_label'] = QLabel(txt)

        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        tgt['button_set'] = QPushButton('...')
        self.customLayout.addWidget(tgt['button_set'], rownum, 1)

        tgt['button_clear'] = QPushButton('Clear')
        self.customLayout.addWidget(tgt['button_clear'], rownum, 2)

        tgt['sel_chan_label'] = QLabel()
        self.customLayout.addWidget(tgt['sel_chan_label'], rownum, 3)

        # Handle mapping multiple buttons to the same routine
        action = QAction(key, self)
        self.chancheck_mapper_set.setMapping(action, key)
        action.triggered.connect(self.chancheck_mapper_set.map)
        tgt['button_set'].clicked.connect(action.trigger)

        action = QAction(key, self)
        self.chancheck_mapper_clear.setMapping(action, key)
        action.triggered.connect(self.chancheck_mapper_clear.map)
        tgt['button_clear'].clicked.connect(action.trigger)

        self.sync_chancheck(key)

    def set_chancheck_enabled(self, key, status):
        tgt = self.chanchecks[key]

        tgt['txt_label'].setEnabled(status)
        tgt['button_set'].setEnabled(status)
        tgt['button_clear'].setEnabled(status)
        tgt['sel_chan_label'].setEnabled(status)

    def set_chancheck_values(self, key):
        tgt = self.chanchecks[key]

        avail_chans = tgt['available_chans']
        sel_chans = tgt['selected_chans']

        idx = MultiCheckBox('Foo', 'Bar', avail_chans, sel_chans, 5)

        if idx is None:
            return

        tgt['selected_chans'] = [avail_chans[x] for x in idx]

        self.sync_chancheck(key)

    def clear_chancheck_values(self, key):
        tgt = self.chanchecks[key]

        tgt['selected_chans'] = []

        self.sync_chancheck(key)

    def sync_chancheck(self, key):
        tgt = self.chanchecks[key]

        sel_chans = sorted(tgt['selected_chans'])
        full_text = ','.join(sel_chans)

        if len(sel_chans) == 0:
            tgt['sel_chan_label'].setText('None')
            tgt['sel_chan_label'].setToolTip('None')
        elif len(sel_chans) < 3:
            tgt['sel_chan_label'].setText(full_text)
            tgt['sel_chan_label'].setToolTip(full_text)
        else:
            tgt['sel_chan_label'].setText(f'{len(sel_chans)} channels')
            tgt['sel_chan_label'].setToolTip(full_text)

    def get_chancheck_args(self, key):
        dat = {}

        dat[key] = self.chanchecks[key]['selected_chans']

        return dat


class CheckBoxMixin():
    def add_checkbox_ctrls(self, key, txt, rownum, default=False):
        if not hasattr(self, 'checks'):
            self.checks = {}

        self.checks[key] = {}

        tgt = self.checks[key]

        tgt['txt_label'] = QLabel(txt)

        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        tgt['check'] = QCheckBox('')
        self.customLayout.addWidget(tgt['check'], rownum, 1)
        tgt['check'].setChecked(default)

    def set_checkbox_enabled(self, key, status):
        tgt = self.checks[key]

        tgt['txt_label'].setEnabled(status)
        tgt['check'].setEnabled(status)

    def get_checkbox_args(self, key):
        return {key: self.checks[key]['check'].isChecked()}


class IntTxtMixin():
    def add_int_ctrls(self, key, txt, rownum, **kwargs):
        if not hasattr(self, 'ints'):
            self.ints = {}

        self.ints[key] = {}

        tgt = self.ints[key]

        tgt['txt_label'] = QLabel(txt)

        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        validator = QIntValidator(kwargs.get('min', -9999999),
                                  kwargs.get('max', 9999999))

        tgt['lineedit'] = QLineEdit()
        tgt['lineedit'].setValidator(validator)

        if 'default' in kwargs:
            tgt['lineedit'].setText(str(kwargs['default']))

        self.customLayout.addWidget(tgt['lineedit'], rownum, 1)

    def set_int_enabled(self, key, status):
        tgt = self.ints[key]

        tgt['txt_label'].setEnabled(status)
        tgt['lineedit'].setEnabled(status)

    def get_int_args(self, key):
        return {key: int(self.ints[key]['lineedit'].text())}


class FloatTxtMixin():
    def add_float_ctrls(self, key, txt, rownum, **kwargs):
        if not hasattr(self, 'floats'):
            self.floats = {}

        self.floats[key] = {}

        tgt = self.floats[key]

        tgt['txt_label'] = QLabel(txt)

        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        validator = QDoubleValidator(kwargs.get('min', -9999999.0),
                                     kwargs.get('max', 9999999.0),
                                     kwargs.get('dp', 3))

        validator.setNotation(QDoubleValidator.StandardNotation)

        tgt['lineedit'] = QLineEdit()
        tgt['lineedit'].setValidator(validator)

        if 'default' in kwargs:
            tgt['lineedit'].setText(str(kwargs['default']))

        self.customLayout.addWidget(tgt['lineedit'], rownum, 1)

    def set_float_enabled(self, key, status):
        tgt = self.floats[key]

        tgt['txt_label'].setEnabled(status)
        tgt['lineedit'].setEnabled(status)

    def get_float_args(self, key):
        return {key: float(self.floats[key]['lineedit'].text())}


class ListCtrlMixin():
    def add_listctrl_ctrls(self, key, txt, rownum, choices):
        if not hasattr(self, 'listctrls'):
            self.listctrls = {}

        self.listctrls[key] = {}

        tgt = self.listctrls[key]

        tgt['txt_label'] = QLabel(txt)
        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        tgt['combo'] = QComboBox()
        tgt['combo'].addItems(choices)
        self.customLayout.addWidget(tgt['combo'], rownum, 1)

    def set_listctrl_enabled(self, key, status):
        tgt = self.listctrls[key]

        tgt['txt_label'].setEnabled(status)
        tgt['combo'].setEnabled(status)

    def get_listctrl_args(self, key):
        return {key: self.listctrls[key]['combo'].currentText()}


class MinMaxMixin():
    def add_minmax_ctrls(self, key, txt, rownum, check=True, delta=False,
                         validator_mode=None):
        if not hasattr(self, 'minmax'):
            self.minmax = {}

        self.minmax[key] = {}

        tgt = self.minmax[key]

        # Whether to show the delta option
        tgt['use_delta'] = delta

        # Whether to show the check option
        tgt['use_check'] = check

        # min and max options
        validator = QDoubleValidator(-999999.0, 999999.0, 3)

        if validator_mode is None:
            notation = QDoubleValidator.StandardNotation
        else:
            notation = validator_mode

        validator.setNotation(notation)

        tgt['txt_label'] = QLabel(f'Set {txt} min/max')
        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        tgt['check'] = QCheckBox('')
        self.customLayout.addWidget(tgt['check'], rownum, 1)

        if not tgt['use_check']:
            tgt['check'].setVisible(False)

        tgt['min_label'] = QLabel('Min: ')
        self.customLayout.addWidget(tgt['min_label'], rownum, 2)

        tgt['min_edit'] = QLineEdit()
        tgt['min_edit'].setValidator(validator)
        self.customLayout.addWidget(tgt['min_edit'], rownum, 3)

        tgt['max_label'] = QLabel('Max: ')
        self.customLayout.addWidget(tgt['max_label'], rownum, 4)

        tgt['max_edit'] = QLineEdit()
        tgt['max_edit'].setValidator(validator)
        self.customLayout.addWidget(tgt['max_edit'], rownum, 5)

        tgt['delta_label'] = QLabel('Delta: ')
        self.customLayout.addWidget(tgt['delta_label'], rownum, 6)

        tgt['delta_edit'] = QLineEdit()
        tgt['delta_edit'].setValidator(validator)
        self.customLayout.addWidget(tgt['delta_edit'], rownum, 7)

        if not tgt['use_delta']:
            tgt['delta_label'].setVisible(False)
            tgt['delta_edit'].setVisible(False)

        if tgt['use_check']:
            tgt['check'].stateChanged.connect(tgt['min_label'].setEnabled)
            tgt['check'].stateChanged.connect(tgt['min_edit'].setEnabled)
            tgt['check'].stateChanged.connect(tgt['max_label'].setEnabled)
            tgt['check'].stateChanged.connect(tgt['max_edit'].setEnabled)
            tgt['check'].stateChanged.connect(tgt['delta_label'].setEnabled)
            tgt['check'].stateChanged.connect(tgt['delta_edit'].setEnabled)

            tgt['check'].setChecked(False)

            tgt['min_label'].setEnabled(False)
            tgt['min_edit'].setEnabled(False)
            tgt['max_label'].setEnabled(False)
            tgt['max_edit'].setEnabled(False)

    def set_minmax_enabled(self, key, status):
        tgt = self.minmax[key]

        tgt['txt_label'].setEnabled(status)
        tgt['check'].setEnabled(status)

        int_label_status = (tgt['use_check'] and tgt['check'].isChecked()) or (not tgt['use_check'])
        label_status = status and int_label_status
        delta_status = label_status and tgt['use_delta']

        tgt['min_label'].setEnabled(label_status)
        tgt['min_edit'].setEnabled(label_status)
        tgt['max_label'].setEnabled(label_status)
        tgt['max_edit'].setEnabled(label_status)
        tgt['delta_label'].setEnabled(delta_status)
        tgt['delta_edit'].setEnabled(delta_status)

    def get_minmax_args(self, key, *args):
        dat = {}

        tgt = self.minmax[key]

        if (not tgt['use_check']) or (tgt['check'].isChecked()):
            # We have two APIs to deal with.
            # The first is for things like ylim=(min, max)
            # and is called as get_minmax_args('y', 'ylim')
            # The second is for things like vmin and vmax
            # and is called as get_minmax_args('v', 'vmin', 'vmax', 'vdelta')
            # delta is always optional
            if len(args) == 1:
                # Need to return as a tuple
                ret = (float(tgt['min_edit'].text()),
                       float(tgt['max_edit'].text()))

                # Include delta if relevant
                if tgt['use_delta']:
                    ret = ret + (float(tgt['delta_edit'].text()), )

                dat[args[0]] = ret
            else:
                # Return as two separate values
                dat[args[0]] = float(tgt['min_edit'].text())
                dat[args[1]] = float(tgt['max_edit'].text())

                if len(args) == 3 and tgt['use_delta']:
                    dat[args[2]] = float(tgt['delta_edit'].text())

        return dat


class FloatListMixin():
    def add_floatlist_ctrls(self, key, txt, startvals, valmin, valmax, rownum):
        if not hasattr(self, 'floatlist'):
            self.floatlist = {}
            self.floatlist_mapper_set = QSignalMapper(self)
            self.floatlist_mapper_set.mapped[str].connect(self.floatlist_set_values)

            self.floatlist_mapper_clear = QSignalMapper(self)
            self.floatlist_mapper_clear.mapped[str].connect(self.floatlist_clear_values)

        self.floatlist[key] = {}

        tgt = self.floatlist[key]

        tgt['description'] = txt

        tgt['valmin'] = valmin
        tgt['valmax'] = valmax

        tgt['txt_label'] = QLabel(txt)
        self.customLayout.addWidget(tgt['txt_label'], rownum, 0)

        # Try and use the start values if valid
        if startvals is not None and isinstance(startvals, list):
            try:
                startvals = [float(x) for x in startvals]

                ok = True

                for value in startvals:
                    if value < valmin or value > valmax:
                        ok = False

                if not ok:
                    startvals = None

            except Exception:
                startvals = None
        else:
            startvals = None

        tgt['values'] = startvals
        tgt['values_label'] = QLabel('Automatic')

        if startvals is not None:
            if len(startvals) < 4:
                txt = ','.join([f'{x:.3f}' for x in startvals])
                tgt['values_label'].setText(txt)
            else:
                tgt['values_label'].setText(f'{len(startvals)} values')

        self.customLayout.addWidget(tgt['values_label'], rownum, 3)

        tgt['button_set'] = QPushButton('Set')
        self.customLayout.addWidget(tgt['button_set'], rownum, 1)

        tgt['button_clear'] = QPushButton('Clear')
        self.customLayout.addWidget(tgt['button_clear'], rownum, 2)

        # Handle mapping multiple buttons to the same routine
        action = QAction(key, self)
        self.floatlist_mapper_set.setMapping(action, key)
        action.triggered.connect(self.floatlist_mapper_set.map)
        tgt['button_set'].clicked.connect(action.trigger)

        action = QAction(key, self)
        self.floatlist_mapper_clear.setMapping(action, key)
        action.triggered.connect(self.floatlist_mapper_clear.map)
        tgt['button_clear'].clicked.connect(action.trigger)

    def set_floatlist_enabled(self, key, status):
        tgt = self.minmax[key]

        tgt['txt_label'].setEnabled(status)
        tgt['values_label'].setEnabled(status)
        tgt['button_set'].setEnabled(status)
        tgt['button_clear'].setEnabled(status)

    def floatlist_set_values(self, key):
        tgt = self.floatlist[key]

        if tgt['values'] is None:
            starttext = ''
        else:
            starttext = ','.join([str(x) for x in tgt['values']])

        # Show the dialog then validate the result
        while True:
            res, ok = QInputDialog.getText(self, f'Enter {tgt["description"]}',
                                           f'Enter comma separated {tgt["description"]} values:',
                                           text=starttext)

            if not ok:
                return

            try:
                values = [float(x) for x in res.split(',')]

                ok = True

                for value in values:
                    if value < tgt['valmin'] or value > tgt['valmax']:
                        ok = False

                        prob = f"Values must be in the range {tgt['valmin']:.3f} to {tgt['valmax']:.3f}"
                        QMessageBox.warning(self, "Invalid Values", prob)

                        # No point in checking the rest
                        break

                if ok:
                    break

            except Exception:
                QMessageBox.warning(self, "Invalid Values",
                                    "Values must be comma separated floats")

        tgt['values'] = values

        if len(values) < 4:
            txt = ','.join([f'{x:.3f}' for x in values])
            tgt['values_label'].setText(txt)
        else:
            tgt['values_label'].setText(f'{len(values)} values')

    def floatlist_clear_values(self, key):
        tgt = self.floatlist[key]

        tgt['values'] = None
        tgt['values_label'].setText('Automatic')

    def get_floatlist_args(self, key, *args):
        dat = {}

        vals = self.floatlist[key]['values']

        if vals is not None:
            dat[key] = vals

        return dat
