
def test_hello(qtbot):
    from yne.ui.study_def_app import StudyDefWin

    app = StudyDefWin()

    qtbot.addWidget(app)

    assert hasattr(app, 'studyParams')
