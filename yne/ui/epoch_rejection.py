#!/usr/bin/env python3

# vim: set expandtab ts=4 sw=4:

# This file is part of the NeuroImaging Analysis Framework
# For copyright and redistribution details, please see the COPYING file

import sys
from os.path import join
from copy import deepcopy

import numpy as np

import matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QMessageBox, QFrame, QMainWindow, QApplication,
                             QPushButton, QVBoxLayout, QLabel, QLineEdit,
                             QFileDialog, QDialog)
from PyQt5.QtGui import QIntValidator
from PyQt5.uic import loadUi

from yne import (Condition, load_data, ChannelGroup, CHANNEL_MEG,
                 CHANNEL_MEG_REF, CHANNEL_EEG, FilterChain, BandFilter, DataBundle, SliceRejection,
                 WIDGET_PATH, YneOptions)

matplotlib.use('Qt5Agg')

# TODO: REPLACE WITH CHAN NAMES (MAYBE PUT IN LIBRARY SOMEWHERE)
sensorder = (61, 62, 63, 64,
             86, 87, 88, 89, 90, 91, 92, 93,
             117, 118, 119, 120, 121, 122, 123, 124, 125,
             149, 150, 151, 152, 153,
             176, 177, 195,
             65, 66, 67, 68, 69, 70, 71,
             94, 95, 96, 97, 98, 99, 100, 101,
             126, 127, 128, 129, 130, 131, 132, 133,
             154, 155, 156, 157, 158, 159, 160,
             178, 179, 180, 181, 182,
             196, 197, 198, 199,
             212, 213, 214, 215, 216, 229, 230, 231, 232, 233, 234,
             1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
             17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
             31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
             45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
             59, 60, 79, 80, 81, 82, 83, 84, 85,
             109, 110, 111, 112, 113, 114, 115, 116,
             141, 142, 143, 144, 145, 146, 147, 148,
             169, 170, 171, 172, 173, 174, 175,
             190, 191, 192, 193, 194,
             208, 209, 210, 211,
             224, 225, 226, 227, 228,
             243, 244, 245, 246, 247, 248,
             72, 73, 74, 75, 76, 77, 78,
             102, 103, 104, 105, 106, 107, 108,
             134, 135, 136, 137, 138, 139, 140,
             161, 162, 163, 164, 165, 166, 167, 168,
             183, 184, 185, 186, 187, 188, 189,
             200, 201, 202, 203, 204, 205, 206, 207,
             217, 218, 219, 220, 221, 222, 223,
             235, 236, 237, 238, 239, 240, 241, 242)


def Mplot(fig, arg1, arg2, colstr='k'):
    myargs = []
    for i in range(arg2.shape[1]):
        myargs.append(arg1)           # always the same x axis vector
        myargs.append(arg2[:, i])
        myargs.append(colstr)
    fig.plot(*myargs)


class FirstPassToolbar(NavigationToolbar2QT):
    def __init__(self, canvas, parent):
        NavigationToolbar2QT.__init__(self, canvas, parent)

        # Remove basic elements
        self.clear()

        self.addSeparator()

        self.cont_button = QPushButton("Continue")
        self.addWidget(self.cont_button)
        self.cont_button.clicked.connect(parent._on_continue)
        self.addSeparator()

        self.pass1_done_button = QPushButton("First Pass Done")
        self.addWidget(self.pass1_done_button)
        self.pass1_done_button.clicked.connect(parent._on_first_pass_done)
        self.addSeparator()

        minepoch = str((parent.datarange * parent.epochcode).max(axis=0).max(axis=0).argmax() + 1)
        maxepoch = str((parent.datarange*parent.epochcode).min(axis=0).min(axis=0).argmin() + 1)

        msg = f"Max value in epoch number {maxepoch}, min in epoch {minepoch}"
        self.min_max_label = QLabel(msg)
        self.addWidget(self.min_max_label)


class SecondPassToolbar(NavigationToolbar2QT):
    def __init__(self, canvas, parent):
        NavigationToolbar2QT.__init__(self, canvas, parent)

        # Remove basic elements
        self.clear()

        self.addSeparator()

        self.back_button = QPushButton("Go Back")
        self.addWidget(self.back_button)
        self.back_button.clicked.connect(parent.parent.SecondStagePrev)
        self.addSeparator()

        self.reject_button = QPushButton("Reject Epoch")
        self.addWidget(self.reject_button)
        self.reject_button.clicked.connect(parent._on_reject)
        self.addSeparator()

        self.next_button = QPushButton("Next Epoch")
        self.addWidget(self.next_button)
        self.next_button.clicked.connect(parent.parent.SecondStageNext)
        self.addSeparator()

        self.save_button = QPushButton("Save")
        self.addWidget(self.save_button)
        self.save_button.clicked.connect(parent._on_save)
        self.addSeparator()

        self.quit_button = QPushButton("Quit")
        self.addWidget(self.quit_button)
        self.quit_button.clicked.connect(parent._on_quit)
        self.addSeparator()

        self.rescale_button = QPushButton("Rescale plot")
        self.addWidget(self.rescale_button)
        self.rescale_button.clicked.connect(parent._on_rescale)
        self.addSeparator()

        self.goto_button = QPushButton("Go To")
        self.addWidget(self.goto_button)
        self.goto_button.clicked.connect(parent._on_goto)

        self.epoch_label = QLabel("Epoch Number")
        self.addWidget(self.epoch_label)

        self.epoch_lineedit = QLineEdit()
        self.epoch_lineedit.setValidator(QIntValidator(1, len(parent.epochcode)))
        self.epoch_lineedit.returnPressed.connect(parent._return_pressed)
        self.addWidget(self.epoch_lineedit)

        self.totalepoch_label = QLabel('of %i' % len(parent.epochcode))
        self.addWidget(self.totalepoch_label)


class InputInfo(QDialog):
    def __init__(self, filename=None, pretrig_dur=None, epoch_dur=None, slice_filename=None):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'epochRejOpen.ui'), self)

        self.setWindowTitle('Load data for epoch rejection')

        # Set up validators
        self.pretrig_dur_edit.setValidator(QIntValidator(0, 500000))
        self.epoch_dur_edit.setValidator(QIntValidator(0, 500000))

        # Buttons
        self.file_open_button.clicked.connect(self.open_datafile)
        self.sf_open_button.clicked.connect(self.open_slicefile)
        self.sf_clear_button.clicked.connect(self.clear_slicefile)

        # Initialise fields as best as we can
        if filename is None:
            self.filename_label.setText('None')
        else:
            self.filename_label.setText(filename)

        if slice_filename is None:
            self.sf_label.setText('None')
        else:
            self.sf_label.setText(slice_filename)

        # Try and use values we were passed
        try:
            if pretrig_dur is not None:
                pretrig_dur = int(pretrig_dur)

            self.pretrig_dur_edit.setText(pretrig_dur)
        except TypeError:
            pass

        try:
            if epoch_dur is not None:
                epoch_dur = int(epoch_dur)

            self.epoch_dur_edit.setText(epoch_dur)
        except TypeError:
            pass

        # Wire up buttons
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def data_filename(self):
        val = self.filename_label.text()

        if val == 'None':
            val = None

        return val

    def slice_filename(self):
        val = self.sf_label.text()

        if val == 'None':
            val = None

        return val

    def pretrig_dur(self):
        val = None

        try:
            val = int(self.pretrig_dur_edit.text())
        except TypeError:
            pass

        return val

    def epoch_dur(self):
        val = None

        try:
            val = int(self.epoch_dur_edit.text())
        except TypeError:
            pass

        return val

    def open_datafile(self):
        types = "BTI (c c,rf* e e,rf*);;All files (*.*)"

        path, _ = QFileDialog.getOpenFileName(self,
                                              'Data',
                                              YneOptions().meg_storage_dir,
                                              types)

        if path == '':
            return

        self.filename_label.setText(path)

    def open_slicefile(self):
        types = "Slice File (*.txt);;All files (*.*)"

        path, _ = QFileDialog.getOpenFileName(self,
                                              'Slice File',
                                              YneOptions().meg_storage_dir,
                                              types)

        if path == '':
            return

        self.sf_label.setText(path)

    def clear_slicefile(self):
        self.sf_label.setText('None')


class YNEEpochRejectMain(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setCentralWidget(self.LoadingScreen())

    def parse_args(self, arg):

        complete = False

        while not complete:

            pretrig_dur_int = None
            epoch_dur_int = None

            try:
                pretrig_dur_int = int(arg['pretrig'])
                epoch_dur_int = int(arg['duration'])
            except TypeError:
                pass

            filename_missing = arg['filename'] is None
            slicefile_missing = arg['sliceFile'] is None
            preepoch_missing = (pretrig_dur_int is None) or (epoch_dur_int is None)

            if filename_missing or (preepoch_missing and slicefile_missing):
                # Show GUI to get arguments
                dlg = InputInfo(arg['filename'], pretrig_dur_int, epoch_dur_int, arg['sliceFile'])

                # If they cancel, close the application
                if not dlg.exec_():
                    self.close()

                arg['filename'] = dlg.data_filename()
                arg['sliceFile'] = dlg.slice_filename()
                arg['pretrig'] = dlg.pretrig_dur()
                arg['duration'] = dlg.epoch_dur()

                dlg.close()

                QApplication.processEvents()
            else:
                complete = True

        # Compulsory arguments: we have a GUI for these
        self.filename = arg['filename']
        self.slice_file = arg['sliceFile']
        self.pretrig_dur = pretrig_dur_int
        self.epoch_dur = epoch_dur_int

        self.badchannels = arg['badchannels']
        self.meg_threshold = arg['meg-threshold']

        self.rejects = SliceRejection()

        # Used to track unsaved changes
        self.last_saved_reject = None

        # TODO: Tidy this up later
        self.arg = arg

        # Lets any dialog disappear before we start loading
        QApplication.processEvents()

    def close_app(self):
        close_ok = False

        if self.last_saved_reject is None:
            # There are no changes
            if len(self.rejects) == 0:
                close_ok = True
        elif self.last_saved_reject == self.rejects:
            close_ok = True

        if not close_ok:
            msg = "You have unsaved changes.  Do you wish to quit without saving?"
            resp = QMessageBox.question(self, "Exit Application", msg,
                                        QMessageBox.Yes | QMessageBox.No)

            if resp == QMessageBox.Yes:
                close_ok = True

        if close_ok:
            self.close()

    def save_reject(self):
        types = "Txt file (*.txt);;"

        try:
            out_filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save Slice Reject File',
                                                          filter=types)

            # Deal with cancel
            if out_filename == '':
                return False

            self.rejects.save(out_filename)

            self.last_saved_reject = deepcopy(self.rejects)

        except Exception as e:
            QMessageBox.warning(self, "Save Failed",
                                f"Could not save to file.  ({e})")

            return False

        return True

    def load_and_run(self, arg):
        self.parse_args(arg)

        # TODO: MOVE LOADING INTO ITS OWN ROUTINE TO CLEAN UP
        try:
            self.m = load_data(self.filename,
                               pretrig_dur=self.pretrig_dur,
                               epoch_dur=self.epoch_dur,
                               slice_file=self.slice_file)
        except IOError:
            QMessageBox.warning(self, "Nonexistent MEG File"
                                "The MEG input file you specified does not exist. Exiting.")
            sys.exit(1)

        # Force pretrig_dur and epoch_dur if we were using a slice file
        if self.slice_file is not None:
            self.pretrig_dur = 0
            temp = np.array(self.slices)
            self.epoch_dur = 1000.0*max(temp[:, 1]-temp[:, 0])/self.m.sample_rate

        if len(self.slices) == 0:
            QMessageBox.critical(self, "No Epochs To View",
                                 "The arguments you provided produce no epochs to view. Exiting.")
            sys.exit(1)

        # Set up some empty bundle info we can use, the name is irrelevant. Passing
        # no args will give us all epochs, which we want
        c = Condition('all')
        # TODO: TRIG/GROUP LIST NEVER USED HERE
        self.cl = ChannelGroup(name='ALLMEGCHANS', channelgroup=CHANNEL_MEG)
        if self.arg['EEG']:
            self.rl = ChannelGroup(name='EEG', channelgroup=CHANNEL_EEG)
        else:
            self.rl = ChannelGroup(name='REFCOILS', channelgroup=CHANNEL_MEG_REF)

        self.dfc = FilterChain()

        if self.arg['lowFreq'] or self.arg['highFreq'] is not None:
            self.dfc.append_filter(BandFilter(low_freq=self.arg['lowFreq'], high_freq=self.arg['highFreq']))
        else:
            # Need to at least remove DC
            self.dfc.add_filter(BandFilter(low_freq=0.1))

        if self.arg['notchFilter']:
            self.dfc.append_filter(BandFilter(low_freq=51.0, high_freq=49.0))

        # Store the filtered data for later use
        self.mne_data = self.dfc.filter_data(self.m)

        db = DataBundle(data=self.m,
                        chanset=self.cl,
                        condition=c,
                        window=None,
                        filterchain=self.dfc)

        # We've already filtered the data so use the epoch generator
        data_gen = db.epoch_generator(self.mne_data)
        meta = next(data_gen)
        self.num_epochs = meta.num_epochs
        self.eplen = meta.num_points

        self.datarange = np.zeros((self.eplen, 2, meta.num_epochs))
        self.epchrange = np.zeros((len(meta.chans), 2, meta.num_epochs))

        if self.arg['inputSlice']:
            self.ReadInputSlice(self.arg['inputSlice'])

        rejs = None
        # Sort out and pre-rejection codes
        if self.arg['trigs'] is not None:
            cr = Condition('rej', trigger_list=self.arg['trigs'])
            crl = self.m.epochdefiner.get_epoch_list(cr)
            rejs = crl
        if self.arg['groups'] is not None:
            cr = Condition('rej', group_list=self.arg['groups'])
            crl = self.m.epochdefiner.get_epoch_list(cr)
            if rejs is None:
                rejs = crl
            else:
                rejs = list(self.arg['join'](set(rejs), set(crl)))
        if self.arg['resps'] is not None:
            cr = Condition('rej', response_list=self.arg['resps'])
            crl = self.m.epochdefiner.get_epoch_list(cr)
            if rejs is None:
                rejs = crl
            else:
                rejs = list(self.arg['join'](set(rejs), set(crl)))
                print(rejs)

        if rejs is not None:
            rejs.sort()
            print("Pre-rejecting %i epochs" % len(rejs))
            for i in rejs:
                # Remember to subtract 1 as the epoch numbers are 1 indexed
                # while the list is 0 indexed
                self.rejects.add_reject_range(self.m.epochdefiner.epochs[i-1])
            self.rejects.sanitise()

        sf1 = 0.5*10**(int(np.log10(0.6*self.epoch_dur)))
        sf2 = int(0.7+float(0.6*self.epoch_dur) /
                  (3*10**(int(np.log10(0.6*self.epoch_dur)))))

        self.xt = (np.arange(9) - 4) * sf1 * sf2

        sample_dur = 1000 / self.m.sample_rate

        self.t = (np.arange(self.eplen) * sample_dur) - self.pretrig_dur

        for j in range(meta.num_epochs):
            sys.stdout.write("\rProcessing epoch %s/%s" % (j+1, meta.num_epochs))
            sys.stdout.flush()

            # Note the .T as this program is written for the
            # old order of data
            data = next(data_gen).T

            # Append slices. This allows us to use other code.
            # Can use the data gen, but as a quick fix to use
            # existing code, just d this

            slicestart = data[:, np.setdiff1d(np.arange(data.shape[1]), np.array(self.badchannels)-1)].max(1)
            self.datarange[0:data.shape[0], 0, j] = slicestart

            sliceend = data[:, np.setdiff1d(np.arange(data.shape[1]), np.array(self.badchannels)-1)].min(1)
            self.datarange[0:data.shape[0], 1, j] = sliceend

            self.epchrange[:, 0, j] = data.max(0)
            self.epchrange[:, 1, j] = data.min(0)
            # Reject epochs above the magnitude threshold if specified
            if self.meg_threshold is not False:
                thr = data[:, np.setdiff1d(np.arange(data.shape[1]),
                                           np.array(self.badchannels)-1)]
                if np.abs(thr).max() > self.meg_threshold:
                    self.rejects.add_reject_range(self.m.epochdefiner.get_epoch_range(j + 1))
                    self.rejects.sanitise()
        self.rejects.sanitise()
        print("\n%s/%s epochs rejected automatically" % (self.rejects.num_rejections, meta.num_epochs))

        del data
        self.FirstPassDone = False
        self.nplot = 1
        self.CurrentFrame = None

        self.UpdateUI()

    def LoadingScreen(self):
        # Prepare a "Loading screen"
        frame = QFrame()
        vbox = QVBoxLayout()
        label = QLabel('<b>Loading...</b>')
        label.setAlignment(Qt.AlignCenter)

        vbox.addWidget(label)
        frame.setLayout(vbox)

        return frame

    def RejectEpoch(self, epochnumber):
        # We track the epoch numbers as indices (0-indexed) internally but
        # the get_epoch_range routine wants them done 1-indexed
        self.rejects.add_reject_range(self.m.epochdefiner.get_epoch_range(epochnumber + 1))
        self.rejects.sanitise()

    def UnRejectEpoch(self, epochnumber):
        # We track the epoch numbers as indices (0-indexed) internally but
        # the get_epoch_range routine wants them done 1-indexed
        self.rejects.remove_reject_range(self.m.epochdefiner.get_epoch_range(epochnumber + 1))
        self.rejects.sanitise()

    @property
    def epochcode(self):
        # Backwards compatibility routine to emulate old self.epochcode variable but in read-only mode
        # TODO: Maybe cache whether anything has changed so that we don't recompute it each time
        ec = np.ones(self.num_epochs)
        ec[self.rejects.check_ranges(self.m.epochdefiner.epochs)] = 0
        return ec

    @property
    def slices(self):
        # Backwards compatibility routine to emulate old self.slices variable but in read-only mode
        return self.m.epochdefiner.epochs

    def ReScale(self):
        scmax = (self.epchrange[np.setdiff1d(np.arange(self.epchrange.shape[0]),
                                             np.array(self.badchannels)-1), :, :]*self.epochcode).max()
        scmin = (self.epchrange[np.setdiff1d(np.arange(self.epchrange.shape[0]),
                                             np.array(self.badchannels)-1), :, :]*self.epochcode).min()
        self.sc = max((scmax, -scmin))
        self.sensorderdisp = np.argsort(sensorder)
        self.sensorderdisp = self.sensorderdisp*self.sc/2
        return self.sc

    def UpdateUI(self):
        if self.FirstPassDone is False:
            # First stage
            if self.CurrentFrame is not None:
                # Set up the first stage if we're starting it
                self.FirstPassDone = self.CurrentFrame.GetFinished()
                self.nplot = self.CurrentFrame.nplot

            if self.FirstPassDone:
                self.setCentralWidget(self.LoadingScreen())
                QApplication.processEvents()

                self.CurrentFrame = None
            else:
                # TODO: Look at moving toolbar into main window
                # instead of Frame - would allow for more consistency
                self.setCentralWidget(self.LoadingScreen())
                QApplication.processEvents()

                # If we're still in the first stage, plot the next
                # bit and return
                self.CurrentFrame = FirstPass(self, self.datarange, self.eplen,
                                              self.t, self.pretrig_dur, self.epoch_dur,
                                              self.nplot)
                self.setCentralWidget(self.CurrentFrame)
                return

        if self.CurrentFrame is None:
            # Set up the second stage if we're starting it
            self.sc = self.ReScale()

            self.finished = False

            df = 1 / (self.eplen / self.m.sample_rate)
            self.lmain = int(round(50/df))
            self.hmain = int(self.eplen - round(50/df))

            sf1 = 10**(int(np.log10(0.6*self.epoch_dur)))
            sf2 = int(0.7+float(0.6*self.epoch_dur)/(3*10**(int(np.log10(0.6*self.epoch_dur)))))

            self.xt = (np.arange(9)-4) * sf1 * sf2

            self.currentEpoch = 0
            while self.epochcode[self.currentEpoch] == 0:
                self.currentEpoch += 1

            self.CurrentFrame = SecondPass(self, self.t, self.pretrig_dur, self.epoch_dur,
                                           self.sc, self.eplen, self.sensorderdisp,
                                           sensorder, self.xt)

        # If we have a frame, pull the status and then redisplay
        else:
            self.finished = self.CurrentFrame.GetFinished()

        # Run the second stage
        # TODO: Break this bit out
        if self.finished is not True:
            # Load the data

            # TODO: UP TO HERE FIGURE OUT FILTERING HERE
            chan_picks = sorted(list(self.m.chanset.intersection(self.cl)))
            ref_picks = sorted(list(self.m.chanset.intersection(self.rl)))

            data = self.mne_data.get_data(start=self.slices[self.currentEpoch][0],
                                          stop=self.slices[self.currentEpoch][1],
                                          picks=chan_picks).T

            ref = self.mne_data.get_data(start=self.slices[self.currentEpoch][0],
                                         stop=self.slices[self.currentEpoch][1],
                                         picks=ref_picks).T

            # Remove DC: TODO
            # data = self.dfc.filter_data(data, self.m.sample_rate)
            # ref = self.dfc.filter_data(ref, self.m.sample_rate)

            # We offset and then reorder the data based on the sensor display order
            data = data - self.sensorderdisp

            for i in self.badchannels:
                data[:, i-1] = 0

            data = data[:, np.array(sensorder)-1]

            # Offset the reference channels
            ref = ref / abs(ref).max(0)

            self.CurrentFrame.DrawEp(self.currentEpoch, data, ref)
            self.setCentralWidget(self.CurrentFrame)

        else:
            self.close()

    def SecondStageNext(self):
        ne = self.currentEpoch

        while ((ne == self.currentEpoch) or (self.epochcode[ne] == 0)):
            # Move forward
            ne = ne + 1
            # If off end, go to front
            if ne >= len(self.epochcode):
                ne = 0
                continue

        self.currentEpoch = ne
        self.UpdateUI()

    def SecondStagePrev(self):
        ne = self.currentEpoch

        while ((ne == self.currentEpoch) or (self.epochcode[ne] == 0)):
            # Move back
            ne = ne - 1
            # If off front, go to end
            if ne < 0:
                ne = len(self.epochcode) - 1
                continue

        self.currentEpoch = ne
        self.UpdateUI()

    def SecondStageGoTo(self, newep):
        ne = self.currentEpoch

        if (newep.isdigit()) and (int(newep) > 0) and (int(newep) <= len(self.epochcode)):
            if (self.epochcode[int(newep) - 1] == 0):
                msg = "You have selected a rejected epoch. Do you wish to un-reject and view this epoch?"
                resp = QMessageBox.question(self, "Rejected Epoch Selected", msg,
                                            QMessageBox.Yes | QMessageBox.No)

                if resp == QMessageBox.Yes:
                    self.UnRejectEpoch(int(newep) - 1)
                else:
                    return
            ne = int(newep) - 1
            self.currentEpoch = ne
            self.UpdateUI()
        else:
            errmsg = "Must be an integer between 1 and %i" % (len(self.epochcode))
            QMessageBox.critical(self, "Error", errmsg)

    def ReadInputSlice(self, filename):
        try:
            self.rejects = SliceRejection.from_file(filename)
            if self.rejects.num_rejections == 0:
                msg = "The rejected slice rejection input file you specified is empty," + \
                      "please check you have specified the correct file."

                QMessageBox.critical(self, "Fatal Error", msg)

                sys.exit(1)

        except ValueError as e:
            msg = "Could not parse SliceRejection file %s (%s)" % (filename, str(e))
            QMessageBox.critical(self, msg, "Fatal Error")
            sys.exit(1)


# TODO: ? Is QFrame right here?
class FirstPass(QFrame):
    @property
    def epochcode(self):
        # Backwards compatibility routine to emulate old self.epochcode variable but in read-only mode
        return self.parent.epochcode

    def mousepress(self, event):
        checkax = event.inaxes
        if isinstance(checkax, matplotlib.axes.Subplot):
            plent = event.inaxes.get_position()
            pos = (int(10*plent.xmin) + 10*(9-int(10*plent.ymin)))
            if pos < 100:
                if (self.epochcode[((self.nplot-1)*100)+pos] == 1):
                    self.hli[pos].set_facecolor([1, 0.6, 0.6])
                    self.parent.RejectEpoch(((self.nplot-1)*100)+pos)
                else:
                    self.hli[pos].set_facecolor([1, 1, 1])
                    self.parent.UnRejectEpoch(((self.nplot-1)*100)+pos)

                self.canvas.draw()
        else:
            if hasattr(event, 'Skip'):
                event.Skip()

    def GetFinished(self):
        return self.finished

    def GetNplot(self):
        return self.nplot

    def _on_continue(self):
        if (self.nplot <= len(self.epochcode)/100):
            self.nplot += 1
        else:
            self.nplot = 1

        self.continuing = True
        self.parent.UpdateUI()

    def _on_close(self, evt):
        # TODO: FIGURE OUT LOGIC HERE AND CHANGE TO QT
        # (PROBABLY MOVE THIS UP TO THE MAINWINDOW)
        if self.continuing:
            self.continuing = False
            evt.Skip()
            return
        if not self.finished:
            msg = "Are you sure you wish to quit without saving?"
            resp = QMessageBox.question(self, "Quit Program", msg,
                                        QMessageBox.Yes | QMessageBox.No)
            if resp == QMessageBox.Yes:
                sys.exit(0)
        else:
            # TODO: wxism
            evt.Skip()

    def _on_first_pass_done(self):
        self.finished = True
        self.parent.UpdateUI()

    def __init__(self, parent, datarange, eplen, t, pretrig_dur, epoch_dur, nplot):
        QFrame.__init__(self)

        self.parent = parent
        self.finished = False
        self.continuing = False
        self.datarange = datarange
        self.eplen = eplen
        self.pretrig_dur = pretrig_dur
        self.epoch_dur = epoch_dur
        self.t = t
        self.nplot = nplot

        sf1 = 10**(int(np.log10(0.6*self.epoch_dur)))
        sf2 = int(0.7+float(0.6*self.epoch_dur)/(3*10**(int(np.log10(0.6*self.epoch_dur)))))
        xt = (np.arange(9)-4) * sf1 * sf2

        scmax = (datarange*self.epochcode).max()
        scmin = (datarange*self.epochcode).min()

        nsubs = min((100, len(self.epochcode) - ((nplot-1)*100)))

        self.figure = Figure()
        self.figure.subplots_adjust(0, 0.02, 1, 1, 0.01, 0.01)
        self.hli = []

        for j in range(nsubs):
            h = self.figure.add_subplot(10, 10, j+1)
            self.hli.extend([h])
            self.hli[j].plot(t, datarange[:, 1, ((self.nplot-1)*100)+j])
            self.hli[j].plot(t, datarange[:, 0, ((self.nplot-1)*100)+j])
            h.set_ylim(scmin, scmax)
            h.set_yticks([])
            h.set_xticks(xt)
            h.set_xlim(-pretrig_dur, epoch_dur-pretrig_dur)
            num = "%i" % (((self.nplot-1)*100)+j+1)
            h.text(((epoch_dur-pretrig_dur)*0.80), (scmax/2), num)
            if (self.epochcode[((self.nplot-1)*100)+j] == 0):
                h.set_facecolor([1, 0.6, 0.6])

        self.canvas = FigureCanvas(self.figure)

        self.layout = QVBoxLayout()

        self.toolbar = FirstPassToolbar(self.canvas, self)

        # TODO: Handle Qt version
        # self.Bind(wx.EVT_CLOSE, self._on_close)

        self.canvas.mpl_connect('button_press_event', self.mousepress)

        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)

        # Update the axes menu on the toolbar
        self.toolbar.update()

        self.setLayout(self.layout)


class SecondPass(QFrame):
    @property
    def epochcode(self):
        # Backwards compatibility routine to emulate old self.epochcode variable but in read-only mode
        return self.parent.epochcode

    def mousepress(self, event):
        if event.ydata is not None:
            pos = -int((event.ydata-self.sc/10)*2/self.sc)

            if pos >= 0 and pos < len(sensorder):
                if self.sensorder[pos] in self.parent.badchannels:
                    self.parent.badchannels.remove(self.sensorder[pos])
                else:
                    self.parent.badchannels.append(self.sensorder[pos])

                self.finished = False
                self.parent.UpdateUI()

    def ReScale(self):
        self.sc = self.parent.ReScale()
        self.sensorderdisp = np.argsort(sensorder)
        self.sensorderdisp = self.sensorderdisp*self.sc/2
        self.DrawLabels()
        self.finished = False
        self.parent.UpdateUI()

    def DrawLabels(self):
        self.l1.clear()
        self.l2.clear()
        self.l3.clear()
        self.l4.clear()

        for j in range(int((self.numsens/4)+0.1)):
            num = "%i" % self.sensorder[j]
            if self.sensorder[j] in self.parent.badchannels:
                self.l1.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10)
            else:
                self.l1.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10, color='r')

        for j in range(int((self.numsens/4)+0.1), int((self.numsens/2)+0.1)):
            num = "%i" % self.sensorder[j]
            if self.sensorder[j] in self.parent.badchannels:
                self.l2.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10)
            else:
                self.l2.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10, color='r')

        for j in range(int((self.numsens/2)+0.1), int((3*self.numsens/4)+0.1)):
            num = "%i" % self.sensorder[j]
            if self.sensorder[j] in self.parent.badchannels:
                self.l3.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10)
            else:
                self.l3.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10, color='r')

        for j in range(int((3*self.numsens/4)+0.1), (self.numsens)):
            num = "%i" % self.sensorder[j]
            if self.sensorder[j] in self.parent.badchannels:
                self.l4.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10)
            else:
                self.l4.text(1, -self.sensorderdisp[self.sensorder[j]-1]-self.sc/4, num, fontsize=10, color='r')

        self.l1.set_ylim(-(len(self.sensorderdisp)*self.sc/8), +self.sc/2)
        self.l1.set_yticks([])
        self.l1.set_xticks([])
        self.l1.set_xlim(0, 5)

        self.l2.set_ylim(-2*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+self.sc,
                         -(len(self.sensorderdisp)*self.sc/8)+self.sc/2)
        self.l2.set_yticks([])
        self.l2.set_xticks([])
        self.l2.set_xlim(0, 5)

        self.l3.set_ylim(-3*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(3*self.sc/2),
                         -2*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(3*self.sc/2))
        self.l3.set_yticks([])
        self.l3.set_xticks([])
        self.l3.set_xlim(0, 5)

        self.l4.set_ylim(-4*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(2*self.sc),
                         (2*self.sc)-3*(len(self.sensorderdisp)*self.sc/8+self.sc/2))
        self.l4.set_yticks([])
        self.l4.set_xticks([])
        self.l4.set_xlim(0, 5)

    def GetFinished(self):
        return self.finished

    def __init__(self, parent, ti, pretrig_dur, epoch_dur, sc, eplen, sensorderdisp, sensorder, xt):
        QFrame.__init__(self)

        self.parent = parent
        self.finished = False
        self.epochnumber = None
        self.ti = ti
        self.pretrig_dur = pretrig_dur
        self.epoch_dur = epoch_dur
        self.sc = sc
        self.eplen = eplen
        self.numsens = len(sensorder)
        self.sensorderdisp = sensorderdisp
        self.sensorder = sensorder
        self.xt = xt
        self.figure = Figure()

        self.figure.subplots_adjust(0, 0, 1, 1, 0.01, 0.01)

        self.r1 = self.figure.add_axes([0, 0, 0.23, 0.2])
        self.r2 = self.figure.add_axes([0.25, 0, 0.23, 0.2])
        self.r3 = self.figure.add_axes([0.5, 0, 0.23, 0.2])
        self.r4 = self.figure.add_axes([0.75, 0, 0.23, 0.2])

        self.d1 = self.figure.add_axes([0, 0.2, 0.23, 0.8])
        self.d2 = self.figure.add_axes([0.25, 0.2, 0.23, 0.8])
        self.d3 = self.figure.add_axes([0.5, 0.2, 0.23, 0.8])
        self.d4 = self.figure.add_axes([0.75, 0.2, 0.23, 0.8])

        self.l1 = self.figure.add_axes([0.23, 0.2, 0.02, 0.8])
        self.l2 = self.figure.add_axes([0.48, 0.2, 0.02, 0.8])
        self.l3 = self.figure.add_axes([0.73, 0.2, 0.02, 0.8])
        self.l4 = self.figure.add_axes([0.98, 0.2, 0.02, 0.8])

        self.b1 = self.figure.add_axes([0.23, 0, 0.02, 0.2])
        self.b2 = self.figure.add_axes([0.48, 0, 0.02, 0.2])
        self.b3 = self.figure.add_axes([0.73, 0, 0.02, 0.2])
        self.b4 = self.figure.add_axes([0.98, 0, 0.02, 0.2])

        self.b1.set_yticks([])
        self.b1.set_xticks([])
        self.b2.set_yticks([])
        self.b2.set_xticks([])
        self.b3.set_yticks([])
        self.b3.set_xticks([])
        self.b4.set_yticks([])
        self.b4.set_xticks([])

        self.canvas = FigureCanvas(self.figure)

        self.DrawLabels()

        self.layout = QVBoxLayout()

        self.toolbar = SecondPassToolbar(self.canvas, self)

        self.canvas.mpl_connect('button_press_event', self.mousepress)

        # TODO: Figure out QT version: MOVE TO Main Window
        # self.Bind(wx.EVT_CLOSE, self._on_close)

        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)

        # update the axes menu on the toolbar
        self.toolbar.update()

        # Set our layout
        self.setLayout(self.layout)

    def _on_rescale(self):
        self.ReScale()

    def _on_reject(self):
        self.parent.RejectEpoch(self.epochnumber)
        self.parent.SecondStageNext()
        self.parent.UpdateUI()

    def _on_close(self, evt):
        # TODO: FIGURE OUT LOGIC HERE AND CHANGE TO QT
        # (PROBABLY MOVE THIS UP TO THE MAINWINDOW)
        msg = "You are about to quit the programme without saving your rejected slices. " \
              "Do you wish to save a rejection file before closing?"

        buttons = QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel

        resp = QMessageBox.question(self, "Quit Program", msg, buttons)
        if resp == QMessageBox.Yes:
            self._on_finish()
        elif resp == QMessageBox.No:
            # TODO: Change evt for QT
            evt.Skip()
            sys.exit(0)
        else:
            return

    def _on_save(self):
        self.parent.save_reject()

    def _on_quit(self):
        self.parent.close_app()

    def _on_goto(self):
        txt = self.toolbar.epoch_lineedit.talue()
        self.parent.SecondStageGoTo(txt)
        self.parent.UpdateUI()

    def _return_pressed(self):
        txt = self.toolbar.epoch_lineedit.text()
        self.parent.SecondStageGoTo(txt)
        self.parent.UpdateUI()

    def DrawEp(self, epochnumber, data, ref):
        self.epochnumber = epochnumber

        self.toolbar.epoch_lineedit.setText('%s' % (self.epochnumber+1))
        self.toolbar.update()

        self.r1.clear()
        self.r2.clear()
        self.r3.clear()
        self.r4.clear()

        numref = ref.shape[1]
        cur_ep_len = ref.shape[0]

        for j in range(int(round(numref/4))):
            self.r1.plot(self.ti[0:cur_ep_len], ref[:, j]+j, 'r')

        self.r1.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))
        self.r1.set_yticks([])

        for j in range(int(round(numref/4)), int(round(numref/2))):
            self.r2.plot(self.ti[0:cur_ep_len], ref[:, j]+j, 'r')

        self.r2.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))
        self.r2.set_yticks([])

        for j in range(int(round(numref/2)), int(round(3*numref/4))):
            self.r3.plot(self.ti[0:cur_ep_len], ref[:, j]+j, 'r')

        self.r3.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))
        self.r3.set_yticks([])

        for j in range(int(round(3*numref/4)), numref):
            self.r4.plot(self.ti[0:cur_ep_len], ref[:, j]+j, 'r')

        self.r4.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))
        self.r4.set_yticks([])

        self.d1.clear()
        self.d2.clear()
        self.d3.clear()
        self.d4.clear()

        j = int((self.numsens / 4) + 0.1)

        # TODO: Make this more generic; especially labelling
        # Should be configurable on a per-system basis to stop it being
        # BTI-WHS3600 only
        Mplot(self.d1, self.ti[0:cur_ep_len], data[:, :int((self.numsens/4)+0.1)])

        self.d1.plot(self.ti, np.ones(self.eplen)*(-28.5)*self.sc/2, 'r', linewidth=2)

        self.d1.text(-self.pretrig_dur, -27.5*self.sc/2, 'Anterior',
                     fontsize=12, color='r')

        self.d1.text(-self.pretrig_dur, -30.5*self.sc/2, 'Left',
                     fontsize=12, color='r')

        self.d1.set_ylim(-(len(self.sensorderdisp)*self.sc/8), +self.sc/2)
        self.d1.set_yticks([])
        self.d1.set_xticks(self.xt)
        self.d1.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))

        Mplot(self.d2, self.ti[0:cur_ep_len], data[:, int((self.numsens/4)+0.1):int((self.numsens/2)+0.1)])

        self.d2.plot(self.ti, np.ones(self.eplen)*(-78.5)*self.sc/2, 'r', linewidth=2)
        self.d2.text(-self.pretrig_dur, -77.5*self.sc/2, 'Left', fontsize=12, color='r')
        self.d2.text(-self.pretrig_dur, -80.5*self.sc/2, 'Central', fontsize=12, color='r')

        self.d2.set_ylim(-2*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+self.sc,
                         -(len(self.sensorderdisp)*self.sc/8)+self.sc/2)
        self.d2.set_yticks([])
        self.d2.set_xticks(self.xt)
        self.d2.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))

        Mplot(self.d3, self.ti[0:cur_ep_len], data[:, int((self.numsens/2)+0.1):int((3*self.numsens/4)+0.1)])

        self.d3.plot(self.ti, np.ones(self.eplen)*(-138.5)*self.sc/2, 'r', linewidth=2)
        self.d3.text(-self.pretrig_dur, -137.5*self.sc/2, 'Central', fontsize=12, color='r')
        self.d3.text(-self.pretrig_dur, -140.5*self.sc/2, 'Right', fontsize=12, color='r')
        self.d3.set_ylim(-3*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(3*self.sc/2),
                         -2*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(3*self.sc/2))
        self.d3.set_yticks([])
        self.d3.set_xticks(self.xt)
        self.d3.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))

        Mplot(self.d4, self.ti[0:cur_ep_len], data[:, int((3*self.numsens/4)+0.1):])

        self.d4.plot(self.ti, np.ones(self.eplen)*(-188.5)*self.sc/2, 'r', linewidth=2)
        self.d4.text(-self.pretrig_dur, -187.5*self.sc/2, 'Right', fontsize=12, color='r')
        self.d4.text(-self.pretrig_dur, -190.5*self.sc/2, 'Posterior', fontsize=12, color='r')
        self.d4.set_ylim(-4*(len(self.sensorderdisp)*self.sc/8+self.sc/2)+(2*self.sc),
                         (2*self.sc)-3*(len(self.sensorderdisp)*self.sc/8+self.sc/2))
        self.d4.set_yticks([])
        self.d4.set_xticks(self.xt)
        self.d4.set_xlim(-self.pretrig_dur, 1*(self.epoch_dur-self.pretrig_dur))

        self.canvas.draw()
