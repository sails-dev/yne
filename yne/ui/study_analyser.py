#!/usr/bin/python3

from os.path import join, exists

from PyQt5.QtCore import QThreadPool
from PyQt5.QtWidgets import (QFileDialog, QMessageBox, QMainWindow, QApplication)
from PyQt5.uic import loadUi

from yne import (WIDGET_PATH, Study, YneOptions)

import mne
from mne.gui._coreg_gui import CoregFrame, _make_view as make_coreg_view

from .sensor_epoch import (SensorEpochResultUI, SensorEpochResult,
                           configure_sensor_epoch, configure_load_epoch)
from .sensor_average import configure_load_averages, SensorAverageResultUI
from .sensor_mvpa import configure_load_mvpa, SensorMVPAResultUI
from .window_register import WindowRegister


class YNEStudyAnalyser(QMainWindow):
    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self)

        loadUi(join(WIDGET_PATH, 'studyAnalyser.ui'), self)

        self.study = None

        self.setup_menu()

        self.threadpool = QThreadPool()
        self.proc_object = None

        if len(args) == 1 and args[0] is not None:
            self.load_file(args[0])

    # def add_log(self, txt):
    #    logtime = f'<tt>{get_log_time()} : </tt>'
    #
    #    self.procSteps.append(logtime + txt)

    def setup_menu(self):
        # File menu
        self.action_Open.triggered.connect(self.open_data)
        self.action_Exit.triggered.connect(self.close)

        # Open existing analysis submenu
        self.actionLoad_MNE_Epochs.triggered.connect(self.open_sensor_epoch)
        self.actionLoad_Epochs_StudyFile.triggered.connect(self.open_epochs_study)
        self.actionLoad_Avg_StudyFile.triggered.connect(self.open_avg_study)
        self.actionLoad_MVPA_StudyFile.triggered.connect(self.open_mvpa_study)

        # Analysis menu
        self.action_SensorEpoch.triggered.connect(self.sensor_epoch)

        # Coreg button
        self.study_frame.coregMenuButton.clicked.connect(self.open_coreg)

    def load_file(self, filename):
        # Try and guess what the file is
        if filename.endswith('yaml'):
            # Assume study
            self.load_study(filename)
            return

        elif filename.endswith('.fif') or filename.endswith('.fif.gz'):
            is_epoch = False

            # Try epoch mode
            try:
                mne.read_epochs(filename, preload=False)
                is_epoch = True
            except Exception:
                is_epoch = False

            if is_epoch:
                self.load_sensor_epoch(filename)
                return

        QMessageBox.warning(self,
                            "Error",
                            f"Could not understand file {filename}")

    def open_data(self):
        types = "YNE Study File (*.yaml);;All Files (*.*)"

        path, typ = QFileDialog.getOpenFileName(self,
                                                'Study to load',
                                                filter=types)

        if path == '':
            return

        self.load_study(path)

    def load_study(self, path):
        # Try and load the study
        try:
            self.study = Study.from_yaml_file(path)
        except Exception as e:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to read study from file {path} ({e})")
            return

        # Ram the filename on the side of the study object for now
        self.study.filename = path

        self.study_frame.setup_study(self.study, None)

        self.menu_Analysis.setEnabled(True)

        # Enable actions which need a study file
        self.actionLoad_Avg_StudyFile.setEnabled(True)
        self.actionLoad_Epochs_StudyFile.setEnabled(True)
        self.actionLoad_MVPA_StudyFile.setEnabled(True)

    def open_coreg(self):
        # Find our run
        subject = self.study_frame.file_choice.currentText()

        self.update_statusbar(f'{subject}: Finding MRI and headshape')

        # General Coreg settings
        guess_mri_subject = False

        # Ensure that we have the anatomy tree set up
        try:
            self.study.create_anatomy_tree_dir()
            subjects_dir = self.study.get_anatomy_tree_dir()
        except OSError:
            # Just allow them to find things themselves
            subjects_dir = None

        # Find the headshape path
        try:
            hs_name = self.study.ensure_run_headshape(subject)
        except OSError:
            # Again, allow them to find things themselves
            hs_name = None

        # If we have an existing transform, load it
        try:
            trans = self.study.get_run_coreg_filename(subject)
            if not exists(trans):
                trans = None
        except OSError:
            # Ignore not finding it
            trans = None

        # Create a symlink to the data in our directory.
        # This makes the GUI use the right name by default when we go
        # to save the transform.

        try:
            # We can't run the coreg gui in a thread because it wants to call
            # exec.  If we use the main mne.gui.coregistration call we end up
            # blocking the whole application until it is done.  So we can
            # construct the Window and run it under our application even though
            # this ends up looking awful (for reasons that I can't fathom).
            self.update_statusbar(f'{subject}: Preparing view')

            cview = make_coreg_view(tabbed=False, scrollable=False, width=1200, height=800)

            self.update_statusbar(f'{subject}: Setting up model')

            cframe = CoregFrame(raw=hs_name,
                                subject=subject,
                                subjects_dir=subjects_dir,
                                guess_mri_subject=guess_mri_subject,
                                trans=trans,
                                advanced_rendering=YneOptions().advanced_rendering)

            # Lock fiducials by default as we want to use ICP
            cframe.data_panel.lock_fiducials = True

            self.update_statusbar(f'{subject}: Opening window')

            cframe.edit_traits(view=cview)

            self.update_statusbar(None)

        except Exception as e:
            self.update_statusbar(f'Failed to start coreg window for {subject}: {e}')

    def open_sensor_epoch(self):
        types = "MNE Epochs file (*-epo.fif *-epo.fif.gz);;"

        path, typ = QFileDialog.getOpenFileName(self,
                                                'Epochs file to load',
                                                filter=types)

        if path == '':
            return

        self.load_sensor_epoch(path)

    def load_sensor_epoch(self, path):
        filenames = {path: path}
        epochs = {}

        try:
            ep = mne.read_epochs(path)
        except Exception as e:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to read epochs file {path} ({e})")
            return

        if not isinstance(ep, (mne.epochs.Epochs, mne.epochs.EpochsFIF)):
            QMessageBox.warning(self,
                                "Error",
                                f"{path} is not an MNE epochs file")
            return

        epochs[path] = ep

        result = SensorEpochResult(filenames, epochs)

        res_dialog = SensorEpochResultUI(data=result, threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

    def open_epochs_study(self):
        """
        Ask for a window, condition, filter and chanset and load the
        epoch files
        """
        # Try and load the data
        if self.proc_object is not None:
            return

        proc_object = configure_load_epoch(self, self.open_epochs_study_finished,
                                           self.update_statusbar, self.study)

        # Check if nothing to do
        if proc_object is None:
            return

        self.proc_object = proc_object

        # Execute
        self.threadpool.start(self.proc_object)

    def open_epochs_study_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to load data: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('Load Failed')

            # TODO: Anything else which needs to be put back
            return

        # Do some checking
        found_count = self.proc_object.found_count
        expected_count = self.proc_object.expected_count

        if found_count == 0:
            QMessageBox.warning(self, "Error",
                                "Could not find any files.  Can not continue")

            self.proc_object = None

            self.update_statusbar('No files found to load')

            return

        # Handle partial reads and give warning
        if found_count != expected_count:
            msg = f"Expected {expected_count} files, found {found_count}.\n"
            msg += "Do you want to continue?\n\n"
            msg += 'Missing files:\n'
            msg += '\n'.join([f'  * {x}' for x in self.proc_object.missing])

            resp = QMessageBox.question(self, "Warning", msg,
                                        QMessageBox.Yes | QMessageBox.No)

            if resp == QMessageBox.No:
                self.proc_object = None

                self.update_statusbar('Loading cancelled')

                return

        # Set up the window
        res_dialog = SensorEpochResultUI(data=self.proc_object.result,
                                         study=self.study,
                                         threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        self.update_statusbar(None)

        # Allow more processing
        # TODO: Re-enable analysis menu
        self.proc_object = None

    def open_avg_study(self):
        """
        Ask for a window, conditions, filter and chanset and load the
        epoch files
        """
        # Try and load the data
        if self.proc_object is not None:
            return

        proc_object = configure_load_averages(self, self.open_avg_study_finished,
                                              self.update_statusbar, self.study)

        # Check if nothing to do
        if proc_object is None:
            return

        self.proc_object = proc_object

        # Execute
        self.threadpool.start(self.proc_object)

    def open_avg_study_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to load averages: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('Load Failed')

            # TODO: Anything else which needs to be put back
            return

        # Do some checking
        found_count = self.proc_object.found_count
        expected_count = self.proc_object.expected_count

        if found_count == 0:
            QMessageBox.warning(self, "Error",
                                "Could not find any files.  Can not continue")

            self.proc_object = None

            self.update_statusbar('No files found to load')

            return

        # Handle partial reads and give warning
        if found_count != expected_count:
            msg = f"Expected {expected_count} files, found {found_count}.\n"
            msg += "Do you want to continue?\n\n"
            msg += 'Missing files:\n'
            msg += '\n'.join([f'  * {x}' for x in self.proc_object.missing])

            resp = QMessageBox.question(self, "Warning", msg,
                                        QMessageBox.Yes | QMessageBox.No)

            if resp == QMessageBox.No:
                self.proc_object = None

                self.update_statusbar('Loading cancelled')

                return

        res_dialog = SensorAverageResultUI(data=self.proc_object.result,
                                           study=self.study,
                                           threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        # Allow more processing
        # TODO: Re-enable analysis menu
        self.proc_object = None

    def sensor_epoch(self):
        if self.proc_object is not None:
            return

        proc_object = configure_sensor_epoch(self, self.sensor_epoch_finished,
                                             self.update_statusbar, self.study)

        # Check if nothing to do
        if proc_object is None:
            return

        self.proc_object = proc_object

        # Execute
        self.threadpool.start(self.proc_object)

    def sensor_epoch_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to analyse data: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('Analysis Failed')

            # TODO: Anything else which needs to be put back
            return

        result = self.proc_object.result

        res_dialog = SensorEpochResultUI(data=result,
                                         study=self.study,
                                         threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        self.update_statusbar(None)

        # Allow more processing
        # TODO: Re-enable analysis menu
        self.proc_object = None

    def open_mvpa_study(self):
        """
        Ask for a conditions and load the MVPA files
        """
        # Try and load the data
        if self.proc_object is not None:
            return

        proc_object = configure_load_mvpa(self, self.open_mvpa_study_finished,
                                          self.update_statusbar, self.study)

        # Check if nothing to do
        if proc_object is None:
            return

        self.proc_object = proc_object

        # Execute
        self.threadpool.start(self.proc_object)

    def open_mvpa_study_finished(self, success):
        if not success:
            QMessageBox.warning(self,
                                "Error",
                                f"Failed to load MVPAs: {self.proc_object.exception}")

            self.proc_object = None

            self.update_statusbar('Load Failed')

            # TODO: Anything else which needs to be put back
            return

        # Do some checking
        found_count = self.proc_object.found_count
        expected_count = self.proc_object.expected_count

        if found_count == 0:
            QMessageBox.warning(self, "Error",
                                "Could not find any files.  Can not continue")

            self.proc_object = None

            self.update_statusbar('No files found to load')

            return

        # Handle partial reads and give warning
        if found_count != expected_count:
            msg = f"Expected {expected_count} files, found {found_count}.\n"
            msg += "Do you want to continue?\n\n"
            msg += 'Missing files:\n'
            msg += '\n'.join([f'  * {x}' for x in self.proc_object.missing])

            resp = QMessageBox.question(self, "Warning", msg,
                                        QMessageBox.Yes | QMessageBox.No)

            if resp == QMessageBox.No:
                self.proc_object = None

                self.update_statusbar('Loading cancelled')

                return

        res_dialog = SensorMVPAResultUI(data=self.proc_object.result,
                                        study=self.study,
                                        threadpool=self.threadpool)

        WindowRegister().register_window(res_dialog)

        res_dialog.show()

        # Allow more processing
        # TODO: Re-enable analysis menu
        self.proc_object = None

    def update_statusbar(self, txt=None):
        if txt is None:
            self.statusBar().clearMessage()
        else:
            self.statusBar().showMessage(txt)

        QApplication.processEvents()
