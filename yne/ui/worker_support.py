#!/usr/bin/python3

from datetime import datetime

from PyQt5.QtCore import QObject, pyqtSignal


__all__ = []


def get_log_time():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')


class WorkerSignals(QObject):
    """
    Defines the signals available from a running worker thread
    """

    finished = pyqtSignal(bool)

    progress = pyqtSignal(str)


__all__.append('WorkerSignals')
