#!/usr/bin/python3

from os.path import join

from PyQt5.QtWidgets import (QCheckBox, QDialog)

from PyQt5.uic import loadUi

from yne import WIDGET_PATH


def MultiCheckBox(title, instructions, options, selected=None, width=1):
    mc = MultiCheckUI()

    mc.setWindowTitle(title)

    mc.instruction_label.setText(instructions)

    mc.set_options(options, selected, width)

    # Use None to indicate Cancel
    if not mc.exec_():
        return None

    return mc.get_checked()


class MultiCheckUI(QDialog):
    def __init__(self, *args, **kwargs):
        QDialog.__init__(self)

        loadUi(join(WIDGET_PATH, 'multiCheck.ui'), self)

        self.checkboxes = []

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def set_options(self, options, selected, width=1):
        if selected is None:
            selected = []

        for optnum, option in enumerate(options):

            rownum = optnum // width
            colnum = optnum % width

            cb = QCheckBox(option)

            self.grid_check.addWidget(cb, rownum, colnum)

            if option in selected:
                cb.setChecked(True)

            self.checkboxes.append(cb)

        self.layout()

    def get_checked(self):
        ret = []

        for idx, checkbox in enumerate(self.checkboxes):
            if checkbox.isChecked():
                ret.append(idx)

        return ret
