#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


__all__ = []


class WindowRegister(object):
    """
    Singleton class for keeping track of windows
    """

    __shared_state = {}

    def __init__(self, *args, **kwargs):
        # Quick way of implementing a singleton
        self.__dict__ = self.__shared_state

        if not getattr(self, 'initialised', False):
            self.initialised = True
            self.setup(*args, **kwargs)

    def setup(self):
        self.windows = {}

    def register_window(self, *args):
        if len(args) == 1 and hasattr(args[0], 'internal_window_id'):
            key = args[0].internal_window_id
            win = args[0]
        else:
            key = args[0]
            win = args[1]

        self.windows[key] = win

    # TODO: Figure out how to release references once the window is gone.
    # If we do it from the close signal, we segfault as - presumably - we
    # release the reference too early
    def delete_window(self, key):
        del self.windows[key]


__all__.append('WindowRegister')
