"""General run definers"""
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from anamnesis import AbstractAnam, register_class


__all__ = []


#######################################################################
# DataRun
#######################################################################


class DataRun(AbstractAnam):
    def __init__(self, name=None, filename=None, datatype=None):
        AbstractAnam.__init__(self)

        # Data
        self.name = name
        self.filename = filename
        self.datatype = datatype

        self.tags = list()
        self.extra_data = dict()

    def __eq__(self, other):
        if other is None:
            return False

        if not (self.name is None and other.name is None):
            if self.name != other.name:
                return False

        if not (self.filename is None and other.filename is None):
            if self.filename != other.filename:
                return False

        if not (self.datatype is None and other.datatype is None):
            if self.datatype != other.datatype:
                return False

        if self.tags != other.tags:
            return False

        if self.extra_data != other.extra_data:
            return False

        return True

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, getattr(self, 'name'))

    # Routines for handling extra values
    def has_value(self, name):
        return name in self.extra_data

    def get_value(self, name, default=None):
        return self.extra_data.get(name, default)

    def set_value(self, name, value):
        self.extra_data[name] = value

    def del_value(self, name):
        del self.extra_data[name]

    # Routines for handling tags
    def has_tag(self, tag):
        return tag in self.tags

    def add_tag(self, tag):
        if tag not in self.tags:
            self.tags.append(tag)

    def del_tag(self, tag):
        self.tags.remove(tag)

    @property
    def relative_output_dir(self):
        """
        Get the relative path to the output directory for the participant
        """
        return str(self.name)

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls(name=data['name'],
                  filename=data['filename'],
                  datatype=data['datatype'])

        ret.tags = data.get('tags', list()).copy()
        ret.extra_data = data.get('extra_data', dict()).copy()

        return ret

    def to_yaml_dict(self):
        ret = {}

        ret['name'] = self.name
        ret['datatype'] = self.datatype
        ret['filename'] = self.filename
        ret['tags'] = self.tags
        ret['extra_data'] = self.extra_data

        return ret


__all__.append('DataRun')
register_class(DataRun)


#######################################################################
# Simple dictionary based Run definer
#######################################################################


class RunDefiner(AbstractAnam):
    """
    Dictionary based implementation of RunDefiner
    """

    hdf5_outputs = ['runs']

    def __init__(self):
        """
        Initialize RunDefiner class.
        """
        AbstractAnam.__init__(self)

        self.runs = {}

    def __eq__(self, other):
        if other is None:
            return False

        return self.runs == other.runs

    def keys(self):
        return sorted(self.runs.keys())

    def has_run(self, runname):
        return runname in self.runs

    def get_run(self, runname):
        return self.runs[runname]

    def get_run_by_path(self, path):
        for runname in self.keys():
            if self.runs[runname].filename == path:
                return self.runs[runname]

        return None

    def get_runs_by_tag(self, tag):
        ret = []

        for runname in self.keys():
            if self.runs[runname].has_tag(tag):
                ret.append(self.runs[runname])

        return ret

    def add_run(self, run):
        self.runs[run.name] = run

    def del_run(self, runname):
        del self.runs[runname]

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        for entry in data:
            ret.add_run(DataRun.from_yaml_dict(entry))

        return ret

    def to_yaml_dict(self):
        ret = []
        for c in self.keys():
            ret.append(self.get_run(c).to_yaml_dict())

        return ret


register_class(RunDefiner)
__all__.append('RunDefiner')
