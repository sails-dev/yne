#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from math import floor

import yaml

from glob import glob
from os import unlink, symlink, environ, readlink, makedirs
from os.path import join, lexists, islink
from copy import copy

from anamnesis import AbstractAnam, register_class

import mne

from .channels import ChannelDefiner
from .conditions import ConditionDefiner
from .windows import WindowDefiner
from .filters import FilterChainDefiner
from .runs import RunDefiner
from .bundles import ConditionBundle, DataBundle
from .readers import AbstractMegReader, load_data


__all__ = []


class Study(AbstractAnam):
    def __init__(self, **kwds):
        AbstractAnam.__init__(self)

        self.init_variables(**kwds)

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, getattr(self, 'name', 'NONE'))

    def __eq__(self, other):
        if self.name != other.name:
            return False

        if str(self.storage_dir) != str(other.storage_dir):
            return False

        if self.epoch_dur != other.epoch_dur:
            return False

        if self.pretrig_dur != other.pretrig_dur:
            return False

        if self.chan_definer != other.chan_definer:
            return False

        if self.condition_definer != other.condition_definer:
            return False

        if self.window_definer != other.window_definer:
            return False

        if self.filter_definer != other.filter_definer:
            return False

        if self.run_definer != other.run_definer:
            return False

        return True

    def init_variables(self, **kwds):
        self.name = kwds.get('name', None)
        self.storage_dir = kwds.get('storage_dir', None)

        self.epoch_dur = kwds.get('epoch_dur', None)
        self.pretrig_dur = kwds.get('pretrig_dur', None)

        self.chan_definer = kwds.get('chan_definer', ChannelDefiner())
        self.condition_definer = kwds.get('condition_definer', ConditionDefiner())
        self.window_definer = kwds.get('window_definer', WindowDefiner())
        self.filter_definer = kwds.get('filter_definer', FilterChainDefiner())
        self.run_definer = kwds.get('run_definer', RunDefiner())

        # Instance variables which don't get read / written
        self.sub_dir = kwds.get('sub_dir', None)

    def get_data_bundle(self, runname, conditionname, windowname,
                        filtername, chansetname):
        """
        Returns a configured DataBundle object based upon the given
        keys.

        :param runname: key used to identify run from run definer
        :param conditionname: key used to identify condition from condition definer
        :param windowname: key used to identify window in window definer.  Can be None.
        :param filtername: key used to identify filter in filter definer.
        :param chansetname: key used to identify chanset in channel definer
        """

        r = self.run_definer.get_run(runname)

        c = self.condition_definer.get_condition(conditionname)

        w = self.window_definer.get_window(windowname)

        f = self.filter_definer.get_filterchain(filtername)
        cs = self.chan_definer.get_chanset(chansetname)

        return DataBundle(epoch_dur=self.epoch_dur,
                          pretrig_dur=self.pretrig_dur,
                          chanset=cs,
                          condition=c,
                          window=w,
                          filterchain=f,
                          data=self.get_run_data(r))

    def save_yaml_file(self, filename):
        out = open(filename, 'w')
        dat = self.to_yaml_dict()
        yaml.safe_dump(dat, out, encoding='utf-8', allow_unicode=True)
        out.close()

    def to_mne_epochs(self, run_name, cond_names, window_name, filter_name,
                      chanset_name, **kwds):
        """
        Convert one or more conditions into a single mne.Epochs object.

        cond_names must be a list, everything else is a string reference to the relevant
        object
        """

        if len(cond_names) == 0:
            raise Exception("No conditions found")

        # Get the relevant objects
        data = self.get_run_data(run_name)
        window = self.get_window(window_name)
        fc = self.get_filterchain(filter_name)
        chanset = self.get_chanset(chanset_name)

        pretrig_samples = int(floor(self.pretrig_dur * (data.sample_rate / 1000)))

        # Get our events array
        events = data.epochdefiner.to_mne_event_array(pretrig_samples)

        event_dict = {}

        for condname in cond_names:
            cond = self.get_condition(condname)

            # Check that the conditions are simple
            if not cond.is_simple:
                raise ValueError(f"Condition {condname} is not simple [single trigger]")

            trig_code = cond.trigger_list[0]

            event_dict[condname] = trig_code

        # Filter the data
        filt_data = fc.filter_data(data)

        # If there is no filterchain, we can end up with an AbstractMegReader
        # instead of an MNE object.  We should probably fix this in filter_data
        if isinstance(filt_data, AbstractMegReader):
            filt_data = filt_data.loader

        # Find the right channels
        chans = data.chanset.intersection(chanset).discard(data.rejected_chanset)
        picks = sorted([str(x) for x in chans])

        # Set up our window
        if window is None:
            tmin = -self.pretrig_dur
            tmax = self.epoch_dur - self.pretrig_dur
        else:
            tmin, tmax = window.get_ms()

        tmin_s = tmin / 1000.0
        tmax_s = tmax / 1000.0

        # Be careful with reject, it whinges if you provide values which aren't
        # there
        if 'reject' in kwds and isinstance(kwds['reject'], dict):
            ch_types = [mne.channel_type(filt_data.info, x) for x in range(len(filt_data.ch_names))]
            for t in list(kwds['reject'].keys()):
                if ch_types.count(t) < 1:
                    del kwds['reject'][t]

            if len(kwds['reject']) == 0:
                kwds['reject'] = None

        epochs = mne.Epochs(filt_data, events, picks=picks, tmin=tmin_s, tmax=tmax_s,
                            event_id=event_dict, preload=True, **kwds)

        return epochs

    @classmethod
    def from_yaml_file(cls, filename):
        f = open(filename, 'r')
        ret = cls.from_yaml_dict(yaml.safe_load(f))
        f.close()
        return ret

    @classmethod
    def from_yaml_dict(cls, data):
        ret = cls()

        ret.chan_definer = ChannelDefiner.from_yaml_dict(data['channels'])
        ret.condition_definer = ConditionDefiner.from_yaml_dict(data['conditions'])
        ret.window_definer = WindowDefiner.from_yaml_dict(data['windows'])
        ret.filter_definer = FilterChainDefiner.from_yaml_dict(data['filterchains'])
        ret.run_definer = RunDefiner.from_yaml_dict(data['runs'])

        # Set these at the end so that any changes propogate
        # to the definers if necessary
        ret.name = data['STUDYINFO']['name']

        # Ensure paths are str as PyYAML can't cope with Path objects
        if 'storage_dir' in data['STUDYINFO']:
            ret.storage_dir = str(data['STUDYINFO']['storage_dir'])

        if 'pretrig_dur' in data['STUDYINFO']:
            ret.pretrig_dur = int(data['STUDYINFO']['pretrig_dur'])

            if ret.pretrig_dur < 0:
                raise ValueError("You cannot specify a negative pre-trigger value")

        if 'epoch_dur' in data['STUDYINFO']:
            ret.epoch_dur = int(data['STUDYINFO']['epoch_dur'])

        return ret

    def to_yaml_dict(self):
        ret = {}

        # Start with the class metadata
        ret['STUDYINFO'] = {}
        ret['STUDYINFO']['name'] = self.name

        if self.storage_dir is not None:
            ret['STUDYINFO']['storage_dir'] = str(self.storage_dir)

        if self.pretrig_dur is not None:
            ret['STUDYINFO']['pretrig_dur'] = self.pretrig_dur

        if self.epoch_dur is not None:
            ret['STUDYINFO']['epoch_dur'] = self.epoch_dur

        ret['channels'] = self.chan_definer.to_yaml_dict()
        ret['conditions'] = self.condition_definer.to_yaml_dict()
        ret['windows'] = self.window_definer.to_yaml_dict()
        ret['filterchains'] = self.filter_definer.to_yaml_dict()
        ret['runs'] = self.run_definer.to_yaml_dict()

        return ret

    def get_output_dir(self, toplevel=False):
        if toplevel or self.sub_dir is None or self.sub_dir == '':
            return self.storage_dir
        else:
            return join(self.storage_dir, self.sub_dir)

    def get_anatomy_tree_dir(self):
        return join(self.get_output_dir(toplevel=True), 'anatomy')

    def get_headshape_tree_dir(self):
        return join(self.get_output_dir(toplevel=True), 'headshape')

    def get_run_output_dir(self, datarun, toplevel=False):
        # Cope with either object or index
        if isinstance(datarun, str):
            datarun = self.run_definer.get_run(datarun)

        return join(self.get_output_dir(toplevel), datarun.relative_output_dir)

    def get_run_coreg_filename(self, datarun):
        # Cope with either object or index
        if isinstance(datarun, str):
            datarun = self.run_definer.get_run(datarun)

        rod = datarun.relative_output_dir

        coregname = f'{rod}-trans.fif'

        return join(self.get_headshape_tree_dir(), coregname)

    def get_run_filenames_from_tags(self, tags):
        ret = []

        if isinstance(tags, str):
            tags = [tags]

        for run_name in self.run_definer.keys():
            run = self.run_definer.get_run(run_name)
            for tag in tags:
                if run.has_tag(tag):
                    ret.append(run.filename)
                    break

        return ret

    #########################################################################
    # Helper routines to get list of available keys and shortcuts to definers
    #########################################################################
    @property
    def runs(self):
        """
        Returns a list of run definer keys.  Shortcut for
        self.run_definer.keys()
        """
        return self.run_definer.keys()

    def get_run(self, key):
        """
        Returns the run object for the given key.
        Raises an error if the run does not exist
        """
        return self.run_definer.get_run(key)

    def get_run_data(self, run):
        """
        Returns an MEG reader object for the given run

        Raises an error if the run does not exist
        """

        # Cope with either a string or a DataRun object
        if isinstance(run, str):
            run = self.run_definer.get_run(run)

        epoch_dur = None
        pretrig_dur = None

        bad_channel_list = run.get_value('bad_channels', list())

        slice_file = run.get_value('slice_file', None)
        slice_rejection_file = run.get_value('slice_rejection_file', None)

        trig_chan_name = run.get_value('trig_chan_name', None)
        resp_chan_name = run.get_value('resp_chan_name', None)

        if slice_file is None:
            epoch_dur = self.epoch_dur
            pretrig_dur = self.pretrig_dur

        output_path_base = self.get_run_output_dir(run)

        dat = load_data(filename=run.filename,
                        datatype=run.datatype,
                        slice_file=slice_file,
                        slice_rejection_file=slice_rejection_file,
                        bad_channel_list=bad_channel_list,
                        pretrig_dur=pretrig_dur,
                        epoch_dur=epoch_dur,
                        trig_chan_name=trig_chan_name,
                        resp_chan_name=resp_chan_name,
                        output_path_base=output_path_base)

        return dat

    def get_run_by_path(self, run_name):
        """
        Get an DataRun object for a given run.

        :param run_name: Filename of the run to search for

        :return: DataRun object for the given file if it exists.  Returns
                 None if one cannot be found
        """
        return self.run_definer.get_run_by_path(run_name)

    @property
    def conditions(self):
        """
        Returns a list of condition definer keys.  Shortcut for
        self.condition_definer.keys()
        """
        return self.condition_definer.keys()

    def get_condition(self, key):
        """
        Returns the condition object for the given key.
        Raises an error if the condition does not exist
        """
        return self.condition_definer.get_condition(key)

    @property
    def windows(self):
        """
        Returns a list of window definer keys.  Shortcut for
        self.window_definer.keys()
        """
        return self.window_definer.keys()

    def get_window(self, key):
        """
        Returns the window object for the given key.
        Raises an error if the window does not exist
        """
        return self.window_definer.get_window(key)

    @property
    def filters(self):
        """
        Returns a list of filter chain definer keys.  Shortcut for
        self.filter_definer.keys()
        """
        return self.filter_definer.keys()

    def get_filterchain(self, key):
        """
        Returns the filterchain object for the given key.
        Raises an error if the filterchain does not exist
        """
        return self.filter_definer.get_filterchain(key)

    @property
    def channels(self):
        """
        Returns a list of channel definer keys.  Shortcut for
        self.chan_definer.keys()
        """
        return self.chan_definer.keys()

    def get_chanset(self, key):
        """
        Returns the chanset object for the given key.
        Raises an error if the chanset does not exist
        """
        return self.chan_definer.get_chanset(key)

    #########################################################################
    # Generators for runs
    #########################################################################
    def generate_runs(self, runs=None, count=False):
        """
        Generator which yields DataRun objects subject to the given conditions.

        If any parameter is set to None, all values of this parameter are
        allowed.  The final value yielded will be None.

        :param runs: List of runs to include or None for all; can use run
                     shortname or filename
        :param count: If True, instead of yielding the items, yield the number
                      of items which would be generated.

        :rtype: DataRun
        :return: Yields an DataRun object for each valid combination.  Yields
                 None when no more combinations remain.

        """
        count_num = 0
        for run_name in self.run_definer.keys():
            run = self.run_definer.get_run(run_name)

            if runs is None or run_name in runs or run.filename in runs:
                if not count:
                    yield run

                count_num += 1

        if count:
            yield count_num

    def count_runs(self, runs=None):
        """
        Shortcut routine which uses generate_runs to count the number
        of runs which would be generated using the given parameters

        :param runs: List of runs to include or None for all

        :rtype: int
        :return: Number of runs for this combination of parameters
        """
        gen = self.generate_runs(runs, count=True)
        return next(gen)

    #########################################################################
    # Generators for conditions
    #########################################################################
    def generate_condition_bundles(self, chans=None, conditions=None, windows=None, filters=None, count=False):
        """
        Generator which yields valid combinations of the input parameters which are seen
        in the study.  If any parameter is set to None, all values of this
        parameter are allowed.

        The final value yielded will be None

        :param chans: List of chan Selectors to include or None for all
        :param conditions: List of condition Selectors to include or None for all
        :param windows: List of window Selectors to include or None for all
        :param filters: List of filter Selectors to include or None for all
        :param count: If True, instead of yielding the items, yield the number
                      of items which would be generated.

        :rtype: ConditionBundle
        :return: Yields a ConditionBundle object for each valid combination.
                 Yields None when no more combinations remain.
        """
        count_num = 0

        # If we're passed a single string, ensure it's a list otherwise when we
        # do the "windowname in windows" for instance, we would end up doing
        # a substring match, meaning that "active" would be considered valid
        # for "activev"
        if isinstance(chans, str):
            chans = [chans]
        if isinstance(conditions, str):
            conditions = [conditions]
        if isinstance(windows, str):
            windows = [windows]
        if isinstance(filters, str):
            filters = [filters]

        for channame in sorted(self.chan_definer.keys()):
            chan = self.chan_definer.get_chanset(channame)
            if chans is None or channame in chans:
                for conditionname in sorted(self.condition_definer.keys()):
                    condition = self.condition_definer.get_condition(conditionname)
                    if conditions is None or conditionname in conditions:
                        for windowname in sorted(self.window_definer.keys()):
                            window = self.window_definer.get_window(windowname)
                            if windows is None or windowname in windows:
                                for filtername in sorted(self.filter_definer.keys()):
                                    filt = self.filter_definer.get_filterchain(filtername)
                                    if filters is None or filtername in filters:
                                        if not count:
                                            yield ConditionBundle(epoch_dur=self.epoch_dur,
                                                                  pretrig_dur=self.pretrig_dur,
                                                                  chanset=chan,
                                                                  condition=condition,
                                                                  window=window,
                                                                  filterchain=filt)
                                        count_num += 1

        if count:
            yield count_num

    def count_condition_bundles(self, chans=None, conditions=None, windows=None, filters=None):
        """
        Shortcut routine which uses generate_single_bundles to count the number
        of bundles which would be generated using the given parameters

        :param chans: List of chan Selector names to include or None for all
        :param conditions: List of condition Selector names to include or None for all
        :param windows: List of window Selector names to include or None for all
        :param filters: List of filter Selector names to include or None for all

        :rtype: int
        :return: Number of bundles for this combination of parameters
        """
        gen = self.generate_condition_bundles(chans, conditions, windows, filters, count=True)
        return next(gen)

    #########################################################################
    # Generators for single conditions + data
    #########################################################################
    def generate_single_bundles(self, runs=None, chans=None, conditions=None, windows=None, filters=None, count=False):
        """
        Generator which yields valid combinations of the input parameters which are seen
        in the study.  If any parameter is set to None, all values of this
        parameter are allowed.

        The final value yielded will be None

        :param chans: List of chan Selector names to include or None for all
        :param conditions: List of condition Selector names to include or None for all
        :param windows: List of window Selector names to include or None for all
        :param filters: List of filter Selector names to include or None for all
        :param count: If True, instead of yielding the items, yield the number
                      of items which would be generated.

        :rtype: DataBundle
        :return: Yields a DataBundle object for each valid combination.
                 Yields None when no more combinations remain.
        """
        if count is True:
            # We can easily count what we'd get
            cbcount = next(self.generate_condition_bundles(chans, conditions, windows, filters, count=True))
            runcount = next(self.generate_runs(runs, count=True))
            yield cbcount * runcount
        else:
            # First of all generate a set of condition bundles
            cbgen = self.generate_condition_bundles(chans, conditions, windows, filters)
            for condition in cbgen:
                # Now for each of these, generate a run
                rungen = self.generate_runs(runs, count=count)

                for datarun in rungen:
                    yield DataBundle(data=self.get_run_data(datarun),
                                     conditionbundle=condition)

    def count_single_bundles(self, runs=None, chans=None, conditions=None, windows=None, filters=None):
        """
        Shortcut routine which uses generate_single_bundles to count the number
        of bundles which would be generated using the given parameters

        :param runs: List of runs to include or None for all
        :param chans: List of chan Selector names to include or None for all
        :param conditions: List of condition Selector names to include or None for all
        :param windows: List of window Selector names to include or None for all
        :param filters: List of filter Selector names to include or None for all

        :rtype: int
        :return: Number of bundles for this combination of parameters
        """

        gen = self.generate_single_bundles(runs, chans, conditions, windows, filters, count=True)
        return next(gen)

    #########################################################################
    # Generators for condition comparisons
    #########################################################################
    def generate_comparison_bundles(self, comparison='conditions', runs=None, chans=None,
                                    conditions=None, windows=None, filters=None, count=False):
        """
        Generator which yields valid combinations of the input parameters which are seen
        in the study.  If any parameter is set to None, all values of this
        parameter are allowed.

        The final value yielded will be None

        :param comparison: One of 'runs', 'channels', 'conditions', 'windows', 'filters'
        :param runs: List of runs to include or None for all
        :param chans: List of chan Selector names to include or None for all
        :param conditions: List of condition Selector names to include or None for all
        :param windows: List of window Selector names to include or None for all
        :param filters: List of filter Selector names to include or None for all
        :param count: If True, instead of yielding the items, yield the number
                      of items which would be generated.

        :rtype: DataBundle
        :return: Yields a DataBundle object for each valid combination.
                 Yields None when no more combinations remain.
        """

        # We generate a set of Condition Bundles for each of the combinations
        # we need, but only using the first of the comparison variant
        cobjs = []
        if comparison == 'runs':
            numcels = len(runs)
            for e in range(numcels):
                cogen = self.generate_runs(runs=runs[e])
                n = 0
                for r in cogen:
                    if n == 0:
                        cobjs.append(self.get_run_data(r))
                    n += 1

                if n != 1:
                    raise ValueError("Could not find unique run for %s" % runs[e])

            runs = runs[0]

        elif comparison == 'channels':
            numcels = len(chans)
            cobjs = chans
            for e in range(numcels):
                cobjs.append(self.chan_definer.get_chanset(chans[e]))
            chans = chans[0]

        elif comparison == 'conditions':
            numcels = len(conditions)
            for e in range(numcels):
                cobjs.append(self.condition_definer.get_condition(conditions[e]))
            conditions = conditions[0]

        elif comparison == 'windows':
            numcels = len(windows)
            for e in range(numcels):
                cobjs.append(self.window_definer.get_window(windows[e]))
            windows = windows[0]

        elif comparison == 'filters':
            numcels = len(filters)
            for e in range(numcels):
                cobjs.append(self.filter_definer.get_filterchain(filters[e]))
            filters = filters[0]
        else:
            raise ValueError('Unknown comparison')

        cbgen = self.generate_single_bundles(runs=runs,
                                             chans=chans,
                                             conditions=conditions,
                                             windows=windows,
                                             filters=filters)

        count_num = 0

        for condition in cbgen:
            complist = []
            for e in range(numcels):
                complist.append(copy(condition))

            # We now need to modify the bundles to include the comparisons
            if comparison == 'runs':
                for e in range(numcels):
                    complist[e].data = cobjs[e]
            elif comparison == 'channels':
                for e in range(numcels):
                    complist[e].chanset = cobjs[e]
            elif comparison == 'conditions':
                for e in range(numcels):
                    complist[e].condition = cobjs[e]
            elif comparison == 'windows':
                for e in range(numcels):
                    complist[e].window = cobjs[e]
            elif comparison == 'filters':
                for e in range(numcels):
                    complist[e].filterchain = cobjs[e]

            count_num += 1

            if not count:
                yield complist

        if count:
            yield count_num

    #########################################################################
    # Generators for group comparisons
    #########################################################################
    def generate_group_bundles(self, comparison='conditions', runs=None, chans=None,
                               conditions=None, windows=None, filters=None, count=False):
        """
        Generator which yields valid combinations of the input parameters which are seen
        in the study.  If any parameter is set to None, all values of this
        parameter are allowed.

        The final value yielded will be None

        :param comparison: One of 'channels', 'conditions', 'windows', 'filters'
        :param runs: List of runs to include or None for all
        :param chans: List of chan Selector names to include or None for all
        :param conditions: List of condition Selector names to include or None for all
        :param windows: List of window Selector names to include or None for all
        :param filters: List of filter Selector names to include or None for all
        :param count: If True, instead of yielding the items, yield the number
                      of items which would be generated.

        :rtype: DataBundle
        :return: Yields a DataBundle object for each valid combination.
                 Yields None when no more combinations remain.
        """

        if comparison == 'runs':
            raise ValueError("Cannot use runs comparison when doing group stats")

        runlist = []
        # Grab our runs
        rungen = self.generate_runs(runs=runs)
        for run in rungen:
            runlist.append(run)

        if len(runlist) == 0:
            raise ValueError("Cannot find a run using the parameters you specified")

        # Now grab our conditions
        cbgen = self.generate_comparison_bundles(runs=runlist[0].filename,
                                                 comparison=comparison,
                                                 chans=chans,
                                                 conditions=conditions,
                                                 windows=windows,
                                                 filters=filters)

        count_num = 0

        for condition in cbgen:
            group = []

            numcels = len(condition)

            for run in runlist:
                # We now generate this condition for each of the runs
                complist = []
                for e in range(numcels):
                    complist.append(copy(condition[e]))
                    complist[e].data = self.get_run_data(run)

                group.append(complist)

            count_num += 1

            if not count:
                yield group

        if count:
            yield count_num

    def create_anatomy_tree_dir(self, fs_subjects_dir=None):
        """
        Creates a symlink tree to the relevant structural directories.
        This is suitable for use as a freesurfer directory and helps
        with the co-registration interface etc.

        Where possible, the routine avoids removing and creating symlinks.

        It will raise an OSError if it is not possible to sort out the
        directory (i.e. non-symlinks are found)

        fs_subjects_dir can be set to where to find the subjects directory
        which contains the fsaverage subject
        """

        anat_d = self.get_anatomy_tree_dir()

        # Ensure that our anatomy directory exists
        makedirs(anat_d, exist_ok=True)

        # Look for an fsaverage subject
        # TODO: We might later want to make this a tree of links to
        # deal with a read-only fsaverage and wanting to create a
        # bem directory
        fsaverage_run_path = join(anat_d, 'fsaverage')

        # If it's there, assume it's ok
        if not lexists(fsaverage_run_path):
            # Try and find an fsaverage subject
            fs_target = None

            # Try fs_subjects_dir first
            fs_possibilities = [fs_subjects_dir]

            # Start with FREESURFER_HOME
            fs_possibilities.extend([environ.get('FREESURFER_HOME', None)])

            # And then try possible sensible locations
            fs_possibilities.extend(sorted(glob('/usr/lib/freesurfer-*/subjects'), reverse=True))

            for d in fs_possibilities:
                # Skip unknown entries
                if d is None:
                    continue

                tgt = join(d, 'fsaverage')

                if lexists(tgt):
                    fs_target = tgt
                    break

            if fs_target is not None:
                symlink(fs_target, fsaverage_run_path, target_is_directory=True)

        # Sort out run links
        for run_name in self.runs:
            run = self.get_run(run_name)

            run_anat_path = join(anat_d, run_name)

            target = run.extra_data.get('mri_structural_dir', None)

            if target is None:
                # No MRI structural directory
                # If we have a symlink, remove it
                if lexists(run_anat_path) and islink(run_anat_path):
                    unlink(run_anat_path)
            else:
                # Do we have a symlink which targets the right place?
                create = True

                if lexists(run_anat_path):
                    if islink(run_anat_path):
                        if readlink(run_anat_path) == target:
                            # Don't need to update it
                            create = False
                        else:
                            # Delete it so that we can re-create it
                            unlink(run_anat_path)
                    else:
                        raise OSError(f'Non-link found in anatomy directory {run_anat_path}')

                if create:
                    symlink(target, run_anat_path, target_is_directory=True)

    def ensure_run_headshape(self, runname):
        """
        Ensure that we have either a symlink to or a minimal copy of a file
        which can provide the headshape information in FIF format.  For format
        such as 4D, this means making a minimal copy - this is handled by the
        reader class.  For FIF, it just means making a symlink.

        Returns the path to the usable headshape
        """

        hs_tree_dir = self.get_headshape_tree_dir()

        # Ensure that our headshape tree exists
        makedirs(hs_tree_dir, exist_ok=True)

        datarun = self.get_run(runname)
        run_data = self.get_run_data(datarun)

        # Work out basename - leave off the extension as need to pass the
        # version without an extension to cope with both .fif and .fif.gz
        hsname = f'{datarun.relative_output_dir}-raw'
        hspath = join(self.get_output_dir(True), 'headshape', hsname)

        return run_data.ensure_run_headshape(hspath)


__all__ += ['Study']
register_class(Study)
