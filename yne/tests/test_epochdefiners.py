#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from os.path import join

import numpy as np

import h5py

import pytest

TEST_DATA_BTI = join('btidata', 'SOFT001', 'SoftNormal', '10%06%09@10:33', '1', 'c,rfDC')


#######################################################################
# Support function tests
#######################################################################


def test_nested_list_to_string():
    from yne.epochdefiners import nested_list_to_string

    inp = [[4096, 4098], [512, 514], 128]

    assert nested_list_to_string(inp) == '4096:4098,512:514,128'


def test_string_to_nested_list():
    from yne.epochdefiners import string_to_nested_list

    inp = '4096:4098,,512:514,128'

    assert string_to_nested_list(inp) == [[4096, 4098], [], [512, 514], [128]]


def test_combine_epoch_lists():
    from yne.epochdefiners import combine_epoch_lists

    cond_list = [4096, 4098]
    trig_list = [[4096, 4098], 512, [4096, 128], 4096]

    assert combine_epoch_lists(cond_list, trig_list) == [1, 3, 4]


#######################################################################
# SliceEpochDefiner tests
#######################################################################


def test_sliceepochdefiner():
    from yne.epochdefiners import SliceEpochDefiner

    s = SliceEpochDefiner()

    assert isinstance(s, SliceEpochDefiner)


def test_sliceepochdefiner_file_io(tmp_path):
    from yne.epochdefiners import SliceEpochDefiner
    from yne.rejection import SliceRejection

    # Put some content into a slice file
    filename = tmp_path / 'slicefile.txt'

    content = '4096:512:128,500:1000\n4100::,1000:1500\n4102:128@256:,2000:2500\n'

    with open(filename, 'w') as f:
        f.write(content)

    s = SliceEpochDefiner(filename=filename)

    assert s.to_string() == '4096:512:128,500:1000\n4100,1000:1500\n4102:128@256,2000:2500\n'

    s2 = SliceEpochDefiner.from_file(filename)

    assert s == s2

    # Round-trip to HDF5 and back
    hdf5_filename = tmp_path / 'test.hdf5'

    f = h5py.File(hdf5_filename, 'w')
    s.to_hdf5(f.create_group('sd'))
    f.close()

    f = h5py.File(hdf5_filename, 'r')
    n = SliceEpochDefiner.from_hdf5(f['sd'])
    f.close()

    assert s == n

    # Test with a SliceRejection
    s.rejected_slices = SliceRejection()

    f = h5py.File(hdf5_filename, 'w')
    s.to_hdf5(f.create_group('sd'))
    f.close()

    f = h5py.File(hdf5_filename, 'r')
    n = SliceEpochDefiner.from_hdf5(f['sd'])
    f.close()

    assert s == n


def test_sliceepochdefiner_equality(tmp_path):
    from yne.epochdefiners import SliceEpochDefiner
    from yne.rejection import SliceRejection

    content = '4096:512:128,500:1000\n4100::,1000:1500\n4102:128@256:,2000:2500\n'

    s = SliceEpochDefiner.from_string(content)
    s2 = SliceEpochDefiner.from_string(content)
    assert s.to_string() == '4096:512:128,500:1000\n4100,1000:1500\n4102:128@256,2000:2500\n'

    assert not s == None  # noqa
    assert s != None  # noqa

    s2.epochs[0] = (500, 1050)
    assert s != s2

    s2.epochs[0] = (500, 1000)
    assert s == s2

    s2.trigger_codes[1] = 4096
    assert s != s2

    s2.trigger_codes[1] = 4100
    assert s == s2

    s2.group_codes[1] = [128]
    assert s != s2

    s2.group_codes[1] = []
    assert s == s2

    s2.response_codes[2] = [32]
    assert s != s2

    s2.response_codes[2] = []
    assert s == s2

    # Fake up a SliceRejection object
    s2.rejected_slices = SliceRejection()
    assert s != s2

    s2.rejected_slices = None
    assert s == s2

    content = '4096,500:1000\n'

    SliceEpochDefiner.from_string(content)


def test_sliceepochdefiner_bad_in():
    from yne.epochdefiners import SliceEpochDefiner

    content = '4096:512:128x500:1000'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceEpochDefiner.from_string(content)

    content = 'a:512:128,500:1000'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceEpochDefiner.from_string(content)

    content = '4096:x:128,500:1000'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceEpochDefiner.from_string(content)

    content = '4096:128:c,500:1000'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceEpochDefiner.from_string(content)


def test_sliceepochdefiner_get_epochs(tmp_path):
    from yne.epochdefiners import SliceEpochDefiner
    from yne.rejection import SliceRejection

    content = '4096:512:128@129,500:1000\n4100::,1000:1500\n4102:128@256:,2000:2500\n'

    s = SliceEpochDefiner.from_string(content)

    assert s.num_epochs == 3
    assert len(s) == 3
    assert s.get_all_epoch_ranges() == [(500, 1000), (1000, 1500), (2000, 2500)]
    assert s.rejected_epochs == []

    # Test epoch ranges
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_range(-1)

    assert s.get_epoch_range(1) == (500, 1000)
    assert s.get_epoch_range(3) == (2000, 2500)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_range(4)

    # Test trigger codes
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_trigger_code(-1)

    assert s.get_epoch_trigger_code(1) == 4096

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_trigger_code(4)

    # Test group codes
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_group_codes(-1)

    assert s.get_epoch_group_codes(1) == [512]

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_group_codes(4)

    # Test response codes
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_response_codes(-1)

    assert s.get_epoch_response_codes(1) == [128, 129]

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.get_epoch_response_codes(4)

    # Reject some slices and check that our epochs are rejected
    sr = SliceRejection()
    sr.add_reject_range((800, 1000))
    sr.add_reject_range((2450, 2600))

    s.rejected_slices = sr

    assert s.rejected_epochs == [1, 3]


def test_sliceepochdefiner_condition(tmp_path):
    from yne.epochdefiners import SliceEpochDefiner
    from yne.conditions import Condition
    from yne.rejection import SliceRejection

    content = '4096:512:128@129,500:1000\n4096::,1000:1500\n4102:128@256:,2000:2500\n'

    s = SliceEpochDefiner.from_string(content)

    # Set up a couple of conditions
    c = Condition('Test', [4096])
    c_all = Condition('Test2', [])

    assert s.get_epoch_list() == [1, 2, 3]
    assert s.get_epoch_list(c) == [1, 2]
    assert s.get_epoch_list(c_all) == [1, 2, 3]

    # Test with a rejected set
    sr = SliceRejection()
    sr.add_reject_range((450, 510))

    s.rejected_slices = sr

    assert s.get_epoch_list() == [2, 3]
    assert s.get_epoch_list(c) == [2]
    assert s.get_epoch_list(c_all) == [2, 3]

    s.rejected_slices = None

    # Test groups
    c.set_trigger_list([])
    c.set_group_list([128, 512])

    assert s.get_epoch_list(c) == [1, 3]

    # Test responses
    c.set_group_list([])
    c.set_response_list([128])

    assert s.get_epoch_list(c) == [1]


def test_sliceepochdefiner_manual():
    from yne.epochdefiners import SliceEpochDefiner

    s = SliceEpochDefiner()

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_epoch(('a', 'b'), 1000)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_epoch((1000, 500), 1000)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_epoch((500, 1000), 'abc')

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_epoch((500, 1000), 1000, 'a')

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_epoch((500, 1000), 1000, 512, 'test')

    s.add_epoch((1000, 2000), 4096, [512], [128, 256])
    s.add_epoch((2200, 3200), 4098, 128, None)
    s.add_epoch((3400, 4400), 4098, None, 256)

    assert len(s) == 3

    assert s.get_epoch_range(1) == (1000, 2000)


def test_sliceepochdefiner_repeating():
    from yne.epochdefiners import SliceEpochDefiner

    s = SliceEpochDefiner.from_repeating_timing(500, 500, 3000, 4000)

    assert len(s) == 5

    assert s.get_epoch_range(1) == (500, 1000)
    assert s.get_epoch_range(5) == (2500, 3000)


#######################################################################
# ChannelEpochDefiner tests
#######################################################################


def test_chanepochdefiner():
    from yne.epochdefiners import ChannelEpochDefiner

    c = ChannelEpochDefiner()

    assert isinstance(c, ChannelEpochDefiner)

    # Load channel data
    trig = np.zeros((10000, 1))
    trig[1100] = 4096
    trig[2100] = 4100
    trig[3100] = 4096

    c = ChannelEpochDefiner.from_arrays((250, 500), trig)

    assert len(c) == 3

    assert c.get_epoch_range(1) == (850, 1600)
    assert c.get_epoch_range(2) == (1850, 2600)
    assert c.get_epoch_range(3) == (2850, 3600)

    mne_ev = c.to_mne_event_array(250)

    assert mne_ev.shape == (3, 3)

    assert mne_ev[0, 0] == 1100
    assert mne_ev[0, 2] == 4096
    assert mne_ev[1, 0] == 2100
    assert mne_ev[1, 2] == 4100
    assert mne_ev[2, 0] == 3100
    assert mne_ev[2, 2] == 4096

    # Check the add epoch routine
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_epoch(('a', 'b'), 1000)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_epoch((1000, 500), 1000)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_epoch((500, 1000), 'abc')

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_epoch((500, 1000), 1000, 'a')

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_epoch((500, 1000), 1000, 512, 'test')

    c.add_epoch((5000, 6000), 4096, [512], [128, 256])
    c.add_epoch((6200, 7200), 4098, 128, None)
    c.add_epoch((7400, 8400), 4098, None, 256)

    assert len(c) == 6

    # Test reading resp channel
    resp = np.zeros((10000, 1))
    resp[1300] = 128
    resp[3300] = 256

    c = ChannelEpochDefiner.from_arrays((250, 500), trig, resp_chan=resp)

    assert len(c) == 3

    assert c.get_epoch_range(1) == (850, 1600)
    assert c.get_epoch_response_codes(1) == [128]
    assert c.get_epoch_range(2) == (1850, 2600)
    assert c.get_epoch_response_codes(2) == []
    assert c.get_epoch_range(3) == (2850, 3600)
    assert c.get_epoch_response_codes(3) == [256]

    # Test splitting into group
    trig = np.zeros((10000, 1))
    trig[1100] = 4096
    trig[1200] = 128
    trig[2100] = 4100
    trig[3100] = 4096

    c = ChannelEpochDefiner.from_arrays((250, 500), trig)

    assert len(c) == 3

    assert c.get_epoch_range(1) == (850, 1600)
    assert c.get_epoch_group_codes(1) == [128]
    assert c.get_epoch_range(2) == (1850, 2600)
    assert c.get_epoch_group_codes(2) == []
    assert c.get_epoch_range(3) == (2850, 3600)
    assert c.get_epoch_group_codes(3) == []


def test_chanepochdefiner_bti(ynetestpath):
    from yne.readers import BTIMegReader
    from yne.epochdefiners import ChannelEpochDefiner

    fname = join(ynetestpath, TEST_DATA_BTI)

    b = BTIMegReader(fname)

    trig_chan = b.get_slice_range(None, ['STI 014'])

    ced = ChannelEpochDefiner.from_arrays((250, 500), trig_chan)

    assert ced.num_epochs == 27

    # Check the first and last epoch
    assert ced.epochs[0] == (1768, 2518)
    assert ced.epochs[-1] == (13221, 13971)

    assert ced.trigger_codes == [512, 1024, 1024, 256, 1024, 512,
                                 1024, 256, 512, 512, 512, 512, 512,
                                 1024, 512, 256, 512, 1024, 1024, 256,
                                 1024, 1024, 1024, 512, 1024, 512, 256]

    assert ced.group_codes == [[]] * 27

    assert ced.response_codes == [[]] * 27
