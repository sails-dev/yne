#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import os
import os.path

import pytest


def find_path(varname, sentinal, subdir=None):
    """
    Looks for a directory using the environment variable given and raises an
    exception if it can't find the sentinal file.

    If subdir is set, it adds a sub directory to the directory found in
    the environment variable
    """

    d = os.environ.get(varname, None)
    if not d:
        raise RuntimeError("%s is not set, cannot find test files" % varname)

    if subdir is not None:
        d = os.path.join(d, subdir)

    if not os.path.isdir(d):
        raise RuntimeError("%s is not a directory" % varname)

    # Our test file
    if not os.path.isfile(os.path.join(d, sentinal)):
        raise RuntimeError("%s does not seem to contain sentinal file %s" % (varname, sentinal))

    return os.path.abspath(d)


@pytest.fixture
def ynetestpath():
    """Looks for the directory of YNE test data using the environment variable YNETEST.
    Raises an exception if it can't find the sentinal file $YNETEST/README-ynetest.md"""

    return find_path('YNETEST', 'README-ynetest.md')
