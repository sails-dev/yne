#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

from copy import deepcopy
from os import makedirs, readlink, unlink, symlink
from os.path import join, dirname, isdir, islink, lexists

import pytest


TEST_DATA_BTI = join('btidata', 'SOFT001', 'SoftNormal', '10%06%09@10:33', '1', 'c,rfDC')


#######################################################################
# Study tests
#######################################################################


def get_study_reqs(ynetestpath):
    """Set up and configure classes which can be used to set up bundles"""

    from yne import (ChannelSet, ChannelGroup, CHANNEL_MEG,
                     Condition, TimeWindow, FilterChain, BandFilter, DataRun,
                     ChannelDefiner, ConditionDefiner, WindowDefiner,
                     RunDefiner, FilterChainDefiner)

    # Set up ChannelDefiner
    chd = ChannelDefiner()

    ch3 = ChannelSet('MEG 3 chan')
    ch3.add_channel('MEG 001', CHANNEL_MEG)
    ch3.add_channel('MEG 002', CHANNEL_MEG)
    ch3.add_channel('MEG 003', CHANNEL_MEG)
    chd.add_chanset(ch3)

    ch5 = ChannelSet('MEG 5 chan')
    ch5.add_channel('MEG 001', CHANNEL_MEG)
    ch5.add_channel('MEG 002', CHANNEL_MEG)
    ch5.add_channel('MEG 003', CHANNEL_MEG)
    ch5.add_channel('MEG 004', CHANNEL_MEG)
    ch5.add_channel('MEG 005', CHANNEL_MEG)
    chd.add_chanset(ch5)

    cg = ChannelGroup(name='MEG', channelgroup='MEG')
    chd.add_chanset(cg)

    # Set up condition definer
    condd = ConditionDefiner()

    co1 = Condition('Face', [512, 4100])
    condd.add_condition(co1)

    co2 = Condition('NotFace', [256])
    condd.add_condition(co2)

    co3 = Condition('All', [])
    condd.add_condition(co3)

    co4 = Condition('Nothing', [666])
    condd.add_condition(co4)

    # Set up window definer
    wd = WindowDefiner()

    w1 = TimeWindow('Active', (200, 500))
    wd.add_window(w1)

    w2 = TimeWindow('Passive', (-300, -100))
    wd.add_window(w2)

    w3 = TimeWindow('All')
    wd.add_window(w3)

    # Set up filter chain definer
    fd = FilterChainDefiner()

    fc1 = FilterChain('Unfiltered')
    fd.add_filterchain(fc1)

    fc2 = FilterChain('Beta')
    fc2.add_filter(BandFilter(low_freq=3, high_freq=30))
    fd.add_filterchain(fc2)

    fc3 = FilterChain('HighPass')
    fc3.add_filter(BandFilter(low_freq=1))
    fd.add_filterchain(fc3)

    # Set up run definer
    rd = RunDefiner()

    dr1 = DataRun('test_data', join(ynetestpath, TEST_DATA_BTI), 'BTI')

    # This isn't actually an mri structural dir but we can use it to test links
    dr1.extra_data['mri_structural_dir'] = join(ynetestpath, dirname(TEST_DATA_BTI))

    rd.add_run(dr1)

    return chd, condd, wd, fd, rd


def test_study_empty():
    from yne.study import Study

    s = Study()

    assert isinstance(s, Study)

    assert repr(s) == '<Study None>'


def test_study(ynetestpath, tmp_path):
    from yne.study import Study

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    assert isinstance(s, Study)

    assert repr(s) == '<Study UnitTest>'

    assert s.epoch_dur == 1000
    assert s.pretrig_dur == 500
    assert s.storage_dir == storage_dir
    assert s.sub_dir == sub_dir
    assert s.chan_definer == chd
    assert s.condition_definer == condd
    assert s.window_definer == wd
    assert s.filter_definer == fd
    assert s.run_definer == rd

    # YAML round-trip tests
    yaml_dict = s.to_yaml_dict()

    s2 = Study.from_yaml_dict(yaml_dict)

    assert s == s2

    # Test a bad YAML structure
    yaml_dict['STUDYINFO']['pretrig_dur'] = -1000

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        Study.from_yaml_dict(yaml_dict)

    test_file = tmp_path / 'test.yaml'

    s.save_yaml_file(test_file)

    s2 = Study.from_yaml_file(test_file)

    assert s == s2


def test_study_equality(ynetestpath, tmp_path):
    from yne.study import Study

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    yaml_dict = s.to_yaml_dict()

    s2 = Study.from_yaml_dict(yaml_dict)

    assert s == s2

    # Equality tests
    s2.name = 'blah'
    assert s != s2

    s2.name = s.name
    assert s == s2

    s2.storage_dir = 'blah/jtedijf'
    assert s != s2

    s2.storage_dir = s.storage_dir
    assert s == s2

    s2.epoch_dur = 1500
    assert s != s2

    s2.epoch_dur = s.epoch_dur
    assert s == s2

    s2.pretrig_dur = 10
    assert s != s2

    s2.pretrig_dur = s.pretrig_dur
    assert s == s2

    s2.chan_definer = None
    assert s != s2

    s2.chan_definer = s.chan_definer
    assert s == s2

    s2.condition_definer = None
    assert s != s2

    s2.condition_definer = s.condition_definer
    assert s == s2

    s2.window_definer = None
    assert s != s2

    s2.window_definer = s.window_definer
    assert s == s2

    s2.filter_definer = None
    assert s != s2

    s2.filter_definer = s.filter_definer
    assert s == s2

    s2.run_definer = None
    assert s != s2

    s2.run_definer = s.run_definer
    assert s == s2


def test_study_misc(ynetestpath, tmp_path):
    from yne.study import Study
    from yne import BTIMegReader

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    assert str(s.get_output_dir()) == str(join(storage_dir, sub_dir))

    s.sub_dir = None

    assert str(s.get_output_dir()) == str(storage_dir)

    s.sub_dir = 'sub_dir'

    expected = str(join(storage_dir, 'sub_dir', 'test_data'))

    assert str(s.get_run_output_dir('test_data')) == expected

    # Coreg dirs aren't in the sub directories
    expected = str(join(storage_dir, 'test_data', 'test_data-trans.fif'))

    assert str(s.get_run_coreg_filename('test_data')) == expected

    # Key and data access
    assert s.runs == ['test_data']
    assert s.get_run('test_data') == rd.get_run('test_data')

    run_filename = join(ynetestpath, TEST_DATA_BTI)

    assert s.get_run_by_path(run_filename) == rd.get_run('test_data')

    assert isinstance(s.get_run_data('test_data'), BTIMegReader)

    assert sorted(s.conditions) == ['All', 'Face', 'NotFace', 'Nothing']

    assert s.get_condition('Face') == condd.get_condition('Face')

    assert sorted(s.windows) == ['Active', 'All', 'Passive']

    assert s.get_window('All') == wd.get_window('All')

    assert sorted(s.filters) == ['Beta', 'HighPass', 'Unfiltered']

    assert s.get_filterchain('Unfiltered') == fd.get_filterchain('Unfiltered')

    assert sorted(s.channels) == ['MEG', 'MEG 3 chan', 'MEG 5 chan']

    assert s.get_chanset('MEG') == chd.get_chanset('MEG')

    # Add another version of the run for the run test
    r2 = deepcopy(rd.get_run('test_data'))
    r2.name = 'test_data_2'
    r2.add_tag('test')

    s.run_definer.add_run(r2)

    assert len(s.get_run_filenames_from_tags('test')) == 1
    assert len(s.get_run_filenames_from_tags('foo')) == 0


def test_study_bundles(ynetestpath, tmp_path):
    from yne.study import Study
    from yne import DataBundle

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    # Check we can get a bundle for a valid set
    bundle = s.get_data_bundle('test_data', 'Face', 'All', 'Beta', 'MEG')

    assert isinstance(bundle, DataBundle)

    assert bundle.chanset.name == 'MEG'
    assert bundle.condition.name == 'Face'
    assert bundle.window.name == 'All'
    assert bundle.filterchain.name == 'Beta'


def test_study_anatomy_dir(ynetestpath, tmp_path):
    from yne.study import Study

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    # Make a fake fsaverage directory
    fs_test_dir = tmp_path / 'test_subjects'
    makedirs(fs_test_dir / 'fsaverage')

    s.create_anatomy_tree_dir(fs_test_dir)

    assert isdir(storage_dir / 'anatomy')
    assert islink(storage_dir / 'anatomy' / 'fsaverage')
    assert islink(storage_dir / 'anatomy' / 'test_data')
    assert readlink(storage_dir / 'anatomy' / 'fsaverage') == str(fs_test_dir / 'fsaverage')
    assert readlink(storage_dir / 'anatomy' / 'test_data') == join(ynetestpath, dirname(TEST_DATA_BTI))

    # Call a second time and check we still work
    s.create_anatomy_tree_dir(fs_test_dir)

    assert isdir(storage_dir / 'anatomy')
    assert islink(storage_dir / 'anatomy' / 'fsaverage')
    assert islink(storage_dir / 'anatomy' / 'test_data')
    assert readlink(storage_dir / 'anatomy' / 'fsaverage') == str(fs_test_dir / 'fsaverage')
    assert readlink(storage_dir / 'anatomy' / 'test_data') == join(ynetestpath, dirname(TEST_DATA_BTI))

    # Replace a link with one which points at the wrong place
    unlink(storage_dir / 'anatomy' / 'test_data')
    symlink('/fdjsaifjsdifjasd', storage_dir / 'anatomy' / 'test_data')

    # and try again
    s.create_anatomy_tree_dir(fs_test_dir)

    assert isdir(storage_dir / 'anatomy')
    assert islink(storage_dir / 'anatomy' / 'fsaverage')
    assert islink(storage_dir / 'anatomy' / 'test_data')
    assert readlink(storage_dir / 'anatomy' / 'fsaverage') == str(fs_test_dir / 'fsaverage')
    assert readlink(storage_dir / 'anatomy' / 'test_data') == join(ynetestpath, dirname(TEST_DATA_BTI))

    # and try again with no mri_structural_dir
    del s.get_run('test_data').extra_data['mri_structural_dir']
    s.create_anatomy_tree_dir(fs_test_dir)

    assert isdir(storage_dir / 'anatomy')
    assert islink(storage_dir / 'anatomy' / 'fsaverage')
    assert not lexists(storage_dir / 'anatomy' / 'test_data')
    assert readlink(storage_dir / 'anatomy' / 'fsaverage') == str(fs_test_dir / 'fsaverage')

    # Finally, put back and make sure that we error on a non-link
    s.get_run('test_data').extra_data['mri_structural_dir'] = join(ynetestpath, dirname(TEST_DATA_BTI))
    makedirs(storage_dir / 'anatomy' / 'test_data', exist_ok=True)

    with pytest.raises(OSError) as wrapped_e:  # noqa
        s.create_anatomy_tree_dir(fs_test_dir)


def test_study_gens(ynetestpath, tmp_path):
    from yne.study import Study

    # Set up some definers
    chd, condd, wd, fd, rd = get_study_reqs(ynetestpath)

    # Set up a temporary save area
    storage_dir = tmp_path / 'test_out'
    sub_dir = 'foo'

    s = Study(name='UnitTest',
              epoch_dur=1000,
              pretrig_dur=500,
              storage_dir=storage_dir,
              sub_dir=sub_dir,
              chan_definer=chd,
              condition_definer=condd,
              window_definer=wd,
              filter_definer=fd,
              run_definer=rd)

    # Test the run generator
    assert next(s.generate_runs(runs=['foo'], count=True)) == 0

    assert len(list(s.generate_runs(runs=['foo']))) == 0

    data = list(s.generate_runs(runs=['test_data']))

    assert len(data) == 1

    assert data[0] == rd.get_run('test_data')

    assert s.count_runs(runs=['foo']) == 0

    assert s.count_runs(runs=['test_data']) == 1

    assert s.count_runs(runs=None) == 1

    # Condition generator
    assert next(s.generate_condition_bundles(chans=['MEG'],
                                             conditions=['Face', 'Nothing'],
                                             windows=['All'],
                                             filters=None,
                                             count=True)) == 6

    assert s.count_condition_bundles(chans=['MEG'],
                                     conditions=['Face', 'Nothing'],
                                     windows=['All'],
                                     filters=None) == 6

    assert next(s.generate_condition_bundles(chans='MEG',
                                             conditions='Face',
                                             windows='All',
                                             filters='Beta',
                                             count=True)) == 1

    dat = list(s.generate_condition_bundles(chans='MEG', conditions='Face',
                                            windows='All', filters='Beta'))

    assert len(dat) == 1

    assert dat[0].epoch_dur == s.epoch_dur
    assert dat[0].pretrig_dur == s.pretrig_dur
    assert dat[0].chanset == chd.get_chanset('MEG')
    assert dat[0].condition == condd.get_condition('Face')
    assert dat[0].window == wd.get_window('All')
    assert dat[0].filterchain == fd.get_filterchain('Beta')

    # Single bundles

    assert next(s.generate_single_bundles(runs=['doesntexit'],
                                          chans=['MEG'],
                                          conditions=['Face', 'Nothing'],
                                          windows=['All'],
                                          filters=None, count=True)) == 0

    assert next(s.generate_single_bundles(runs=None,
                                          chans=['MEG'],
                                          conditions=['Face', 'Nothing'],
                                          windows=['All'],
                                          filters=None, count=True)) == 6

    dat = list(s.generate_single_bundles(runs=None,
                                         chans=['MEG'],
                                         conditions='Face',
                                         windows=['All'],
                                         filters=None))

    # There are three filters
    assert len(dat) == 3

    assert s.count_single_bundles(runs=None,
                                  chans=['MEG'],
                                  conditions='Face',
                                  windows=['All'],
                                  filters=None) == 3

    # Do comparisons
    dat = list(s.generate_comparison_bundles(runs=None,
                                             comparison='conditions',
                                             chans=['MEG'],
                                             conditions=['Face', 'NotFace'],
                                             windows=['All'],
                                             filters=None))

    assert len(dat) == 3

    dat = list(s.generate_comparison_bundles(runs=None,
                                             comparison='channels',
                                             chans=['MEG 3 chan', 'MEG 5 chan'],
                                             conditions='Face',
                                             windows=['All'],
                                             filters=None))

    assert len(dat) == 3

    dat = list(s.generate_comparison_bundles(runs=None,
                                             comparison='windows',
                                             chans='MEG',
                                             conditions='Face',
                                             windows=['Active', 'Passive'],
                                             filters=None))

    assert len(dat) == 3

    dat = list(s.generate_comparison_bundles(runs=None,
                                             comparison='filters',
                                             chans='MEG',
                                             conditions='Face',
                                             windows='All',
                                             filters=['Unfiltered', 'Beta']))

    assert len(dat) == 1

    # Add another version of the run for the run test
    r2 = deepcopy(rd.get_run('test_data'))
    r2.name = 'test_data_2'

    s.run_definer.add_run(r2)

    dat = list(s.generate_comparison_bundles(runs=['test_data', 'test_data2'],
                                             comparison='runs',
                                             chans='MEG',
                                             conditions='Face',
                                             windows='All',
                                             filters=['Unfiltered', 'Beta']))

    assert len(dat) == 2

    dat = next(s.generate_comparison_bundles(runs=['test_data', 'test_data2'],
                                             comparison='runs',
                                             chans='MEG',
                                             conditions='Face',
                                             windows='All',
                                             filters=['Unfiltered', 'Beta'],
                                             count=True))

    assert dat == 2

    # Test a bad comparison
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        dat = list(s.generate_comparison_bundles(runs=['test_data', 'test_data2'],
                                                 comparison='nothing',
                                                 chans='MEG',
                                                 conditions='Face',
                                                 windows='All',
                                                 filters=['Unfiltered', 'Beta']))

    # Group bundles
    dat = list(s.generate_group_bundles(runs=None,
                                        comparison='conditions',
                                        chans='MEG',
                                        conditions=['Face', 'NotFace'],
                                        windows='All',
                                        filters=['Unfiltered', 'Beta']))

    # 2 participants with 2 combinations each
    assert len(dat) == 4

    dat = next(s.generate_group_bundles(runs=None,
                                        comparison='conditions',
                                        chans='MEG',
                                        conditions=['Face', 'NotFace'],
                                        windows='All',
                                        filters=['Unfiltered', 'Beta'],
                                        count=True))

    assert dat == 4

    # Test bad input
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        dat = list(s.generate_group_bundles(runs=None,
                                            comparison='runs',
                                            chans='MEG',
                                            conditions=['Face', 'NotFace'],
                                            windows='All',
                                            filters=['Unfiltered', 'Beta']))

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        dat = list(s.generate_group_bundles(runs=['foobar'],
                                            comparison='conditions',
                                            chans='MEG',
                                            conditions=['Face', 'NotFace'],
                                            windows='All',
                                            filters=['Unfiltered', 'Beta']))
