#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

#######################################################################
# Condition tests
#######################################################################


def test_condition():
    from yne.conditions import Condition

    d = Condition()

    assert isinstance(d, Condition)

    assert d.all_triggers
    assert d.all_groups
    assert d.all_responses


def test_condition_simple():
    from yne.conditions import Condition

    trig_list = '4096'

    d = Condition('Test', trig_list)

    assert d.is_simple
    assert d != None  # noqa


def test_condition_data():
    from yne.conditions import Condition

    trig_list = '4096'
    group_list = [512, 1024, 2048]
    resp_list = '256-259'

    d = Condition('Test', trig_list, group_list, resp_list)

    assert len(d.trigger_list) == 1
    assert len(d.group_list) == 3
    assert len(d.response_list) == 4

    assert not d.all_triggers
    assert not d.all_groups
    assert not d.all_responses
    assert not d.is_simple

    assert str(d) == 'Test'

    yaml_dict = d.to_yaml_dict()

    d2 = Condition.from_yaml_dict(yaml_dict)

    assert d != None  # noqa
    assert d == d2


def test_condition_comparison():
    from yne.conditions import Condition

    trig_list = '4096'
    group_list = [512, 1024, 2048]
    resp_list = '256-259'

    d = Condition('Test', trig_list, group_list, resp_list)

    yaml_dict = d.to_yaml_dict()

    d2 = Condition.from_yaml_dict(yaml_dict)

    assert d == d2

    d2.name = 'Foo'

    assert d != d2

    d2.name = 'Test'

    assert d == d2

    d2._trigger_list = [4098]

    assert d != d2

    d2._trigger_list = [4096]

    assert d == d2

    d2._group_list = [512, 1024, 2048, 4096]

    assert d != d2

    d2._group_list = [512, 1024, 2048]

    assert d == d2

    d2._response_list = [256]

    assert d != d2

    d2._response_list = [256, 257, 258, 259]

    assert d == d2


#######################################################################
# ConditionDefiner tests
#######################################################################


def test_conditiondefiner():
    from yne.conditions import ConditionDefiner

    c = ConditionDefiner()

    assert isinstance(c, ConditionDefiner)

    assert not c.has_condition('nothere')

    yaml_dict = c.to_yaml_dict()

    c2 = ConditionDefiner.from_yaml_dict(yaml_dict)

    assert c == c2

    assert c != None  # noqa


def test_conditiondefiner_addconds():
    from yne.conditions import ConditionDefiner, Condition

    trig_list = '4096'
    group_list = [512, 1024, 2048]
    resp_list = '256-259'

    d1 = Condition('Test', trig_list, group_list, resp_list)
    d2 = Condition('Test2', trig_list, group_list, resp_list)

    c = ConditionDefiner()

    c.add_condition(d1)

    assert c.has_condition('Test')

    assert c.get_condition('Test') == d1

    assert c.keys() == ['Test']

    c.add_condition(d2)

    assert c.keys() == ['Test', 'Test2']

    c.del_condition('Test2')

    assert c.keys() == ['Test']

    yaml_dict = c.to_yaml_dict()

    c2 = ConditionDefiner.from_yaml_dict(yaml_dict)

    assert c == c2
