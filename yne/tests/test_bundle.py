#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


from os.path import join

TEST_DATA_BASE = join('btidata', 'SOFT001', 'SoftNormal', '10%06%09@10:33', '1', 'c,rfDC')


def get_bundle_reqs(ynetestpath):
    """Set up and configure classes which can be used to set up bundles"""
    from yne import (ChannelSet, CHANNEL_MEG, Condition, TimeWindow,
                     FilterChain, BandFilter, BTIMegReader)

    # Set up channel group
    ch = ChannelSet('MEG')
    ch.add_channel('MEG 001', CHANNEL_MEG)
    ch.add_channel('MEG 002', CHANNEL_MEG)
    ch.add_channel('MEG 003', CHANNEL_MEG)

    # Set up condition
    co = Condition('Face', [512, 4100])

    # Set up window definer
    w = TimeWindow('Active', (-100, 300))

    # Set up filter chain
    fc = FilterChain('Beta')
    fc.add_filter(BandFilter(low_freq=3, high_freq=30))

    # Set up run
    fname = join(ynetestpath, TEST_DATA_BASE)
    r = BTIMegReader(filename=fname)

    # Reject channel 2 and 4 (which isn't in our include list)
    rch = ChannelSet('Rejected')
    rch.add_channel('MEG 002')
    rch.add_channel('MEG 004')

    r.set_rejected_channels(rch)

    return ch, co, w, fc, r


#######################################################################
# ConditionBundle tests
#######################################################################


def test_conditionbundle_empty():
    from yne.bundles import ConditionBundle

    cb = ConditionBundle()

    expected = '<ConditionBundle EpochDur <None>, PreTrig <None>, ' \
               'Chanset <None>, Condition <None>, Window <None>, ' \
               'FilterChain <None>>'

    assert str(cb) == expected


def test_conditionbundle(ynetestpath):
    from yne.bundles import ConditionBundle

    # Get some definers to configure the bundle
    ch, co, w, f, r = get_bundle_reqs(ynetestpath)

    # Set up our condition bundle
    cb = ConditionBundle(1500, 500, ch, co, w, f)

    expected = '<ConditionBundle EpochDur <1500.0>, PreTrig <500.0>, ' \
               'Chanset <MEG>, Condition <Face>, ' \
               'Window <Active>, FilterChain <Beta>>'

    assert str(cb) == expected
    assert cb.relative_output_path == 'MEG:Face:Active:Beta'

    # Should get a different copy with shared references to objects
    cb2 = cb.copy()

    assert id(cb2) != id(cb)

    assert id(cb2.chanset) == id(cb.chanset)
    assert id(cb2.condition) == id(cb.condition)
    assert id(cb2.window) == id(cb.window)
    assert id(cb2.filterchain) == id(cb.filterchain)

    cb.window = None
    assert cb.relative_output_path == 'MEG:Face:ALLTIME:Beta'


#######################################################################
# DataBundle tests
#######################################################################


def test_databundle_empty():
    from yne.bundles import DataBundle

    db = DataBundle()

    expected = '<DataBundle Data <None>, EpochDur <None>, PreTrig <None>, ' \
               'Chanset <None>, Condition <None>, Window <None>, ' \
               'FilterChain <None>>'

    assert str(db) == expected
    assert db.num_epochs == 0


def test_databundle_init(ynetestpath):
    from yne.bundles import ConditionBundle, DataBundle

    # Get some definers to configure the bundle
    ch, co, w, f, r = get_bundle_reqs(ynetestpath)

    # Set up our condition bundle
    cb = ConditionBundle(1500, 500, ch, co, w, f)

    # Set up our data bundle from the condition bundle
    db = DataBundle(cb, data=r)

    datapath = join(ynetestpath, TEST_DATA_BASE)

    expected = f'<DataBundle Data <{datapath}>, EpochDur <1500.0>, PreTrig <500.0>, ' \
               'Chanset <MEG>, Condition <Face>, ' \
               'Window <Active>, FilterChain <Beta>>'

    assert str(db) == expected

    # And another way
    db = DataBundle(conditionbundle=cb, data=r)

    assert str(db) == expected

    # And by hand
    db = DataBundle(data=r, epoch_dur=1500, pretrig_dur=500, chanset=ch, condition=co,
                    window=w, filterchain=f)

    assert str(db) == expected

    # Check that we reject channels
    db_chset = db.get_chanset()

    assert len(db_chset) == 2

    assert 'MEG 001' in db_chset
    assert 'MEG 003' in db_chset

    assert db.get_channel_at_index(1) == 'MEG 003'

    # Turn off rejection - force to None
    db.data.rejected_chanset = None

    db_chset = db.get_chanset()

    assert len(db_chset) == 3

    assert 'MEG 001' in db_chset
    assert 'MEG 002' in db_chset
    assert 'MEG 003' in db_chset

    assert db.get_channel_at_index(1) == 'MEG 002'


def test_databundle_copy(ynetestpath):
    from yne.bundles import DataBundle

    # Get some definers to configure the bundle
    ch, co, w, f, r = get_bundle_reqs(ynetestpath)

    # And by hand
    db = DataBundle(chanset=ch, condition=co, window=w, filterchain=f)

    db2 = db.copy()

    assert id(db2) != id(db)
    assert id(db2.data) == id(db.data)
    assert id(db2.chanset) == id(db.chanset)
    assert id(db2.condition) == id(db.condition)
    assert id(db2.window) == id(db.window)
    assert id(db2.filterchain) == id(db.filterchain)


def test_databundle_bti(ynetestpath):
    from yne.conditions import Condition
    from yne.bundles import ConditionBundle, DataBundle
    from yne.epochdefiners import ChannelEpochDefiner

    # Get some definers to configure the bundle
    ch, co, w, f, r = get_bundle_reqs(ynetestpath)

    # Set up our condition bundle
    cb = ConditionBundle(1000, 200, ch, co, w, f)

    # Set up our data bundle from the condition bundle
    db = DataBundle(cb, data=r)

    # Set up an epoch definer and attach it
    trig = db.data.get_slice_range(None, ['STI 014'])

    # Start without a window
    db.window = None

    ced = ChannelEpochDefiner.from_arrays((db.pretrig_samples, db.posttrig_samples),
                                          trig)

    db.data.epochdefiner = ced

    assert db.num_epochs == 11

    # Epoch duration is 200ms pretrigger, 1000ms total
    # Data should therefore be 290 samples long
    gen = db.unfiltered_data()

    meta = next(gen)

    assert db.epoch_samples == 290
    assert db.pretrig_samples == 58
    assert db.posttrig_samples == 232

    assert meta.num_epochs == 11
    assert meta.num_points == 290

    t = db.get_time_vector()

    assert meta.num_points == len(t)

    assert len(meta.chans) == 2
    assert 'MEG 001' in meta.chans
    assert 'MEG 003' in meta.chans

    cnt = 0

    for _ in gen:
        cnt += 1

    assert cnt == 11

    mne_events = db.to_mne_event_array()

    assert mne_events.shape == (11, 3)
    assert mne_events[0, 0] == 2018
    assert mne_events[0, 2] == 1

    mne_events = db.to_mne_event_array(trig_code=5)

    assert mne_events.shape == (11, 3)
    assert mne_events[0, 0] == 2018
    assert mne_events[0, 2] == 5

    mne_epochs = db.to_mne_epochs()

    assert len(mne_epochs) == 11
    assert mne_epochs.events[0, 2] == 1

    mne_epochs = db.to_mne_epochs(force_trig_code=5)

    assert len(mne_epochs) == 11
    assert mne_epochs.events[0, 2] == 5

    # Quick simple case test (should retain trigger code)
    co = db.condition

    db.condition = Condition('Face', [512])

    mne_epochs = db.to_mne_epochs()

    assert len(mne_epochs) == 11
    assert mne_epochs.events[0, 2] == 512

    # Put the original condition back
    db.condition = co

    # Put window back
    # Window is -100 to 300ms
    # Window should therefore be 116 slices long

    db.window = w

    assert db.epoch_samples == 290
    assert db.pretrig_samples == 58
    assert db.posttrig_samples == 232
    assert db.window_samples == (28, 144)

    gen = db.unfiltered_data(return_slices=True, return_times=True)

    meta = next(gen)

    assert meta.num_epochs == 11
    assert meta.num_points == 116

    t = db.get_time_vector()

    assert meta.num_points == len(t)

    cnt = 0

    # We test the contents elsewhere
    for _ in gen:
        cnt += 1

    assert cnt == 11

    mne_epochs = db.to_mne_epochs()

    # Due to the difference in rounding, MNE gives us a 117 sample long window;
    # we live with this
    assert len(mne_epochs) == 11
    assert len(mne_epochs.times) == 117
    assert mne_epochs.events[0, 2] == 1

    # Test where we end up with no epochs
    co2 = Condition('Face', [666])

    db.condition = co2

    gen = db.unfiltered_data()

    meta = next(gen)

    assert meta.num_epochs == 0
    assert meta.num_points == 0

    # Check the slice range function whilst we have no condition
    gen = db.slice_ranges()

    meta = next(gen)

    assert meta.num_epochs == 0
    assert meta.num_points == 0

    # Put the original condition back and try filtered data
    db.condition = co

    assert db.epoch_samples == 290
    assert db.pretrig_samples == 58
    assert db.posttrig_samples == 232
    assert db.window_samples == (28, 144)

    gen = db.filtered_data(return_slices=True, return_times=True)

    meta = next(gen)

    assert meta.num_epochs == 11
    assert meta.num_points == 116

    cnt = 0

    # We test the contents elsewhere
    for _ in gen:
        cnt += 1

    assert cnt == 11

    # Check the slice range function
    gen = db.slice_ranges()

    meta = next(gen)

    assert meta.num_epochs == 11

    cnt = 0

    # We test the contents elsewhere
    for _ in gen:
        cnt += 1

    assert cnt == 11

    # Finally, test the all data routines
    assert db.all_unfiltered_data().shape == (2, 116, 11)

    assert db.all_filtered_data().shape == (2, 116, 11)
