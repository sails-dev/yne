#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


def test_options():
    from yne import YneOptions

    opt = YneOptions()

    # Force verbose to 1 and progress to True
    opt.verbose = 1
    opt.progress = True

    opt.write('Test')
    opt.write_progress('100%')
