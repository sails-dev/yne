#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import pytest


#######################################################################
# parse_input_as_int_list tests
#######################################################################


def test_parse_input_as_int_list_none():
    from yne.utils import parse_input_as_int_list

    res = parse_input_as_int_list(None)

    assert isinstance(res, list)
    assert len(res) == 0


def test_parse_input_as_int_list_list():
    from yne.utils import parse_input_as_int_list

    inlist = [1, 2, 3]

    res = parse_input_as_int_list(inlist)

    assert res == inlist


def test_parse_input_as_int_list_singleint():
    from yne.utils import parse_input_as_int_list

    res = parse_input_as_int_list(1)

    assert res == [1]


def test_parse_input_as_int_list_emptystr():
    from yne.utils import parse_input_as_int_list

    res = parse_input_as_int_list('')

    assert isinstance(res, list)
    assert len(res) == 0


def test_parse_input_as_int_list_singlestr():
    from yne.utils import parse_input_as_int_list

    res = parse_input_as_int_list('1')

    assert res == [1]


def test_parse_input_as_int_list_range():
    from yne.utils import parse_input_as_int_list

    res = parse_input_as_int_list('1-5')

    assert res == [1, 2, 3, 4, 5]


def test_parse_input_as_int_list_badinput():
    from yne.utils import parse_input_as_int_list

    with pytest.raises(ValueError):
        parse_input_as_int_list('abca')


#######################################################################
# parse_codes tests
#######################################################################

def test_parse_codes():
    from yne.utils import parse_codes

    # None or ALL should give an empty list (which we use for "everything")
    assert parse_codes(None) == []
    assert parse_codes('ALL') == []

    assert parse_codes(['1', '2', '3']) == [1, 2, 3]
    assert parse_codes('1,2,3') == [1, 2, 3]
    assert parse_codes('1 2 3') == [1, 2, 3]
