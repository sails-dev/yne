#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


from os.path import join

import h5py

import pytest

TEST_BTI_DATA = join('btidata', 'SOFT001', 'SoftNormal', '10%06%09@10:33', '1', 'c,rfDC')
TEST_BTIFIFF_DATA = join('btififf', 'SOFT001_SoftNormal_10_06_09_10_33_1-raw.fif.gz')


#######################################################################
# Filename munging and parsing tests
#######################################################################


def test_datafilename_to_outputname():
    from yne.readers import datafilename_to_outputname

    # Test 4D
    in_file = '/mnt/megdata/R2590/P1318aa/16%08%31@13:33/1/c,rfDC'
    expected = 'R2590_P1318aa_16_08_31_13_33_1'

    assert datafilename_to_outputname(in_file) == expected

    # Test CTF
    in_file = '/path/to/SOFT002/08104_CrossSite_20101215_03.ds'
    expected = '08104_CrossSite_20101215_03'

    assert datafilename_to_outputname(in_file) == expected

    in_file = '/path/to/SOFT002/08104_CrossSite_20101215_03.ds/08104_CrossSite_20101215_03.res4'
    expected = '08104_CrossSite_20101215_03'

    assert datafilename_to_outputname(in_file) == expected

    # Test misc
    in_file = '/mnt/megdata/this/is/a/fiff.fif.gz'
    expected = 'this_is_a_fiff'

    assert datafilename_to_outputname(in_file) == expected


def test_guess_datatype():
    from yne.readers import guess_datatype

    # Test 4D
    in_file = '/mnt/megdata/R2590/P1318aa/16%08%31@13:33/1/c,rfDC'
    expected = 'BTI'

    assert guess_datatype(in_file) == expected

    # Test CTF
    in_file = '/path/to/SOFT002/08104_CrossSite_20101215_03.ds'
    expected = 'CTF'

    assert guess_datatype(in_file) == expected

    in_file = '/path/to/SOFT002/08104_CrossSite_20101215_03.ds/08104_CrossSite_20101215_03.res4'
    expected = 'CTF'

    assert guess_datatype(in_file) == expected

    # Test FIFF
    in_file = '/mnt/megdata/this/is/a/fiff.fif.gz'
    expected = 'FIFF'

    assert guess_datatype(in_file) == expected

    in_file = '/mnt/megdata/this/is/a/fiff.fif'
    expected = 'FIFF'

    assert guess_datatype(in_file) == expected

    # Test "shrug"
    in_file = '/mnt/megdata/this/is/a/file.nii.gz'

    assert guess_datatype(in_file) is None


#######################################################################
# Support routine tests
#######################################################################

def test_get_meg_reader_class():
    from yne.readers import (get_meg_reader_class,
                             BTIMegReader, CTFMegReader)

    reader = get_meg_reader_class('BTI')

    assert reader == BTIMegReader

    reader = get_meg_reader_class('CTF')

    assert reader == CTFMegReader

    with pytest.raises(RuntimeError) as wrapped_e:  # noqa
        reader = get_meg_reader_class('blah')


def test_load_data_bad():
    from yne.readers import load_data

    with pytest.raises(RuntimeError) as wrapped_e:  # noqa
        load_data('/doesnotmatter', 'foo')


#######################################################################
# BTI tests
#######################################################################

def test_btimegreader_bad(ynetestpath):
    from yne.readers import BTIMegReader

    with pytest.raises(RuntimeError) as wrapped_e:  # noqa
        BTIMegReader('/definitely/not/a/file/c,rfDC')


def test_btimegreader_basic(ynetestpath):
    from yne.readers import BTIMegReader
    from yne.channels import ChannelSet

    fname = join(ynetestpath, TEST_BTI_DATA)

    b = BTIMegReader(fname)

    assert len(b.chanset) == 274
    assert b.num_channels == 274

    assert b.num_slices == 16704

    # BTI sample rates are horrible divisors of the master clock
    assert (b.sample_rate - 290.64) < 0.01

    # Check get_slices: two channels
    dat = b.get_slice_range(None, ['STI 014', 'STI 013'])

    assert dat.shape == (2, 16704)

    assert dat[0, 2018] == 512.0

    # Get just one slice and reverse channel order
    dat = b.get_slice_range((2018, 2019), ['STI 013', 'STI 014'])

    assert dat[0, 0] == 0.0
    assert dat[1, 0] == 512.0

    # And the same but with a channel set
    cset = ChannelSet()
    cset.add_channel('STI 013')
    cset.add_channel('STI 014')

    dat = b.get_slice_range((2018, 2019), cset)

    assert dat[0, 0] == 0.0
    assert dat[1, 0] == 512.0

    # And finally with times
    dat, t = b.get_slice_range((512, 1024), cset, return_times=True)

    assert dat[0, 0] == 0
    assert t[0] == 512 / b.sample_rate
    assert t[-1] == 1023 / b.sample_rate

    # Check bad slices
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b.get_slice_range((100, 50), ['STI 014'])

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b.get_slice_range((100, 18000), ['STI 014'])

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b.get_slice_range((-5, 100), ['STI 014'])


def test_btimegreader_epoch(ynetestpath):
    from yne.readers import BTIMegReader
    from yne.epochdefiners import ChannelEpochDefiner

    fname = join(ynetestpath, TEST_BTI_DATA)

    b = BTIMegReader(fname)

    # Get trigger and response data
    dat = b.get_slice_range(None, ['STI 013', 'STI 014'])

    # Set up an epoch definer based on the trigger channel
    ced = ChannelEpochDefiner.from_arrays((250, 500), dat[1, :], resp_chan=dat[0, :])

    assert ced.num_epochs == 27

    # Check for an error without an EpochDefiner
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b.get_epoch(1)

    # Set up and use the epoch definer
    b.epochdefiner = ced

    # Basic data retrieval
    dat = b.get_epoch(1)
    assert dat.shape == (274, 750)
    assert dat[0, 0] == pytest.approx(-5.169121068546645e-13)
    assert dat[200, 200] == pytest.approx(9.728597627196942e-13)

    # Get slices and times too
    dat, slices, times = b.get_epoch(1, return_slices=True, return_times=True)
    assert dat.shape == (274, 750)
    assert dat[0, 0] == pytest.approx(-5.169121068546645e-13)
    assert dat[200, 200] == pytest.approx(9.728597627196942e-13)

    assert slices == (1768, 2518)

    # Check times remembering that the end is a python style exclusive range
    assert times[0] == slices[0] / b.sample_rate
    assert times[-1] == (slices[1] - 1) / b.sample_rate

    # Use specific channels
    dat, slices, times = b.get_epoch(1, picks=['MEG 001', 'STI 014'],
                                     return_slices=True, return_times=True)
    assert dat.shape == (2, 750)
    assert dat[0, 0] == pytest.approx(-5.169121068546645e-13)
    assert dat[1, 200] == pytest.approx(9.728597627196942e-13)

    # Check trigger is in the right place
    assert dat[1, 249] == 0.0
    assert dat[1, 250] == 512.0

    assert slices == (1768, 2518)


def test_btimegreader_hdf5_roundtrip(ynetestpath, tmp_path):
    from yne.readers import BTIMegReader
    from yne.epochdefiners import ChannelEpochDefiner

    fname = join(ynetestpath, TEST_BTI_DATA)

    b = BTIMegReader(fname)

    # Get trigger and response data
    dat = b.get_slice_range(None, ['STI 013', 'STI 014'])

    # Set up an epoch definer based on the trigger channel
    ced = ChannelEpochDefiner.from_arrays((250, 500), dat[1, :], resp_chan=dat[0, :])

    b.epochdefiner = ced

    # Save to HDF5
    hdf5_filename = tmp_path / 'test.hdf5'

    with h5py.File(hdf5_filename, 'w') as f:
        b.to_hdf5(f.create_group('reader'))

    # Read back
    with h5py.File(hdf5_filename, 'r') as f:
        b2 = BTIMegReader.from_hdf5(f['reader'])

    assert b == b2

    # Test some equality changes
    b2.filename = 'fdjifjd'
    assert b != b2

    b2.filename = b.filename
    assert b == b2

    b2.epochdefiner = None
    assert b != b2

    b2.epochdefiner = b.epochdefiner
    assert b == b2

    b2.output_path_base = 'fdsijafiasdjfdas'
    assert b != b2

    b2.output_path_base = b.output_path_base
    assert b == b2

    b2.datatype = 'BLAH'
    assert b != b2

    b2.datatype = b.datatype
    assert b == b2

    b2.rejected_chanset = None
    assert b2 != b

    b2.rejected_chanset = b.rejected_chanset
    assert b2 == b

    b2.chanset = None
    assert b2 != b

    b2.chanset = b.chanset
    assert b2 == b

    b2.meg_chanset = None
    assert b2 != b

    b2.meg_chanset = b.meg_chanset
    assert b2 == b

    b2.meg_ref_chanset = None
    assert b2 != b

    b2.meg_ref_chanset = b.meg_ref_chanset
    assert b2 == b

    b2.eeg_chanset = None
    assert b2 != b

    b2.eeg_chanset = b.eeg_chanset
    assert b2 == b

    b2.eeg_ref_chanset = None
    assert b2 != b

    b2.eeg_ref_chanset = b.eeg_ref_chanset
    assert b2 == b

    b2.aux_chanset = None
    assert b2 != b

    b2.aux_chanset = b.aux_chanset
    assert b2 == b


def test_bti_load_data(ynetestpath, tmp_path):
    from yne import ChannelSet
    from yne.readers import load_data, BTIMegReader

    fname = join(ynetestpath, TEST_BTI_DATA)

    # Test auto-detection of data type
    b = load_data(fname, None,
                  pretrig_dur=250, epoch_dur=500,
                  output_path_base=str(tmp_path))

    assert isinstance(b, BTIMegReader)

    assert b.epochdefiner.num_epochs == 27
    assert b.epochdefiner.epochs[0] == (1946, 2091)
    assert b.epochdefiner.epochs[-1] == (13399, 13544)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b = load_data(fname, slice_file='/path/to/file',
                      pretrig_dur=250, epoch_dur=500)

    # Create a quick slice file
    slice_file_path = tmp_path / 'slices.txt'

    content = '4096:512:128,500:1000\n4100::,1000:1500\n4102:128@256:,2000:2500\n'
    with open(slice_file_path, 'w') as f:
        f.write(content)

    b = load_data(fname, slice_file=slice_file_path)

    assert b.epochdefiner.num_epochs == 3

    # Create a quick slice rejection file
    slice_rej_file = tmp_path / 'slicerej.txt'

    content = '1000,2000\n'
    with open(slice_rej_file, 'w') as f:
        f.write(content)

    b = load_data(fname, slice_file=slice_file_path,
                  slice_rejection_file=slice_rej_file)

    assert len(b.epochdefiner.rejected_slices) == 1

    # Test a slice rejection file on a CED
    slice_rej_file = tmp_path / 'slicerej.txt'

    content = '13398,13400\n'
    with open(slice_rej_file, 'w') as f:
        f.write(content)

    b = load_data(fname, None,
                  pretrig_dur=250, epoch_dur=500,
                  output_path_base=str(tmp_path),
                  slice_rejection_file=slice_rej_file)

    assert b.epochdefiner.num_epochs == 27
    assert b.epochdefiner.epochs[0] == (1946, 2091)
    assert b.epochdefiner.epochs[-1] == (13399, 13544)
    assert b.epochdefiner.rejected_epochs == [27]

    # Should be 26 epochs (1-indexed for historical reasons)
    assert b.epochdefiner.get_epoch_list() == list(range(1, 27))

    # Test passing bad arguments
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b = load_data(fname, slice_file='/path/to/file',
                      pretrig_dur=250, epoch_dur=500)

    # Add a bad channel list
    cset = ChannelSet()

    cset.add_channel('MEG 001')
    cset.add_channel('MEG 002')

    b = load_data(fname, slice_file=slice_file_path,
                  slice_rejection_file=slice_rej_file,
                  bad_channel_list=cset)

    assert len(b.epochdefiner.rejected_slices) == 1


def test_fiff_load_data(ynetestpath, tmp_path):
    from yne import ChannelSet
    from yne.readers import load_data, FIFFMegReader

    fname = join(ynetestpath, TEST_BTIFIFF_DATA)

    # Test auto-detection of data type
    b = load_data(fname, None,
                  pretrig_dur=250, epoch_dur=500,
                  output_path_base=str(tmp_path))

    assert isinstance(b, FIFFMegReader)

    assert b.epochdefiner.num_epochs == 27
    assert b.epochdefiner.epochs[0] == (1946, 2091)
    assert b.epochdefiner.epochs[-1] == (13399, 13544)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b = load_data(fname, slice_file='/path/to/file',
                      pretrig_dur=250, epoch_dur=500)

    # Create a quick slice file
    slice_file_path = tmp_path / 'slices.txt'

    content = '4096:512:128,500:1000\n4100::,1000:1500\n4102:128@256:,2000:2500\n'
    with open(slice_file_path, 'w') as f:
        f.write(content)

    b = load_data(fname, slice_file=slice_file_path)

    assert b.epochdefiner.num_epochs == 3

    # Create a quick slice rejection file
    slice_rej_file = tmp_path / 'slicerej.txt'

    content = '1000,2000\n'
    with open(slice_rej_file, 'w') as f:
        f.write(content)

    b = load_data(fname, slice_file=slice_file_path,
                  slice_rejection_file=slice_rej_file)

    assert len(b.epochdefiner.rejected_slices) == 1

    # Test a slice rejection file on a CED
    slice_rej_file = tmp_path / 'slicerej.txt'

    content = '13398,13400\n'
    with open(slice_rej_file, 'w') as f:
        f.write(content)

    b = load_data(fname, None,
                  pretrig_dur=250, epoch_dur=500,
                  output_path_base=str(tmp_path),
                  slice_rejection_file=slice_rej_file)

    assert b.epochdefiner.num_epochs == 27
    assert b.epochdefiner.epochs[0] == (1946, 2091)
    assert b.epochdefiner.epochs[-1] == (13399, 13544)
    assert b.epochdefiner.rejected_epochs == [27]

    # Should be 26 epochs (1-indexed for historical reasons)
    assert b.epochdefiner.get_epoch_list() == list(range(1, 27))

    # Test passing bad arguments
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        b = load_data(fname, slice_file='/path/to/file',
                      pretrig_dur=250, epoch_dur=500)

    # Add a bad channel list
    cset = ChannelSet()

    cset.add_channel('MEG 001')
    cset.add_channel('MEG 002')

    b = load_data(fname, slice_file=slice_file_path,
                  slice_rejection_file=slice_rej_file,
                  bad_channel_list=cset)

    assert len(b.epochdefiner.rejected_slices) == 1
