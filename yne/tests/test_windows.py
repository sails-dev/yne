#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

#######################################################################
# TimeWindow tests
#######################################################################


def test_timewindow():
    from yne.windows import TimeWindow

    w = TimeWindow()

    assert isinstance(w, TimeWindow)

    assert w.get_ms() is None


def test_timewindow_ms():
    from yne.windows import TimeWindow

    w = TimeWindow(ms=(0, 200), name='Foo')

    assert str(w) == 'Foo'

    assert w.get_ms() == (0, 200)

    yaml_dict = w.to_yaml_dict()

    w2 = TimeWindow.from_yaml_dict(yaml_dict)

    assert w == w2

    assert w != None  # noqa


#######################################################################
# WindowDefiner tests
#######################################################################


def test_windowdefiner():
    from yne.windows import WindowDefiner

    d = WindowDefiner()

    assert isinstance(d, WindowDefiner)

    assert len(d.keys()) == 0


def test_windowdefiner_data():
    from yne.windows import WindowDefiner, TimeWindow

    d = WindowDefiner()

    w1 = TimeWindow('Test', ms=(0, 1000))
    w2 = TimeWindow('Full')
    w3 = TimeWindow('Test2', ms=(0, 500))

    assert w1 != w2
    assert w2.get_slices(1000) is None

    d.add_window(w1)
    d.add_window(w2)

    assert isinstance(d, WindowDefiner)

    assert len(d.keys()) == 2

    d.add_window(w3)

    assert len(d.keys()) == 3

    assert d.has_window('Test')
    assert not d.has_window('TestNotThere')

    assert d.get_window('Test').data == (0, 1000)
    assert d.get_window('Test2').data == (0, 500)

    assert d.get_window('Test2').get_slices(2000) == (0, 1000)

    # Now test the case where we have 200ms pre-trigger, so 400 samples
    # at 2000Hz.  Window should be at slices 400->1400.
    # Rationale: Slice 0 is -200ms and we start at 0ms
    assert d.get_window('Test2').get_slices(2000, 400) == (400, 1400)

    # Similar test with negative windows.  This time our window is
    # -100ms to 500ms and our main pre-trigger is 200ms.
    # Start should therefore be 200
    w4 = TimeWindow('Test3', ms=(-100, 500))

    assert w4.get_slices(2000, 400) == (200, 1400)

    d.del_window('Full')

    assert len(d.keys()) == 2

    yaml_dict = d.to_yaml_dict()

    d2 = WindowDefiner.from_yaml_dict(yaml_dict)

    assert d == d2
