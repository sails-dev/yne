#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import os
import pytest


def test_empty_parser():
    from yne.parser import YneOptionParser

    parser = YneOptionParser(usage="Test")

    op, args = parser.parse_args([])

    # Can't really check op here
    assert len(args) == 0


def test_help_parser():
    from yne.parser import YneOptionParser

    parser = YneOptionParser(usage="Test")

    # We expect to exit
    with pytest.raises(SystemExit) as wrapped_e:
        op, args = parser.parse_args(['--help'])

    assert wrapped_e.type == SystemExit
    assert wrapped_e.value.code == 0


def test_base_parser():
    from yne import YneOptions
    from yne.parser import YneOptionParser, OptionBaseParameters

    parser = YneOptionParser(usage="Test")
    parser.register_handlers(OptionBaseParameters())

    # Force options to default state
    YneOptions().setup()

    # Parse our arguments
    op, args = parser.parse_args(['--progress', '-v', '-v', '-v'])

    # Check the args
    parser.sanity_check(op)

    # Check the options are set
    assert YneOptions().verbose == 3
    assert YneOptions().progress is True


def test_base_parser_bad_args():
    from yne import YneOptions
    from yne.parser import YneOptionParser, OptionBaseParameters

    parser = YneOptionParser(usage="Test")
    parser.register_handlers(OptionBaseParameters())

    # Force options to default state
    YneOptions().setup()

    # Parse our arguments
    op, args = parser.parse_args(['--progress', '-v', '-v', '-v'])

    # Check the args
    parser.sanity_check(op)

    # Check the options are set
    assert YneOptions().verbose == 3
    assert YneOptions().progress is True


def test_base_parser_env():
    from yne import YneOptions
    from yne.parser import YneOptionParser, OptionBaseParameters

    parser = YneOptionParser(usage="Test")
    parser.register_handlers(OptionBaseParameters())

    # Force options to default state
    YneOptions().setup()

    # Put verbose in the command line
    os.environ['YNEVERBOSE'] = '10'

    # Parse our arguments
    op, args = parser.parse_args([])

    # Check the args
    parser.sanity_check(op)

    # Check the options are set
    assert YneOptions().verbose == 10
    assert YneOptions().progress is False

    # Force options to default state
    YneOptions().setup()

    # Check that the default works
    os.environ['YNEVERBOSE'] = 'abc'

    # Parse our arguments
    op, args = parser.parse_args([])

    # Check the args
    parser.sanity_check(op)

    # Check the options are set
    assert YneOptions().verbose == 1
    assert YneOptions().progress is False


def test_resubmit():
    from yne.parser import YneOptionParser, OptionBaseParameters

    parser = YneOptionParser(usage="Test")
    parser.register_handlers([OptionBaseParameters()])

    # Parse our arguments
    op, args = parser.parse_args(['--progress', '-v', '-v', '-v'])

    # Check that we can resubmit a basic command
    resub = parser.resubmit_string(op)

    assert resub == ['-v', '-v', '-v', '--progress']
