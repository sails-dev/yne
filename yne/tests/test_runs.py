#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

#######################################################################
# DataRun tests
#######################################################################


def test_datarun():
    from yne.runs import DataRun

    d = DataRun()

    assert d.name is None
    assert d.filename is None
    assert d.datatype is None

    assert repr(d) == '<DataRun None>'


def test_datarun_equality():
    from yne.runs import DataRun

    d = DataRun(name='Blah')
    assert repr(d) == '<DataRun Blah>'
    assert d.relative_output_dir == 'Blah'

    assert d != None  # noqa

    yaml_dict = d.to_yaml_dict()

    d2 = DataRun.from_yaml_dict(yaml_dict)
    assert d == d2

    d.name = 'foo'
    assert d != d2

    d.name = 'Blah'
    assert d == d2

    d.filename = 'blah'
    assert d != d2

    d.filename = None
    assert d == d2

    d.datatype = 'BTI'
    assert d != d2

    d.datatype = None
    assert d == d2

    d.add_tag('foo')
    assert d != d2

    assert d.has_tag('foo')

    d.del_tag('foo')
    assert d == d2

    assert not d.has_tag('foo')

    d.set_value('Test', 1)
    assert d != d2

    assert d.has_value('Test')

    assert d.get_value('Test') == 1
    d.set_value('Test', 2)
    assert d.get_value('Test') == 2

    assert d != d2

    d.del_value('Test')
    assert d == d2


#######################################################################
# RunDefiner tests
#######################################################################


def test_rundefiner():
    from yne.runs import RunDefiner

    c = RunDefiner()

    assert isinstance(c, RunDefiner)

    assert not c.has_run('nothere')

    yaml_dict = c.to_yaml_dict()

    c2 = RunDefiner.from_yaml_dict(yaml_dict)

    assert c == c2


def test_rundefiner_addruns():
    from yne.runs import RunDefiner, DataRun

    r1 = DataRun('Test', '/path/to/file', 'TESTTYPE1')
    r2 = DataRun('Test2', '/path/to/file2', 'TESTTYPE1')

    c = RunDefiner()

    c.add_run(r1)

    assert c.has_run('Test')

    assert c.get_run('Test') == r1

    assert c.keys() == ['Test']

    c.add_run(r2)

    assert c.keys() == ['Test', 'Test2']

    c.del_run('Test2')

    assert c.keys() == ['Test']

    yaml_dict = c.to_yaml_dict()

    c2 = RunDefiner.from_yaml_dict(yaml_dict)

    assert c == c2


def test_rundefiner_paths():
    from yne.runs import DataRun, RunDefiner

    r1 = DataRun('Test', '/path/to/file', 'TESTTYPE1')
    r2 = DataRun('Test2', '/path/to/file2', 'TESTTYPE1')

    c = RunDefiner()

    c.add_run(r1)
    c.add_run(r2)

    assert c.get_run_by_path('/path/to/file') == r1
    assert c.get_run_by_path('/path/to/file2') == r2
    assert c.get_run_by_path('/path/to/file3') is None


def test_rundefiner_tags():
    from yne.runs import DataRun, RunDefiner

    r1 = DataRun('Test', '/path/to/file', 'TESTTYPE1')
    r1.add_tag('blah')
    r1.add_tag('foo')

    r2 = DataRun('Test2', '/path/to/file2', 'TESTTYPE1')
    r2.add_tag('foo')

    c = RunDefiner()

    c.add_run(r1)
    c.add_run(r2)

    runs = c.get_runs_by_tag('blah')
    assert len(runs) == 1
    assert runs[0] == r1

    runs = c.get_runs_by_tag('foo')
    assert len(runs) == 2
    assert (runs[0] == r1 and runs[1] == r2) or (runs[1] == r1 and runs[0] == r2)
