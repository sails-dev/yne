#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import numpy as np

import pytest

#######################################################################
# SliceRejection tests
#######################################################################


def test_slicerejection_empty():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    assert isinstance(s, SliceRejection)

    assert len(s) == 0

    assert s.num_rejections == 0

    s.sanitise()

    assert s.num_rejections == 0

    assert repr(s) == 'SliceRejection([])'


def test_slicerejection_manual():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((3000, 4000))

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_reject_range((1000, 1000))

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.add_reject_range((1000, 999))

    assert s.num_rejections == 2

    assert s.to_string() == '1000,2000\n3000,4000\n'

    s.remove_reject_range((3000, 4000))

    assert s.num_rejections == 1

    assert repr(s) == 'SliceRejection([(1000,2000),])'

    s.remove_reject_range((1500, 2000))

    assert s.num_rejections == 1

    assert np.all(s[0, :] == (1000, 1500))

    assert np.all(next(iter(s)) == (1000, 1500))

    s.remove_reject_range((1050, 1100))

    assert np.all(s[0, :] == (1000, 1050))
    assert np.all(s[1, :] == (1100, 1500))

    assert s.num_rejections == 2

    s.remove_reject_range((800, 1025))

    assert np.all(s[0, :] == (1025, 1050))
    assert np.all(s[1, :] == (1100, 1500))

    assert s.num_rejections == 2

    s.remove_reject_range((0, 2000))

    assert s.num_rejections == 0

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.remove_reject_range((100, 99))


def test_slicerejection_check_ranges():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((3000, 4000))

    idx = s.check_ranges([(500, 1000), (1750, 2250),
                          (3000, 3500), (4000, 4500)])

    assert idx == [1, 2]


def test_slicerejection_sanity_check():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((3000, 4000))

    bad = s.sanity_check()

    assert len(bad) == 0

    # Force something bad
    s.rejects[1, 1] = 2500

    # Before we sanity check, check that sanitise would error
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        s.sanitise()

    bad = s.sanity_check()

    assert len(bad) == 1
    assert bad == [1]


def test_slicerejection_sanitise():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((1050, 4000))
    s.add_reject_range((5000, 5050))

    assert s.num_rejections == 3

    s.sanitise()

    assert s.num_rejections == 2
    assert np.all(s[0, :] == (1000, 4000))
    assert np.all(s[1, :] == (5000, 5050))


def test_slicerejection_from_input():
    from yne.rejection import SliceRejection

    dat = '1000,2000\n2500,2570\n3000,3100\n#foo\n\n\n'

    s = SliceRejection(inp=dat)

    assert s.num_rejections == 3

    assert np.all(s[0, :] == (1000, 2000))
    assert np.all(s[1, :] == (2500, 2570))
    assert np.all(s[2, :] == (3000, 3100))


def test_slicerejection_bad_input():
    from yne.rejection import SliceRejection

    dat = '1000,blah\n2500,2570\n3000,3100\n\n'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceRejection(inp=dat)

    dat = '1000,1200,3000\n2500,2570\n3000,3100\n\n'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceRejection(inp=dat)

    dat = True

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceRejection(inp=dat)

    dat = [True]

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        SliceRejection(inp=dat)


def test_slicerejection_equality():
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((3000, 4000))

    s2 = SliceRejection()

    s2.add_reject_range((1000, 2000))
    s2.add_reject_range((3000, 4000))

    assert s == s2

    s2.add_reject_range((4500, 5000))

    assert s != s2


def test_slicerejection_file_io(tmp_path):
    from yne.rejection import SliceRejection

    s = SliceRejection()

    s.add_reject_range((1000, 2000))
    s.add_reject_range((3000, 4000))

    filename = tmp_path / 'foo.txt'

    s.save(filename)

    s2 = SliceRejection.from_file(filename)

    assert s == s2
