#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import h5py
import pytest

#######################################################################
# ChannelType tests
#######################################################################


def test_channelname_sort_equal():
    from yne.channels import ChannelName

    assert ChannelName('A1') != None  # noqa
    assert not ChannelName('A1') == None  # noqa

    assert ChannelName('A1') == ChannelName('A1')
    assert ChannelName('A1') == ChannelName('A001')
    assert ChannelName('MEG1') == ChannelName('MEG1')
    assert ChannelName('MEG1') == ChannelName('MEG01')


def test_channelname_sort_notequal():
    from yne.channels import ChannelName

    assert ChannelName('A1') != ChannelName('A2')
    assert ChannelName('MEG1') != ChannelName('MEG2')


def test_channelname_sort_lt():
    from yne.channels import ChannelName

    assert ChannelName('A') < ChannelName('B')
    assert ChannelName('A1') < ChannelName('A100')
    assert not ChannelName('A5') < ChannelName('A4')
    assert ChannelName('MEG0') < ChannelName('O')
    assert ChannelName('MEG1') < ChannelName('MEG100')
    assert not ChannelName('MEG005') < ChannelName('MEG4')


def test_channelname_sort_le():
    from yne.channels import ChannelName

    assert ChannelName('A5') <= ChannelName('A5')
    assert ChannelName('A5') <= ChannelName('A06')
    assert ChannelName('A5') <= ChannelName('B100')
    assert ChannelName('MEG005') <= ChannelName('MEG5')
    assert ChannelName('MEG005') <= ChannelName('MEG06')
    assert ChannelName('MEG005') <= ChannelName('Z100')


def test_channelname_sort_gt():
    from yne.channels import ChannelName

    assert ChannelName('A6') > ChannelName('A005')
    assert ChannelName('A10') > ChannelName('A5')
    assert ChannelName('B1') > ChannelName('A1')
    assert ChannelName('P1') > ChannelName('MEG1')


def test_channelname_sort_ge():
    from yne.channels import ChannelName

    assert ChannelName('A2') >= ChannelName('A1')
    assert ChannelName('A2') >= ChannelName('A001')
    assert ChannelName('A101') >= ChannelName('A100')
    assert ChannelName('MEG101') >= ChannelName('A100')
    assert ChannelName('MEG101') >= ChannelName('MEG100')


def test_channelname_hdf5name():
    from yne.channels import ChannelName

    assert ChannelName.get_hdf5name() == 'yne.channels.ChannelName'


#######################################################################
# ChannelGroup tests
#######################################################################


def check_cg(cg, MEG, MEG_REF, EEG, AUX):
    # Check these longhand so we can pass MEG = None for example to skip that
    # part of the test
    if MEG is True:
        assert cg.meg
    elif MEG is False:
        assert not cg.meg

    if MEG_REF is True:
        assert cg.meg_ref
    elif MEG_REF is False:
        assert not cg.meg_ref

    if EEG is True:
        assert cg.eeg
    elif EEG is False:
        assert not cg.eeg

    if AUX is True:
        assert cg.aux
    elif AUX is False:
        assert not cg.aux


def test_channelgroup():
    """Check that we can instantiate a ChannelGroup"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()

    assert not cg.get_meg()
    assert not cg.get_eeg()
    assert not cg.get_meg_ref()
    assert not cg.get_aux()

    yaml_dict = cg.to_yaml_dict()

    cg2 = ChannelGroup.from_yaml_dict(yaml_dict)

    assert cg == cg2


def test_channelgroup_forced_to_none():
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    cg.channelgroup = None

    assert not cg.get_meg()
    assert not cg.get_eeg()
    assert not cg.get_meg_ref()
    assert not cg.get_aux()

    assert str(cg) == 'NONE'

    yaml_dict = cg.to_yaml_dict()

    cg2 = ChannelGroup.from_yaml_dict(yaml_dict)

    assert cg == cg2

    cg2.name = 'blah'

    assert cg != cg2


def test_channelgroup_init():
    """Check that we can instantiate a ChannelGroup and that it's sane"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    assert str(cg) == ''


def test_channelgroup_equality():
    """Check that we can compare channel groups"""
    from yne.channels import ChannelGroup

    cg1 = ChannelGroup(channelgroup='MEG')

    assert not cg1 == None  # noqa
    assert cg1 != None  # noqa

    check_cg(cg1, MEG=True, MEG_REF=False, EEG=False, AUX=False)

    cg2 = ChannelGroup(channelgroup='MEG')
    check_cg(cg2, MEG=True, MEG_REF=False, EEG=False, AUX=False)

    cg3 = ChannelGroup(channelgroup='EEG')
    check_cg(cg3, MEG=False, MEG_REF=False, EEG=True, AUX=False)

    cg4 = ChannelGroup(channelgroup='MEG,EEG')
    check_cg(cg4, MEG=True, MEG_REF=False, EEG=True, AUX=False)

    assert cg1 == cg2
    assert cg2 != cg3
    assert cg1 != cg3
    assert cg1 != cg4
    assert cg2 != cg4
    assert cg3 != cg4

    assert str(cg1) == 'MEG'
    assert str(cg2) == 'MEG'
    assert str(cg3) == 'EEG'
    assert str(cg4) == 'MEG,EEG'

    assert repr(cg4) == 'MEG,EEG'


def test_channelgroup_initfromstringsingle():
    """Check that we can instantiate a ChannelGroup from a string representation (single)"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup(channelgroup="MEG")
    check_cg(cg, MEG=True, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_initfromstringint():
    """Check that we can instantiate a ChannelGroup from a string int representation"""
    from yne.channels import ChannelGroup, CHANNEL_MEG

    cg = ChannelGroup(channelgroup=str(CHANNEL_MEG))
    check_cg(cg, MEG=True, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_initfromstringmultiple():
    """Check that we can instantiate a ChannelGroup from a string representation (multiple)"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup(channelgroup="MEG,EEG")
    check_cg(cg, MEG=True, MEG_REF=False, EEG=True, AUX=False)


def test_channelgroup_initfromstringall():
    """Check that we can instantiate a ChannelGroup from a string representation (all)"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup(channelgroup="MEG,EEG,MEG_REF,AUX")
    check_cg(cg, MEG=True, MEG_REF=True, EEG=True, AUX=True)


def test_channelgroup_initfromstringspacing():
    """Check that we can instantiate a ChannelGroup from a string representation (spaces)"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup(channelgroup="MEG, EEG, MEG_REF , AUX")
    check_cg(cg, MEG=True, MEG_REF=True, EEG=True, AUX=True)


def test_channelgroup_initfromstringinvalid():
    """Check that we fail to instantiate a ChannelGroup from an invalid string representation"""
    from yne.channels import ChannelGroup

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        ChannelGroup(channelgroup="MEG,EEG,FOO")


def test_channelgroup_settings_MEG():
    """Check that we can instantiate a ChannelGroup and set and unset the MEG bit"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.meg = True
    check_cg(cg, MEG=True, MEG_REF=False, EEG=False, AUX=False)

    cg.meg = False
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.set_meg(True)
    check_cg(cg, MEG=True, MEG_REF=False, EEG=False, AUX=False)

    cg.set_meg(False)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_settings_MEG_REF():
    """Check that we can instantiate a ChannelGroup and set and unset the MEG_REF bit"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.meg_ref = True
    check_cg(cg, MEG=False, MEG_REF=True, EEG=False, AUX=False)

    cg.meg_ref = False
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.set_meg_ref(True)
    check_cg(cg, MEG=False, MEG_REF=True, EEG=False, AUX=False)

    cg.set_meg_ref(False)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_settings_EEG():
    """Check that we can instantiate a ChannelGroup and set and unset the EEG bit"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.eeg = True
    check_cg(cg, MEG=False, MEG_REF=False, EEG=True, AUX=False)

    cg.eeg = False
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.set_eeg(True)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=True, AUX=False)

    cg.set_eeg(False)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_settings_AUX():
    """Check that we can instantiate a ChannelGroup and set and unset the AUX bit"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.aux = True
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=True)

    cg.aux = False
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.set_aux(True)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=True)

    cg.set_aux(False)
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)


def test_channelgroup_settings_mixed():
    """Check that we can instantiate a ChannelGroup and set and unset some bits"""
    from yne.channels import ChannelGroup

    cg = ChannelGroup()
    check_cg(cg, MEG=False, MEG_REF=False, EEG=False, AUX=False)

    cg.aux = True
    cg.meg = True
    cg.eeg = True
    check_cg(cg, MEG=True, MEG_REF=False, EEG=True, AUX=True)

    cg.eeg = False
    cg.meg_ref = True
    check_cg(cg, MEG=True, MEG_REF=True, EEG=False, AUX=True)

    cg.meg = False
    check_cg(cg, MEG=False, MEG_REF=True, EEG=False, AUX=True)


def test_channelgroup_channelextraction():
    """Check that given a ChannelSet, we can extract the right Channels using a ChannelGroup"""
    from yne.channels import (Channel, ChannelSet, CHANNEL_MEG,
                              CHANNEL_MEG_MAG, CHANNEL_EEG, CHANNEL_AUX, ChannelGroup)

    a = ChannelSet()
    e = ChannelSet()
    m = ChannelSet()

    # Test a couple of ways of adding channels

    a.add_channel(Channel('TRIGGER', CHANNEL_AUX))

    e.add_channel('E1', CHANNEL_EEG)
    e.add_channel('E2', CHANNEL_EEG)

    m.add_channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)
    m.add_channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG)
    m.add_channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG)
    m.add_channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG)
    m.add_channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG)

    t = a + e + m

    cg = ChannelGroup()
    cg.meg = True

    meg_only = t.intersection(cg)
    assert meg_only == m

    cg.meg = False
    cg.eeg = True

    eeg_only = t.intersection(cg)
    assert eeg_only == e

    cg.eeg = False
    cg.aux = True

    aux_only = t.intersection(cg)
    assert aux_only == a

    cg.aux = False

    nothing = t.intersection(cg)
    assert nothing == ChannelSet()

    cg.meg_ref = True

    nothing = t.intersection(cg)
    assert nothing == ChannelSet()

    cg.meg = True
    cg.eeg = True

    meg_and_eeg = t.intersection(cg)
    assert meg_and_eeg == (m + e)

    cg.aux = True

    total = t.intersection(cg)
    assert total == t

    cg.meg_ref = True

    total = t.intersection(cg)
    assert total == t


#######################################################################
# ChannelType tests
#######################################################################


def test_channeltype():
    """Check that we can create a chantype"""
    from yne.channels import (ChannelType, CHANNEL_MEG, CHANNEL_MEG_MAG,
                              CHANNEL_MEG_REF, CHANNEL_MEG_GRAD, CHANNEL_EEG,
                              CHANNEL_AUX)

    meg = ChannelType(CHANNEL_MEG | CHANNEL_MEG_MAG)

    assert not meg == None  # noqa
    assert meg != None  # noqa

    assert isinstance(meg, ChannelType)

    assert meg.is_meg
    assert meg.is_meg_data
    assert meg.is_meg_mag
    assert not meg.is_meg_grad
    assert str(meg) == 'MEG (MAG)'

    meg_g = ChannelType(CHANNEL_MEG | CHANNEL_MEG_GRAD)

    assert isinstance(meg_g, ChannelType)

    assert meg_g.is_meg
    assert meg_g.is_meg_data
    assert meg_g.is_meg_grad
    assert not meg_g.is_meg_mag
    assert str(meg_g) == 'MEG (GRAD)'

    meg_ref = ChannelType(CHANNEL_MEG_REF | CHANNEL_MEG_MAG)

    assert isinstance(meg_ref, ChannelType)

    assert meg_ref.is_meg
    assert meg_ref.is_meg_ref
    assert meg_ref.is_meg_mag
    assert str(meg_ref) == 'MEGREF (MAG)'

    eeg = ChannelType(CHANNEL_EEG)

    assert isinstance(eeg, ChannelType)

    assert eeg.is_eeg
    assert not eeg.is_meg_mag
    assert not eeg.is_meg_grad
    assert not eeg.is_aux
    assert str(eeg) == 'EEG'

    aux = ChannelType(CHANNEL_AUX)

    assert isinstance(aux, ChannelType)

    assert aux.is_aux
    assert not aux.is_meg_mag
    assert not aux.is_meg_grad
    assert str(aux) == 'AUX'


def test_channeltype_compare():
    """Check that we can create and compare a chantype"""
    from yne.channels import (ChannelType, CHANNEL_MEG, CHANNEL_MEG_MAG,
                              CHANNEL_EEG)

    c1 = ChannelType(CHANNEL_MEG | CHANNEL_MEG_MAG)
    c2 = ChannelType(CHANNEL_MEG | CHANNEL_MEG_MAG)
    c3 = ChannelType(CHANNEL_EEG)

    assert c1 == c2
    assert c2 != c3
    assert c1 != c3


def test_channeltype_empty():
    from yne.channels import ChannelType

    c = ChannelType()

    assert not c.is_meg
    assert not c.is_meg_ref
    assert not c.is_meg_data
    assert not c.is_eeg
    assert not c.is_aux
    assert str(c) == 'UNSET'
    assert repr(c) == 'UNSET'

    # And a bad channel type
    c.chantype = 2**32
    assert str(c) == 'UNKNOWN'


def test_channeltype_sanity():
    from yne.channels import ChannelType, CHANNEL_MEG, CHANNEL_EEG, CHANNEL_MEG_GRAD

    # These tests involve forcing bad channel information into the object
    c = ChannelType()
    c.chantype = 'abc'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.sanity_check()

    c = ChannelType(CHANNEL_MEG)
    c.chantype |= CHANNEL_EEG

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.sanity_check()

    c = ChannelType(CHANNEL_EEG)
    c.chantype |= CHANNEL_MEG_GRAD

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.sanity_check()

    c = ChannelType(CHANNEL_MEG)
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.sanity_check()

    # Actually check a good channel
    c = ChannelType(CHANNEL_MEG | CHANNEL_MEG_GRAD)
    assert c.sanity_check()


def test_channeltype_majorcompare():
    """Check that our major compare routine works"""
    from yne.channels import (ChannelType, CHANNEL_MEG, CHANNEL_MEG_REF,
                              CHANNEL_MEG_MAG, CHANNEL_MEG_GRAD,
                              CHANNEL_EEG, CHANNEL_AUX)

    c_meg = ChannelType(CHANNEL_MEG)
    c_megref = ChannelType(CHANNEL_MEG_REF)
    c_eeg = ChannelType(CHANNEL_EEG)
    c_aux = ChannelType(CHANNEL_AUX)

    c_meg_mag = ChannelType(CHANNEL_MEG | CHANNEL_MEG_MAG)
    c_meg_grad = ChannelType(CHANNEL_MEG | CHANNEL_MEG_GRAD)
    c_megref_mag = ChannelType(CHANNEL_MEG_REF | CHANNEL_MEG_MAG)
    c_megref_grad = ChannelType(CHANNEL_MEG_REF | CHANNEL_MEG_GRAD)

    assert(c_meg.major_compare(c_meg))
    assert(c_megref.major_compare(c_megref))
    assert(c_eeg.major_compare(c_eeg))
    assert(c_aux.major_compare(c_aux))

    assert(not c_meg.major_compare(c_megref))
    assert(not c_meg.major_compare(c_eeg))
    assert(not c_meg.major_compare(c_aux))

    assert(not c_megref.major_compare(c_meg))
    assert(not c_megref.major_compare(c_eeg))
    assert(not c_megref.major_compare(c_aux))

    assert(not c_eeg.major_compare(c_meg))
    assert(not c_eeg.major_compare(c_megref))
    assert(not c_eeg.major_compare(c_aux))

    assert(not c_aux.major_compare(c_meg))
    assert(not c_aux.major_compare(c_megref))
    assert(not c_aux.major_compare(c_eeg))

    assert(c_meg_mag.major_compare(c_meg_mag))
    assert(c_meg_grad.major_compare(c_meg_grad))

    assert(c_meg.major_compare(c_meg_mag))
    assert(c_meg.major_compare(c_meg_grad))

    assert(c_meg_mag.major_compare(c_meg))
    assert(c_meg_grad.major_compare(c_meg))

    assert(not c_meg_mag.major_compare(c_meg_grad))
    assert(not c_meg_grad.major_compare(c_meg_mag))

    assert(not c_megref_mag.major_compare(c_megref_grad))
    assert(not c_megref_grad.major_compare(c_megref_mag))

    assert(not c_meg_mag.major_compare(ChannelType()))


#######################################################################
# Channel tests
#######################################################################


def test_channel():
    """Check that we can create a Channel"""
    from yne.channels import (Channel, CHANNEL_MEG, CHANNEL_MEG_MAG)

    c = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)

    assert isinstance(c, Channel)

    assert c != None  # noqa
    assert not c == None  # noqa

    assert c.name == 'A1'
    assert c.ctype.is_meg
    assert c.ctype.is_meg_mag

    assert c.is_meg
    assert c.is_meg_data
    assert c.is_meg_mag
    assert not c.is_meg_ref
    assert not c.is_meg_grad
    assert not c.is_eeg
    assert not c.is_aux

    assert str(c) == 'A1 (MEG (MAG))'
    assert repr(c) == 'A1 (MEG (MAG))'


def test_channel_equality():
    """Check that Channel equality works"""
    from yne.channels import Channel, CHANNEL_MEG, CHANNEL_MEG_MAG

    c = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)
    d = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)

    assert c == d


def test_channel_inequality():
    """Check that Channel inequality works"""
    from yne.channels import Channel, CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG

    c = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)
    d = Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG)
    e = Channel('A1', CHANNEL_EEG)

    assert c != d
    assert c != e
    assert d != e


def test_channel_sort():
    """Check that Channel sorting works"""
    from yne.channels import Channel, CHANNEL_MEG, CHANNEL_MEG_MAG

    c = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)
    d = Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG)

    assert c < d
    assert d > c

    assert c <= c
    assert c <= d

    assert d >= d
    assert d >= c


#######################################################################
# ChannelSet tests
#######################################################################


def test_channelset():
    """Check that an empty ChannelSet can be created"""
    from yne.channels import ChannelSet

    c = ChannelSet('Hello')

    assert isinstance(c, ChannelSet)
    assert ChannelSet.get_hdf5name() == 'yne.channels.ChannelSet'

    assert c != None   # noqa
    assert not c == None   # noqa

    assert str(c) == 'Hello'
    assert repr(c) == 'ChannelSet(Hello: {})'

    yaml_dict = c.to_yaml_dict()

    c2 = ChannelSet.from_yaml_dict(yaml_dict)

    assert c == c2

    c2.name = 'blah'

    assert c != c2

    assert c != None  # noqa


def test_channelset_add_channel():
    """Check that adding Channels works"""
    from yne.channels import (Channel, ChannelSet,
                              CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG)

    c = ChannelSet(name='Blah')

    # Three different ways of adding channels
    c.add_channel(Channel('E2', CHANNEL_EEG))
    c.add_channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)
    c.add_channel('A10')

    expectedorder = ['A1', 'A10', 'E2']

    assert len(c.values()) == len(expectedorder)
    assert c.keys() == expectedorder

    # Check we fail to add a bad channel
    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_channel()

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_channel(True)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_channel(2)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.add_channel('a', 'b', 'c')


def test_channelset_sorting():
    """Check that sorting and iteration over Channels works"""
    from yne.channels import (Channel, ChannelSet,
                              CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG)

    c = ChannelSet(name='Blah')

    c.add_channel(Channel('E2', CHANNEL_EEG))
    c.add_channel(Channel('E1', CHANNEL_EEG))
    c.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    c.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    c.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    c.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    c.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    # Check equality first
    yaml_dict = c.to_yaml_dict()

    c2 = ChannelSet.from_yaml_dict(yaml_dict)

    assert c == c2

    expectedorder = ['A1', 'A2', 'A3', 'A4', 'A5', 'E1', 'E2']

    assert len(c.values()) == len(expectedorder)
    assert c.keys() == expectedorder

    assert list(c.itervalues())[0] == c['A1']
    assert list(c.iteritems())[0][1] == c['A1']

    idx = 0
    for j in c:
        assert (j == expectedorder[idx])
        idx += 1

    assert c.at_index(0) == 'A1'
    assert c.at_index(2) == 'A3'

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.at_index(-1)

    with pytest.raises(ValueError) as wrapped_e:  # noqa
        c.at_index(7)

    str_rep = "{'A1': A1 (MEG (MAG)), 'A2': A2 (MEG (MAG)), " + \
              "'A3': A3 (MEG (MAG)), 'A4': A4 (MEG (MAG)), " + \
              "'A5': A5 (MEG (MAG)), 'E1': E1 (EEG), 'E2': E2 (EEG)}"

    assert str(c) == 'Blah'
    assert repr(c) == f'ChannelSet(Blah: {str_rep})'


def test_channelset_additionchannel():
    """Check that we can add Channels to a ChannelSet"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG
    e = ChannelSet()

    e.add_channel(Channel('E2', CHANNEL_EEG))
    e.add_channel(Channel('E1', CHANNEL_EEG))

    m = Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG)

    expectedinitial = ['E1', 'E2']
    expectedafter = ['A1', 'E1', 'E2']

    assert (e.keys() == expectedinitial)

    f = e + m

    # We shouldn't have touched e
    assert (e.keys() == expectedinitial)
    assert (f.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(e) != id(f))


def test_channelset_additionchannelset():
    """Check that we can add ChannelSets together"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG
    e = ChannelSet()
    m = ChannelSet()

    e.add_channel(Channel('E2', CHANNEL_EEG))
    e.add_channel(Channel('E1', CHANNEL_EEG))

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    t = e + m

    expectedeeg = ['E1', 'E2']
    expectedmeg = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedall = expectedmeg + expectedeeg

    assert (e.keys() == expectedeeg)
    assert (m.keys() == expectedmeg)
    assert (t.keys() == expectedall)

    # Check that the IDs aren't the same
    assert (id(e) != id(m) != id(t))


def test_channelset_subtractionstring():
    """Check that we can subtract individual Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A2', 'A3', 'A4', 'A5']

    assert (m.keys() == expectedinitial)

    n = m - 'A1'

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_subtractionstringmissing():
    """Check that we raise an error if a Channel is missing in a subtraction"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m - 'A2'


def test_channelset_subtractionlist():
    """Check that we can subtract lists of Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A1', 'A3', 'A5']

    assert (m.keys() == expectedinitial)

    n = m - ['A2', Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG)]

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_subtractionlistmissing():
    """Check that we raise an error if a Channel is missing in a subtraction using a list"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m - ['A2', 'A3']


def test_channelset_subtractionchannelset():
    """Check that we can subtract lists of Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedlose = ['A3', 'A4']
    expectedafter = ['A1', 'A2', 'A5']

    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)

    o = m - n

    # We shouldn't have touched m or n
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)
    assert (o.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n) != id(o))


def test_channelset_subtractionchannelsetmissing():
    """Check that we raise an error if a Channel is missing in a subtraction using a ChannelSet"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m - n


def test_channelset_discardstring():
    """Check that we can discard individual Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A2', 'A3', 'A4', 'A5']

    assert (m.keys() == expectedinitial)

    n = m.discard('A1')

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_discardstringmissing():
    """Check that we can discard individual Channels from ChannelSets where the Channel is missing"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A1', 'A2', 'A3', 'A4', 'A5']

    assert (m.keys() == expectedinitial)

    n = m.discard('A6')

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_discardlist():
    """Check that we can discard lists of Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A1', 'A3', 'A5']

    assert (m.keys() == expectedinitial)

    n = m.discard(['A2', Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG)])

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_discardlistmissing():
    """Check that we can discard lists of Channels from ChannelSets where one Channel is missing"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A1', 'A3', 'A5']

    assert (m.keys() == expectedinitial)

    n = m.discard(['A2', 'A6', Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG)])

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_discardchannelset():
    """Check that we can discard Channels"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedlose = ['A3', 'A4']
    expectedafter = ['A1', 'A2', 'A5']

    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)

    o = m.discard(n)

    # We shouldn't have touched m or n
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)
    assert (o.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n) != id(o))


def test_channelset_discardchannelsetmissing():
    """Check that we can discard Channels in cases where one Channel is missing"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A6', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedlose = ['A5', 'A6']
    expectedafter = ['A1', 'A2', 'A3', 'A4']

    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)

    o = m.discard(n)

    # We shouldn't have touched m or n
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedlose)
    assert (o.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n) != id(o))


def test_channelset_intersectionstring():
    """Check that we can perform an intersection on individual Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A1']

    assert (m.keys() == expectedinitial)

    n = m.intersection('A1')

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_intersectionlist():
    """Check that we can perform an intersection on lists of Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedafter = ['A2', 'A4']

    assert (m.keys() == expectedinitial)

    n = m.intersection(['A2', Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG)])

    # We shouldn't have touched m
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n))


def test_channelset_intersectionchannelset():
    """Check that we can perform an intersection on lists of Channels from ChannelSets"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG
    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedintersect = ['A3', 'A4']
    expectedafter = ['A3', 'A4']

    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedintersect)

    o = m.intersection(n)

    # We shouldn't have touched m or n
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedintersect)
    assert (o.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n) != id(o))


def test_channelset_intersectionnonexistantchannel():
    """Check that we can perform an intersection between ChannelSets with non-overlapping Channels"""
    from yne.channels import Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG

    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A4', CHANNEL_MEG | CHANNEL_MEG_MAG))
    n.add_channel(Channel('A6', CHANNEL_MEG | CHANNEL_MEG_MAG))

    expectedinitial = ['A1', 'A2', 'A3', 'A4', 'A5']
    expectedintersect = ['A3', 'A4', 'A6']
    expectedafter = ['A3', 'A4']

    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedintersect)

    o = m.intersection(n)

    # We shouldn't have touched m or n
    assert (m.keys() == expectedinitial)
    assert (n.keys() == expectedintersect)
    assert (o.keys() == expectedafter)

    # Check that the IDs aren't the same
    assert (id(m) != id(n) != id(o))

    assert(len(m.intersection(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))) == 1)


def test_channelset_intersectiontype():
    """Check that we raise an error if channel types don't match"""
    from yne.channels import (Channel, ChannelSet, CHANNEL_MEG, CHANNEL_MEG_MAG,
                              CHANNEL_MEG_GRAD, CHANNEL_EEG)

    m = ChannelSet()
    n = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_GRAD))

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m.intersection(n, check_types=True)

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m.intersection([Channel('A1', CHANNEL_EEG)], check_types=True)

    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m.intersection(Channel('A1', CHANNEL_EEG), check_types=True)


def test_channelset_addition():
    """Check that we can add channelsets"""
    from yne.channels import (Channel, ChannelSet, CHANNEL_MEG,
                              CHANNEL_MEG_MAG, CHANNEL_MEG_GRAD, CHANNEL_EEG)

    m = ChannelSet()
    n = ChannelSet()
    o = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))

    n.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_GRAD))

    o.add_channel(Channel('A1', CHANNEL_EEG))

    assert len(m + None) == m.num_channels
    assert len(m + n) == 2

    # This shouldn't work - different channel types for same name
    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m + o


def test_channelset_addition_chan():
    """Check that we can add channels to a channelset"""
    from yne.channels import (Channel, ChannelSet, CHANNEL_MEG,
                              CHANNEL_MEG_MAG, CHANNEL_MEG_GRAD, CHANNEL_EEG)

    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))

    a2 = Channel('A2', CHANNEL_MEG | CHANNEL_MEG_GRAD)

    bad_a1 = Channel('A1', CHANNEL_EEG)

    m2 = m + a2
    assert len(m2) == 2

    # This shouldn't work - different channel types for same name
    with pytest.raises(ValueError) as wrapped_e:  # noqa:
        m + bad_a1


def test_channelset_hdf5_roundtrip(tmp_path):
    from yne.channels import (Channel, ChannelSet, CHANNEL_MEG,
                              CHANNEL_MEG_MAG, CHANNEL_EEG)

    m = ChannelSet()

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('E1', CHANNEL_EEG))

    filename = tmp_path / 'foo.hdf5'

    f = h5py.File(filename, 'w')
    m.to_hdf5(f.create_group('cs'))
    f.close()

    f = h5py.File(filename, 'r')
    n = ChannelSet.from_hdf5(f['cs'])
    f.close()

    assert m == n


def test_channelset_hdf5_roundtrip_empty(tmp_path):
    from yne.channels import ChannelSet

    m = ChannelSet()

    filename = tmp_path / 'foo.hdf5'

    f = h5py.File(filename, 'w')
    m.to_hdf5(f.create_group('cs'))
    f.close()

    f = h5py.File(filename, 'r')
    n = ChannelSet.from_hdf5(f['cs'])
    f.close()

    assert m == n


#######################################################################
# ChannelDefiner tests
#######################################################################


def test_channeldefiner_empty():
    from yne.channels import ChannelDefiner

    cd = ChannelDefiner()

    assert len(cd.keys()) == 0

    assert cd != None  # noqa
    assert not cd == None  # noqa

    assert not cd.has_chanset('foo')

    yaml_dict = cd.to_yaml_dict()

    cd2 = ChannelDefiner.from_yaml_dict(yaml_dict)

    assert cd == cd2


def test_channeldefiner():
    from yne.channels import (ChannelDefiner, ChannelSet, ChannelGroup,
                              Channel, CHANNEL_MEG, CHANNEL_MEG_MAG, CHANNEL_EEG)

    m = ChannelSet(name='Blah')

    m.add_channel(Channel('A1', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A2', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('E1', CHANNEL_EEG))

    n = ChannelSet(name='Foo')
    m.add_channel(Channel('A3', CHANNEL_MEG | CHANNEL_MEG_MAG))
    m.add_channel(Channel('A5', CHANNEL_MEG | CHANNEL_MEG_MAG))

    p = ChannelGroup(name='AA', channelgroup='MEG')

    cd = ChannelDefiner()

    cd.add_chanset(m)
    cd.add_chanset(n)
    cd.add_chanset(p)

    assert len(cd.keys()) == 3

    assert not cd.has_chanset('foo')
    assert cd.has_chanset('Foo')

    yaml_dict = cd.to_yaml_dict()

    cd2 = ChannelDefiner.from_yaml_dict(yaml_dict)

    assert cd == cd2

    cd.del_chanset('Foo')

    assert len(cd.keys()) == 2


#######################################################################
# Helper routine tests
#######################################################################

def test_mne_chantype_to_yne():
    from mne.io import constants as mc

    from yne.channels import (mne_chantype_to_yne, CHANNEL_MEG, CHANNEL_MEG_REF,
                              CHANNEL_MEG_MAG, CHANNEL_MEG_GRAD,
                              CHANNEL_EEG, CHANNEL_AUX)

    assert mne_chantype_to_yne('mag', {}) == CHANNEL_MEG | CHANNEL_MEG_MAG

    assert mne_chantype_to_yne('grad', {}) == CHANNEL_MEG | CHANNEL_MEG_GRAD

    ch_info = {'coil_type': mc.FIFF.FIFFV_COIL_MAGNES_REF_GRAD}
    assert mne_chantype_to_yne('ref_meg', ch_info) == CHANNEL_MEG_REF | CHANNEL_MEG_GRAD

    ch_info = {'coil_type': mc.FIFF.FIFFV_COIL_MAGNES_REF_MAG}
    assert mne_chantype_to_yne('ref_meg', ch_info) == CHANNEL_MEG_REF | CHANNEL_MEG_MAG

    ch_info = {'coil_type': 0}
    assert mne_chantype_to_yne('ref_meg', ch_info) == CHANNEL_MEG_REF

    assert mne_chantype_to_yne('eeg', {}) == CHANNEL_EEG

    assert mne_chantype_to_yne('blah', {}) == CHANNEL_AUX
