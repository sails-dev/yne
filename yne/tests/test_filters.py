#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

import numpy as np

import mne

#######################################################################
# Support routines
#######################################################################


def make_test_dataset(freqs, dur=10.0, sample_rate=200.0):
    # Mock up info structure
    info = mne.create_info(ch_names=[f'MEG{x:03}' for x in range(len(freqs))],
                           ch_types=['mag'] * 2,
                           sfreq=sample_rate)

    # Create data
    times = np.linspace(0, dur, int(sample_rate * dur), endpoint=False)

    data = np.zeros((len(freqs), times.shape[0]))

    for idx, freq in enumerate(freqs):
        data[idx, :] = np.sin(2 * np.pi * freq * times) * 1e-13

    return mne.io.RawArray(data, info)


#######################################################################
# BandFilter tests
#######################################################################


def test_bandfilter_empty():
    from yne.filters import BandFilter

    bf = BandFilter()

    assert bf.description == 'NoFilter (Unconfigured)'
    assert str(bf) == '<BandFilter [NoFilter (Unconfigured)]>'
    assert bf.get_rec_freq_range() is None

    yaml_dict = bf.to_yaml_dict()

    bf2 = BandFilter.from_yaml_dict(yaml_dict)

    assert bf == bf2

    bf2.low_freq = 1.0

    assert bf != bf2


def test_bandfilter_bandpass():
    from yne.filters import BandFilter

    bf = BandFilter(low_freq=3.0, high_freq=30.0)

    assert bf.description == 'BandPass (3.0-30.0Hz)'
    assert bf.get_rec_freq_range() == (3.0, 30.0)

    assert bf != None  # noqa

    yaml_dict = bf.to_yaml_dict()

    bf2 = BandFilter.from_yaml_dict(yaml_dict)

    assert bf == bf2

    bf2.filter_length = 'nope'

    assert bf != bf2


def test_bandfilter_bandpass_data():
    from yne.filters import BandFilter

    bf = BandFilter(low_freq=3.0, high_freq=30.0)

    raw = make_test_dataset([20.0, 50.0])

    out = bf.filter_data(raw)

    out_data = out.get_data()

    # Check that channel 1 has retained most of its variance
    # in the middle part
    assert abs(out_data[0, 500:1500].min()) >= 0.8e-14
    assert abs(out_data[0, 500:1500].max()) >= 0.8e-14

    # And that the second channel hasn't
    assert abs(out_data[1, 500:1500].min()) <= 1e-16
    assert abs(out_data[1, 500:1500].max()) <= 1e-16


def test_bandfilter_bandstop():
    from yne.filters import BandFilter

    bf = BandFilter(low_freq=51.0, high_freq=49.0)

    assert bf.description == 'BandStop (49.0-51.0Hz)'
    assert bf.get_rec_freq_range() is None

    yaml_dict = bf.to_yaml_dict()

    bf2 = BandFilter.from_yaml_dict(yaml_dict)

    assert bf == bf2

    bf2.pad = 100

    assert bf != bf2


def test_bandfilter_lowpass():
    from yne.filters import BandFilter

    bf = BandFilter(high_freq=10.0)

    assert bf.description == 'LowPass (0-10.0Hz)'

    fr = bf.get_rec_freq_range()
    assert fr[0] is None
    assert fr[1] == 10.0

    yaml_dict = bf.to_yaml_dict()

    bf2 = BandFilter.from_yaml_dict(yaml_dict)

    assert bf == bf2

    bf2.method = 'iir'

    assert bf != bf2


def test_bandfilter_lowpass_data():
    from yne.filters import BandFilter

    bf = BandFilter(high_freq=30.0)

    raw = make_test_dataset([20.0, 50.0])

    out = bf.filter_data(raw)

    out_data = out.get_data()

    # Check that channel 1 has retained most of its variance
    # in the middle part
    assert abs(out_data[0, 500:1500].min()) >= 0.8e-14
    assert abs(out_data[0, 500:1500].max()) >= 0.8e-14

    # And that the second channel hasn't
    assert abs(out_data[1, 500:1500].min()) <= 1e-16
    assert abs(out_data[1, 500:1500].max()) <= 1e-16


def test_bandfilter_highpass():
    from yne.filters import BandFilter

    bf = BandFilter(low_freq=10.0)

    assert bf.description == 'HighPass (10.0Hz-)'

    fr = bf.get_rec_freq_range()

    assert fr[0] == 10.0
    assert fr[1] is None

    yaml_dict = bf.to_yaml_dict()

    bf2 = BandFilter.from_yaml_dict(yaml_dict)

    assert bf == bf2

    bf2.phase = 'any'

    assert bf != bf2


def test_bandfilter_highpass_data():
    from yne.filters import BandFilter

    bf = BandFilter(low_freq=35.0)

    raw = make_test_dataset([20.0, 50.0])

    out = bf.filter_data(raw)

    out_data = out.get_data()

    # Check that channel 2 has retained most of its variance
    # in the middle part
    assert abs(out_data[1, 500:1500].min()) >= 0.8e-14
    assert abs(out_data[1, 500:1500].max()) >= 0.8e-14

    # And that the first channel hasn't
    assert abs(out_data[0, 500:1500].min()) <= 1e-16
    assert abs(out_data[0, 500:1500].max()) <= 1e-16


#######################################################################
# FilterChain tests
#######################################################################


def test_filterchain_empty():
    from yne.filters import FilterChain

    fc = FilterChain('foo')

    assert str(fc) == 'foo'

    assert fc != None  # noqa

    yaml_dict = fc.to_yaml_dict()

    fc2 = FilterChain.from_yaml_dict(yaml_dict)

    assert fc2 == fc

    assert fc.get_rec_freq_range() is None


def test_filterchain_multiple():
    from yne.filters import FilterChain, BandFilter

    fc = FilterChain('foo')

    f1 = BandFilter(low_freq=3.0)
    f2 = BandFilter(low_freq=3.0, high_freq=30.0)
    f3 = BandFilter(low_freq=3.0, high_freq=40.0)

    fc.add_filter(f1)
    fc.add_filter(f2)

    yaml_dict = fc.to_yaml_dict()

    fc2 = FilterChain.from_yaml_dict(yaml_dict)

    assert fc2 == fc

    fc.name = 'blah'

    assert fc != fc2

    fc.name = 'foo'

    fc.add_filter(f3)

    assert fc != fc2

    fc3 = FilterChain(name='foo')

    fc3.add_filter(f1)
    fc3.add_filter(f3)

    assert fc3 != fc2

    assert fc.get_rec_freq_range() == (3.0, 30.0)
    assert fc3.get_rec_freq_range() == (3.0, 40.0)

    fc3.add_filter(BandFilter())
    assert fc3.get_rec_freq_range() == (3.0, 40.0)

    fc3.add_filter(BandFilter(low_freq=10.0))
    assert fc3.get_rec_freq_range() == (10.0, 40.0)

    fc3.add_filter(BandFilter(high_freq=30.0))
    assert fc3.get_rec_freq_range() == (10.0, 30.0)

    assert fc3.num_filters == 5


def test_filterchain_low_highpass_data():
    from yne.filters import BandFilter, FilterChain

    bf_l = BandFilter(high_freq=30.0)
    bf_h = BandFilter(low_freq=3.0)

    fc = FilterChain()

    fc.add_filter(bf_l)
    fc.add_filter(bf_h)

    raw = make_test_dataset([20.0, 50.0])

    out = fc.filter_data(raw)

    out_data = out.get_data()

    # Check that channel 1 has retained most of its variance
    # in the middle part
    assert abs(out_data[0, 500:1500].min()) >= 0.8e-14
    assert abs(out_data[0, 500:1500].max()) >= 0.8e-14

    # And that the second  channel hasn't
    assert abs(out_data[1, 500:1500].min()) <= 1e-16
    assert abs(out_data[1, 500:1500].max()) <= 1e-16


#######################################################################
# FilterChainDefiner tests
#######################################################################


def test_filterchaindefiner():
    from yne.filters import FilterChain, FilterChainDefiner, BandFilter

    fc1 = FilterChain('foo')

    fc1.add_filter(BandFilter(high_freq=30.0))
    fc1.add_filter(BandFilter(low_freq=3.0))

    fc2 = FilterChain('empty')

    fcd = FilterChainDefiner()

    fcd.add_filterchain(fc1)
    fcd.add_filterchain(fc2)

    assert fcd.has_filterchain('empty')
    assert not fcd.has_filterchain('foobar')

    assert fc1 == fcd.get_filterchain('foo')

    yaml_dict = fcd.to_yaml_dict()

    fcd2 = FilterChainDefiner.from_yaml_dict(yaml_dict)

    assert fcd2 == fcd
