#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file


def test_log():
    from yne.log import get_logger

    ylog = get_logger()

    ylog.info('Test')
