#!/usr/bin/python3
# vim: set expandtab ts=4 sw=4:

# This file is part of the YNiC MNE Tools
# For copyright and redistribution details, please see the COPYING file

# TODO issue #1: This entire setup should be ported to argparse

import sys
import imp

from os import environ
from os.path import split
from optparse import Option, OptionParser

from . import YneOptions


__all__ = []

_ABC_WARNING = 'This is an Abstract Base Class'


##########################################################################
# Custom Option class
##########################################################################


class YneOption(Option):
    """
    An Option class which contains an extend option so that options
    can be specified multiple times but not split on
    commas.  We use this for run names and so on as we can't use comma
    seperated values there as the filenames can contain commas
    """

    ACTIONS = Option.ACTIONS + ("extend",)
    STORE_ACTIONS = Option.STORE_ACTIONS + ("extend",)
    TYPED_ACTIONS = Option.TYPED_ACTIONS + ("extend",)
    ALWAYS_TYPED_ACTIONS = Option.ALWAYS_TYPED_ACTIONS + ("extend",)

    def take_action(self, action, dest, opt, value, values, parser):
        if action == "extend":
            values.ensure_value(dest, []).extend([value])
        else:
            Option.take_action(
                self, action, dest, opt, value, values, parser)


__all__ += ['YneOption']

##########################################################################
# Custom Option class
##########################################################################


class YneOptionParser(OptionParser):
    """
    A modified OptionParser class which can have a set of option handlers
    registered to it to add groups of options and automatically perform sanity
    checks
    """

    def __init__(self, **kwargs):
        # Default to YneOption
        if 'option_class' not in kwargs:
            kwargs['option_class'] = YneOption

        OptionParser.__init__(self, **kwargs)

        self.handlers = []

    def parse_args(self, *args, **kwds):
        op, args = OptionParser.parse_args(self, *args, **kwds)

        # Post process the options
        for handler in self.handlers:
            op = handler.post_process(op)

        return op, args

    def resubmit_string(self, op):
        ret = []
        for handler in self.handlers:
            ret.extend(handler.resubmit_string(op))

        return ret

    def register_handlers(self, option_handlers):
        """
        :type option_handlers: list
        :param option_handlers: A list of objects derived from
                                OptionHandlerBase which will be used to
                                automatically add options
        """
        if not isinstance(option_handlers, list):
            option_handlers = [option_handlers]

        for handler in option_handlers:
            self.handlers.append(handler)
            handler.add_options(self)

    def sanity_check(self, op):
        """
        Run all sanity_check routines from the registered option handlers
        """
        for handler in self.handlers:
            try:
                handler.sanity_check(op)
            except Exception as e:  # pragma: nocover
                self.print_help()
                print("\nERROR: Failed to parse command line arguments")
                print(str(e))
                sys.exit(1)


__all__ += ['YneOptionParser']


##########################################################################
# Option handling classes
##########################################################################


class OptionHandlerBase(object):  # pragma: nocover
    """
    ABC which describes classes which can be used to quickly build up and parse
    commonly used options
    """

    def add_options(self, optparser):
        """
        Routine which adds the relevant options and groups to the argument
        parser.

        :param optparser: The OptionParser instance currently in use.  This
                          must have been created setting option_class to
                          YneOption as many of the derived classes rely on
                          the additional behaviour which this class provides.
                          If you use YneOptionParser, this will happen
                          automatically.

        :return: The modified version of optparser
        """
        raise NotImplementedError(_ABC_WARNING)

    def post_process(self, op):
        """
        Routine called as part of parse_args

        :param op: The options dictionary returned after parsing.

        :return: A post-processed version of op
        """
        raise NotImplementedError(_ABC_WARNING)

    def sanity_check(self, op):
        """
        :param op: The options dictionary returned after parsing.

        :return: Nothing
        """
        raise NotImplementedError(_ABC_WARNING)

    def resubmit_string(self, op):
        """
        :param op: The options dictionary returned after parsing.

        :return: List of strings to use when resubmitting
        """
        return []


__all__ += ['OptionHandlerBase']


class OptionBaseParameters(OptionHandlerBase):
    """
    Class which provides simple and usual command line options such as
    --verbose and --progress
    """
    def add_options(self, optparser):
        optparser.add_option("-v", "--verbose", dest="verbose", action="count", default=0,
                             help="Turn on verbose output.  Use multiple times for greater verbosity")

        optparser.add_option("--progress", dest="progress", action="store_true", default=False,
                             help="Show progress bars for data loading and reading")

        optparser.add_option("--load-module", dest="loadmodulestr", action="extend", default=list(),
                             help="Load extra python modules to add functionality")

        return optparser

    def post_process(self, op):
        # If YNEVERBOSE is set, make sure op.verbose is set appropriately
        if environ.get('YNEVERBOSE', None) is not None:
            try:
                # Try and take verbosity as an int
                v = int(environ.get('YNEVERBOSE'))
            except ValueError:
                v = 1

            # Take maximum of YNEVERBOSE and op.verbose
            if v > op.verbose:
                op.verbose = v

        # Set options on singleton
        YneOptions().verbose = op.verbose
        delattr(op, 'verbose')

        YneOptions().progress = op.progress
        delattr(op, 'progress')

        if len(op.loadmodulestr) > 0:
            for mod in op.loadmodulestr:
                try:
                    # Strip off the filename, remove .py, .pyc or .pyo
                    # then search in that directory
                    d, f = split(mod)
                    if f.endswith('.py'):
                        f = f[:-3]
                    elif f.endswith('.pyc') or f.endswith('.pyo'):
                        f = f[:-4]
                    else:
                        raise Exception("Will only attempt to load .py, .pyo or .pyc files")

                    modinfo = imp.find_module(f, [d])
                    imp.load_module(f, modinfo[0], modinfo[1], modinfo[2])
                except Exception as e:
                    raise ImportError("Cannot load file %s (%s)" % (mod, str(e)))

        return op

    def sanity_check(self, op):
        return

    def resubmit_string(self, op):
        ret = []

        for j in range(YneOptions().verbose):
            ret.append('-v')

        if YneOptions().progress:
            ret.append('--progress')

        for m in op.loadmodulestr:
            ret.append('--load-module=%s' % m)

        return ret


__all__ += ['OptionBaseParameters']
