PYTEST_OPTS = --cov --cov-config=setup.cfg --cov-report=term-missing

YNETEST ?= ../yne-testdata

all:


test:
	YNETEST=$(YNETEST) pytest-3 $(PYTEST_OPTS) $(PYTEST_LOCAL_OPTS)

.PHONY: all test
