#!/usr/bin/python

from os import path

from setuptools import find_packages, setup

# Scripts
scripts = []

# read the contents of the README file
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

name = 'yne'
version = '0.1'
release = '0.1.0'

setup(
    name=name,

    version=release,

    description="YNiC's mNE scripts",

    # this becomes the PyPI landing page
    long_description=long_description,
    long_description_content_type='text/x-markdown',

    # Author details
    author='Mark Hymers <mark.hymers@ynic.york.ac.uk>',

    # Choose your license
    license='GPL-2+',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
      'Development Status :: 4 - Beta',

      # Indicate who your project is intended for
      'Intended Audience :: Science/Research',
      'Topic :: Scientific/Engineering :: Bio-Informatics',
      'Topic :: Scientific/Engineering :: Information Analysis',
      'Topic :: Scientific/Engineering :: Mathematics',

      # Pick your license as you wish (should match "license" above)
      'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',

      'Programming Language :: Python :: 3',
      'Programming Language :: Python :: 3.7',
      'Programming Language :: Python :: 3.8',
      'Programming Language :: Python :: 3.9',
      'Programming Language :: Python :: 3.10',
    ],

    keywords='MNE MEG magnetoencephalography',

    packages=find_packages(),

    package_data={
        # Include any .ui files
        "yne.ui": ["widgets/*.ui"],
    },

    extras_require={
        'test': ['coverage'],
    },

    command_options={
            'build_sphinx': {
                'project': ('setup.py', name),
                'version': ('setup.py', name),
                'release': ('setup.py', name)}},

    entry_points={
        'console_scripts': [
            'yne-epoch-rejection = yne.scripts.epoch_rejection:main',
            'yne-menu = yne.scripts.menu:main',
            'yne-raw-data-viewer = yne.scripts.raw_data_viewer:main',
            'yne-study-definer = yne.scripts.study_definer:main',
            'yne-study-analyser = yne.scripts.study_analyser:main'
        ]},

    zip_safe=False
)
