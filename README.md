# yne

This is a set of tools, based on code originally from YNiC's NAF suite, which
provides some graphical and command line interfaces to MNE.

The tools are primarily designed for event-related experiments and mainly used
for teaching purposes to deal with the fact that about half of the students
on the MEG analysis course have not worked with Python before.
